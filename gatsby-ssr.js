const React = require("react")

export const onPreRenderHTML = ({
  getHeadComponents,
  replaceHeadComponents,
}) => {
  const headComponents = getHeadComponents()
  const orderedComponents = headComponents.sort((a, b) => {
    if (a.type === "meta") {
      return -1
    } else if (b.type === "meta") {
      return 1
    }
    return 0
  })
  replaceHeadComponents(orderedComponents)
}

export const onRenderBody = (
  { setHeadComponents, setPostBodyComponents, setPreBodyComponents },
  pluginOptions
) => {
  setHeadComponents([
    // <script
    //   rel="preload"
    //   as="script"
    //   src="//app-lon05.marketo.com/js/forms2/js/forms2.min.js"
    // ></script>,
  ]),
    setPreBodyComponents([
      <noscript>
        <iframe
          src="https://www.googletagmanager.com/ns.html?id=GTM-WP8B6B"
          height="0"
          width="0"
          style={{ display: "none", visibility: "hidden" }}
        ></iframe>
      </noscript>,
    ]),
    setPostBodyComponents([
      <script
        rel="preload"
        as="script"
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossOrigin="anonymous"
      ></script>,
      <script
        rel="preload"
        as="script"
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossOrigin="anonymous"
      ></script>,
      // <script
      //   rel="preload"
      //   as="script"
      //  src="https://payment.flywire.com/assets/js/flywire.js"></script>,
    ])
}
