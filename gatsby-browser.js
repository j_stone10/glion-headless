const $ = require("jquery")

// trigger an immediate page refresh when an update is found
// export const onServiceWorkerUpdateReady = () => window.location.reload();
exports.onServiceWorkerUpdateReady = () => window.location.reload(true);

exports.onRouteUpdate = ({ location, prevLocation }) => {
  $(document).ready(function () {

    var blue = '#243646'
    var bronze = '#9E785B';
    // var light_brown = '#C1ABA5'; 
    // var grey = '#E2E0DF';
    // var purple = '#B4ADB9';
    // var beige = '#F0D4C6';



    /***************************************************
    Hash link scroll 
    ***************************************************/

    if ($('.post-template-default').length > 0) {
      //  $('.social_share_toggle.gatsby-image-wrapper img').attr("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAiCAYAAADVhWD8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAHsSURBVHgB7Zg9UsJQEMd3n9HW3MA4YG0KUcsUwtCpJ1BPADeQnEC5gRzBzkFnwA4ZC+xhzA3EGRsxvHUf+JGExMEhJBT8miT7dnb+eW+zb19way9fIgkVQNQhLYj6IISN2d38C0CKQn7piwURotC1UDNPGwmoje/xEAEMSIAJMYRU7bVvyx5TOZMrXCHCCcwZ4X0gAKf34BMyYvgxYBsnWZJieDmewpycTrPPSjuQpBjOlXVIEb8YRMswi0bQacvMm2oMEhXDrKzJRma3MEpWw7T0TC5/SqvQgATgolcgWBAELBChdQYJfZ8xEemIWPIYmpxD9yHxzr/HScrqcHXYcVpNx7AsXXvTTI6xLRHLUUV0YplcITad1o3jtRn7RUOT8tljsrvteiUYLJvjfY7ouPt414QIvmJd8O1RcCzWZSJ+67+EKNSLuoPBmSqwME8xvXa9No2fKqKIsgrzFPMf3Hf3KmjTIGZ8zZpqmhAvOb/soJ+aHa5njjeZY52Z7E7RIsLLn65xfK1kdw6sMH8E6dvvYk5g9zR0QAgr1B/Eq88NYgQBN8Ls/JVtwBTEncAztbBx15nFETMrSzFRLMVEsRQTxURzNVunR6WIs7vag64n3emQ45iRYtJEJHFsnRL+JYJkj/qONBn3PfYnnrm7KSnG1eEAAAAASUVORK5CYII=");
      //  $('.social_share_toggle.gatsby-image-wrapper picture source').attr("srcset", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAiCAYAAADVhWD8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAHsSURBVHgB7Zg9UsJQEMd3n9HW3MA4YG0KUcsUwtCpJ1BPADeQnEC5gRzBzkFnwA4ZC+xhzA3EGRsxvHUf+JGExMEhJBT8miT7dnb+eW+zb19way9fIgkVQNQhLYj6IISN2d38C0CKQn7piwURotC1UDNPGwmoje/xEAEMSIAJMYRU7bVvyx5TOZMrXCHCCcwZ4X0gAKf34BMyYvgxYBsnWZJieDmewpycTrPPSjuQpBjOlXVIEb8YRMswi0bQacvMm2oMEhXDrKzJRma3MEpWw7T0TC5/SqvQgATgolcgWBAELBChdQYJfZ8xEemIWPIYmpxD9yHxzr/HScrqcHXYcVpNx7AsXXvTTI6xLRHLUUV0YplcITad1o3jtRn7RUOT8tljsrvteiUYLJvjfY7ouPt414QIvmJd8O1RcCzWZSJ+67+EKNSLuoPBmSqwME8xvXa9No2fKqKIsgrzFPMf3Hf3KmjTIGZ8zZpqmhAvOb/soJ+aHa5njjeZY52Z7E7RIsLLn65xfK1kdw6sMH8E6dvvYk5g9zR0QAgr1B/Eq88NYgQBN8Ls/JVtwBTEncAztbBx15nFETMrSzFRLMVEsRQTxURzNVunR6WIs7vag64n3emQ45iRYtJEJHFsnRL+JYJkj/qONBn3PfYnnrm7KSnG1eEAAAAASUVORK5CYII=");

      $('.share').html('<div class="container"> <div class="row my-1"> <div class="col" style="max-width: 55px"> <svg class="social_share_toggle" width="47" height="46" viewBox="0 0 47 46" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M2.2207 40.334C2.27539 40.7285 2.38281 41.0234 2.54297 41.2188C2.83594 41.5742 3.33789 41.752 4.04883 41.752C4.47461 41.752 4.82031 41.7051 5.08594 41.6113C5.58984 41.4316 5.8418 41.0977 5.8418 40.6094C5.8418 40.3242 5.7168 40.1035 5.4668 39.9473C5.2168 39.7949 4.82422 39.6602 4.28906 39.543L3.375 39.3379C2.47656 39.1348 1.85547 38.9141 1.51172 38.6758C0.929688 38.2773 0.638672 37.6543 0.638672 36.8066C0.638672 36.0332 0.919922 35.3906 1.48242 34.8789C2.04492 34.3672 2.87109 34.1113 3.96094 34.1113C4.87109 34.1113 5.64648 34.3535 6.28711 34.8379C6.93164 35.3184 7.26953 36.0176 7.30078 36.9355H5.56641C5.53516 36.416 5.30859 36.0469 4.88672 35.8281C4.60547 35.6836 4.25586 35.6113 3.83789 35.6113C3.37305 35.6113 3.00195 35.7051 2.72461 35.8926C2.44727 36.0801 2.30859 36.3418 2.30859 36.6777C2.30859 36.9863 2.44531 37.2168 2.71875 37.3691C2.89453 37.4707 3.26953 37.5898 3.84375 37.7266L5.33203 38.084C5.98438 38.2402 6.47656 38.4492 6.80859 38.7109C7.32422 39.1172 7.58203 39.7051 7.58203 40.4746C7.58203 41.2637 7.2793 41.9199 6.67383 42.4434C6.07227 42.9629 5.2207 43.2227 4.11914 43.2227C2.99414 43.2227 2.10938 42.9668 1.46484 42.4551C0.820312 41.9395 0.498047 41.2324 0.498047 40.334H2.2207ZM10.1062 43V34.3633H11.8934V37.6562H15.2684V34.3633H17.0613V43H15.2684V39.1445H11.8934V43H10.1062ZM22.3512 39.7363H24.5426L23.4645 36.3379L22.3512 39.7363ZM22.4625 34.3633H24.5016L27.5602 43H25.6031L25.0465 41.2246H21.8648L21.2672 43H19.3805L22.4625 34.3633ZM31.6664 35.8633V38.1836H33.7113C34.1176 38.1836 34.4223 38.1367 34.6254 38.043C34.9848 37.8789 35.1645 37.5547 35.1645 37.0703C35.1645 36.5469 34.9906 36.1953 34.643 36.0156C34.4477 35.9141 34.1547 35.8633 33.7641 35.8633H31.6664ZM34.1391 34.3633C34.7445 34.375 35.2094 34.4492 35.5336 34.5859C35.8617 34.7227 36.1391 34.9238 36.3656 35.1895C36.5531 35.4082 36.7016 35.6504 36.8109 35.916C36.9203 36.1816 36.975 36.4844 36.975 36.8242C36.975 37.2344 36.8715 37.6387 36.6645 38.0371C36.4574 38.4316 36.1156 38.7109 35.6391 38.875C36.0375 39.0352 36.3188 39.2637 36.4828 39.5605C36.6508 39.8535 36.7348 40.3027 36.7348 40.9082V41.4883C36.7348 41.8828 36.7504 42.1504 36.7816 42.291C36.8285 42.5137 36.9379 42.6777 37.1098 42.7832V43H35.1234C35.0688 42.8086 35.0297 42.6543 35.0063 42.5371C34.9594 42.2949 34.934 42.0469 34.9301 41.793L34.9184 40.9902C34.9105 40.4395 34.809 40.0723 34.6137 39.8887C34.4223 39.7051 34.0609 39.6133 33.5297 39.6133H31.6664V43H29.9027V34.3633H34.1391ZM46.1203 35.8926H41.55V37.7266H45.7453V39.2266H41.55V41.4473H46.3312V43H39.7863V34.3633H46.1203V35.8926Z" fill="#253645" /> <path d="M14.5 13.5V22.5C14.5 23.0967 14.7371 23.669 15.159 24.091C15.581 24.5129 16.1533 24.75 16.75 24.75H30.25C30.8467 24.75 31.419 24.5129 31.841 24.091C32.2629 23.669 32.5 23.0967 32.5 22.5V13.5" stroke="#253645" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" /> <path d="M28 6.75L23.5 2.25L19 6.75" stroke="#253645" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" /> <path d="M23.5 2.25V16.875" stroke="#253645" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" /> <rect x="10" width="27" height="27" /> </svg> </div> <div class="col social_view pt-1"> <a data-toggle="tooltip" data-placement="top" title="Share on WhatsApp" class="social-share wattsap" target="_blank" data-action="share/whatsapp/share"> <svg width="22" height="22" viewBox="0 0 22 22" fill="#253645" xmlns="http://www.w3.org/2000/svg"> <path d="M11.0027 0H10.9973C4.93213 0 0 4.72563 0 10.5365C0 12.8414 0.7755 14.9777 2.09413 16.7123L0.72325 20.6266L4.95138 19.3319C6.69075 20.4356 8.76562 21.0731 11.0027 21.0731C17.0679 21.0731 22 16.3461 22 10.5365C22 4.72695 17.0679 0 11.0027 0ZM17.4034 14.8789C17.138 15.5967 16.0847 16.192 15.2446 16.3659C14.6699 16.4831 13.9191 16.5766 11.3919 15.573C8.15925 14.2902 6.0775 11.1437 5.91525 10.9396C5.75988 10.7354 4.609 9.27346 4.609 7.76147C4.609 6.24948 5.41062 5.51324 5.73375 5.19714C5.99913 4.93768 6.43775 4.81915 6.8585 4.81915C6.99463 4.81915 7.117 4.82573 7.227 4.831C7.55013 4.84417 7.71237 4.86261 7.9255 5.35124C8.19088 5.96368 8.83712 7.47567 8.91412 7.63108C8.9925 7.7865 9.07088 7.99723 8.96088 8.20137C8.85775 8.4121 8.767 8.50561 8.60475 8.68473C8.4425 8.86386 8.2885 9.00083 8.12625 9.19312C7.97775 9.36039 7.81 9.53951 7.997 9.84902C8.184 10.1519 8.83025 11.1621 9.78175 11.9734C11.0096 13.0205 12.0051 13.3551 12.3612 13.4973C12.6266 13.6027 12.9429 13.5776 13.1368 13.3801C13.3829 13.1259 13.6867 12.7044 13.9961 12.2895C14.2161 11.9919 14.4939 11.955 14.7854 12.0604C15.0824 12.1592 16.654 12.9033 16.9771 13.0574C17.3003 13.2128 17.5134 13.2866 17.5917 13.417C17.6687 13.5473 17.6687 14.1598 17.4034 14.8789Z" fill="#253645" /> </svg> </a> <a data-toggle="tooltip" data-placement="top" title="Share on Facebook" class="social-share facebook" href=""> <svg width="22" height="22" viewBox="0 0 22 22" fill="#253645" xmlns="http://www.w3.org/2000/svg"> <g clip-path="url(#clip0)"> <path d="M11 0C4.92525 0 0 4.36739 0 9.75551C0 12.8256 1.59913 15.5638 4.09888 17.3523V21.0731L7.84437 19.104C8.844 19.3688 9.90275 19.5123 11 19.5123C17.0748 19.5123 22 15.1449 22 9.75683C22 4.36871 17.0748 0 11 0ZM12.0931 13.1377L9.29225 10.2758L3.82662 13.1377L9.8395 7.02391L12.7091 9.8859L18.106 7.02391L12.0931 13.1377Z" fill="#253645" /> </g> <defs> <clipPath id="clip0"> <rect width="22" height="21.0731" fill="#253645" /> </clipPath> </defs> </svg> </a> <a data-toggle="tooltip" data-placement="top" title="Copy URL to clipboard" class="social-share copy" href=""> <svg width="25" height="25" viewBox="0 0 25 25" fill="#253645" xmlns="http://www.w3.org/2000/svg"> <path d="M20.3982 9.82129H11.7444C10.6823 9.82129 9.82129 10.6823 9.82129 11.7444V20.3982C9.82129 21.4603 10.6823 22.3213 11.7444 22.3213H20.3982C21.4603 22.3213 22.3213 21.4603 22.3213 20.3982V11.7444C22.3213 10.6823 21.4603 9.82129 20.3982 9.82129Z" stroke="#253645" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" /> <path d="M5.56333 15.1787H4.60179C4.09176 15.1787 3.60261 14.9761 3.24197 14.6155C2.88132 14.2548 2.67871 13.7657 2.67871 13.2556V4.60179C2.67871 4.09176 2.88132 3.60261 3.24197 3.24197C3.60261 2.88132 4.09176 2.67871 4.60179 2.67871H13.2556C13.7657 2.67871 14.2548 2.88132 14.6155 3.24197C14.9761 3.60261 15.1787 4.09176 15.1787 4.60179V5.56333" stroke="#253645" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" /> </svg> </a> <a data-toggle="tooltip" data-placement="top" class="social-share email" title="Share via Email" href=""> <svg width="25" height="25" viewBox="0 0 25 25" fill="#253645" xmlns="http://www.w3.org/2000/svg"> <path d="M22.8027 2.97852H2.19727C0.987207 2.97852 0 3.96465 0 5.17578V19.8242C0 21.0312 0.982813 22.0215 2.19727 22.0215H22.8027C24.0098 22.0215 25 21.0387 25 19.8242V5.17578C25 3.96875 24.0172 2.97852 22.8027 2.97852ZM22.4993 4.44336L12.5466 14.3961L2.50776 4.44336H22.4993ZM1.46484 19.5209V5.47212L8.51948 12.4663L1.46484 19.5209ZM2.50063 20.5566L9.55972 13.4976L12.0332 15.9498C12.3195 16.2337 12.7816 16.2328 13.0667 15.9476L15.4785 13.5358L22.4994 20.5566H2.50063ZM23.5352 19.5208L16.5143 12.5L23.5352 5.4791V19.5208Z" fill="#253645" /> </svg> </a> </div> </div> </div> </div>')
    }

    $(document).on('click', 'a[href^="#"]', function (event) {
      event.preventDefault();

      $('html, body').animate({
        scrollTop: ($($.attr(this, 'href')).offset().top - 160)
      }, 500);
    });


    setShareLinks();

    function socialWindow(url) {
      var params = "";
      // this loads share link in a pop-up.
      // var params = "menubar=no,toolbar=no,status=no,width=570,height=570,top=" + top + ",left=" + left;
      //   var left = (screen.width - 570) / 2;
      //   var top = (screen.height - 570) / 2;
      window.open(url, "NewWindow", params);
    }

    function setShareLinks(url) {

      let campaign = '';

      let utm_whatsapp_source = '';
      let utm_whatsapp_link = '';

      let utm_facebook_source = '';
      let utm_facebook_link = '';

      let utm_copy_source = '';
      let utm_copy_link = '';

      let utm_email_source = '';
      let utm_email_link = '';

      if ($('.program_page_template').length > 0) {
        // alert('true')

        if ($('h1').html() == "Bachelor’s in International  Hospitality Business") {

          campaign = 'Bachelor_share'

          //whatssap share generation
          utm_whatsapp_source = 'whatsapp_link_share'
          utm_whatsapp_link = '?utm_source=' + utm_whatsapp_source + '&utm_medium=Direct&utm_campaign=' + campaign
          // alert(utm_whatsapp_link)

          //facebook share generation
          utm_facebook_source = 'Facebook_link_share'
          utm_facebook_link = '?utm_source=' + utm_facebook_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //copy share generation
          utm_copy_source = 'copy_link_share'
          utm_copy_link = '?utm_source=' + utm_copy_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //email share generation
          utm_email_source = 'email_share'
          utm_email_link = '?utm_source=' + utm_email_source + '&utm_medium=Direct&utm_campaign=' + campaign

        } else if ($('h1').html() == "Master of Science in International Hospitality Business") {

          campaign = 'MSIHB_share'

          //whatssap share generation
          utm_whatsapp_source = 'whatsapp_link_share'
          utm_whatsapp_link = '?utm_source=' + utm_whatsapp_source + '&utm_medium=Direct&utm_campaign=' + campaign


          //facebook share generation
          utm_facebook_source = 'Facebook_link_share'
          utm_facebook_link = '?utm_source=' + utm_facebook_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //copy share generation
          utm_copy_source = 'copy_link_share'
          utm_copy_link = '?utm_source=' + utm_copy_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //email share generation
          utm_email_source = 'email_share'
          utm_email_link = '?utm_source=' + utm_email_source + '&utm_medium=Direct&utm_campaign=' + campaign


        } else if ($('h1').html() == "Master of Science in Hospitality, Entrepreneurship and Innovation") {

          campaign = 'MSHEI_share'

          //whatssap share generation
          utm_whatsapp_source = 'whatsapp_link_share'
          utm_whatsapp_link = '?utm_source=' + utm_whatsapp_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //facebook share generation
          utm_facebook_source = 'Facebook_link_share'
          utm_facebook_link = '?utm_source=' + utm_facebook_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //copy share generation
          utm_copy_source = 'copy_link_share'
          utm_copy_link = '?utm_source=' + utm_copy_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //email share generation
          utm_email_source = 'email_share'
          utm_email_link = '?utm_source=' + utm_email_source + '&utm_medium=Direct&utm_campaign=' + campaign


        } else if ($('h1').html() == "Master of Science in Real Estate, Finance and Hotel Development") {

          campaign = 'MSREFHD_share'

          //whatssap share generation
          utm_whatsapp_source = 'whatsapp_link_share'
          utm_whatsapp_link = '?utm_source=' + utm_whatsapp_source + '&utm_medium=Direct&utm_campaign=' + campaign


          //facebook share generation
          utm_facebook_source = 'Facebook_link_share'
          utm_facebook_link = '?utm_source=' + utm_facebook_source + '&utm_medium=Direct&utm_campaign=' + campaign


          //copy share generation
          utm_copy_source = 'copy_link_share'
          utm_copy_link = '?utm_source=' + utm_copy_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //email share generation
          utm_email_source = 'email_share'
          utm_email_link = '?utm_source=' + utm_email_source + '&utm_medium=Direct&utm_campaign=' + campaign

        } else if ($('h1').html() == "Master of Science in Luxury Management and Guest Experience") {

          campaign = 'MSLMGE_share'

          //whatssap share generation
          utm_whatsapp_source = 'whatsapp_link_share'
          utm_whatsapp_link = '?utm_source=' + utm_whatsapp_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //facebook share generation
          utm_facebook_source = 'Facebook_link_share'
          utm_facebook_link = '?utm_source=' + utm_facebook_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //copy share generation
          utm_copy_source = 'copy_link_share'
          utm_copy_link = '?utm_source=' + utm_copy_source + '&utm_medium=Direct&utm_campaign=' + campaign

          //email share generation
          utm_email_source = 'email_share'
          utm_email_link = '?utm_source=' + utm_email_source + '&utm_medium=Direct&utm_campaign=' + campaign

        }

        var link = window.location.href + encodeURIComponent(utm_whatsapp_link)
        // var pageUrl = encodeURIComponent(document.URL) + utm_facebook_link


      } else if ($('.blog_post_template').length > 0) {
        // alert('false')

        campaign = 'blog_share'

        //whatssap share generation
        utm_whatsapp_source = 'whatsapp_blog'
        utm_whatsapp_link = '?utm_source=' + utm_whatsapp_source + '&utm_medium=Direct&utm_campaign=' + campaign

        //facebook share generation
        utm_facebook_source = 'Facebook_blog'
        utm_facebook_link = '?utm_source=' + utm_facebook_source + '&utm_medium=Direct&utm_campaign=' + campaign

        //copy share generation
        utm_copy_source = 'copy_blog'
        utm_copy_link = '?utm_source=' + utm_copy_source + '&utm_medium=Direct&utm_campaign=' + campaign

        //email share generation
        utm_email_source = 'email_blog'
        utm_email_link = '?utm_source=' + utm_email_source + '&utm_medium=Direct&utm_campaign=' + campaign


        // let heroImage = $('.block-hero-01').find('.gatsby-image-wrapper picture img').attr('src')
        // // console.log(heroImage)
        // $("meta[property='og:image']").attr('content', heroImage)

      var link = window.location.href + encodeURIComponent(utm_whatsapp_link)

      } else {
        var link = window.location.href

      }

 
      $(".wattsap").attr('href', 'https://api.whatsapp.com/send?text=' + link)

      var pageUrl = encodeURIComponent(document.URL)

      $(".social-share.facebook").on("click", function () {
        if (utm_facebook_link !== '') {
          url = "https://www.facebook.com/sharer.php?u=" + pageUrl + utm_facebook_link;
        } else {
          url = "https://www.facebook.com/sharer.php?u=" + pageUrl
        }
        socialWindow(url);
      });


      var tweet = encodeURIComponent($("meta[property='og:description']").attr("content"));


      $(".social-share.twitter").on("click", function () {
        url = "https://twitter.com/intent/tweet?url=" + pageUrl + "&text=" + tweet;
        socialWindow(url);
      });

      $(".social-share.linkedin").on("click", function () {
        url = "https://www.linkedin.com/shareArticle?mini=true&url=" + pageUrl;
        socialWindow(url);
      })



      $(".social-share.email").on("click", function () {
        if (utm_email_link !== '') {
          // alert(utm_email_link)
          url = "mailto:?Subject=Checkout out this great page I found on the Glion website&Body=" + pageUrl + encodeURIComponent(utm_email_link);
        } else {
          url = "mailto:?Subject=Checkout out this great page I found on the Glion website&Body=" + pageUrl;
        }
        socialWindow(url);
      });



      $(".social-share.copy").on("click", function (event) {
        event.preventDefault()
        let updateText = '';

        if (utm_copy_link !== '') {
          updateText = window.location.href + utm_copy_link;
        } else {
          updateText = window.location.href
        }

        var dummy = document.createElement('input'),
          text = updateText;
        document.body.appendChild(dummy);
        dummy.value = text;
        dummy.select();
        document.execCommand('copy');
        document.body.removeChild(dummy);
        alert('Link copied to your clipboard')
      });
    }

    /***************************************************
    Reset program video to 0:00 when it ends
    ***************************************************/
    if ($(".program-structure").length > 0) {
      var video = document.querySelector('video');

      if (video) {
        video.addEventListener('ended', function () {
          video.load();
        });
      }
    }


    /***************************************************
        Data slide to toggle faculty resolution
         ***************************************************/

    $('.facultyModal[data-slide-to]').on('click', function () {
      $('#carouselExampleControls').carousel($(this).data('slide-to'));
    });


    /***************************************************
     Nav Burger toggle active
      ***************************************************/
    //  if ($(window).width() <= 767) {
    //   $('.navbar-toggler, .toggler-link').on('click', function() {
    //     $('.navbar-toggler-icon').toggleClass('active')
    //     $('body').toggleClass('nav-open')
    //   })
    // }

    /***************************************************
     Faculty Selector
    ***************************************************/
    $('#facultyOption').on('change', function () {
      let option = $(this).val()
      // console.log(option)

      $(".white-border").each(function () {
        if ($(this).hasClass(option)) {
          $(this).css({ 'display': 'flex', 'position': 'relative' });
        } else {
          $(this).css({ 'display': 'none', 'position': 'absolute' });
        }
      });
    })
    /***************************************************
      Add white bg slide up on load
      ***************************************************/


    if ($('.video').length > 0) {
      $('.video').on('click', function () {
        $('video').play();
      }, false);
    }

    /***************************************************
      Add drop shadow to header on first scroll
      ***************************************************/

    $(window).scroll(function (event) {
      var scroll = $(window).scrollTop();
      // console.log(scroll)

      if (scroll > 1) {
        $('header').addClass('scrolled')
      } else {
        $('header').removeClass('scrolled')
      }
    })


    /***************************************************
     Create clickable parent links above burger breakpoint
      ***************************************************/

    if ($(window).width() >= 769) {
      $('.aboutMenu').on('click', function () {
        window.location = "/about-us/"
      })
      $('.locationMenu').on('click', function () {
        window.location = "/locations/"
      })
      $('.programMenu').on('click', function () {
        window.location = "/programs/"
      })
      $('.admissionMenu').on('click', function () {
        window.location = "/apply-to-glion/"
      })
      $('.proposMenu').on('click', function () {
        window.location = "/fr/a-propos-de-glion/"
      })
      $('.programmesMenu').on('click', function () {
        window.location = "/fr/programmes/"
      })
      $('.nosMenu').on('click', function () {
        window.location = "/fr/nos-campus/"
      })
      $('.inscrireMenu').on('click', function () {
        window.location = "/fr/procedure-dinscription/"
      })
    }



    /***************************************************
     Video modal modifydata_target for when more than 1 block
      ***************************************************/
     if ( $('.video_wrapper').length > 0 ) {
        var countVid = 1
        $(".video_wrapper").each(function () {
          var dataTargeVid = "#content" + countVid;
          $(this).find('a').attr('data-target', dataTargeVid);
          countVid++
        })
        var modalVid = 1
        $(".modal").each(function () {
          var modalID = "content" + modalVid;
          $(this).attr('id', modalID);
          modalVid++
        })
      }
      



    /***************************************************
     Add style classes to gutenburg tables
      ***************************************************/
    if ($('.wp-block-table').length > 0) {
      $('table').addClass('fees_table')
      $("tr").addClass('py-3 opacity-bg').css('height', '40px')
      $("tr").first().removeClass('py-3 opacity-bg')
      $("tr").first().addClass('grey-bg py-2').css('height', '40px')
      $("tr").last().removeClass('py-3 opacity-bg')
      $("tr").last().addClass('grey-bg py-2').css('height', '40px')
      $('td').css('padding-left', '10px')
    }


    /***************************************************
     Add lavender background to any item contaiuning the word Master
      ***************************************************/

    $('.col-10.py-5.m-auto.text-center:contains("MASTER")').closest('.item').addClass('lavender-bg');
    /***************************************************
     Nav Hover Effects
      ***************************************************/

    if ($(window).width() >= 768) {



      $(".nav-parent, .dropdown-menu, .dropdown-menu-right, .mega-menu-column").hover(function () {
        $('header, .bg-light').css("background-color", blue);
        // $('.mega-menu-item a').animate({color: "#ffffff"}, 500 );
        $('.nav-parent').css('color', '#fff')
        $('.navbar-brand svg path').css('fill', '#fff')
      }, function () {
        $('header, .bg-light').css("background-color", "#fff");
        $('.nav-parent').css('color', bronze)
        // $('.mega-menu-item a').animate({color: blue}, 500 );
        $('.navbar-brand svg path').css('fill', bronze)


      });

     
    }

    /***************************************************
    Carousel Toggle
    ***************************************************/

    $('.carousel-toggle img, .svg-click').on('click', function (event) {
      var scrollLeft = $(this).attr('data_target')
      event.preventDefault();
      $(this).addClass('active');
      $(this).siblings().removeClass('active');
      $('.font4').siblings().each(function (i) {
        $(this).animate({
          left: scrollLeft + "%",
        }, 500)
      });

    })





    //mutli tab pane for home page
    $('#myTab li').click(function (event) {
      event.preventDefault();
      $(this).siblings().removeClass('active')
      $(this).addClass('active')
      // var href = $(this).attr('href');
      // if (href ==== "#bachelors_this") {

      //   $('.bachelors_this, .bachelors_that').addClass('show')
      //   $('.bachelors_this, .bachelors_that').removeClass('hide')
      //   $('.masters_this, .masters_that').removeClass('show')
      //   $('.masters_this, .masters_that').addClass('hide')
      // }
      // if (href ==== "#masters_this") {
      //   $('.masters_this, .masters_that').addClass('show')
      //   $('.masters_this, .masters_that').removeClass('hide')

      //   $('.bachelors_this, .bachelors_that').removeClass('show')
      //   $('.bachelors_this, .bachelors_that').addClass('hide')
      // }
    })

  });



  /***************************************************
  Social share toggle
  ***************************************************/
 $(document).ready(function () {
 $('.social_share_toggle').on('click', function (event) {
  event.preventDefault();
  $(".social_view").slideToggle("fast");

})
})




  /***************************************************
  Bootstrap Carousel  
  ***************************************************/

  /*
      Carousel
  */
  $('#carousel-example').on('slide.bs.carousel', function (e) {
    /*
        CC 2.0 License Iatek LLC 2018 - Attribution required
    */
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction === "left") {
          $('.carousel-item').eq(i).appendTo('.carousel-inner');
        }
        else {
          $('.carousel-item').eq(0).appendTo('.carousel-inner');
        }
      }
    }
  });

  //mutli tab pane for home page
  $('#myTab li').click(function (event) {
    event.preventDefault();
    $(this).siblings().removeClass('active')
    var href = $(this).attr('href');
    if (href === "#bachelors_this") {

      $('.bachelors_this, .bachelors_that').addClass('show')
      $('.bachelors_this, .bachelors_that').removeClass('hide')
      $('.masters_this, .masters_that').removeClass('show')
      $('.masters_this, .masters_that').addClass('hide')
    }
    if (href === "#masters_this") {
      $('.masters_this, .masters_that').addClass('show')
      $('.masters_this, .masters_that').removeClass('hide')

      $('.bachelors_this, .bachelors_that').removeClass('show')
      $('.bachelors_this, .bachelors_that').addClass('hide')
    }
  })


  /***************************************************
  Program Structure Slider
  ***************************************************/


  $('.program-structure li').on('click', function (event) {
    var scrollLeft = $(this).attr('data_target')
    var activeSlide = $(this).attr('activate_slide')
    // console.log(activeSlide)
    event.preventDefault();
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
    $('.scrollbar').animate({ scrollTop: 0 }, 200, function () {
      $('.program-structure .slide').each(function (i) {
        $(this).addClass('active_slide')
        $(this).animate({
          right: scrollLeft + "%",
          top: 0
        }, 500, function () {

          if ($(this).attr('id') === activeSlide) {
            $(this).addClass('active_slide')
            $(this).siblings().removeClass('active_slide')
          }
        });
      });
    })
  })


  /***************************************************
    Video function - Add custom play button, reset program video to 0:00 when it ends
    ***************************************************/

  // Apply custom play button/controls to videos on the program pages (pages that contain .program-structure class)
  $(document).ready(function () {
    if ($(".program-structure")[0]) {

      var slidevid = document.querySelector('video');

      if (slidevid) {
        var videotag_width = slidevid.offsetWidth;
        var videotag_height = slidevid.offsetHeight;
      }

      $('video').each(function () {
        $(this).get(0).controls = false;
      });

      var programplaybtn = '<div class="play_icon_btn"><img style="left:45%;position:absolute;top:50%;transform:translateY(-50%)" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABUCAYAAAB9czKDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAqASURBVHgB7V15jFXlFT+DIlCliK0LqDDQiqm4tmK1xTRpqraNtRa7WLRa/yix1mlMpDFiNe4aI3ED10jUKNEYd1Gj4BKXuOGKAioqi4C4ICiDsh1/P7/zeb95M++9cd599373zfslv9wZ5nLv+c63n++cc1skUqjq5rjsAO4LjgF3AYeB24GDwK3AvrwVXAe2g5+Cy8Fl4ALwOeNHLS0tGyRCtEhEgNKp2D+BB4H7gUPB70ltWA++Dz4PPgDOQGWskkiQewVA6f1wORw8CjwE3KLMrV+AHxjZ0j8Xp1yWgc8YAG4DbguOAAeWeQ7/7xPgreAsVMYnkiNyqwAonsPJMeB4ccPL5sGfqdz54lrtK+Ab4CJwJdgOpWmZZ7I87DFUPithN3Afcb1ptHTuTXPBW8Ab8cwl0hsAJW0PngF+pB2xCZwJTgR3lRSB520G7gGeBD4Cri1591KTaTtpVKBwA8DjwOUlhV8IXgaOkIxgjWAy+F6JLJ+ZjAOkkYAC7QfOKSksK+JIcKDkBLx7S/Ao6wEhKOueUnRY1z8d/Coo3ArwFP5NIoH1zn+XDIuU+aKY5PxOgODDwHuDAnGMvxlslUjBOQC8wWT1uAfcRooECLwv+HZQiEXgn6UAgJwt4OEl8wPLcoAUARD0EHBxIPwD4G5SMEDm4eDdQTmWgAdKzGArB9sDoa8E+0pBYXPYtSVD0u8lRkCw8ZpMthvASdIgULd/WB9MzsdKTIBAh6pbQ3v8R92utGGA8hwfNDCW9dcSAyDILuDnJhhbyYnSoEDZ/hk0stWa914BAmwLzjWBOE6e2WgtvxQo3zngRivzW9SB5AG8uB94V9AirlFnw29ooIx91JlOPG4Ft5CsgZeepsnq4EnN0aSQNdSZMJ4Nev5pkiXwwhE2BvoJaSfpZUCZh4Arg7mvVbIAhxl1mytf+8dJLwXK/rdgFJiVyRCsznrocZv0ckAH0wN9jJd6Ai/4IbjAXrZMIzasZQV1y/APTCdvgD+QekGdadljojTxDaCLtkAv/5d6AA/ur8nBxYretOqpBlsVLTPdsDf0l7SBh04Iavl0aaID1NmLPE6WNKHOKjjfHs5esKU00QHWCxaZjl7TNM+V8bCxQe1eLE10CejmQtMRl6bpnR3gYdfZg78E95YmuoQ61xdvtr5W0gAetLUmfjQzNENjG941FTxa87C19ACUU5NzcFoKtq72f/pIdbAr+Vn9jnJeaXXCj8CbwNuL0POgGzoJP2i/cpV4kNQKFHxKMP63SobA+x4K3s0zhwvA7SViqPMG8eaJK6RW4CGz7WFvSsYIKuCxYGz9VJ1DV/pr7ZQA2d41WedKLcADBgcFnyoZI6gAelrQu+6poEc8oZF6r6lz6PKroZ6bJtRZ+zx+KxkjrAD7nZbYv2iyI2cBaQzbWSIC5Dk40NvfK91bbRL+mV3phz9PcgajXMDb8eMe4HngRpAF5OHIqRrPcShd69fbzz1fPGhy5EifyX6SMUp7QBd/30vdsm+D3Udr5KF5V4S6XbG3kN5Z6d5qPWC4XZeg5X0lkQEyvYrLOHAC+I64gAz2EJ7T/kRyAuRag8uH9mtrpXvLVoA6r2C/5Is2esSGpWn4cX+Qyz42lL+Cj6MMkzQ/q63XGc9Qym4kK/UARiF6o9tyiRyM9QL/K64iuBlitAvnCRoRx+cwLHmdUY9lAw0rVQDX2X7cXykFASphHkj/zcPAheAQcXFgDH/6sWSHz+zaXxJLQidUqgB2G+9Yu0YKBlTCfbhwn8CzC8pPV0JO0vRfqmqjSQFeZ30l0WMnVKqAPsHf10kBgUpYDZ4rbjn9sLhGxQn7TXXOBfX0YvA628zYJbpjjGsE7CgujtiDscIf1zl6PpxzyhowK7WAjUbeUwhzcCnQwql4DkFHi1tQLAavBq+C8us9r/lhhzrcVO6mShXAndw6u2crKRDUBYW0gfRQGCyuBd4AngLFr5Bs4Je/1GPZPVSlCvjSyCVU/XxdUgaUPxaXKeBe9k9PgsdD8VlbcwfblTpcW+6mShXA7COcyX3+hahhVscrwSPETXpMyEG3yYeg/LWSPfwmljpsL3dT2UkYQrPr+M3EcIkUZiE9CT++JW4HTGVPBoeiDHflpHximF2XQoaN5W6qtgpaaFeG9teaNiZ1QKZfiUvocYm4nnovOAYFngi2S05Q57azg/26qNK91Spgvl05ng2RSIACjgK5u31cXDYUZlQ5Akr/I3fCkj84ZPt5c36lG6ttRF6yK1cVTPeyQHKE7WD/AZ4vbmVGi+NF4NTIrLVsFL5xz5aeQp1Lij+SvFwyRsmR5G/UxWSpyTQNHCoRQhM/Kso5SGoBHvCiPWyxZIygAl7SBE+D+0ikUJfq4B2T9YVq93fHFPGYXXfK8eyVCudww7RmB2K4eVnixShwpP38bLWbu1MBM4Ofx0n24EaGO9pRUPx0cJNECjRQXn4niR3ofqkV6tztPrYu9ahmmPsB7/oXOFoKAtPVo5qco9c2/gcPviSYVPaXJrqEujQ96S9aqHRN3O2mSBOdwOFHEzdO6urnkhbUBWi85ldDWrQMUhkAOhmoLnSLeBVM96wFDzwmWAqeKk18C2v9kwP9HClpw9a3S+wFa7QZpvQt1CUt8ek4ee5cHyc2dXmAfKaQs7TBM6N0B9b6zw5a/wlSL9gyy7urfwHuLr0c6sKSfIgqrQb1PbzCC8Zp4ot5pxY4J1ytsNZ/f9D6D5N6w1ZEM4LlVlz50zKCKb9Nk+U53eSzSfKqLlXLKnsxIydbpRfBlD9Sk3Q1TNmzo2QJdfnTfO0zbVn058ZpwJTPVc/LwSgwQfKAdgzgu17T3nxECFuITAvKfYXmFUarLn3NcyYIl6eZx5FlCWv9UwPlPwPm6zOlLsXvskCo/0mDwRRPMuu7N7bxuwcjJQZAkF9qshNkctOTtUE2aaZ4ZkqcFDQyhsqOlZigLjJwdRRjY0ow5TM956VBudjQDpYYAcEO0I7Ju7lRy8IXP3WY8un0NVOT1R5tYL+QmAEBdwffDyqBB+qFMVkE4/1ocF5QDpapGJliIOjOmqyTCW5UToh9XgjGexodVwTyMw45qmDwqoDAg9T5x3i7EbsxY3qj+5iDKb7Feu+sQPGU+fqiDqPebtRmqwYPbt+Z9SR3p6pA8SNNpvATW0yHcKIW9SM+IayAD2rHL1HQnD0x664djPFUPD3/zi5pIJSRDmHReoT3CFbgP6jbwISgUY+WxL21jkFzgdL7gj9VZ05YX6J4TrpxfqIkLahbV7dp54+nUQGvg+eCe6bR9QOlk7uqSyMzRzv2RP78IXispuXH8x2Q58c8eW7KT1sxA29Xy7ulIF0Q6aH9NMi8EPzIZ3c+5kl3euaK4Jp9jF0Hl9zOAJQ5IP13plu6scwRw+dsuVtmBTCSkb6f5VxeGFLKyMZVdmXoj1can0EnASqZrZgBcl1Fp7Pi+Pna6eI+Zzs7L8V7xPZB5++LC6rmVp8fTmOgXU+XgL6XMGXAi+Az4r4j/JSFX0WB2DdGlI+VwM+a0+uYH4pgEg62dLZ4nkfzHrZifvCZPYNhqMxU8rY4xb9eKUYrb3wNdT1yuklV7YcAAAAASUVORK5CYII=" alt="play-icon" /></div>';

      $('.program-structure video').before(programplaybtn)

      $(".program-structure .play_icon_btn").css({ 'width': videotag_width, 'height': videotag_height });

      callNextVideoPlayBtn("slide1");

    } else {
    }



  });

  // function for when another tab has been selected
  function callNextVideoPlayBtn(activeSlide) {

    var slidevid = document.getElementById(activeSlide).querySelector('video');
    if (slidevid != null) {
      var videotag_width = slidevid.offsetWidth;
      var videotag_height = slidevid.offsetHeight;
    }

    var activeslideclass = '.program-structure .active_slide .play_icon_btn';

    if ($(window).width() <= 767) {
      $(".program-structure .play_icon_btn").css({ 'width': videotag_width, 'height': videotag_height });
      activeslideclass = '#' + activeSlide + ' .play_icon_btn';
    }

    if (slidevid != null) {

      var slidevidtime = document.getElementById(activeSlide).getElementsByTagName('video')
      var vidsperslide = document.getElementById(activeSlide).getElementsByTagName('video').length

      // this stops the controls disappearing if you have started playing a video, then close the accordion/switch tab, then open it again

      for (var i = 0; i < vidsperslide; i++) {
        if (slidevidtime[i].currentTime == 0) {
          slidevidtime[i].controls = false;
        } else {
          slidevidtime[i].controls = true;
        }
      }

      $(document).on('click', activeslideclass, function () {

        // get the video after the clicked .play_icon_btn
        var siblingvideo = this.nextElementSibling;

        // play first video directly under the clicked .play_icon_btn
        siblingvideo.play();
        // add video controls
        siblingvideo.controls = true;

        // hide play button
        $(this).css({ 'display': 'none' });

        // when current video ends, return it to the start, remove default controls and add custom play button back
        siblingvideo.addEventListener('ended', function () {
          siblingvideo.load();
          siblingvideo.controls = false;
          $(this).prev().css({ 'display': 'block' });
          $(activeslideclass).css({ 'width': videotag_width, 'height': videotag_height });
        });

      });

      // when the window resizes, calculate the new height of the video (used to center align the custom play button)
      window.onresize = function (event) {
        var videotag_width = slidevid.offsetWidth;
        var videotag_height = slidevid.offsetHeight;
        $(activeslideclass).css({ 'width': videotag_width, 'height': videotag_height });
      };

    }
  }


  /***************************************************
    pause program videos if you switch tabs
    ***************************************************/
  $(".program-structure .structure_nav li").click(function () {
    document.querySelectorAll('video').forEach(vid => vid.pause());
    // document.querySelectorAll('video').forEach(vid => vid.controls = false);
    // $(".program-structure .active_slide .play_icon_btn").remove();
  });

  /***************************************************
    modal footer 
    ***************************************************/
  $(".modal-footer button").click(function () {
    $('iframe').each(function () {
      $('iframe').attr('src', $('iframe').attr('src'));
    });
  });

 





  /***************************************************
  Accordion - only call callNextVideoPlayBtn function if on the program page (.program-structure exists) and there is a video on the page
  ***************************************************/
  $("h6.accordion_title_black_text:not(.coloured), h2.accordion_title_black_text:not(.coloured)").on('click', function () {
    var activeSlide = $(this).attr('activate_slide');
    var videoexists = document.querySelectorAll('video');

    $(this).toggleClass('active')
    $(this).siblings('.expand-icon').toggleClass('active')
    let expandIcon = $(this).siblings('.expand-icon').find('img');
    if ($(expandIcon).attr('src') == 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K') {
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K');
      if ((videoexists != null) && ($(".program-structure")[0])) {
        document.querySelectorAll('video').forEach(vid => vid.pause());
        setTimeout(function () {
          callNextVideoPlayBtn(activeSlide);
        }, 200);
      }
    } else {
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
      if ((videoexists != null) && ($(".program-structure")[0])) {
        document.querySelectorAll('video').forEach(vid => vid.pause());
      }
    };
    $(this).siblings(".accordion_text").slideToggle("fast", function () {
      // Animation complete
    });
  });
  $("h6.accordion_title_black_text.coloured").on('click', function () {
    var activeSlide = $(this).attr('activate_slide');
    var videoexists = document.querySelectorAll('video');
    $(this).toggleClass('active')
    $(this).siblings('.expand-icon').toggleClass('active')
    let expandIcon = $(this).siblings('.expand-icon').find('img');
    if ($(expandIcon).attr('src') == 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K') {
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
      if ((videoexists != null) && ($(".program-structure")[0])) {
        document.querySelectorAll('video').forEach(vid => vid.pause());
        setTimeout(function () {
          callNextVideoPlayBtn(activeSlide);
        }, 200);
      }
    } else {
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
      if ((videoexists != null) && ($(".program-structure")[0])) {
        document.querySelectorAll('video').forEach(vid => vid.pause());
      }
    }
    $(this).siblings(".accordion_text").slideToggle("fast", function () {
      // Animation complete
    });
  });
  $("h6.accordion_title_white_text").on('click', function () {
    $(this).toggleClass('active')
    $(this).siblings('.expand-icon').toggleClass('active')
    let expandIcon = $(this).siblings('.expand-icon').find('img');
    if ($(expandIcon).attr('src') == 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K')
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K');
    else
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K');
    $(this).siblings(".accordion_text").slideToggle("fast", function () {
      // Animation complete
    });
  });

  $("footer .accordion span").on('click', function () {
    $(this).toggleClass('active')
    $(this).siblings('.expand-icon').toggleClass('active')
    let expandIcon = $(this).siblings('.expand-icon').find('img');
    if ($(expandIcon).attr('src') == 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K')
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMSIgdmlld0JveD0iMCAwIDIwIDEiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8bGluZSB5MT0iMC41IiB4Mj0iMjAiIHkyPSIwLjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
    else
      $(expandIcon).attr('src', 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K');
    $(this).siblings(".accordion_text").slideToggle("fast", function () {
      // Animation complete
    });
  });



  if ($('.counter').length > 0) {



    function isScrolledIntoView1(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
      // return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function isScrolledIntoView2(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
      // return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function isScrolledIntoView3(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
      // return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function isScrolledIntoView4(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
      // return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function isScrolledIntoView5(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
      // return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
    if ($('.font1').length > 0) {
      $(window).scroll(testScroll1);
      var viewed1 = false;
      function testScroll1() {
        if (isScrolledIntoView1($(".font1")) && !viewed1) {
          viewed1 = true;
          $('.value1').each(function () {
            if (($(this).text() % 1 != 0)) {
              // alert('true');
              $(this).prop('Counter', $(this).text() - .4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(parseFloat(now).toFixed(1));
                }
              });
            } else {
              $(this).prop('Counter', $(this).text() - 4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(Math.ceil(now));
                }
              });
            }
          });
        }
      }
    }
    if ($('.font2').length > 0) {
      $(window).scroll(testScroll2);
      var viewed2 = false;
      function testScroll2() {
        if (isScrolledIntoView2($(".font2")) && !viewed2) {
          viewed2 = true;
          $('.value2').each(function () {
            if (($(this).text() % 1 != 0)) {
              $(this).prop('Counter', $(this).text() - .4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(parseFloat(now).toFixed(1));
                }
              });
            } else {
              $(this).prop('Counter', $(this).text() - 4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(Math.ceil(now));
                }
              });
            }
          });
        }
      }
    }
    if ($('.font3').length > 0) {
      $(window).scroll(testScroll3);
      var viewed3 = false;
      function testScroll3() {
        if (isScrolledIntoView3($(".font3")) && !viewed3) {
          viewed3 = true;
          $('.value3').each(function () {
            if (($(this).text() % 1 != 0)) {
              $(this).prop('Counter', $(this).text() - .4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(parseFloat(now).toFixed(1));
                }
              });
            } else {
              $(this).prop('Counter', $(this).text() - 4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(Math.ceil(now));
                }
              });
            }
          });
        }
      }
    }
    if ($('.font4').length > 0) {
      $(window).scroll(testScroll4);
      var viewed4 = false;
      function testScroll4() {
        if (isScrolledIntoView4($(".font4")) && !viewed4) {
          viewed4 = true;
          $('.value4').each(function () {
            if (($(this).text() % 1 != 0)) {
              $(this).prop('Counter', $(this).text() - .4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(parseFloat(now).toFixed(1));
                }
              });
            } else {
              $(this).prop('Counter', $(this).text() - 4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(Math.ceil(now));
                }
              });
            }
          });
        }
      }
    }
    if ($('.font5').length > 0) {
      $(window).scroll(testScroll5);
      var viewed5 = false;
      function testScroll5() {
        if (isScrolledIntoView5($(".font5")) && !viewed5) {
          viewed5 = true;
          $('.value5').each(function () {
            if (($(this).text() % 1 != 0)) {
              $(this).prop('Counter', $(this).text() - .4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(parseFloat(now).toFixed(1));
                }
              });
            } else {
              $(this).prop('Counter', $(this).text() - 4).animate({
                Counter: $(this).text()
              }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                  $(this).text(Math.ceil(now));
                }
              });
            }
          });
        }
      }
    }
  }
}