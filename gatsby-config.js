module.exports = {
  siteMetadata: {
    title: `Glion Institute of Higher Education`,
    description: `Hospitality Management School`,
    author: `@Jamie Stone`,
    siteUrl: `https://www.glion.edu`,
  },
  plugins: [
    // {
    //   resolve: "gatsby-plugin-google-tagmanager",
    //   options: {
    //     id: "GTM-WP8B6B",

    //     // Include GTM in development.
    //     //
    //     // Defaults to false meaning GTM will only be loaded in production.
    //     includeInDevelopment: false,

    //     // datalayer to be set before GTM is loaded
    //     // should be an object or a function that is executed in the browser
    //     //
    //     // Defaults to null
    //     defaultDataLayer: { platform: "gatsby" },

    //     // Specify optional GTM environment details.
    //     // gtmAuth: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_AUTH_STRING",
    //     // gtmPreview: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_PREVIEW_NAME",
    //     // dataLayerName: "YOUR_DATA_LAYER_NAME",

    //     // Name of the event that is triggered
    //     // on every Gatsby route change.
    //     //
    //     // Defaults to gatsby-route-change
    //     // routeChangeEventName: "YOUR_ROUTE_CHANGE_EVENT_NAME",

    //   },
    // },
    // {
    //   resolve: `gatsby-plugin-google-analytics`,
    //   options: {
    //     // The property ID; the tracking code won't be generated without it
    //     trackingId: "UA-12007454-26",
    //     // Defines where to place the tracking script - `true` in the head and `false` in the body
    //     head: true,
    //     // Setting this parameter is optional
    //   },
    // },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: "https://www.glion.edu",
        sitemap: "https://www.glion.edu/sitemap.xml",
        policy: [
          {
            userAgent: "*",
            allow: "/",
            disallow: [
              "/thank-you-brochure-all",
              "/thank-you-contact",
              "/fr/thank-you-brochure-all-french",
            ],
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        exclude: [
          "/thank-you-brochure-all",
          "/thank-you-contact",
          "/fr/thank-you-brochure-all-french",
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 2000,
              quality: 100,
            },
          },
        ],
      },
    },
    `gatsby-plugin-react-helmet`,
    // {
    //   resolve: `gatsby-plugin-algolia`,
    //   options: {
    //     appId: process.env.ALGOLIA_APP_ID,
    //     apiKey: process.env.ALGOLIA_API_KEY,
    //     indexName: process.env.ALGOLIA_INDEX_NAME,
    //     queries,
    //     chunkSize: 10000, // default: 1000
    //     settings: {
    //       // optional, any index settings
    //     },
    //     enablePartialUpdates: true, // default: false
    //     matchFields: ['slug', 'modified'], // Array<String> default: ['modified']
    //   },
    // },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pdf`,
        path: `${__dirname}/src/assets`,
        ignore: [`**/\.*`], // ignore files starting with a dot
      },
    },
    {
      resolve: "gatsby-plugin-anchor-links",
      options: {
        offset: -160,
      },
    },
    // {
    //   resolve: `gatsby-plugin-gdpr-cookies`,
    //   options: {
    //     googleAnalytics: {
    //       trackingId: 'YOUR_GOOGLE_ANALYTICS_TRACKING_ID', // leave empty if you want to disable the tracker
    //       cookieName: 'gatsby-gdpr-google-analytics', // default
    //       anonymize: true // default
    //     },
    //     googleTagManager: {
    //       trackingId: 'GTM-WP8B6B', // leave empty if you want to disable the tracker
    //       cookieName: 'gatsby-gdpr-google-tagmanager', // default
    //       dataLayerName: 'dataLayer', // default
    //     },
    //     facebookPixel: {
    //       pixelId: 'YOUR_FACEBOOK_PIXEL_ID', // leave empty if you want to disable the tracker
    //       cookieName: 'gatsby-gdpr-facebook-pixel', // default
    //     },
    //     // defines the environments where the tracking should be available  - default is ["production"]
    //     environments: ['production', 'development']
    //   },
    // },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/glion-favicon.png`, // This path is relative to the root of the site.
        icon_options: {
          purpose: `maskable`,
        },
      },
    },
    {
      resolve: `gatsby-source-wordpress-experimental`,
      options: {
        url:
          process.env.WPGRAPHQL_URL ||
          `https://gy635czgvgogs1a3tdly-admin.glion.edu/graphql`,
        // `http://glion.local/graphql`,
        verbose: false,
        develop: {
          hardCacheMediaFiles: true,
          nodeUpdateInterval: 10000000,
        },
        // debug: {
        //   graphql: {
        //     writeQueriesToDisk: true,
        //     onlyReportCriticalErrors: false,
        //   },
        // },
        schema: {
          typePrefix: `Wp`,
        },
        html: {
          useGatsbyImage: false,
          imageMaxWidth: 2500,
          fallbackImageMaxWidth: 2000,
          imageQuality: 90,
        },
        type: {
          Post: {
            limit:
              process.env.NODE_ENV === `development`
                ? // Lets just pull 50 posts in development to make it easy on ourselves.
                  50
                : // and we don't actually need more than 5000 in production for this particular site
                  5000,
          },
        },
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `playfair display:400`,
          // `roboto:300,400,700`,
        ],
        display: "swap",
      },
    },

    // {
    //   resolve: 'gatsby-plugin-prefetch-google-fonts',
    //   options: {
    //     fonts: [
    //       {
    //         family: `Roboto`,
    //         variants: [`400`, `700`],
    //       },
    //     ],
    //   },
    // },
    "gatsby-plugin-styled-components",
    {
      resolve: `gatsby-plugin-netlify`,
      options: {
        allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
        mergeSecurityHeaders: true, // boolean to turn off the default security headers
        mergeLinkHeaders: true, // boolean to turn off the default gatsby js headers
        mergeCachingHeaders: true, // boolean to turn off the default caching headers
        transformHeaders: (headers, path) => headers, // optional transform for manipulating headers under each path (e.g.sorting), etc.
        generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
      },
    },
    "gatsby-plugin-remove-serviceworker",
    // {
    //   resolve: `gatsby-plugin-offline`,
    //   options: {
    //     workboxConfig: {
    //       runtimeCaching: [
    //         {
    //           urlPattern: /(\.js$|\.css$|static\/)/,
    //           handler: `CacheFirst`,
    //         },
    //         {
    //           urlPattern: /^https?:.*\/page-data\/.*\/(page-data|app-data)\.json$/,
    //           handler: `NetworkFirst`,
    //           options: {
    //             networkTimeoutSeconds: 1,
    //           },
    //         },
    //         {
    //           urlPattern: /^https?:.*\.(png|jpg|jpeg|webp|svg|gif|tiff|js|woff|woff2|json|css)$/,
    //           handler: `StaleWhileRevalidate`,
    //         },
    //         {
    //           urlPattern: /^https?:\/\/fonts\.googleapis\.com\/css/,
    //           handler: `StaleWhileRevalidate`,
    //         },
    //         {
    //           urlPattern: /\/$/,
    //           handler: `NetworkFirst`,
    //           options: {
    //             networkTimeoutSeconds: 1,
    //           },
    //         },
    //       ],
    //     },
    //   },
    // }
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
