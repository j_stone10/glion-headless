const path = require('path')
const slash = require('slash')
const { paginate } = require('gatsby-awesome-pagination')

// gatsby-node.js
// exports.createSchemaCustomization = ({ actions }) => {
//   actions.createTypes(`
//     type SitePage implements Node @dontInfer {
//       path: String!
//     }
//   `)
// }

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const homeTemplate = path.resolve('./src/templates/home.js');
  const aboutTemplate = path.resolve('./src/templates/about.js');
  const alumniTemplate = path.resolve('./src/templates/alumni.js');
  const careersTemplate = path.resolve('./src/templates/careers.js');
  const visitACampusTemplate = path.resolve('./src/templates/visit.js');
  const campusTemplate = path.resolve('./src/templates/campus.js');
  const programTemplate = path.resolve('./src/templates/program.js');
  const facultyTemplate = path.resolve('./src/templates/faculty.js');
  const pageTemplate = path.resolve('./src/templates/page.js');
  const postTemplate = path.resolve('./src/templates/post.js');
  const postTemplate2021 = path.resolve('./src/templates/post-2021.js');
  const postTemplate2021FR = path.resolve('./src/templates/post-2021-fr.js');
  const postTemplateFR = path.resolve('./src/templates/post-fr.js');
  const blogTemplate = path.resolve('./src/templates/blog.js');
  const blogTemplateFR = path.resolve('./src/templates/blog-fr.js');
  const archiveTemplate = path.resolve('./src/templates/archive.js');
  const archiveTemplateFR = path.resolve('./src/templates/archive-fr.js');
  const alumniPostTemplate = path.resolve('./src/templates/alumni-post.js');
  const facultyPostTemplate = path.resolve('./src/templates/faculty-post.js');

  const result = await graphql(`
    {
      home: allWpPage(filter: {pageTemplate: {eq: "page-templates/home.php"}}) {
        edges {
          node {
            id
            slug
            status
            link
            uri
          }
        }
      }
      about: allWpPage(filter: {pageTemplate: {eq: "page-templates/about-us.php"}}) {
        edges {
          node {
            id
            slug
            status
            link

          }
        }
      }
      alumni: allWpPage(filter: {pageTemplate: {eq: "page-templates/alumni-landing.php"}}) {
        edges {
          node {
            id
            slug
            status
            link

          }
        }
      }
      careers: allWpPage(filter: {pageTemplate: {eq: "page-templates/careers.php"}}) {
        edges {
          node {
            id
            slug
            status
            link

          }
        }
      }
      visit: allWpPage(filter: {pageTemplate: {eq: "page-templates/visit-a-campus.php"}}) {
        edges {
          node {
            id
            slug
            status
            link
          }
        }
      }
      campuses: allWpPage(filter: {pageTemplate: {eq: "page-templates/campus.php"}}) {
        edges {
          node {
            id
            slug
            status
            link
          }
        }
      }
      programs: allWpPage(filter: {pageTemplate: {eq: "page-templates/program-ind.php"}}) {
        edges {
          node {
            id
            slug
            status
            link
          }
        }
      }

      faculty: allWpPage(filter: {pageTemplate: {eq: "page-templates/faculty.php"}}) {
        edges {
          node {
            id
            slug
            status
            link

          }
        }
      }

      pages: allWpPage {
        edges {
          node {
            id
            slug
            status
            link
            parentId
            pageTemplate
          }
        }
      }

      englishallWpPost: allWpPost(filter: {language: {slug: {eq: "en"}}}) {
        edges {
          node {
            id
            slug
            link
            status
            content
            categories {
              nodes{
                id
                name
              }
            }
          }
        }
      }
      frenchallWpPost: allWpPost(filter: {language: {slug: {eq: "fr"}}}) {
        edges {
          node {
            id
            slug
            link
            status
            content
            categories {
              nodes{
                id
                name
              }
            }
          }
        }
      }
      newallWpPost: allWpPost(filter: {template: {templateName: {in: "2021 Blog Template"}}}) {
        edges {
          node {
            id
            slug
            link
            status
            content
            categories {
              nodes{
                id
                name
              }
            }
          } 
        }
      }
      newallWpPostFR: allWpPost(filter: {template: {templateName: {in: "2021 Blog Template"}}, language: {slug: {eq: "fr"}}}) {
        edges {
          node {
            id
            slug
            link
            status
            content
            categories {
              nodes {
                id
                name
              }
            }
          }
        }
      }
      
      allWpCategory {
        edges {
          node {
            id
            name
            slug
            count
            link
            description
          }
        }
      }
      frenchCategory:   allWpCategory(filter: {language: {slug: {eq: "fr"}}}) {
        totalCount
        edges {
          node {
            id
            name
            slug
            count
            link
            description
          }
        }
      }
        alumniPosts:  allWpAlumni {
          edges {
            node {
              id
              slug
              link
              status
     
             
            }
          }
        }
        facultyPosts:  allWpFaculty {
          edges {
            node {
              id
              slug
              link
              status
      
             
            }
          }
        }    
      }
    `);

  // Check for errors
  if (result.errors) {
    throw new Error(result.errors);
  }

  const {
  
    home,
    about,
    alumni,
    faculty,
    careers,
    visit,
    campuses,
    programs,
    pages,
    englishallWpPost,
    frenchallWpPost,
    allWpCategory,
    frenchCategory,
    alumniPosts,
    facultyPosts,
    newallWpPost,
    newallWpPostFR,
  } = result.data;

  

  pages.edges.forEach(edge => {
    if ( ( edge.node.status === 'publish' ) && ( edge.node.pageTemplate === "page-templates/modules.php" )  ) {
      createPage({
        path: edge.node.link,
        component: slash(pageTemplate),
        context: {
          id: edge.node.id,
         
        }
      })
    };
  });
  

  home.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.uri,
        component: slash(homeTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });
  about.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(aboutTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });
  alumni.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(alumniTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });
  careers.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(careersTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });
  visit.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(visitACampusTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });
  programs.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(programTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });

  campuses.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(campusTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });
  faculty.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(facultyTemplate),
        context: {
          id: edge.node.id,
          
        }
      })

    };
  });


  
  // // Create archive pages for each category
  allWpCategory.edges.forEach(catEdge => {
    // First filter out the posts that belongs to the current category
    const filteredPosts = englishallWpPost.edges.filter(
      ({ node: { categories } }) =>
      categories.nodes.some(el => el.name === catEdge.node.name)
    );
    // const filteredPosts = allWpPost.edges.filter(
    //   ({ node: { categories } }) =>
    //     categories.nodes.some(el => el.id === catEdge.node.id)
    // );
    // Some categories may be empty and we don't want to show them
    if (filteredPosts.length > 0) {
      paginate({
        createPage,
        items: filteredPosts,
        itemsPerPage: 12,
        pathPrefix: `/magazine/category/${catEdge.node.slug}`,
        component: slash(archiveTemplate),
        context: {
          // catId: catEdge.node.id,
          catName: catEdge.node.name,
          catSlug: catEdge.node.slug,
          catCount: catEdge.node.count,
          // categories: allWpCategory.edges,
          description: catEdge.node.description
        },
      });
    }
  });
  
  frenchCategory.edges.forEach(catEdge => {
    // First filter out the posts that belongs to the current category
    const filteredPosts = frenchallWpPost.edges.filter(
      ({ node: { categories } }) =>
      categories.nodes.some(el => el.name === catEdge.node.name)
    );
    // const filteredPosts = allWpPost.edges.filter(
    //   ({ node: { categories } }) =>
    //     categories.nodes.some(el => el.id === catEdge.node.id)
    // );
    // Some categories may be empty and we don't want to show them
    if (filteredPosts.length > 0) {
      paginate({
        createPage,
        items: filteredPosts,
        itemsPerPage: 12,
        pathPrefix: `/fr/magazine/category/${catEdge.node.slug}`,
        component: slash(archiveTemplateFR),
        context: {
          // catId: catEdge.node.id,
          catName: catEdge.node.name,
          catSlug: catEdge.node.slug,
          catCount: catEdge.node.count,
          // categories: allWpCategory.edges,
          description: catEdge.node.description
        },
      });
    }
  });


  
  englishallWpPost.edges.forEach(edge => {
    if ( (edge.node.status === 'publish') && ( edge.node.content !== null ) ) {
      createPage({
        path: edge.node.link,
        component: slash(postTemplate),
        context: {
          id: edge.node.id,
        
          // description: edge.node.description,
        },
      });
      const blogPosts = englishallWpPost.edges;
      // const pathPrefix = ({ pageNumber, numberOfPages }) =>
      // pageNumber === 0 ? '/blog' : '/blog/page'
        paginate({
          createPage, // The Gatsby `createPage` function
          items: blogPosts, // An array of objects
          itemsPerPage: 12, // How many items you want per page
          pathPrefix: '/magazine', // Creates pages like `/blog`, `/blog/2`, etc
          component: path.resolve(blogTemplate)
        })
    }
  });

  frenchallWpPost.edges.forEach(edge => {
    if ( (edge.node.status === 'publish') && ( edge.node.content !== null ) ) {
      createPage({
        path: edge.node.link,
        component: slash(postTemplateFR),
        context: {
          id: edge.node.id,
        
          // description: edge.node.description,
        },
      });
      const blogPosts = frenchallWpPost.edges;

      // const pathPrefix = ({ pageNumber, numberOfPages }) =>
      // pageNumber === 0 ? '/blog' : '/blog/page'
        paginate({
          createPage, // The Gatsby `createPage` function
          items: blogPosts, // An array of objects
          itemsPerPage: 12, // How many items you want per page
        
          pathPrefix: '/fr/magazine-fr', // Creates pages like `/blog`, `/blog/2`, etc
          component: path.resolve(blogTemplateFR)
        })
    }
  });

  newallWpPost.edges.forEach(edge => {
    if ( (edge.node.status === 'publish') && ( edge.node.content !== null ) ) {
      createPage({
        path: edge.node.link,
        component: slash(postTemplate2021),
        context: {
          id: edge.node.id,
        
          // description: edge.node.description,
        },
      });
      // const blogPosts2021 = newallWpPost.edges;
      // paginate({
      //   createPage, // The Gatsby `createPage` function
      //   items: blogPosts2021, // An array of objects
      //   itemsPerPage: 12, // How many items you want per page
      //   pathPrefix: '/magazine', // Creates pages like `/blog`, `/blog/2`, etc
      //   component: path.resolve(blogTemplate)
      // })
    }
  })

  newallWpPostFR.edges.forEach(edge => {
    if ( (edge.node.status === 'publish') && ( edge.node.content !== null ) ) {
      createPage({
        path: edge.node.link,
        component: slash(postTemplate2021FR),
        context: {
          id: edge.node.id,
        
          // description: edge.node.description,
        },
      });
      // const blogPosts2021 = newallWpPost.edges;
      // paginate({
      //   createPage, // The Gatsby `createPage` function
      //   items: blogPosts2021, // An array of objects
      //   itemsPerPage: 12, // How many items you want per page
      //   pathPrefix: '/magazine', // Creates pages like `/blog`, `/blog/2`, etc
      //   component: path.resolve(blogTemplate)
      // })
    }
  })

  alumniPosts.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(alumniPostTemplate),
        context: {
          id: edge.node.id,
          
          
        },
      });
    }
  });

  facultyPosts.edges.forEach(edge => {
    if (edge.node.status === 'publish') {
      createPage({
        path: edge.node.link,
        component: slash(facultyPostTemplate),
        context: {
          id: edge.node.id,
          
          
        },
      });
    }
  });


}