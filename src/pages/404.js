import React, { Component } from 'react'
import Layout from "../components/layout"
import SEO from "../components/seo"




class NotFoundPage extends Component {
  render() {
    const polyLang = []
    const currLang = "en"
    polyLang.splice(0, 0, { "link": "/" });
    return (
      <Layout polylang_translations={polyLang} currentLang={currLang}  >

        <SEO title="404: Not found" lang={currLang} description={null} image={null} uri={null}  />
        <div id="content">
          <div className="container-fluid">
            <h1>NOT FOUND</h1>
            <p>You just hit a route that doesn&#39;t exist. Please use the menu to try again.</p>
          </div>
        </div>
      </Layout>
    )
  }
}
export default NotFoundPage
