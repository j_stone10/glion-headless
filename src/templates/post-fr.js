/* eslint-disable react/no-danger */
import React, { Component } from 'react'
import SEO from '../components/seo';
import { graphql, Link } from 'gatsby';
import Helmet from "react-helmet"
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Parser from 'html-react-parser'
import Img from 'gatsby-image'
import Video from "../components/video"
import Layout from '../components/layout';
import Breadcrumb from '../components/Breadcrumb';
import InsiderSVG from '../images/NEw_insider.svg';
import '../components/styles/blog.css';
// import $ from 'jQuery';



const PostContent = styled.article`
  margin: 20px 0 0 0;
`;

class postTemplateFR extends Component {

  componentDidMount() {

    //show post date
    var newItem = document.createElement("div");
    var textnode = "<div class='row'><div class='col-12'><h3 class='text-center mt-5 mb-0'>" + this.props.data.post.date + "</h3></div></div>"

    newItem.innerHTML = textnode
    var list = document.querySelector('.block-editorial-content-01');

    list.insertBefore(newItem, list.childNodes[0]);

  }

  render() {

    const post = this.props.data.post
    // console.log(post)
    // const media = this.props.data.allWpMediaItem.edges
    const allWpPost = this.props.data.allWpPost.edges

    // let linkedinShare = post.content
    // // console.log(linkedinShare)


    // linkedinShare = document.querySelector('.background_image picture img ')
    // // console.log(linkedinShare)

    // let linkedinShareValue =  linkedinShare.src
    
    // linkedinShareValue = linkedinShareValue.split('http://127.0.0.1:8000/').join('/')
    // // linkedinShareValue.replace('http://127.0.0.1:8000/', '')
    // console.log(linkedinShareValue)

    // var linkedinShareValueArray = linkedinShareValue.split(',').pop();
    // console.log(linkedinShareValueArray)

    // const image = {
    //   'localFile': {
    //     'publicURL': linkedinShareValue
    //   },
    //   'mediaDetails': {
    //     'width': 1445,
    //     'height': 875,
    //   },
    // }

    // console.log(image)

    if (allWpPost.length > 12) {
      allWpPost.length = 12
    }



    // console.log(allWpPost)
    // console.log(currentPage.content)
    // console.log( currentPage.acf );
    const Content = Parser(post.content, {
      replace: domNode => {
        // console.dir( "domNode" + domNode )

        // if (domNode.name === 'img') {
        //   // TODO: also check src attribute for 'wp-content/uploads'
        //   let image = media.filter(m => {
        //     // console.log(m.node.sourceUrl)
        //     // console.log(domNode.attribs.src)
        //     return m.node.sourceUrl == domNode.attribs.src

        //   })
        //   if (image != '') {
        //     console.log(image)
        //     image = image[0].node.localFile
        //     return <Img fluid={image.childImageSharp.fluid} />
        //   }
        // }



        // if (domNode.name === 'video') {
        //   // TODO: also check src attribute for 'wp-content/uploads'
        //   let video = media.filter(v => {
        //     // console.log("1: " + domNode.attribs.poster)
        //     // console.log("2: " + v.node.source_url)
        //     return v.node.source_url === domNode.attribs.src
        //   })
        //   let poster = media.filter(v => {
        //     // console.log("1: " + domNode.attribs.poster)
        //     // console.log("2: " + v.node.source_url)
        //     return v.node.source_url === domNode.attribs.poster
        //   })
        //   if (video != '') {
        //     // console.log(video)
        //     // console.log(poster)
        //     video = video[0].node.localFile
        //     poster = poster[0].node.localFile

        //     return <video className="video" controls src={video.publicURL}
        //       poster={poster.publicURL}
        //     // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


        //     />
        //     // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

        //   }
        // }



      },
    })

    return (
      <Layout polylang_translations={post.translations} currentLang={post.language.slug}  >
        
        <SEO title={post.seo.title} lang={post.language.slug} description={post.seo.metaDesc} image={post.linkedInShare.linkedinShareImage !== null ? post.linkedInShare.linkedinShareImage : post.featuredImage.node} uri={post.uri} author={post.author} />


        <div id="content" className="blog_post_template">
          <div className="container-fluid">
            <Breadcrumb
              parent={{
                link: '/fr/magazine-fr/',
                title: 'Magazine',
              }
              } currentLang={post.language.slug} title={post.title}
            />

            <div className="row magazine-header my-5" style={{ maxWidth: "100%" }}>
              <div className="col-12 col-md-5  mx-auto my-5">
                <Link to="/fr/magazine">
                  <img src={InsiderSVG} style={{ maxWidth: "100%", height: "auto" }} />
                </Link>
              </div>

            </div>
            <div className="row">
              <div className="col-12 col-md-10 p-md-0 m-auto">
                <h1 className="mb-5" dangerouslySetInnerHTML={{ __html: post.title }} />

              </div>
            </div>
            <div className="row">
              <div className="col-12 post-template-default" dangerouslySetInnerHTML={{__html: post.content}}>
              </div>

            </div>
          </div>
          <div className="row share mb-3 mx-0">
            <div className="container">
              <div className="row my-1">
                <div className="col" style={{ maxWidth: "55px" }}>
                  <svg className="social_share_toggle" width="47" height="46" viewBox="0 0 47 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M2.2207 40.334C2.27539 40.7285 2.38281 41.0234 2.54297 41.2188C2.83594 41.5742 3.33789 41.752 4.04883 41.752C4.47461 41.752 4.82031 41.7051 5.08594 41.6113C5.58984 41.4316 5.8418 41.0977 5.8418 40.6094C5.8418 40.3242 5.7168 40.1035 5.4668 39.9473C5.2168 39.7949 4.82422 39.6602 4.28906 39.543L3.375 39.3379C2.47656 39.1348 1.85547 38.9141 1.51172 38.6758C0.929688 38.2773 0.638672 37.6543 0.638672 36.8066C0.638672 36.0332 0.919922 35.3906 1.48242 34.8789C2.04492 34.3672 2.87109 34.1113 3.96094 34.1113C4.87109 34.1113 5.64648 34.3535 6.28711 34.8379C6.93164 35.3184 7.26953 36.0176 7.30078 36.9355H5.56641C5.53516 36.416 5.30859 36.0469 4.88672 35.8281C4.60547 35.6836 4.25586 35.6113 3.83789 35.6113C3.37305 35.6113 3.00195 35.7051 2.72461 35.8926C2.44727 36.0801 2.30859 36.3418 2.30859 36.6777C2.30859 36.9863 2.44531 37.2168 2.71875 37.3691C2.89453 37.4707 3.26953 37.5898 3.84375 37.7266L5.33203 38.084C5.98438 38.2402 6.47656 38.4492 6.80859 38.7109C7.32422 39.1172 7.58203 39.7051 7.58203 40.4746C7.58203 41.2637 7.2793 41.9199 6.67383 42.4434C6.07227 42.9629 5.2207 43.2227 4.11914 43.2227C2.99414 43.2227 2.10938 42.9668 1.46484 42.4551C0.820312 41.9395 0.498047 41.2324 0.498047 40.334H2.2207ZM10.1062 43V34.3633H11.8934V37.6562H15.2684V34.3633H17.0613V43H15.2684V39.1445H11.8934V43H10.1062ZM22.3512 39.7363H24.5426L23.4645 36.3379L22.3512 39.7363ZM22.4625 34.3633H24.5016L27.5602 43H25.6031L25.0465 41.2246H21.8648L21.2672 43H19.3805L22.4625 34.3633ZM31.6664 35.8633V38.1836H33.7113C34.1176 38.1836 34.4223 38.1367 34.6254 38.043C34.9848 37.8789 35.1645 37.5547 35.1645 37.0703C35.1645 36.5469 34.9906 36.1953 34.643 36.0156C34.4477 35.9141 34.1547 35.8633 33.7641 35.8633H31.6664ZM34.1391 34.3633C34.7445 34.375 35.2094 34.4492 35.5336 34.5859C35.8617 34.7227 36.1391 34.9238 36.3656 35.1895C36.5531 35.4082 36.7016 35.6504 36.8109 35.916C36.9203 36.1816 36.975 36.4844 36.975 36.8242C36.975 37.2344 36.8715 37.6387 36.6645 38.0371C36.4574 38.4316 36.1156 38.7109 35.6391 38.875C36.0375 39.0352 36.3188 39.2637 36.4828 39.5605C36.6508 39.8535 36.7348 40.3027 36.7348 40.9082V41.4883C36.7348 41.8828 36.7504 42.1504 36.7816 42.291C36.8285 42.5137 36.9379 42.6777 37.1098 42.7832V43H35.1234C35.0688 42.8086 35.0297 42.6543 35.0063 42.5371C34.9594 42.2949 34.934 42.0469 34.9301 41.793L34.9184 40.9902C34.9105 40.4395 34.809 40.0723 34.6137 39.8887C34.4223 39.7051 34.0609 39.6133 33.5297 39.6133H31.6664V43H29.9027V34.3633H34.1391ZM46.1203 35.8926H41.55V37.7266H45.7453V39.2266H41.55V41.4473H46.3312V43H39.7863V34.3633H46.1203V35.8926Z" fill="#253645" />
                    <path d="M14.5 13.5V22.5C14.5 23.0967 14.7371 23.669 15.159 24.091C15.581 24.5129 16.1533 24.75 16.75 24.75H30.25C30.8467 24.75 31.419 24.5129 31.841 24.091C32.2629 23.669 32.5 23.0967 32.5 22.5V13.5" stroke="#253645" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                    <path d="M28 6.75L23.5 2.25L19 6.75" stroke="#253645" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                    <path d="M23.5 2.25V16.875" stroke="#253645" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                    <rect x="10" width="27" height="27" />
                  </svg>
                </div>
                <div className="col social_view pt-1">
                  <a data-toggle='tooltip' data-placement='top' title='Share on WhatsApp' className='social-share wattsap' target='_blank' data-action='share/whatsapp/share'>
                    <svg width="22" height="22" viewBox="0 0 22 22" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                      <path d="M11.0027 0H10.9973C4.93213 0 0 4.72563 0 10.5365C0 12.8414 0.7755 14.9777 2.09413 16.7123L0.72325 20.6266L4.95138 19.3319C6.69075 20.4356 8.76562 21.0731 11.0027 21.0731C17.0679 21.0731 22 16.3461 22 10.5365C22 4.72695 17.0679 0 11.0027 0ZM17.4034 14.8789C17.138 15.5967 16.0847 16.192 15.2446 16.3659C14.6699 16.4831 13.9191 16.5766 11.3919 15.573C8.15925 14.2902 6.0775 11.1437 5.91525 10.9396C5.75988 10.7354 4.609 9.27346 4.609 7.76147C4.609 6.24948 5.41062 5.51324 5.73375 5.19714C5.99913 4.93768 6.43775 4.81915 6.8585 4.81915C6.99463 4.81915 7.117 4.82573 7.227 4.831C7.55013 4.84417 7.71237 4.86261 7.9255 5.35124C8.19088 5.96368 8.83712 7.47567 8.91412 7.63108C8.9925 7.7865 9.07088 7.99723 8.96088 8.20137C8.85775 8.4121 8.767 8.50561 8.60475 8.68473C8.4425 8.86386 8.2885 9.00083 8.12625 9.19312C7.97775 9.36039 7.81 9.53951 7.997 9.84902C8.184 10.1519 8.83025 11.1621 9.78175 11.9734C11.0096 13.0205 12.0051 13.3551 12.3612 13.4973C12.6266 13.6027 12.9429 13.5776 13.1368 13.3801C13.3829 13.1259 13.6867 12.7044 13.9961 12.2895C14.2161 11.9919 14.4939 11.955 14.7854 12.0604C15.0824 12.1592 16.654 12.9033 16.9771 13.0574C17.3003 13.2128 17.5134 13.2866 17.5917 13.417C17.6687 13.5473 17.6687 14.1598 17.4034 14.8789Z" fill="#253645" />
                    </svg>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" title="Share on Facebook" className='social-share facebook' href="">
                    <svg width="22" height="22" viewBox="0 0 22 22" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                      <g clipPath="url(#clip0)">
                        <path d="M11 0C4.92525 0 0 4.36739 0 9.75551C0 12.8256 1.59913 15.5638 4.09888 17.3523V21.0731L7.84437 19.104C8.844 19.3688 9.90275 19.5123 11 19.5123C17.0748 19.5123 22 15.1449 22 9.75683C22 4.36871 17.0748 0 11 0ZM12.0931 13.1377L9.29225 10.2758L3.82662 13.1377L9.8395 7.02391L12.7091 9.8859L18.106 7.02391L12.0931 13.1377Z" fill="#253645" />
                      </g>
                      <defs>
                        <clipPath id="clip0">
                          <rect width="22" height="21.0731" fill="#253645" />
                        </clipPath>
                      </defs>
                    </svg>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" title="Copy URL to clipboard" className='social-share copy' href="">
                    <svg width="25" height="25" viewBox="0 0 25 25" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                      <path d="M20.3982 9.82129H11.7444C10.6823 9.82129 9.82129 10.6823 9.82129 11.7444V20.3982C9.82129 21.4603 10.6823 22.3213 11.7444 22.3213H20.3982C21.4603 22.3213 22.3213 21.4603 22.3213 20.3982V11.7444C22.3213 10.6823 21.4603 9.82129 20.3982 9.82129Z" stroke="#253645" strokeWidth="2" sstrokeinecap="round" strokeLinejoin="round" />
                      <path d="M5.56333 15.1787H4.60179C4.09176 15.1787 3.60261 14.9761 3.24197 14.6155C2.88132 14.2548 2.67871 13.7657 2.67871 13.2556V4.60179C2.67871 4.09176 2.88132 3.60261 3.24197 3.24197C3.60261 2.88132 4.09176 2.67871 4.60179 2.67871H13.2556C13.7657 2.67871 14.2548 2.88132 14.6155 3.24197C14.9761 3.60261 15.1787 4.09176 15.1787 4.60179V5.56333" stroke="#253645" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" className='social-share email' title="Share via Email" href="">
                    <svg width="25" height="25" viewBox="0 0 25 25" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                      <path d="M22.8027 2.97852H2.19727C0.987207 2.97852 0 3.96465 0 5.17578V19.8242C0 21.0312 0.982813 22.0215 2.19727 22.0215H22.8027C24.0098 22.0215 25 21.0387 25 19.8242V5.17578C25 3.96875 24.0172 2.97852 22.8027 2.97852ZM22.4993 4.44336L12.5466 14.3961L2.50776 4.44336H22.4993ZM1.46484 19.5209V5.47212L8.51948 12.4663L1.46484 19.5209ZM2.50063 20.5566L9.55972 13.4976L12.0332 15.9498C12.3195 16.2337 12.7816 16.2328 13.0667 15.9476L15.4785 13.5358L22.4994 20.5566H2.50063ZM23.5352 19.5208L16.5143 12.5L23.5352 5.4791V19.5208Z" fill="#253645" />
                    </svg>
                  </a>

                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid mt-0 similar-stories">
          <div className="col-12 col-md-12">
            <h2 className="text-center">Dans la même rubrique</h2>
          </div>
          <div className="row latest-news mt-5">
            <div className="col-12 col-md-10 m-auto">
              <div className="row ">
                {allWpPost.map((post, i) => (
                  <div className="blogPost col-12 col-md-6 item mb-5" key={i}>
                    <div className="row ">
                      <div className="col-12 d-flex col-md-10 ml-auto mr-auto ">
                        <div className="row mt-auto mb-0 line-after">
                        <Link className="w-100" to={post.node.link}>
                          <div className="image-container">
                            {post.node.featuredImage !== null &&
                              // <Img fluid={post.node.featuredImage.node.localFile.childImageSharp.fluid} alt={post.node.featuredImage.node.altText} />
                              <img src={post.node.featuredImage.node.localFile.publicURL} alt={post.node.featuredImage.node.altText} />
                            }
                            <div className="image-overlay"></div>
                          </div>
                          </Link>
                        </div>
                        <div className="row">
                          <div className="absolute-excerpt col-11 ml-auto col-md-12 ml-auto text-center mr-auto">
                            <p className="category mt-3" dangerouslySetInnerHTML={{ __html: post.node.categories.nodes[0].name }} />
                            <Link to={post.node.link} ><h3 className="mt-3" dangerouslySetInnerHTML={{ __html: post.node.title }} /></Link>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}

              </div>
            </div>
          </div>
        </div>

      </Layout>
    )
  }
}

postTemplateFR.propTypes = {
  data: PropTypes.object.isRequired,
};

export default postTemplateFR;

export const pageQuery = graphql`
  query($id: String!) {
    post: wpPost(id: { eq: $id }) {
      title
      content
      uri
      author {
        node {
          name
        }
      }
      seo {
        title
        metaDesc
       
      }
      language {
        slug
      }
      translations {
        title
        link
      
      }
      linkedInShare {
        linkedinShareImage {
          mediaDetails{
            width
            height
          }
          altText
          localFile {
            publicURL
          }
        }
      }
      featuredImage {
        node {
          mediaDetails{
            width
            height
          }
          altText
          localFile { 
            childImageSharp{
              fluid{
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
            id
          }
        }
      }
      
      date(formatString: "DD, MMMM, YYYY", locale: "fr")
      categories {
        nodes{
          id
          name
        slug
        }
      }
      
    }
    allWpPost(filter: {categories: {nodes: {elemMatch: {name: {nin: ["FR Featured Priority 1", "FR Featured Priority 2", "FR Featured Priority 3", "FR Featured Priority 4"]}}}}, language: {slug: {eq: "fr"}}}) {
      totalCount
      edges {
        node {
          id
          title
          excerpt
          slug
          link
          categories {
            nodes {
              id
              name
            }
          }
          date(formatString: "DD, MMMM, YYYY", locale: "fr")
          featuredImage {
            node {
              altText
              localFile {
                publicURL 
                id
                childImageSharp{
                  fluid{
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
                publicURL
              }
            }
          }
        }
      }
    }
  }
`;