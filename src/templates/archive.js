/* eslint-disable react/no-danger */
import React from 'react';
import { Link, graphql } from 'gatsby';
import Helmet from "react-helmet"
import Img from "gatsby-image"
import Layout from '../components/layout';
// import Masthead from '../components/masthead'
import '../components/styles/blog.css';
import InsiderSVG from '../images/Insider-logo.png';
import BlogPagination from '../components/archive/BlogPagination';
import $ from "jquery"




const archiveTemplate = ({
  data: { allWpPost, blogPage },
  pageContext: {
    catName,
    catSlug,
    humanPageNumber,
    numberOfPages,
    description,
  },
}) => {
  function handleClick() {  
    var x = document.querySelector(".dropdown-menu");
    if (x.style.display === "none") {
      $('.dropdown').addClass('show');
      $('.dropdown-menu').show();
      $('.dropdown-menu').addClass('show');
    } else {
      $('.dropdown').removeClass('show');
      $('.dropdown-menu').hide();
      $('.dropdown-menu').removeClass('show');
    }
  }

  // console.log(allWpPost)
  return (

    <Layout polylang_translations={blogPage.translations} currentLang={blogPage.language.slug}  >
      

      <div id="content">
        <div className="container-fluid">
          <div className="container breacrumb-trail-container d-none d-md-block absolute archive">
            <div className="row">
              <div className="col">
                <ul id="breadcrumbs">
                  <li><a href="/">Home</a></li>
                  <li className="separator"> - </li>
                  <li><a href="/magazine">Magazine</a></li>
                  <li className="separator"> - </li>
                  <li><u>Category: {catName}</u></li>
                </ul>
              </div>
            </div>
          </div>

          <div className="row magazine-header mb-4">
            <div className="col-12 col-md-6  mx-auto my-5">
              <Link to="/magazine">
                <img className="blog-logo" src={InsiderSVG} style={{ maxWidth: "100%", height: "auto" }} />
              </Link>
              <div className="col-12 mr-auto d-flex align-items-center text-md-left text-center">
                <p className='text-center col'>The magazine of Glion Institute<br class="d-md-none d-block" /> of Higher Education</p>
              </div>
            </div>

          </div>

        </div>

        <div class="magazine-header-section">
          <div className="container">
            <div className="row magazine-header d-md-flex d-none">
              <div className="col-12 p-0">
                <ul className="mag_filter mb-0">
                  <li><a className={catName === 'LEADERSHIP INSIGHTS' ? 'active' : ''} href="/magazine/category/leadership-insights/">Leadership insights</a></li>
                  <li><a className={catName === 'HOSPITALITY UNCOVERED' ? 'active' : ''} href="/magazine/category/hospitality-uncovered/">hOSPITALITY UNCOVERED</a></li>
                  <li><a className={catName === 'BUSINESS OF LUXURY' ? 'active' : ''} href="/magazine/category/business-of-luxury/">Business of luxury</a></li>
                  <li><a className={catName === 'LIVING WELL' ? 'active' : ''} href="/magazine/category/living-well/">LIVING WELL</a></li>
                  <li><a className={catName === 'GLION SPIRIT' ? 'active' : ''} href="/magazine/category/glion-spirit/">GLION SPIRIT</a></li>
                </ul>
              </div>
            </div>

            <div class="row magazine-header-mob d-md-none d-flex justify-content-center ">
              <div class="col-12">
                <p class="text-center">FILTER:</p>
              </div>
              <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" onClick={handleClick}>
                  {catName}
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item " href="/magazine/">All Posts</a>
                  <a class={`dropdown-item ${catName === 'LEADERSHIP INSIGHTS' ? 'active' : ''}`} href="/magazine/category/leadership-insights/">Leadership Insights</a>
                  <a class={`dropdown-item ${catName === 'HOSPITALITY UNCOVERED' ? 'active' : ''}`} href="/magazine/category/hospitality-uncovered/">Hospitality Uncovered</a>
                  <a class={`dropdown-item ${catName === 'BUSINESS OF LUXURY' ? 'active' : ''}`} href="/magazine/category/business-of-luxury/">Business of Luxury</a>
                  <a class={`dropdown-item ${catName === 'LIVING WELL' ? 'active' : ''}`} href="/magazine/category/living-well/">Living Well</a>
                  <a class={`dropdown-item ${catName === 'GLION SPIRIT' ? 'active' : ''}`} href="/magazine/category/glion-spirit/">Glion Spirit</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container category-title mt-5 pt-4">
          <div className="row magazine-header mb-5">
            <div className="col-12 col-md-8 mb-5 m-auto">
              <h2 className="text-center mx-auto pb-0" style={{borderBottom: '3px solid #9E785B', width: 'max-content'}} dangerouslySetInnerHTML={{ __html: catName }} />
              {/* <div className="taxonomy-description text-center" dangerouslySetInnerHTML={{ __html: description }}></div> */}
            </div>
          </div>
        </div>
        <div className="container mt-0 archive-news overflow-hidden">
          <div className="row latest-news">
            <div className="col-12 col-md-12 m-auto">
              <div className="row ">
                {allWpPost.edges.map(post => (
                  <div className="blogPost col-12 col-md-6 item mb-5">
                    <div className="row ">
                      <div className="col-12 d-flex col-md-10 ml-auto mr-auto ">
                        <div className="row mt-auto mb-0 line-after archive-post">
                          <Link className="w-100" to={post.node.link} >
                            <div className="image-container">
                              {/* <Img fluid={post.node.featuredImage.node.localFile.childImageSharp.fluid} alt={post.node.featuredImage.node.altText} /> */}
                              <img src={post.node.featuredImage.node.localFile.publicURL} alt={post.node.featuredImage.node.altText} />
                              <div className="image-overlay"></div>
                            </div>
                          </Link>
                        </div>
                        <div className="row">
                          <div className="absolute-excerpt col-12 ml-md-auto text-center">
                            <p className="category mt-3" dangerouslySetInnerHTML={{ __html: catName }} />
                            <p class="post-date text-capitalize d-md-none d-block"><span>{post.node.date.replace(/,/g, "")}</span></p>
                            <Link to={post.node.link} ><h3 className="mt-3" dangerouslySetInnerHTML={{ __html: post.node.title }} /></Link>
                            <p class="post-date text-capitalize d-md-block d-none"><span>{post.node.date.replace(/,/g, "")}</span></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
                <BlogPagination
                  catSlug={catSlug}
                  page={humanPageNumber}
                  totalPages={numberOfPages}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
};

export default archiveTemplate;

export const pageQuery = graphql`
  query($catName: String!, $skip: Int!, $limit: Int!) {
    allWpPost(
      filter: {categories: {nodes: {elemMatch: {name:  { eq: $catName  } }}}} 
      skip: $skip
      limit: $limit
    ) {
      edges {
        node {
          id
          title
          excerpt
          slug
          link
          categories {
            nodes {
              id
              name
            }
          }
          date(formatString: "DD, MMM, YYYY")
          featuredImage {
            node {
              altText
              localFile {
                publicURL
                id
                childImageSharp{
                  fluid{
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
                publicURL
              }
            }
          }
        }
      }
      
    }
    blogPage: wpPage(id: { eq: "cG9zdDoxMQ==" }) {
      language {
        slug
      }
      translations {
        title
        link
      
      }
      seo {
        title
        metaDesc
      }
    }
   
  }
`;

