import React, { Component } from 'react'
import Img from "gatsby-image"
import { Link, graphql } from 'gatsby';
import Helmet from "react-helmet"
import SEO from '../components/seo';
import Layout from '../components/layout'

import '../components/styles/faculty.css';




class FacultyMembers extends Component {
  render() {
    const page = this.props.data.page
    const faculty = this.props.data.allWpFaculty
    //   const faculty = this.props.data.allWordpressAcfFaculty
    //   const media = this.props.data.allWpMediaItem.edges
    // console.log(faculty)

    // console.log(page)
    return (
      <Layout polylang_translations={page.translations} currentLang={page.language.slug}  >
        
        <SEO title={page.seo.title} lang={page.language.slug} description={page.seo.metaDesc} image={page.acfFacultyPage.facultyHero} uri={page.uri} author={page.author} />
        <div id="content">
          <div className="container-fluid">
            <div className="row campus hero">
              <div className="background_image ">
                <Img fluid={page.acfFacultyPage.facultyHero.localFile.childImageSharp.fluid} alt={page.acfFacultyPage.facultyHero.altText} />

              </div>
              <div className="scrollElementStart foreground_text col-md-9 col-11 m-auto">
                <h1 className="mb-3">{page.acfFacultyPage.facultyHeroTitle}</h1>
              </div>
            </div>
            <div className="wrapper row  pt-5">
              <div className=" col-12 col-md-9 m-auto" dangerouslySetInnerHTML={{ __html: page.acfFacultyPage.introText }}>

              </div>
            </div>
            <div className="row  my-5">
              <div className="form-selector m-auto">
                <form>
                  <div className="form-group">
                    <select id="facultyOption" className="form-control" name="facultyselect">
                      <option selected value="all">{page.language.slug === "en" ? "Select All" : "Tout Sélectionner"}</option>

                      <option value="financeFaculty">{page.language.slug === "en" ? "Finance" : "Finances"}</option>
                      <option value="businessFaculty">{page.language.slug === "en" ? "Business and Management" : "Commerce et management"}</option>
                      <option value="marketingFaculty">{page.language.slug === "en" ? "Marketing, Event Management and Luxury" : "Marketing, Événementiel et Luxe"}</option>
                      <option value="entrepreneurshipFaculty">{page.language.slug === "en" ? "Entrepreneurship and Innovation" : "Entrepreneuriat et Innovation"}</option>
                      <option value="practicalFaculty">{page.language.slug === "en" ? "Practical Arts" : "Arts pratiques"}</option>
                      <option value="generalFaculty">{page.language.slug === "en" ? "General Education" : "Culture Générale"}</option>
                      <option value="hospitalityFaculty">{page.language.slug === "en" ? "Hospitality Management" : "Gestion des opérations hôtelières"}</option>
                    </select>
                  </div>
                </form>



              </div>
            </div>
            <div className="row faculty-loop">
              <div className="col-12 col-md-3 p-0 white-border blue-bg financeFaculty all">
                <div className="d-flex justify-content-center align-items-center h-100 w-100">
                  <h4 className="p-5 text-center">{page.language.slug === "en" ? "Finance" : "Finances"}</h4>
                </div>
              </div>
              {faculty.edges.map((item, i) => (

                (() => {
                  if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 88 : item.node.categories.nodes[0].databaseId === 383) {
                    return (


                      <div className="col-12 col-md-3 p-0 white-border financeFaculty blue-opacity all " key={i}>
                        <a data-toggle="modal" data-target={`#finance_Modal${i}`} href="#">
                          <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                          <div className="faculty-content">
                            <div dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyExcerpt }}></div>
                            <p className="learnClick">Learn more</p>
                          </div>
                        </a>
                      </div>

                    )
                  }

                }
                )()
              ))}
              <div className="col-12 col-md-3 p-0 white-border grey-bg businessFaculty all">
                <div className="d-flex justify-content-center align-items-center h-100 w-100">
                  <h4 className="p-5 text-center">{page.language.slug === "en" ? "Business and Management" : "Commerce et management"}</h4>
                </div>
              </div>
              {faculty.edges.map((item, i) => (

                (() => {
                  if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 90 : item.node.categories.nodes[0].databaseId === 391) {
                    return (


                      <div className="col-12 col-md-3 p-0 white-border businessFaculty blue-opacity all" key={i}>
                        <a data-toggle="modal" data-target={`#business_management${i}`} href="#">
                          <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                          <div className="faculty-content">
                            <div dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyExcerpt }}></div>
                            <p className="learnClick">Learn more</p>
                          </div>
                        </a>
                      </div>

                    )
                  }

                }
                )()
              ))}
              <div className="col-12 col-md-3 p-0 white-border beige-bg marketingFaculty all">
                <div className="d-flex justify-content-center align-items-center h-100 w-100">
                  <h4 className="p-5 text-center">{page.language.slug === "en" ? "Marketing, Event Management and Luxury" : "Marketing, Événementiel et Luxe"}</h4>
                </div>
              </div>
              {faculty.edges.map((item, i) => (

                (() => {
                  if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 92 : item.node.categories.nodes[0].databaseId === 393) {
                    return (


                      <div className="col-12 col-md-3 p-0 white-border marketingFaculty blue-opacity all " key={i}>
                        <a data-toggle="modal" data-target={`#marketing_Modal${i}`} href="#">
                          <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                          <div className="faculty-content">
                            <div dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyExcerpt }}></div>
                            <p className="learnClick">Learn more</p>
                          </div>
                        </a>
                      </div>

                    )
                  }

                }
                )()
              ))}
              <div className="col-12 col-md-3 p-0 white-border grey-bg entrepreneurshipFaculty all">
                <div className="d-flex justify-content-center align-items-center h-100 w-100">
                  <h4 className="p-5 text-center">{page.language.slug === "en" ? "Entrepreneurship and Innovation" : "Entrepreneuriat et Innovation"}</h4>
                </div>
              </div>
              {faculty.edges.map((item, i) => (

                (() => {
                  if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 94 : item.node.categories.nodes[0].databaseId === 389) {
                    return (


                      <div className="col-12 col-md-3 p-0 white-border entrepreneurshipFaculty blue-opacity all" key={i}>
                        <a data-toggle="modal" data-target={`#entrepreneur_Modal${i}`} href="#">
                          <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                          <div className="faculty-content">
                            <div dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyExcerpt }}></div>
                            <p className="learnClick">Learn more</p>
                          </div>
                        </a>
                      </div>

                    )
                  }

                }
                )()
              ))}
              <div className="col-12 col-md-3 p-0 white-border bronze-bg practicalFaculty all">
                <div className="d-flex justify-content-center align-items-center h-100 w-100">
                  <h4 className="p-5 text-center">{page.language.slug === "en" ? "Practical Arts" : "Arts pratiques"}</h4>
                </div>
              </div>
              {faculty.edges.map((item, i) => (

                (() => {
                  if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 96 : item.node.categories.nodes[0].databaseId === 395) {
                    return (


                      <div className="col-12 col-md-3 p-0 white-border practicalFaculty blue-opacity all" key={i}>
                        <a data-toggle="modal" data-target={`#practical_Modal${i}`} href="#">
                          <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                          <div className="faculty-content">
                            <div dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyExcerpt }}></div>
                            <p className="learnClick">Learn more</p>
                          </div>
                        </a>
                      </div>

                    )
                  }

                }
                )()
              ))}
              <div className="col-12 col-md-3 p-0 white-border blue-bg generalFaculty all">
                <div className="d-flex justify-content-center align-items-center h-100 w-100">
                  <h4 className="p-5 text-center">{page.language.slug === "en" ? "General Education" : "Culture Générale"}</h4>
                </div>
              </div>
              {faculty.edges.map((item, i) => (

                (() => {
                  if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 98 : item.node.categories.nodes[0].databaseId === 385) {
                    return (


                      <div className="col-12 col-md-3 p-0 white-border generalFaculty blue-opacity all" key={i}>
                        <a data-toggle="modal" data-target={`#general_Modal${i}`} href="#">
                          <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                          <div className="faculty-content">
                            <div dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyExcerpt }}></div>
                            <p className="learnClick">Learn more</p>
                          </div>
                        </a>
                      </div>

                    )
                  }

                }
                )()
              ))}
              <div className="col-12 col-md-3 p-0 white-border grey-bg   hospitalityFaculty all">
                <div className="d-flex justify-content-center align-items-center h-100 w-100">
                  <h4 className="p-5 text-center">{page.language.slug === "en" ? "Hospitality Management" : "Gestion des opérations hôtelières"}</h4>
                </div>
              </div>
              {faculty.edges.map((item, i) => (

                (() => {
                  if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 100 : item.node.categories.nodes[0].databaseId === 387) {
                    return (


                      <div className="col-12 col-md-3 p-0 white-border hospitalityFaculty blue-opacity all" key={i}>
                        <a data-toggle="modal" data-target={`#hostpitality_Modal${i}`} href="#">
                          <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                          <div className="faculty-content">
                            <div dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyExcerpt }}></div>
                            <p className="learnClick">Learn more</p>
                          </div>
                        </a>
                      </div>

                    )
                  }

                }
                )()
              ))}
            </div>

          </div>
          {faculty.edges.map((item, i) => (

            (() => {

              if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 88 : item.node.categories.nodes[0].databaseId === 383) {
                return (


                  <div className="container-fluid mt-0" key={i}>
                    <div className="row faculty-loop">
                      <div className="modal fade alumniModal" id={`finance_Modal${i}`} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                            </div>
                            <div className="modal-body">
                              <div id="alumni1" className="left_img_right_text alumni_profile">
                                <div className="row m-0">
                                  <div className={`col-md-6 col-12  p-0 d-flex justify-content-center align-items-center beige-bg ${i === 0 && ' order-1 order-md-2 beige-bg'} ${i === 2 && ' order-1 order-md-2 beige-bg'}`}>
                                    <div className=" col-9 m-auto text-center text-md-left" dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyContent }}></div>

                                  </div>
                                  <div className="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center facImage">

                                    <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                )
              }

            }
            )()
          ))}
          {faculty.edges.map((item, i) => (

            (() => {
              if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 90 : item.node.categories.nodes[0].databaseId === 391) {
                return (


                  <div className="container-fluid mt-0" key={i}>
                    <div className="row faculty-loop">
                      <div className="modal fade alumniModal" id={`business_management${i}`} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                            </div>
                            <div className="modal-body">
                              <div id="alumni1" className="left_img_right_text alumni_profile">
                                <div className="row m-0">
                                  <div className={`col-md-6 col-12  p-0 d-flex justify-content-center align-items-center beige-bg ${i === 0 || i === 2 && ' order-1 order-md-2 beige-bg'}`}>
                                    <div className=" col-9 m-auto text-center text-md-left" dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyContent }}></div>

                                  </div>
                                  <div className="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center facImage">

                                    <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                )
              }

            }
            )()
          ))}
          {faculty.edges.map((item, i) => (

            (() => {
              if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 92 : item.node.categories.nodes[0].databaseId === 393) {
                return (


                  <div className="container-fluid mt-0" key={i}>
                    <div className="row faculty-loop">
                      <div className="modal fade alumniModal" id={`marketing_Modal${i}`} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                            </div>
                            <div className="modal-body">
                              <div id="alumni1" className="left_img_right_text alumni_profile">
                                <div className="row m-0">
                                  <div className={`col-md-6 col-12  p-0 d-flex justify-content-center align-items-center beige-bg ${i === 0 || i === 2 && ' order-1 order-md-2 beige-bg'}`}>
                                    <div className=" col-9 m-auto text-center text-md-left" dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyContent }}></div>

                                  </div>
                                  <div className="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center facImage">

                                    <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                )
              }

            }
            )()
          ))}
          {faculty.edges.map((item, i) => (

            (() => {
              if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 94 : item.node.categories.nodes[0].databaseId === 389) {
                return (


                  <div className="container-fluid mt-0" key={i}>
                    <div className="row faculty-loop">
                      <div className="modal fade alumniModal" id={`entrepreneur_Modal${i}`} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                            </div>
                            <div className="modal-body">
                              <div id="alumni1" className="left_img_right_text alumni_profile">
                                <div className="row m-0">
                                  <div className={`col-md-6 col-12  p-0 d-flex justify-content-center align-items-center beige-bg${i === 0 || i === 2 && ' order-1 order-md-2 beige-bg'}`}>
                                    <div className=" col-9 m-auto text-center text-md-left" dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyContent }}></div>

                                  </div>
                                  <div className="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center facImage">

                                    <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                )
              }

            }
            )()
          ))}
          {faculty.edges.map((item, i) => (

            (() => {
              if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 96 : item.node.categories.nodes[0].databaseId === 395) {
                return (


                  <div className="container-fluid mt-0" key={i}>
                    <div className="row faculty-loop">
                      <div className="modal fade alumniModal" id={`practical_Modal${i}`} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                            </div>
                            <div className="modal-body">
                              <div id="alumni1" className="left_img_right_text alumni_profile">
                                <div className="row m-0">
                                  <div className={`col-md-6 col-12  p-0 d-flex justify-content-center align-items-center beige-bg ${i === 0 || i === 2 && ' order-1 order-md-2 beige-bg'}`}>
                                    <div className=" col-9 m-auto text-center text-md-left" dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyContent }}></div>

                                  </div>
                                  <div className="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center facImage">

                                    <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                )
              }

            }
            )()
          ))}
          {faculty.edges.map((item, i) => (

            (() => {
              if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 98 : item.node.categories.nodes[0].databaseId === 385) {
                return (


                  <div className="container-fluid mt-0" key={i}>
                    <div className="row faculty-loop">
                      <div className="modal fade alumniModal" id={`general_Modal${i}`} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                            </div>
                            <div className="modal-body">
                              <div id="alumni1" className="left_img_right_text alumni_profile">
                                <div className="row m-0">
                                  <div className={`col-md-6 col-12  p-0 d-flex justify-content-center align-items-center beige-bg ${i === 0 || i === 2 && ' order-1 order-md-2 beige-bg'}`}>
                                    <div className=" col-9 m-auto text-center text-md-left" dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyContent }}></div>

                                  </div>
                                  <div className="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center facImage">

                                    <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                )
              }

            }
            )()
          ))}
          {faculty.edges.map((item, i) => (

            (() => {
              if (page.language.slug === "en" ? item.node.categories.nodes[0].databaseId === 100 : item.node.categories.nodes[0].databaseId === 387) {
                return (


                  <div className="container-fluid mt-0" key={i}>
                    <div className="row faculty-loop">
                      <div className="modal fade alumniModal" id={`hostpitality_Modal${i}`} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                            </div>
                            <div className="modal-body">
                              <div id="alumni1" className="left_img_right_text alumni_profile">
                                <div className="row m-0">
                                  <div className={`col-md-6 col-12  p-0 d-flex justify-content-center align-items-center beige-bg ${i === 0 || i === 2 && ' order-1 order-md-2 beige-bg'}`}>
                                    <div className=" col-9 m-auto text-center text-md-left" dangerouslySetInnerHTML={{ __html: item.node.acfFaculty.facultyContent }}></div>

                                  </div>
                                  <div className="col-md-6 col-12  p-0 d-md-flex  justify-content-center align-items-center facImage">

                                    <Img fluid={item.node.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.node.acfFaculty.facultyImage.altText} />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                )
              }

            }
            )()
          ))}


        </div>
      </Layout >
    )
  }
}



export default FacultyMembers;


export const pageQuery = graphql`
    
    query($id: String!) {
          page:  wpPage(id: {eq: $id }) {
          title
        content
        isFrontPage
        language {
          slug
        }
        translations {
          title
          link
      }
      uri
      author {
        node {
          name
        }
      }
      seo {
        title
        metaDesc
        
      }
        acfFacultyPage {
     
          facultyHeroTitle
          introText
          facultyHero {
            mediaDetails{
              width
              height
            }
            sourceUrl
          altText
            localFile {
              publicURL
          childImageSharp {
            
          fluid {
          ...GatsbyImageSharpFluid_withWebp
        }
        }
      }
    }
  }
}
      allWpFaculty {
          edges {
          node {
          id
            acfFaculty {
          facultyContent
              facultyExcerpt
              facultyImage {
          altText
                localFile {
          childImageSharp {
          fluid {
          ...GatsbyImageSharpFluid_withWebp
        }
        }
      }
    }
  }
            categories {
          nodes {
          databaseId
        }
        }
      }
    }
  }
}
`