import React, { Component } from 'react'
import { graphql } from 'gatsby';
import SEO from '../components/seo';
import Helmet from "react-helmet"
import Layout from '../components/layout'
import ProgramOverview from '../components/ProgramOverview'
import ProgramShapes from '../components/ProgramShapes'
import ProgramStructure from '../components/ProgramStructure'
import ProgramShapesMobile from '../components/ProgramShapesMobile'
import ProgramStructureMobile from '../components/ProgramStructureMobile'
import ProgramStats from '../components/ProgramStats'
import Hero from '../components/Hero'
import TextLeftImage from '../components/TextLeftImage'
import ProgramFaculty from '../components/ProgramFaculty'
import BrochureDownload from '../components/BrochureDownload'
import CampusLocations from '../components/CampusLocations'
import Accordion from '../components/Accordion'
import { Img, Link } from "gatsby-image"



class ProgramPage extends Component {


  render() {

    const currentPage = this.props.data
    const faculty = currentPage.page.acfProgramFields.facultySelector
    console.log(faculty);
    // console.log(faculty)
    const media = this.props.data.allWpMediaItem.edges

    const allfile = this.props.data.allFile.edges
    // console.log(allfile)
    // const wattsapShare = "https://api.whatsapp.com/send?text=" + "https://master.d3jhca9ge8o60k.amplifyapp.com/" + currentPage.page.link

    var parentId = currentPage.page.parentId
    const allWpPage = this.props.data.allWpPage
    // console.log(parentId)
    var parent = null

    if (parentId !== null) {


      var parentObject = allWpPage.edges.filter(
        ({ node }) =>
          node.id === parentId
      );

      var parent = parentObject[0].node
      // console.log(parent)
    }



    // const translink = this.props.data.translink

    // const currentLang = this.props.data.page.lang
    // const polylang_translations = this.props.data.page.polylang_translations

    // console.log(currentPage.parent)

    // let transID = ''
    // if ( currentLang === 'en') {
    //   transID = polylang_translations[0].translations.fr
    // } else {
    //   transID = polylang_translations[0].translations.en
    // }
    // console.log(currentPage.page.link)

    return (

      <Layout polylang_translations={currentPage.page.translations} currentLang={currentPage.page.language.slug} frontPage={currentPage.page.isFrontPage} >
      
        <SEO title={currentPage.page.seo.title} lang={currentPage.page.language.slug} description={currentPage.page.seo.metaDesc} image={currentPage.page.acfProgramFields.programHeaderImage} uri={currentPage.page.uri} author={currentPage.author} />
        <div id="content" className="program_page_template">
          <div className="container-fluid">


            <ProgramOverview

              lang={currentPage.page.language.slug}
              title={currentPage.page.title}
              program_overview={currentPage.page.acfProgramFields.programOverview}
              next_intake={currentPage.page.acfProgramFields.nextIntake}
              button_1={currentPage.page.acfProgramFields.programOverviewButton1}
              button_2={currentPage.page.acfProgramFields.programOverviewButton2}
              program_header_image={currentPage.page.acfProgramFields.programHeaderImage}
              parent={parent}
              lavender_background={currentPage.page.acfProgramFields.lavenderBackground}
              currentLang={currentPage.page.language.slug}
              show_remote_learning={currentPage.page.acfProgramFields.showRemoteLearning}
              remote_learning_text={currentPage.page.acfProgramFields.remoteLearningText}
              learn_more_button_text={currentPage.page.acfProgramFields.learnMoreButtonText}
            />


            <div className="wrapper row white-strip mt-3 mb-5 justify-content-center align-items-center">
              <div className="scrollElement col-10 col-md-7 p-5 m-auto text-center">
                <h3 className="mb-5" dangerouslySetInnerHTML={{ __html: currentPage.page.acfProgramFields.introTitle }} />
                <p className="mb-5" dangerouslySetInnerHTML={{ __html: currentPage.page.acfProgramFields.introText }} />
                <div className="keypoints mt-5" dangerouslySetInnerHTML={{ __html: currentPage.page.acfProgramFields.keypoints }} />
              </div>
            </div>

            <div className="d-none d-md-block">

              {currentPage.page.acfProgramFields.semesterShapes.semesterShapesOptions === "Option 1" &&
                <ProgramShapes
                  semester_shapes_options={currentPage.page.acfProgramFields.semesterShapes.semesterShapesOptions}
                  option_1_block_1={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block1}
                  option_1_block_2={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block2}
                  option_1_block_3={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block3}
                  option_1_block_4={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block4}
                  option_1_block_5={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block5}
                  option_1_block_6={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block6}
                  option_1_block_7={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block7}
                  option_1_block_8={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block8}
                  asterisk_content_1={currentPage.page.acfProgramFields.semesterShapes.asteriskContent1}
                />
              }

              {currentPage.page.acfProgramFields.semesterShapes.semesterShapesOptions === 'Option 2' &&
                <ProgramShapes
                  semester_shapes_options={'Option 2'}
                  option_2_block_1={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block1}
                  option_2_block_2={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block2}
                  option_2_block_3={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block3}
                  option_2_block_4={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block4}
                  option_2_block_5={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block5}
                  asterisk_content_2={currentPage.page.acfProgramFields.semesterShapes.asteriskContent2}
                />
              }

              <ProgramStructure
                tab_1_content={currentPage.page.acfProgramFields.tab1Content}
                tab_2_content={currentPage.page.acfProgramFields.tab2Content}
                tab_3_content={currentPage.page.acfProgramFields.tab3Content}
                tab_4_content={currentPage.page.acfProgramFields.tab4Content}
                tab_5_content={currentPage.page.acfProgramFields.tab5Content}
                tab_fees_content_1={currentPage.page.acfProgramFields.tabFeesContent1}
                tab_fees_content_2={currentPage.page.acfProgramFields.tabFeesContent22}
                media={media}
                allfile={allfile}
                lavender_background={currentPage.page.acfProgramFields.lavender_background}
                lang={currentPage.page.language.slug}
              />

            </div>

            <div className="d-block d-md-none  program-structure">
              <div className="row">
                {currentPage.page.language.slug === "en" ? <h3 className="col text-center">Program Structure</h3> : <h3 className="col text-center">Structure  du programme</h3>}
              </div>
              <div className="row">
                {currentPage.page.acfProgramFields.semesterShapes.semesterShapesOptions === 'Option 1' &&
                  <ProgramShapesMobile
                    semester_shapes_options={'Option 1'}
                    option_1_block_1={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block1}
                    option_1_block_2={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block2}
                    option_1_block_3={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block3}
                    option_1_block_4={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block4}
                    option_1_block_5={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block5}
                    option_1_block_6={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block6}
                    option_1_block_7={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block7}
                    option_1_block_8={currentPage.page.acfProgramFields.semesterShapes.shapesOption1.option1Block8}
                    asterisk_content_1={currentPage.page.acfProgramFields.semesterShapes.asteriskContent1}
                    lang={currentPage.page.language.slug}
                  />
                }

                {currentPage.page.acfProgramFields.semesterShapes.semesterShapesOptions === 'Option 2' &&
                  <ProgramShapesMobile
                    semester_shapes_options={'Option 2'}
                    option_2_block_1={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block1}
                    option_2_block_2={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block2}
                    option_2_block_3={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block3}
                    option_2_block_4={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block4}
                    option_2_block_5={currentPage.page.acfProgramFields.semesterShapes.shapesOption2.option2Block5}
                    asterisk_content_2={currentPage.page.acfProgramFields.semesterShapes.asteriskContent2}
                    lang={currentPage.page.language.slug}
                  />
                }

                <ProgramStructureMobile
                  tab_1_content={currentPage.page.acfProgramFields.tab1Content}
                  tab_2_content={currentPage.page.acfProgramFields.tab2Content}
                  tab_3_content={currentPage.page.acfProgramFields.tab3Content}
                  tab_4_content={currentPage.page.acfProgramFields.tab4Content}
                  tab_5_content={currentPage.page.acfProgramFields.tab5Content}
                  tab_fees_content_1={currentPage.page.acfProgramFields.tabFeesContent1}
                  tab_fees_content_2={currentPage.page.acfProgramFields.tabFeesContent22}
                  media={media}
                  allfile={allfile}
                  lavender_background={currentPage.page.acfProgramFields.lavenderBackground}
                  lang={currentPage.page.language.slug}

                />
              </div>
            </div>
            {currentPage.page.acfProgramFields.leftRightText != null &&
              <div className={`row wrapper  ${currentPage.page.acfProgramFields.hideLeftRightSection === true && 'd-none'}`}>
                <div id="remote_learning_option" className="col-12 bronze-bg left_text_right_img">
                  <div className="row ">
                    <div className="col-md-6 pt-5 pb-3 py-md-0 col-12 d-flex justify-content-center align-items-center  order-md-1 order-2">
                      <div className="scrollElement col-xl-9 col-lg-11 m-auto">
                        <h2>{currentPage.page.acfProgramFields.leftRightTitle}</h2>
                        <span dangerouslySetInnerHTML={{ __html: currentPage.page.acfProgramFields.leftRightText }} />
                      </div>
                    </div>
                    <div className="col-md-6 col-12 p-0  order-md-2 order-1" >
                      <img style={{ maxWidth: "100%", minWidth: "100%" }} src={currentPage.page.acfProgramFields.leftRightImage.localFile.childImageSharp.fluid.src} alt={currentPage.page.acfProgramFields.leftRightImage.altText} />
                    </div>
                  </div>
                </div>
              </div>
            }

            {currentPage.page.acfProgramFields.leftRightText2 != null &&
              <div className={`row wrapper  ${currentPage.page.acfProgramFields.hideLeftRightSection2 === true && 'd-none'}`}>
                <div className="col-12 blue-bg left_text_right_img">
                  <div className="row ">

                    <div className="col-md-6 col-12 p-0  " >

                      <img style={{ maxWidth: "100%", minWidth: "100%" }} src={currentPage.page.acfProgramFields.leftRightImage2.localFile.childImageSharp.fluid.src} alt={currentPage.page.acfProgramFields.leftRightImage2.altText} />


                    </div>
                    <div className="col-md-6 pt-5 pb-3 py-md-0 col-12 d-flex justify-content-center align-items-center ">
                      <div className="scrollElement col-xl-9 col-lg-11 m-auto">
                        <h2>{currentPage.page.acfProgramFields.leftRightTitle2}</h2>
                        <span dangerouslySetInnerHTML={{ __html: currentPage.page.acfProgramFields.leftRightText2 }} />

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }


            <Hero hero_image={currentPage.page.acfProgramFields.backgroundImage} hero_title={currentPage.page.acfProgramFields.overlayText} line_after="yes"  ></Hero>

            <ProgramStats
              stats_title={currentPage.page.acfProgramFields.overlayText}
              stat_1_intro={currentPage.page.acfProgramFields.stat1Intro}
              stat_1_figure={currentPage.page.acfProgramFields.stat1Figure}
              stat_1_figure_after={currentPage.page.acfProgramFields.stat1FigureAfter}
              stat_1_close_text={currentPage.page.acfProgramFields.stat1CloseText}
              stat_2_intro={currentPage.page.acfProgramFields.stat2Intro}
              stat_2_figure={currentPage.page.acfProgramFields.stat2Figure}
              stat_2_figure_after={currentPage.page.acfProgramFields.stat2FigureAfter}
              stat_2_close_text={currentPage.page.acfProgramFields.stat2CloseText}
              stat_3_intro={currentPage.page.acfProgramFields.stat3Intro}
              stat_3_figure={currentPage.page.acfProgramFields.stat3Figure}
              stat_3_figure_after={currentPage.page.acfProgramFields.stat3FigureAfter}
              stat_3_close_text={currentPage.page.acfProgramFields.stat3CloseText}
              stat_4_intro={currentPage.page.acfProgramFields.stat4Intro}
              stat_4_figure={currentPage.page.acfProgramFields.stat4Figure}
              stat_4_figure_after={currentPage.page.acfProgramFields.stat4FigureAfter}
              stat_4_close_text={currentPage.page.acfProgramFields.stat4CloseText}
              stats_link={currentPage.page.acfProgramFields.programStatsLink}
            />

            <TextLeftImage


              main_quote={currentPage.page.acfProgramFields.mainQuote}
              quote_image_text={currentPage.page.acfProgramFields.quoteImageText}
              quote_image={currentPage.page.acfProgramFields.quoteImage}
              quote_link={currentPage.page.acfProgramFields.programQuoteLink}
              cite={currentPage.page.acfProgramFields.cite}
            />

            <ProgramFaculty
              faculty={faculty}
              facultyTitle={currentPage.page.acfProgramFields.facultyTitle}
              facultyText={currentPage.page.acfProgramFields.facultyText}
              currentLang={currentPage.page.language.slug}
            />

            <BrochureDownload

              formCode={currentPage.page.acfProgramFields.formCode}
              formId={currentPage.page.acfProgramFields.formId}
              lang={currentPage.page.language.slug}
            />



            <div className="row wrapper my-5" id="campusScrollInit" >
              <div className=" scrollElement col-12">
                <h2 className="text-center">{currentPage.page.language.slug === "en" ? "Campus Locations" : "Nos campus"}</h2>
              </div>
            </div>
            <CampusLocations
              main_campus_title={currentPage.campusPage.acfHome.mainCampusTitle}
              campus_1_image={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus1Image : currentPage.campusPageFR.acfHome.campus1Image}
              campus_1_title={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus1Title : currentPage.campusPageFR.acfHome.campus1Title}
              campus_1_location={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus1Location : currentPage.campusPageFR.acfHome.campus1Location}
              campus_1_link={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus1LinkHomee : currentPage.campusPageFR.acfHome.campus1LinkHomee}
              campus_2_image={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus2Image : currentPage.campusPageFR.acfHome.campus2Image}
              campus_2_title={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus2Title : currentPage.campusPageFR.acfHome.campus2Title}
              campus_2_location={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus2Location : currentPage.campusPageFR.acfHome.campus2Location}
              campus_2_link={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.campus2LinkHomee : currentPage.campusPageFR.acfHome.campus2LinkHomee}
              show_campuses={currentPage.page.language.slug === "en" ? currentPage.campusPage.acfHome.showCampuses : currentPage.campusPageFR.acfHome.showCampuses}
            />
            {currentPage.page.acfProgramFields.accordionRowTest != null && currentPage.page.acfProgramFields.hideFaqSection !== true &&
              <div className="row my-5">
                <div className="col-12">
                  <h2 className="text-center mt-5">{currentPage.page.acfProgramFields.frequentlyAskedTitle}</h2>
                </div>
              </div>
            }
            {currentPage.page.acfProgramFields.accordionRowTest != null && currentPage.page.acfProgramFields.hideFaqSection !== true &&
              <Accordion
                accordion_row={currentPage.page.acfProgramFields.accordionRowTest}
              />
             }
            {currentPage.page.acfProgramFields.viewProgramsTitle != null &&
              <div className="wrapper row my-5">
                <div className="scrollElement col-12  my-3">
                  <h2 className="text-center">{currentPage.page.acfProgramFields.viewProgramsTitle}</h2>
                  <p className="text-center" dangerouslySetInnerHTML={{ __html: currentPage.page.acfProgramFields.viewProgramsText }} />

                  <div className="col text-center m-auto">
                    <a className="my-5 btn btn-primary blue" href={currentPage.page.acfProgramFields.programsProgramsLink.url}>{currentPage.page.acfProgramFields.programsProgramsLink.title}</a>

                  </div>

                </div>
              </div>
            }
          </div>
        </div>
      </Layout>
    )
  }
}



export default ProgramPage;


export const pageQuery = graphql`
query($id: String!) {

    page:  wpPage(id: { eq: $id }) {
      title
      link
      content
      isFrontPage
      language {
        slug
      }
      uri
      author {
        node {
          name
        }
      }
      translations {
        title
        link
      
      }
      parentId
      seo {
        title
        metaDesc
       
      }
      acfProgramFields {
        programsProgramsLink {
          url
          title
        }
        remoteLearningText
        remoteLearningOverviewText
        showRemoteLearning
        learnMoreButtonText
        cite
        facultyText
        facultyTitle
        showOrHideFacultySection
        fieldGroupName
        formId
        formCode
        frequentlyAskedTitle
        hideFaqSection
        hideLeftRightSection
        hideLeftRightSection2
        introText
        introTitle
        keypoints
        lavenderBackground
        leftRightText
        leftRightTitle
        leftRightText2
        leftRightTitle2
        mainQuote
        nextIntake
        overlayText
        programOverview
        quoteImageText
        showCampuses
        stat1CloseText
        stat1Figure
        stat1FigureAfter
        stat1Intro
        stat2CloseText
        stat2Figure
        stat2FigureAfter
        stat2Intro
        stat3CloseText
        stat3Figure
        stat3FigureAfter
        stat3Intro
        stat4CloseText
        stat4Figure
        stat4FigureAfter
        stat4Intro
        statsTitle
        viewProgramsText
        viewProgramsTitle
        accordionRowTest {
          contentLeft
          contentRight
          fieldGroupName
          numberAccColumns
          title
        }
        backgroundImage {
          altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 3000, webpQuality: 100) {
                ...GatsbyImageSharpFluid
                
              }
            } 
            publicURL  
          }
        }
        quoteImage {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
        }
        facultySelector {
          ... on WpFaculty {
            acfFaculty {
              facultyContent
              facultyImage {
                altText
                localFile {
                  childImageSharp {
                    fluid {
                      ...GatsbyImageSharpFluid_withWebp
                      
                    }
                  } 
                  publicURL  
                }
              }
              facultyExcerpt
            }
          }
        }
        leftRightImage {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
        }
        leftRightImage2 {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
        }
      
        programHeaderImage {
          mediaDetails{
            width
            height
          }
          altText
          sourceUrl
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp       
              }
            } 
            publicURL  
        
          }
        }
        programOverviewButton1 {
          title
          url
        }
        programOverviewButton2 {
          title
          url
        }
        programQuoteLink {
          title
          url
        }
        programStatsLink {
          title
          url
        }
        programsProgramsLink {
          title
          url
        }
        semesterShapes { 
          asteriskContent1
          asteriskContent2
          semesterShapesOptions
          shapesOption1 {
            option1Block1 {
              colour
              signoff
              text
              title
            }
            option1Block2 {
              colour
              signoff
              text
              title
            }
            option1Block3 {
              colour
              signoff
              text
              title
            }
            option1Block4 {
              colour
              text
              signoff
              title
            }
            option1Block5 {
              colour
              signoff
              text
              title
            }
            option1Block6 {
              signoff
              text
              title
              colour
            }
            option1Block7 {
              colour
              signoff
              text
              title
            }
            option1Block8 {
              colour
              signoff
              text
              title
            }
          }
          shapesOption2 {
            option2Block1 {
              colour
              signoff
              text
              title
            }
            option2Block2 {
              colour
              isSplit
              signoff
              signoff1
              signoff2
              text
              text1
              text2
              title
              title1
              title2
            }
            option2Block3 {
              colour
              isSplit
              signoff
              signoff1
              signoff2
              text
              text1
              text2
              title
              title1
              title2
            }
            option2Block4 {
              colour
              isSplit
              signoff
              signoff1
              signoff2
              text
              text1
              text2
              title
              title1
              title2
            }
            option2Block5 {
              colour
              signoff
              text
              title
            }
          }
        }
        tab1Content {
          statFigure
          statFigure2
          statText
          statText2
          text
          title
        }
        tab2Content {
          statFigure
          statFigure2
          statText
          statText2
          text
          title
        }
        tab3Content {
          statFigure
          statFigure2
          statText
          statText2
          text
          title
        }
        tab4Content {
          statFigure
          statFigure2
          statText
          statText2
          text
          title
        }
        tab5Content {
          statFigure
          statFigure2
          statText
          statText2
          text
          title
        }
        tabFeesContent1 {
          tableRow1Column1Title
          tableRow1Column2Value
          tableRow1Intro
          tableRow1Items {
            fee
            product
          }
          tableRow1SignOff
          tableRow1SignOffTitle
          tableRow2Column1Title
          tableRow2Column2Value
          tableRow2Items {
            product
            fee
          }
          tableRow3Column2Value
          tableRow3SignOffTitle
        }
        tabFeesContent22 {
          tableRow111Items {
            fee
            product
          }
          tableRow12Items {
            fee
            product
          }
          tableRow1Column1Title
          tableRow1Column2Value
          tableRow1Intro
          tableRow1SignOffTitle
          tableRow1SignOffValue
          tableRow2Column1Title
          tableRow2Column2Value
          tableRow3Column2Value
          tableRow3SignOffTitle
        }
      }
    }
    allWpPage {
      edges {
        node {
          id
          title
          link
        }
      }
    }
    campusPage:  wpPage(id: { eq: "cG9zdDoyMw=="}) { 
      acfHome {
        mainCampusTitle 
        campus1Image{
          id
          altText
          localFile {
            id
            childImageSharp {
              fluid(maxWidth: 1366, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
                base64
                tracedSVG
                srcWebp
                srcSetWebp
                originalImg
                originalName
              }
            }
          }
        }
        campus1Title
        campus1Location
        campus1LinkHomee{
          target
          title
          url
        }
        campus2Image{
          id
          altText
          localFile {
            id
            childImageSharp {
              fluid(maxWidth: 1366, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
                base64
                tracedSVG
                srcWebp
                srcSetWebp
                originalImg
                originalName
              }
            }
          }
        }
        campus2Title
        campus2Location
        campus2LinkHomee{
          target
          title
          url
        }
       
      }
    }
    campusPageFR:  wpPage(id: { eq: "cG9zdDo0NTQ5"}) { 
      acfHome {
        mainCampusTitle 
        campus1Image{
          id
          altText
          localFile {
            id
            childImageSharp {
              fluid(maxWidth: 1366, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
                base64
                tracedSVG
                srcWebp
                srcSetWebp
                originalImg
                originalName
              }
            }
          }
        }
        campus1Title
        campus1Location
        campus1LinkHomee{
          target
          title
          url
        }
        campus2Image{
          id
          altText
          localFile {
            id
            childImageSharp {
              fluid(maxWidth: 1366, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
                base64
                tracedSVG
                srcWebp
                srcSetWebp
                originalImg
                originalName
              }
            }
          }
        }
        campus2Title
        campus2Location
        campus2LinkHomee{
          target
          title
          url
        }
       
      }
    }
    
  #  parent: wpPage(id: { eq: "cG9zdDo3" }) {
  #    title
  #    link
  #  }
  

    allWpMediaItem {
    edges {
      node {
        sourceUrl
        localFile {
            childImageSharp {
              fluid(maxWidth: 3040, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                
              }
              
            } 
            publicURL
             
          }
          title
        }
      }
    }
    allFile {
      edges {
        node {
          id
          relativePath
          publicURL
          name
        }
      }
    }
  }
   


  

`