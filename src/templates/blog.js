import React from 'react';
import { Link, graphql } from 'gatsby';
import Helmet from "react-helmet"
import Img from "gatsby-image"
import SEO from '../components/seo';
import Layout from '../components/layout';

// import Masthead from '../components/masthead'
import '../components/styles/blog.css';
import InsiderSVG from '../images/Insider-logo.png';

import BlogPagination from '../components/archive/BlogPagination';
import $ from "jquery"




const blogTemplate = ({
  data: {
    blogRoll,
    blogPage,
    featuredWpPost1,
    featuredWpPost2,
    featuredWpPost3,
    featuredWpPost4
  },
  pageContext: {

    humanPageNumber,
    numberOfPages,
    description,
  },
}) => {

  console.log(featuredWpPost1);

  // console.log(featuredWpPost2)


  const filteredPosts = blogRoll.edges.filter(
    ({ node: { categories } }) =>
      // categories.nodes.some(el => el.name === "BUSINESS OF LUXURY"  )
      // categories.nodes.some(el => el.name === "LIVING WELL"  )
      categories.nodes.some(el => el.name != "Featured Priority 1")
  );
  // console.log(filteredPosts)

  var featuredWpPost1DisplayCat = featuredWpPost1.categories.nodes.filter(function (e) {
    return e.name !== 'Featured Priority 1' && e.name !== 'Featured Priority 2' && e.name !== 'Featured Priority 3' && e.name !== 'Featured Priority 4'
  });

  // console.log(featuredWpPost1DisplayCat);

  var featuredWpPost2DisplayCat = featuredWpPost2.categories.nodes.filter(function (e) {
    return e.name !== 'Featured Priority 1' && e.name !== 'Featured Priority 2' && e.name !== 'Featured Priority 3' && e.name !== 'Featured Priority 4'
  });

  // console.log(featuredWpPost2DisplayCat);

  var featuredWpPost3DisplayCat = featuredWpPost3.categories.nodes.filter(function (e) {
    return e.name !== 'Featured Priority 1' && e.name !== 'Featured Priority 2' && e.name !== 'Featured Priority 3' && e.name !== 'Featured Priority 4'
  });

  // console.log(featuredWpPost3DisplayCat);

  var featuredWpPost4DisplayCat = featuredWpPost4.categories.nodes.filter(function (e) {
    return e.name !== 'Featured Priority 1' && e.name !== 'Featured Priority 2' && e.name !== 'Featured Priority 3' && e.name !== 'Featured Priority 4'
  });


  function handleClick() {  
    var x = document.querySelector(".dropdown-menu");
    if (x.style.display === "none") {
      $('.dropdown').addClass('show');
      $('.dropdown-menu').show();
      $('.dropdown-menu').addClass('show');
    } else {
      $('.dropdown').removeClass('show');
      $('.dropdown-menu').hide();
      $('.dropdown-menu').removeClass('show');
    }
  }

  // console.log(featuredWpPost4DisplayCat);
  return (

    <Layout polylang_translations={blogPage.translations} currentLang={blogPage.language.slug}  >
     
      <SEO title={blogPage.seo.title} description={blogPage.seo.metaDesc} image={featuredWpPost1.blogHeroUpdated.blogFeaturedHeroOverview} uri={blogPage.uri} author={blogPage.author} />


      <div id="content">
        <div className="container-fluid">
          <div className="row magazine-header mb-4">
            <div className="col-12 col-md-6 mx-auto">

              <img className="blog-logo" src={InsiderSVG} style={{ maxWidth: "100%", height: "auto" }} />
            </div>
            <div className="col-12 mr-auto d-flex align-items-center text-md-left text-center">
              <p className='text-center col'>The magazine of Glion Institute<br class="d-md-none d-block" /> of Higher Education</p>
            </div>
          </div>
        </div>

        <div class="magazine-header-section">
          <div className="container">
            <div className="row magazine-header d-md-flex d-none">
              <div className="col-12 p-0">
                <ul className="mag_filter mb-0">
                  <li><a href="/magazine/category/leadership-insights/">Leadership insights</a></li>
                  <li><a href="/magazine/category/hospitality-uncovered/">hOSPITALITY UNCOVERED</a></li>
                  <li><a href="/magazine/category/business-of-luxury/">Business of luxury</a></li>
                  <li><a href="/magazine/category/living-well/">LIVING WELL</a></li>
                  <li><a href="/magazine/category/glion-spirit/">GLION SPIRIT</a></li>
                </ul>
              </div>
            </div>

            <div class="row magazine-header-mob d-md-none d-flex justify-content-center ">
              <div class="col-12">
                <p class="text-center">FILTER:</p>
              </div>
              <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" onClick={handleClick}>
                  All Posts
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item active" href="/magazine/">All Posts</a>
                  <a class="dropdown-item" href="/magazine/category/leadership-insights/">Leadership Insights</a>
                  <a class="dropdown-item" href="/magazine/category/hospitality-uncovered/">Hospitality Uncovered</a>
                  <a class="dropdown-item" href="/magazine/category/business-of-luxury/">Business of Luxury</a>
                  <a class="dropdown-item" href="/magazine/category/living-well/">Living Well</a>
                  <a class="dropdown-item" href="/magazine/category/glion-spirit/">Glion Spirit</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container editors-pick mt-5 pt-4 overflow-hidden">
          <div className="row ">
            <div className="col-12">
              <h1 className="text-center mb-5 mx-auto" style={{borderBottom: '3px solid #9E785B', width: 'max-content'}}>Editor's Pick</h1>
            </div>
          </div>

          <div className="row line-after main-featured-image">
            <div className="blogPost col-12 p-0 d-flex justify-content-center">
              <Link className="w-100" to={featuredWpPost1.link}>
                <div className="image_container">
                  <Img fluid={featuredWpPost1.blogHeroUpdated.blogFeaturedHeroOverview.localFile.childImageSharp.fluid} alt={featuredWpPost1.blogHeroUpdated.blogFeaturedHeroOverview.localFile.altText} />
                  <div className="image-overlay"></div>
                </div>
              </Link>
            </div>
          </div>

          <div className="row main-featured-post line-after">
            <div className="col-12 mb-5 pb-4">
             <span className='mag_span text-center mb-0' >
                <a className="category-editors" href={"/magazine/category/" + featuredWpPost1.categories.nodes[1].slug + "/"}>{featuredWpPost1.categories.nodes[1].name}</a>
                {/* <a href="/magazine/history-revisited-how-georges-kern-is-transforming-breitling/">{featuredWpPost1DisplayCat[0].name}</a> */}
              </span>
              <p class="post-date text-capitalize d-md-none d-block"><span>{featuredWpPost1.date.replace(/,/g, "")}</span></p>
              <h3 className="text-center mb-2">
                <Link to={featuredWpPost1.link} >{featuredWpPost1.title}</Link>
              </h3>
              <p class="post-date text-capitalize d-md-block d-none"><span>{featuredWpPost1.date.replace(/,/g, "")}</span></p>
            </div>
          </div>


          <div className="d-block d-md-block">
            <div className="row magazine-header">
              <div className="col-12 col-md-6">
                <div className="row">
                  <div className="blogPost col-12 p-0 col-md-10 ml-auto mr-auto">
                  <Link to={featuredWpPost2.link}>
                    <div className="image-container">
                      <Img fluid={featuredWpPost2.featuredImage.node.localFile.childImageSharp.fluid} alt={featuredWpPost2.featuredImage.node.altText} />
                      <div className="image-overlay"></div>
                    </div>
                    </Link>
                  </div>
                </div>
                <div className="row middle-featured-images line-after">
                  <div className="col-12 col-md-11 m-auto">
                    <span className='mag_span text-center mb-0' >
                      <a className="category-editors" href={"/magazine/category/" + featuredWpPost2DisplayCat[0].slug + "/"}>{featuredWpPost2DisplayCat[0].name}</a>
                    </span>
                    <p class="post-date text-capitalize d-md-none d-block"><span>{featuredWpPost2.date.replace(/,/g, "")}</span></p>
                    <h3 className="text-center mb-2">
                      <Link to={featuredWpPost2.link} >{featuredWpPost2.title}</Link>
                    </h3>
                    <p class="post-date text-capitalize d-md-block d-none"><span>{featuredWpPost2.date.replace(/,/g, "")}</span></p>
                  </div>
                </div>
              </div>

              <div className="col-12 col-md-6">
                <div className="row magazine-header">
                  <div className="blogPost col-12 p-0 col-md-10 ml-auto mr-auto">
                  <Link to={featuredWpPost3.link} >
                    <div className="image-container">
                      <Img fluid={featuredWpPost3.featuredImage.node.localFile.childImageSharp.fluid} alt={featuredWpPost3.featuredImage.node.altText} />
                      <div className="image-overlay"></div>
                    </div>
                  </Link>
                  </div>
                </div>
                <div className="row middle-featured-images line-after">
                  <div className="col-12 col-md-11 m-auto">
                  <span className='mag_span text-center mb-0' >
                      <a className="category-editors" href={"/magazine/category/" + featuredWpPost3DisplayCat[0].slug + "/"}>{featuredWpPost3DisplayCat[0].name}</a>
                  </span> 
                  <p class="post-date text-capitalize d-md-none d-block"><span>{featuredWpPost3.date.replace(/,/g, "")}</span></p>
                    <h3 className="text-center mb-2">
                      <Link to={featuredWpPost3.link} >{featuredWpPost3.title}</Link>
                    </h3>
                    <p class="post-date text-capitalize d-md-block d-none"><span>{featuredWpPost3.date.replace(/,/g, "")}</span></p>
                  </div>
                </div>
              </div>

            </div>


            <div className="row magazine-header main-featured-image last-main-featured-image">
              <div className="col-12 col-md-10 m-auto">
                <div className="row">
                  <div className="blogPost col-12  line-after">
                    <img className="img-fluid" src={featuredWpPost4.blogHeroUpdated.blogFeaturedHeroOverview.localFile.publicURL} alt={featuredWpPost4.blogHeroUpdated.blogFeaturedHeroOverview.localFile.altText} />
                    <div className="image-overlay"></div>
                  </div>
                </div>
                <div className="row main-featured-post line-after">
                  <div className="col-12 m-auto">
                    <span className='mag_span text-center mb-0' >
                      <a className="category-editors" href={"/magazine/category/" + featuredWpPost4DisplayCat[0].slug + "/"}>{featuredWpPost4DisplayCat[0].name}</a>
                    </span>
                    <p class="post-date text-capitalize d-md-none d-block"><span>{featuredWpPost4.date.replace(/,/g, "")}</span></p>
                    <h3 className="text-center mb-2">
                      <Link to={featuredWpPost4.link} >{featuredWpPost4.title}</Link>
                    </h3>
                    <p class="post-date text-capitalize d-md-block d-none"><span>{featuredWpPost4.date.replace(/,/g, "")}</span></p>
                  </div>
                </div>
              </div>
            </div>


          </div>


          <div className="container-fluid p-0 d-none d-md-none">
            <div className="row my-md-5 alumni ">
              <div id="alumni-carousel" className="wrapper carousel slide" data-ride="carousel" data-interval="false">
                <ol className="carousel-indicators d-flex d-md-none">
                  <li data-target="#alumni-carousel" data-slide-to="0" className="active"></li>
                  <li data-target="#alumni-carousel" data-slide-to="1"></li>
                  <li data-target="#alumni-carousel" data-slide-to="2"></li>
                </ol>
                <div className="carousel-inner row w-100 mx-auto" role="listbox">

                  <div className="carousel-item item col-12 col-md-4  p-0 active">
                    <div className="row ">
                      <div className="col-12">
                        <img src={featuredWpPost2.featuredImage.node.localFile.publicURL} alt={featuredWpPost2.featuredImage.node.altText} />
                      </div>
                    </div>
                    <div className="row d-flex  align-items-end">
                      <div className="col-10 py-5 m-auto text-center featuredCarousel">
                        <h6 className="text-center mb-2">
                          <a href={featuredWpPost2.link}>
                            {featuredWpPost2.title}
                          </a>
                        </h6>
                        <div className="row  mt-5">
                          <div className="learn_more_container m-auto text-center">
                            <a href={"/magazine/category/" + featuredWpPost2DisplayCat[0].slug}>{featuredWpPost2DisplayCat[0].name}</a>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="carousel-item item col-12 col-md-4  p-0 ">
                    <div className="row ">
                      <div className="col-12">
                        <img src={featuredWpPost3.featuredImage.node.localFile.publicURL} alt={featuredWpPost3.featuredImage.node.altText} />
                      </div>
                    </div>
                    <div className="row d-flex  align-items-end">
                      <div className="col-10 py-5 m-auto text-center featuredCarousel">
                        <h6 className="text-center mb-2">
                          <a href={featuredWpPost3.link}>
                            {featuredWpPost3.title}
                          </a>
                        </h6>
                        <div className="row  mt-5">
                          <div className="learn_more_container m-auto text-center">
                            <a href={"/magazine/category/" + featuredWpPost3DisplayCat[0].slug}>{featuredWpPost3DisplayCat[0].name}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="carousel-item item col-12 col-md-4  p-0 ">
                    <div className="row ">
                      <div className="col-12">
                        <img src={featuredWpPost4.featuredImage.node.localFile.publicURL} alt={featuredWpPost4.featuredImage.node.altText} />
                      </div>
                    </div>
                    <div className="row d-flex  align-items-end">
                      <div className="col-10 py-5 m-auto text-center featuredCarousel">
                        <h6 className="text-center mb-2">
                          <a href={featuredWpPost4.link}>
                            {featuredWpPost4.title}
                          </a>
                        </h6>
                        <div className="row  mt-5">
                          <div className="learn_more_container m-auto text-center">
                            <a href={"/magazine/category/" + featuredWpPost4DisplayCat[0].slug}>{featuredWpPost4DisplayCat[0].name}</a>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <a className="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true"></span>
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
          <div id="latestNews" className="container-fluid mt-5 pt-5">
            <div className="row">
              <div className="col-12">
                <h2 className="text-center mb-5 mt-md-0 pt-md-0 mt-5 pt-4 mx-auto" style={{borderBottom: '3px solid #9E785B', width: 'max-content', paddingBottom: '.3em'}}>Latest News</h2>
              </div>
            </div>
          </div>
          {/* <div className="container">
            <div className="row magazine-header">
              <div className="col-12 p-0">
                <ul className="mag_filter mb-2">
                <li><a href="/magazine/category/leadership-insights/">Leadership insights</a></li>
                  <li><a href="/magazine/category/hospitality-uncovered/">hOSPITALITY UNCOVERED</a></li>
                  <li><a href="/magazine/category/business-of-luxury/">Business of luxury</a></li>
                  <li><a href="/magazine/category/living-well/">LIVING WELL</a></li>
                  <li><a href="/magazine/category/glion-spirit/">GLION SPIRIT</a></li>
                </ul>
              </div>
            </div>
          </div>
          <hr /> */}

          <div className="container mt-4 overflow-hidden">
            <div className="row latest-news">
              <div className="col-12 col-md-12 m-auto">
                <div className="row ">
                  {filteredPosts.map((post, i) => (
                    <div className="blogPost col-12 col-md-6 item mb-5" key={i}>
                      <div className="row ">
                        <div className="col-12 d-flex col-md-10 ml-auto mr-auto ">
                          <div className="row mt-auto mb-0 line-after">
                            <Link className="w-100" to={post.node.link}>
                              <div className="image-container">
                                {post.node.featuredImage !== null &&
                                  // <Img fluid={post.node.featuredImage.node.localFile.childImageSharp.fluid} alt={post.node.featuredImage.node.altText} />
                                  <img src={post.node.featuredImage.node.localFile.publicURL} alt={post.node.featuredImage.node.altText} />
                                }
                                <div className="image-overlay"></div>
                              </div>
                            </Link>
                          </div>
                          <div className="row">
                            <div className="absolute-excerpt col-12 ml-auto text-center">
                              <a href={"/magazine/category/" + post.node.categories.nodes[0].slug + "/"}><p className="category mt-3" dangerouslySetInnerHTML={{ __html: post.node.categories.nodes[0].name }} /></a>
                              <p class="post-date text-capitalize d-md-none d-block"><span>{post.node.date.replace(/,/g, "")}</span></p>
                              <Link to={post.node.link} ><h3 className="mt-3" dangerouslySetInnerHTML={{ __html: post.node.title }} /></Link>
                              <p class="post-date text-capitalize d-md-block d-none"><span>{post.node.date.replace(/,/g, "")}</span></p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                  <BlogPagination

                    page={humanPageNumber}
                    totalPages={numberOfPages}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
};

export default blogTemplate;

export const pageQuery = graphql`
query( $skip: Int!, $limit: Int! ) {
  featuredWpPost1: wpPost(categories: {nodes: {elemMatch: {name: {eq: "Featured Priority 1"}}}}) {
    id
    title
    excerpt
    slug
    link
    uri
    categories {
      nodes {
        id
        name
        slug
      }
    }
    blogHeroUpdated {
      blogFeaturedHeroOverview {
        mediaDetails{
          width
          height
        }
        sourceUrl
        date
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
        }
      }
    }
    date(formatString: "DD, MMM, YYYY")
    featuredImage {
      node {
        altText
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
        }
      }
    }
  }
  featuredWpPost2: wpPost(categories: {nodes: {elemMatch: {name: {eq: "Featured Priority 2"}}}}) {
    id
    title
    excerpt
    slug
    link
    categories {
      nodes {
        id
        name
        slug
      }
    }
    blogHeroUpdated {
      blogFeaturedHeroOverview {
        date
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
        }
      }
    }
    date(formatString: "DD, MMM, YYYY")
    featuredImage {
      node {
        altText
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
          id
        }
      }
    }
  }
  featuredWpPost3: wpPost(categories: {nodes: {elemMatch: {name: {eq: "Featured Priority 3"}}}}) {
    id
    title
    excerpt
    slug
    link
    categories {
      nodes {
        id
        name
        slug
      }
    }
    blogHeroUpdated {
      blogFeaturedHeroOverview {
        date
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
        }
      }
    }
    date(formatString: "DD, MMM, YYYY")
    featuredImage {
      node {
        altText
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
          id
          publicURL
        }
      }
    }
  }
  featuredWpPost4: wpPost(categories: {nodes: {elemMatch: {name: {eq: "Featured Priority 4"}}}}) {
    id
    title
    excerpt
    slug
    link
    categories {
      nodes {
        id
        name
        slug
      }
    }
    blogHeroUpdated {
      blogFeaturedHeroOverview {
        mediaDetails{
          width
          height
        }
        sourceUrl
        date
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
        }
      }
    }
    date(formatString: "DD, MMM, YYYY")
    featuredImage {
      node {
        altText
        localFile {
          childImageSharp{
            fluid{
              ...GatsbyImageSharpFluid_withWebp
            }
          }
          publicURL
          id
          publicURL
        }
      }
    }
  }
  blogRoll: allWpPost( skip: $skip, limit: $limit, filter: {categories: {nodes: {elemMatch: {name: {nin: ["Featured Priority 1", "Featured Priority 2", "Featured Priority 3", "Featured Priority 4"]}}}}, language: {slug: {eq: "en"}}}) {
    totalCount
    edges {
      node {
        id
        title
        excerpt
        slug
        link
        categories {
          nodes {
            id
            name
            slug
          }
        }
        date(formatString: "DD, MMM, YYYY")
        featuredImage {
          node {
            altText
            localFile {
              publicURL
              id
              childImageSharp{
                fluid{
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
              publicURL
            }
          }
        }
      }
    }
  }  

  blogPage: wpPage(id: { eq: "cG9zdDoxMQ==" }) {
    uri
    language {
      slug
    }
    translations {
      title
      link
    
    }
    seo {
      title
      metaDesc
    }
  }
  
}
  
`;
