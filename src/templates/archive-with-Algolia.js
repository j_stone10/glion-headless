// /* eslint-disable react/no-danger */
// import React from 'react';
// import { Link, graphql } from 'gatsby';
// import algoliasearch from 'algoliasearch/lite';
// import { InstantSearch, SearchBox, Hits } from 'react-instantsearch-dom';
// import Layout from '../components/layout';
// // import Masthead from '../components/masthead'
// import Breadcrumb from '../components/Breadcrumb';
// import Pagination from '../components/archive/Pagination';
// import ArchiveSidebar from '../components/archive/ArchiveSidebar';
// import PostPreview from '../components/Post-Preview'

// import {
//   PageContent,
//   StyledH2,
//   StyledDate,
//   StyledReadMore,
// } from './styles/archiveStyles';




// const searchClient = algoliasearch('KBHEQF8O0R', '57217e2cbe1c2d37e1b28faf628983b6');




// const archiveTemplate = ({
//   data: { file, allWordpressPost },
//   pageContext: {
//     catId,
//     catName,
//     catSlug,
//     categories,
//     humanPageNumber,
//     numberOfPages,
//   },
// }) => (
//     <Layout>
     


//       {/* <PageHero img={file.childImageSharp.fluid} /> */}
//       <Breadcrumb
//         parent={{
//           link: '/blog/all-posts',
//           title: 'blog',
//         }}
//       />
//       <InstantSearch searchClient={searchClient} indexName="Blog">
//         <SearchBox />
//         <Hits hitComponent={PostPreview}  />
//       </InstantSearch>
//       <div className="container">
//         <div className="row" style={{ marginBottom: '40px' }}>

//           <ArchiveSidebar catId={catId} categories={categories} />
//           <PageContent className="col-lg-9">
//             <Pagination
//               catSlug={catSlug}
//               page={humanPageNumber}
//               totalPages={numberOfPages}
//             />
//             <h1 dangerouslySetInnerHTML={{ __html: catName }} />
//             {allWordpressPost.edges.map(post => (
//               <article key={post.node.id} className="entry-content">
//                 {/* <PostPreview post={hitComponent} /> */}
//                 <Link to={`/blog/${post.node.slug}/`}>
//                   <StyledH2
//                     dangerouslySetInnerHTML={{ __html: post.node.title }}
//                   />
//                 </Link>
//                 <StyledDate
//                   dangerouslySetInnerHTML={{
//                     __html: post.node.date.replace(/\W+/g, ' '),
//                   }}
//                 />
//                 <p dangerouslySetInnerHTML={{ __html: post.node.excerpt }} />
//                 <StyledReadMore to={`/blog/${post.node.slug}`}>
//                   Read More
//               </StyledReadMore>
//                 <div className="dot_divider">
//                   <hr />
//                 </div>
//               </article>
//             ))}
//             <Pagination
//               catSlug={catSlug}
//               page={humanPageNumber}
//               totalPages={numberOfPages}
//             />
//           </PageContent>
//         </div>
//       </div>
//     </Layout>
//   );

// export default archiveTemplate;

// export const pageQuery = graphql`
//   query($catId: String!, $skip: Int!, $limit: Int!) {
//     allWordpressPost(
//       filter: { categories: { elemMatch: { id: { eq: $catId } } } }
//       skip: $skip
//       limit: $limit
//     ) {
//       edges {
//         node {
//           id
//           title
//           excerpt
//           slug
//           date(formatString: "DD, MMM, YYYY")
//         }
//       }
//     }
//     file(relativePath: { eq: "archive_headerImage.jpg" }) {
//       childImageSharp {
//         fluid(quality: 100, maxWidth: 4000) {
//           ...GatsbyImageSharpFluid_withWebp
//         }
//       }
//     }
//   }
// `;