/* eslint-disable react/no-danger */
import React, { Component } from "react"
import SEO from "../components/seo"
import { graphql, Link } from "gatsby"
import Helmet from "react-helmet"
import PropTypes from "prop-types"
import styled from "styled-components"
import Parser from "html-react-parser"
import Img from "gatsby-image"
import Video from "../components/video"
import Layout from "../components/layout"
import Breadcrumb from "../components/Breadcrumb"
import InsiderSVG from "../images/Insider-logo.png"
import "../components/styles/blog.css"
import $ from "jquery"

const PostContent = styled.article`
  margin: 20px 0 0 0;
`

class postTemplate2021FR extends Component {
  componentDidMount() {
    var title = this.props.data.post.title
    var categoryName = this.props.data.post.categories.nodes[0].name
    var categoryTitle = this.props.data.post.categories
    var catName = ""
    if (
      categoryName === "FR Featured Priority 1" ||
      categoryName === "FR Featured Priority 2" ||
      categoryName === "FR Featured Priority 3" ||
      categoryName === "FR Featured Priority 4"
    ) {
      catName = this.props.data.post.categories.nodes[1].name
    } else {
      catName = this.props.data.post.categories.nodes[0].name
    }
    var dateName = this.props.data.post.date;
    //show post date
    // var newItem = document.createElement("div");
    // var textnode = "<div class='row'><div class='col-12'><h3 class='text-center mt-5 mb-0'>" + this.props.data.post.date + "</h3></div></div>"
    // newItem.innerHTML = textnode
    // var list = document.querySelector('.block-editorial-content-01');
    // list.insertBefore(newItem, list.childNodes[0]);
    $(".blog-content-2021 .block-hero-01 .row.hero").wrap(
      "<div class='container'></div>"
    )

    $(".blog-content-2021 .block-hero-01 .row.hero").after(
      '<div class="container post-preview-section"><div class="row"><div class="col-12 col-md-10 m-auto post-preview-content"><h1 class=""><div class="d-flex justify-content-md-start justify-content-between mb-md-0 mb-4"><p>' +
        catName +
        "</p><p>" +
        dateName +
        "</p></div>" +
        title +
        "</h1></div></div></div>'"
    )
    $(".blog_post_template_2021 .breadcrumb-container").addClass("container")
  }

  render() {
    const post = this.props.data.post
    // console.log(post)
    // const media = this.props.data.allWpMediaItem.edges
    const allWpPost = this.props.data.allWpPost.edges

    // let linkedinShare = post.content
    // // console.log(linkedinShare)

    // linkedinShare = document.querySelector('.background_image picture img ')
    // // console.log(linkedinShare)

    // let linkedinShareValue =  linkedinShare.src

    // linkedinShareValue = linkedinShareValue.split('http://127.0.0.1:8000/').join('/')
    // // linkedinShareValue.replace('http://127.0.0.1:8000/', '')
    // console.log(linkedinShareValue)

    // var linkedinShareValueArray = linkedinShareValue.split(',').pop();
    // console.log(linkedinShareValueArray)

    // const image = {
    //   'localFile': {
    //     'publicURL': linkedinShareValue
    //   },
    //   'mediaDetails': {
    //     'width': 1445,
    //     'height': 875,
    //   },
    // }

    // console.log(image)

    if (allWpPost.length > 12) {
      allWpPost.length = 12
    }

    // console.log(allWpPost)
    // console.log(currentPage.content)
    // console.log( currentPage.acf );
    const Content = Parser(post.content, {
      replace: domNode => {
        // console.dir( "domNode" + domNode )
        // if (domNode.name === 'img') {
        //   // TODO: also check src attribute for 'wp-content/uploads'
        //   let image = media.filter(m => {
        //     // console.log(m.node.sourceUrl)
        //     // console.log(domNode.attribs.src)
        //     return m.node.sourceUrl == domNode.attribs.src
        //   })
        //   if (image != '') {
        //     console.log(image)
        //     image = image[0].node.localFile
        //     return <Img fluid={image.childImageSharp.fluid} />
        //   }
        // }
        // if (domNode.name === 'video') {
        //   // TODO: also check src attribute for 'wp-content/uploads'
        //   let video = media.filter(v => {
        //     // console.log("1: " + domNode.attribs.poster)
        //     // console.log("2: " + v.node.source_url)
        //     return v.node.source_url === domNode.attribs.src
        //   })
        //   let poster = media.filter(v => {
        //     // console.log("1: " + domNode.attribs.poster)
        //     // console.log("2: " + v.node.source_url)
        //     return v.node.source_url === domNode.attribs.poster
        //   })
        //   if (video != '') {
        //     // console.log(video)
        //     // console.log(poster)
        //     video = video[0].node.localFile
        //     poster = poster[0].node.localFile
        //     return <video className="video" controls src={video.publicURL}
        //       poster={poster.publicURL}
        //     // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>
        //     />
        //     // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"
        //   }
        // }
      },
    })

    return (
      <Layout
        polylang_translations={post.translations}
        currentLang={post.language.slug}
      >
        <SEO
          title={post.seo.title}
          lang={post.language.slug}
          description={post.seo.metaDesc}
          image={
            post.linkedInShare.linkedinShareImage !== null
              ? post.linkedInShare.linkedinShareImage
              : post.featuredImage.node
          }
          uri={post.uri}
          author={post.author}
        />

        <div id="content" className="blog_post_template_2021">
          <div className="container-fluid">
            <div className="row magazine-header mb-4">
              <div className="col-12 col-md-6  mx-auto">
                <Link to="/fr/magazine-fr/">
                  <img
                    className="blog-logo"
                    src={InsiderSVG}
                    style={{ maxWidth: "100%", height: "auto" }}
                  />
                </Link>
                <div className="col-12 mr-auto d-flex align-items-center text-md-left text-center">
                  <p className="text-center col">
                    le magazine de Glion Institut
                    <br class="d-md-none d-block" /> de Hautes Études
                  </p>
                </div>
              </div>
            </div>

            <Breadcrumb
              parent={{
                link: "/fr/magazine-fr/",
                title: "Magazine",
              }}
              currentLang={post.language.slug}
              title={post.title}
            />

            {/* <div className="row">
              <div className="col-12 col-md-10 p-md-0 m-auto">
                <h1
                  className="mb-5"
                  dangerouslySetInnerHTML={{ __html: post.title }}
                />
              </div>
            </div> */}
            <div className="row blog-content-2021">
              <div
                className="col-12 post-template-default"
                dangerouslySetInnerHTML={{ __html: post.content }}
              ></div>
            </div>
          </div>
        </div>

        <div className="container mt-0 similar-stories">
          <div className="col-12 col-md-12">
            <h2 className="text-center">Dans la même rubrique</h2>
          </div>
          <div className="row latest-news mt-5">
            <div className="col-12 col-md-12 m-auto">
              <div className="row ">
                {allWpPost.map((post, i) => (
                  <div className="blogPost col-12 col-md-6 item mb-5" key={i}>
                    <div className="row ">
                      <div className="col-12 d-flex col-md-10 ml-auto mr-auto ">
                        <div className="row mt-auto mb-0 line-after">
                          <Link className="w-100" to={post.node.link}>
                            <div className="image-container">
                              {post.node.featuredImage !== null && (
                                // <Img
                                //   fluid={
                                //     post.node.featuredImage.node.localFile
                                //       .childImageSharp.fluid
                                //   }
                                //   alt={post.node.featuredImage.node.altText}
                                // />
                                <img
                                  src={
                                    post.node.featuredImage.node.localFile
                                      .publicURL
                                  }
                                  alt={post.node.featuredImage.node.altText}
                                />
                              )}
                              <div className="image-overlay"></div>
                            </div>
                          </Link>
                        </div>
                        <div className="row">
                          <div className="absolute-excerpt col-11 ml-auto col-md-12 ml-auto text-center">
                            <p
                              className="category mt-3"
                              dangerouslySetInnerHTML={{
                                __html: post.node.categories.nodes[0].name,
                              }}
                            />
                            <Link to={post.node.link}>
                              <h3
                                className="mt-3"
                                dangerouslySetInnerHTML={{
                                  __html: post.node.title,
                                }}
                              />
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

postTemplate2021FR.propTypes = {
  data: PropTypes.object.isRequired,
}

export default postTemplate2021FR

export const pageQuery = graphql`
  query($id: String!) {
    post: wpPost(
      id: { eq: $id }
      template: { templateName: { in: "2021 Blog Template" } }
      language: { slug: { eq: "fr" } }
    ) {
      title
      content
      uri
      author {
        node {
          name
        }
      }
      seo {
        title
        metaDesc
      }
      language {
        slug
      }
      translations {
        title
        link
      }
      linkedInShare {
        linkedinShareImage {
          mediaDetails {
            width
            height
          }
          altText
          localFile {
            publicURL
          }
        }
      }
      featuredImage {
        node {
          mediaDetails {
            width
            height
          }
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
            id
          }
        }
      }
      date(formatString: "DD MMMM YYYY", locale: "fr")
      categories {
        nodes {
          id
          name
          slug
        }
      }
    }
    allWpPost(
      filter: {
        categories: {
          nodes: {
            elemMatch: {
              name: {
                nin: [
                  "FR Featured Priority 1"
                  "FR Featured Priority 2"
                  "FR Featured Priority 3"
                  "FR Featured Priority 4"
                ]
              }
            }
          }
        }
        language: { slug: { eq: "fr" } }
      }
    ) {
      totalCount
      edges {
        node {
          id
          title
          excerpt
          slug
          link
          categories {
            nodes {
              id
              name
            }
          }
          date(formatString: "DD MMMM YYYY", locale: "fr")
          featuredImage {
            node {
              altText
              localFile {
                publicURL
                id
                childImageSharp {
                  fluid {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
                publicURL
              }
            }
          }
        }
      }
    }
  }
`
