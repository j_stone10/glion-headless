import React, { Component } from 'react'
import { Link, graphql } from 'gatsby';
import Helmet from "react-helmet"
import Img from 'gatsby-image'
import SEO from '../components/seo';
import Layout from '../components/layout'
import BreadCrumb from '../components/Breadcrumb'
import Loadable from 'react-loadable';
import Loading from '../components/LoadingIndicator';
import '../components/styles/shield.css';

const NewsSlider = Loadable({
  loader: () => import('../components/NewsSlider'),
  loading: Loading,
});


class AlumniPage extends Component {
  render() {
    const page = this.props.data.page
    const allWpPost = this.props.data.allWpPost
    // if (page.
    var newsPosts = ''
    var pageCat = ''
    // if (page.acfAlumniLanding !== null) {
    //   var pageCat = page.acfAlumniLanding.categorySelect[0].name
    // }

    // console.log(page)

    var newsPosts = allWpPost.edges.filter(
      ({ node: { categories } }) =>
        categories.nodes.some(el => el.name !== pageCat)
    );

    // console.log(newsPosts)

    if (newsPosts.length > 5) {
      newsPosts.length = 5
    }



    var parentId = this.props.data.page.parentId
    const allWpPage = this.props.data.allWpPage

    // console.log(parentId)
    var parent = null

    if (parentId !== null) {


      var parentObject = allWpPage.edges.filter(
        ({ node }) =>
          node.id === parentId
      );

      var parent = parentObject[0].node
      // console.log(parent)
    }

    return (
      <Layout polylang_translations={page.translations} currentLang={page.language.slug} frontPage={page.isFrontPage} >
       
        <SEO title={page.seo.title} lang={page.language.slug} description={page.seo.metaDesc} image={page.acfAlumniLanding.alumniHeaderImage} uri={page.uri} author={page.author} />
        <div id="content">
          <div className="container-fluid">
            <div className="row line-after">
              <div className="col-md-6 col-12 pr-md-0 program-content ind order-2 order-md-1">
                <div className="breadcrumb-container d-none d-md-block">
                  <BreadCrumb parent={parent} title={page.title} currentLang={page.language.slug}
                  />
                </div>
                <div className="row program-text individual mr-md-0">
                  <div className="col-xl-9 col-lg-11 col-11 m-auto p-sm-2">
                    <h1>{page.title}
                    </h1>
                    <div dangerouslySetInnerHTML={{ __html: page.acfAlumniLanding.alumniOverview }} />
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-12 order-1 order-md-2 image fill p-0" style={{ minHeight: "700px" }}>
                {page.acfAlumniLanding.alumniHeaderImage.localFile &&
                  <Img fluid={page.acfAlumniLanding.alumniHeaderImage.localFile.childImageSharp.fluid} alt={page.acfAlumniLanding.alumniHeaderImage.altText} />
                }
              </div>
            </div>
            <div className="row stats">
              <div className="col-12 my-5">
                <div className="counter  no-background text-center bronze-text">
                  <div className="row">
                    <div className="col-sm-8 col-10 m-auto">
                      <h3 dangerouslySetInnerHTML={{ __html: page.acfAlumniLanding.statsTitle }} />

                    </div>
                  </div>
                  <div className="wrapper row">
                    <div className="col-md-4 ml-auto col-12">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAlumniLanding.stat1Intro !== '' &&
                            <p className="col-12"> {page.acfAlumniLanding.stat1Intro}</p>
                          }
                          <div className="scrollElementOffset font"> <span className="value "> {page.acfAlumniLanding.stat1Figure}</span> {page.acfAlumniLanding.stat1FigureAfter}</div>
                          <p className="col-12">
                            {page.acfAlumniLanding.stat1CloseText}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 mr-auto col-12 ">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAlumniLanding.stat2Intro !== '' &&
                            <p className="col-12"> {page.acfAlumniLanding.stat2Intro}</p>
                          }
                          <div className="scrollElementOffsetThird font"> <span className="value "> {page.acfAlumniLanding.stat2Figure}</span> {page.acfAlumniLanding.stat2FigureAfter}</div>
                          <p className="col-12">
                            {page.acfAlumniLanding.stat2CloseText}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 mr-auto col-12">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAlumniLanding.stat3Intro !== '' &&
                            <p className="col-12"> {page.acfAlumniLanding.stat3Intro}</p>
                          }
                          <div className="scrollElementOffsetLast font"> <span className="value "> {page.acfAlumniLanding.stat3Figure}</span> {page.acfAlumniLanding.stat3FigureAfter}</div>
                          <p className="col-12">
                            {page.acfAlumniLanding.stat3CloseText}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  {page.acfAlumniLanding.secondary_title !== '' &&
                    <div className="row">
                      <div className="col-sm-8 col-10 m-auto">
                        <h3 dangerouslySetInnerHTML={{ __html: page.acfAlumniLanding.secondaryTitle }} />
                      </div>
                    </div>
                  }
                  <div className="wrapper row">

                    <div className="col-md-4 ml-auto col-12">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAlumniLanding.stat4Intro !== '' &&
                            <p className="col-12"> {page.acfAlumniLanding.stat4Intro}</p>
                          }
                          <div className="scrollElementOffset font"> <span className="value "> {page.acfAlumniLanding.stat4Figure}</span> {page.acfAlumniLanding.stat4FigureAfter}</div>
                          <p className="col-12">
                            {page.acfAlumniLanding.stat4CloseText}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-12 mr-auto">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAlumniLanding.stat5Intro !== '' &&
                            <p className="col-12"> {page.acfAlumniLanding.stat5Intro}</p>
                          }
                          <div className="scrollElementOffsetThird font"> <span className="value "> {page.acfAlumniLanding.stat5Figure}</span> {page.acfAlumniLanding.stat5FigureAfter}</div>
                          <p className="col-12">
                            {page.acfAlumniLanding.stat5CloseText}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* {page.acfAlumniLanding.stats_link &&
                  
                                <div className="row">
                                    <div className="col text-center m-auto">

                                    <Link to={page.acfAlumniLanding.stats_link.title} className="btn btn-primary blue">{page.acfAlumniLanding.stats_link.title}</Link>
                                   
                                    </div>
                                </div>
                              } */}
                </div>
              </div>
            </div>

            {page.acfAlumniLanding.imageTextRow.map((item, i) => {



              if (item.layoutForThisRow === "Option 1") {
                return (
                  <>
                    <div key={i} className="wrapper row left_img_right_text alumni_landing no-image" >

                      <div className={`col-md-6 col-12 p-0 d-flex justify-content-center align-items-center ${item.blockOption1.backgroundColourBlock1} ${item.blockOption1.hasShieldBlock1 === true && 'shield'} `} >
                        {item.blockOption1.imageBlock1 === null &&
                          <div className="scrollElement col-9 m-auto text-center" dangerouslySetInnerHTML={{ __html: item.blockOption1.contentBlock1 }} />
                        }
                        {item.blockOption1.imageBlock1 !== null &&
                          <>
                            <div className="overlay"></div>
                            {item.blockOption1.imageBlock1.localFile &&
                              <Img fluid={item.blockOption1.imageBlock1.localFile.childImageSharp.fluid} alt={item.blockOption1.imageBlock1.altText} />
                            }
                            <div className="scrollElementOffsetThird .counter col-9 m-auto text-center " dangerouslySetInnerHTML={{ __html: item.blockOption1.contentBlock1 }} />

                          </>
                        }
                      </div>
                      <div className={`col-md-6 col-12 p-0  d-flex justify-content-center align-items-center ${item.blockOption1.backgroundColourBlock2} ${item.blockOption1.hasShieldBlock2 === true && 'shield'} `}>
                        {item.blockOption1.imageBlock2 === null &&
                          <div className="scrollElement col-9 m-auto text-center" dangerouslySetInnerHTML={{ __html: item.blockOption1.contentBlock2 }} />
                        }
                        {item.blockOption1.imageBlock2 !== null &&
                          <>
                            <div className="overlay"></div>
                            {item.blockOption1.imageBlock2.localFile &&
                              <Img fluid={item.blockOption1.imageBlock2.localFile.childImageSharp.fluid} alt={item.blockOption1.imageBlock2.altText} />
                            }
                            <div className="scrollElementOffsetThird  col-9 m-auto text-center  " dangerouslySetInnerHTML={{ __html: item.blockOption1.contentBlock2 }} />
                          </>
                        }
                      </div>
                    </div>
                  </>
                )
              }
              else if (item.layoutForThisRow === "Option 2") {
                return (
                  <div key={i} className="wrapper row left_img_right_text image alumni_landing" >
                    <div className="col-12 p-0  d-flex justify-content-center align-items-center">
                      <div className="overlay"></div>
                      <div className="image_container">
                        {item.blockOption2.quoteImageOption2.localFile &&
                          <Img fluid={item.blockOption2.quoteImageOption2.localFile.childImageSharp.fluid} alt={item.blockOption2.quoteImageOption2.altText} />
                        }
                      </div>
                      <div className="scrollElement absolute col-11 m-auto text-center white-text">
                        <h3 className="mb-5">“{item.blockOption2.quoteImageTextOption2}”</h3>
                        <p className="cite mb-5">{item.blockOption2.quoteImageCiteOption2}</p>
                        <Link to={item.blockOption2.quoteImageLinkOption2.url} className="mt-5 btn btn-primary bronze">{item.blockOption2.quoteImageLinkOption2.title}</Link>

                      </div>
                    </div>
                  </div>
                )
              }
            }

            )}

            <div className="wrapper row mb-5 bronze-bg">
              <div className="scrollElement col-12 col-md-6 m-auto my-3 py-5 text-center">
                <h2 className="text-center pt-5">{page.acfAlumniLanding.alumniTitle}</h2>
                <p className="text-center" dangerouslySetInnerHTML={{ __html: page.acfAlumniLanding.alumniText }} />
                <div className="col text-center m-auto">
                  <Link to={page.acfAlumniLanding.alumniLink.url} className="my-5 btn btn-primary blue ">{page.acfAlumniLanding.alumniLink.title}</Link>

                </div>
              </div>
            </div>

            <div className={`wrapper row my-5 ${page.acfAlumniLanding.hideProgramsSection === true && 'd-none'} `}>
              <div className="col-12  my-3">
                <div className="scrollElement text-center">
                  <h2 className="text-center pt-5">{page.acfAlumniLanding.viewProgramsTitle}</h2>
                  <p className="text-center" dangerouslySetInnerHTML={{ __html: page.acfAlumniLanding.viewProgramsText }} />
                  <div className="col text-center m-auto">
                    <Link to={page.acfAlumniLanding.viewProgramsLink.url} className="my-5 btn btn-primary blue ">{page.acfAlumniLanding.viewProgramsLink.title}</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {page.acfAlumniLanding.categorySelect !== null && page.language.slug === "en" &&
            <>
              <div className="wrapper row my-5">
                <div className="scrollElement col-12 text-center">
                  <h2>News</h2>
                </div>
              </div>


              <NewsSlider
                newsPosts={newsPosts} />


            </>
          }
        </div>
      </Layout >
    )
  }
}

export default AlumniPage;

export const pageQuery = graphql`
query($id: String!) {

    page:  wpPage(id: { eq: $id }) {

title
content
      parentId
      language {
        slug
      }
      translations {
        title
        link
      
      }
      uri
          author {
            node {
              name
            }
          }
          seo {
            title
            metaDesc
          
          }
acfAlumniLanding {
    categorySelect {
        name
      }
    alumniOverview
    alumniText
    alumniTitle
    fieldGroupName
    hideProgramsSection
    secondaryTitle
    stat1CloseText
    stat1Figure
    stat1FigureAfter 
    stat1Intro
    stat2CloseText
    stat2Figure
    stat2FigureAfter
    stat2Intro
    stat3CloseText
    stat3Figure
    stat3FigureAfter
    stat3Intro
    stat4CloseText
    stat4Figure
    stat4FigureAfter
    stat4Intro
    stat5CloseText
    stat5Figure
    stat5FigureAfter
    stat5Intro
    statsTitle
    viewProgramsText
    viewProgramsTitle
    alumniLink {
      title
      url
    }
    alumniHeaderImage {
      mediaDetails{
        width
        height
      }
      sourceUrl
      altText
      localFile{
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
        publicURL
      }
    }
    button1 {
      title
      url
    }
    button2 {
      title
      url
    }
    imageTextRow {
      blockOption1 {
        backgroundColourBlock1
        backgroundColourBlock2
        contentBlock1
        contentBlock2
        fieldGroupName
        hasShieldBlock1
        hasShieldBlock2
        imageBlock1 {
          altText
          localFile{
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        imageBlock2 {
          altText
          localFile{
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
      }
      blockOption2 {
        fieldGroupName
        quoteImageCiteOption2
        quoteImageLinkOption2 {
          title
          url
        }
        quoteImageOption2 {
          altText
          localFile{
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        quoteImageTextOption2
      }
      layoutForThisRow
    }
    statsLink {
      title
      url
    }
    viewProgramsLink {
      title
      url
    }







   
  }

}   
allWpPage {
  edges {
    node {
      id
      title
      link
    }
  }
} 
allWpPost(filter: {categories: {nodes: {elemMatch: {name:  { nin: ["Featured Priority 1", "Featured Priority 2", "Featured Priority 3", "Featured Priority 4" ] } }}}}) {
    edges {
      node {
        id
        title
        link
        categories {
          nodes{
            name
          }
        }
        featuredImage {
          node {
            altText
            localFile {
              publicURL
              id
              childImageSharp{
                fluid{
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
              publicURL
            }
          }
        }
      }
    }
  }
}
`