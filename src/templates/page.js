import React, { Component } from "react"
import ReactDOM from "react-dom"
import SEO from "../components/seo"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import Parser from "html-react-parser"
import { Helmet } from "react-helmet"
import Video from "../components/video"
import $ from "jquery"
import Layout from "../components/layout"
import BreadCrumb from "../components/Breadcrumb"
import CalendlyEmbed from "../components/CandelyWidget"
// const PageContent = styled.article`
//     margin-top: 50px;
//     color: black
// `

const marketoScriptId = "mktoForm"

class PageTemplate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoaded: false,
      formId: 0,
      flywire: false,
    }
  }

  loadScript() {
    var form = $("form").attr("id")
    // console.log(form)
    if (form) {
      var id = form.split("_").pop()
    }
    // if (form) {
    //   var id = form.split("_").pop()
    //   this.setState({ formId: id });
    // }
    var s = document.createElement("script")
    s.id = marketoScriptId
    s.type = "text/javascript"
    s.async = true
    s.src = "//app-lon05.marketo.com/js/forms2/js/forms2.min.js"
    s.onreadystatechange = function() {
      if (this.readyState === "complete" || this.readyState === "loaded") {
        this.setState({ isLoaded: true, formId: id })
      }
    }
    // var id = $("form").attr("id")
    s.onload = () => this.setState({ isLoaded: true, formId: id })
    document.getElementsByTagName("head")[0].appendChild(s)
  }
  // loadFlywire = () => {
  //   // var form = $("form").attr("id")
  //   // // console.log(form)
  //   // if (form) {
  //   // var id = form.split("_").pop()
  //   // }
  //   // if (form) {
  //   //   var id = form.split("_").pop()
  //   //   this.setState({ formId: id });
  //   // }
  //   window.flywire = function() {
  //     var flywireScript = document.createElement("script")
  //     flywireScript.id = "flywire"
  //     flywireScript.type = "text/javascript"
  //     flywireScript.async = true
  //     flywireScript.src = "//payment.flywire.com/assets/js/flywire.js"
  //     flywireScript.onreadystatechange = function() {
  //       if (this.readyState === "complete" || this.readyState === "loaded") {
  //         this.setState({ flywire: true })
  //       }
  //     }
  //     // var id = $("form").attr("id")
  //     flywireScript.onload = () => this.setState({ flywire: true })
  //     document.getElementsByTagName("head")[0].appendChild(flywireScript)
  //   }
  // }

  componentDidMount() {
    console.log(this.state)
    // if (!window.flywire) {
    //   // this.loadFlywire()
    // } else {
    //   this.setState({ flywire: true })
    // }


    // console.log('did mount')

    var form = $("form").attr("id")
    if (form) {
      var id = form.split("_").pop()
      // console.log(form)
    }
    if (!document.getElementById(marketoScriptId)) {
      this.loadScript()
    } else {
      this.setState({ isLoaded: true, formId: id })
    }



    var count = 0
    $("#videosModals .col-12").each(function() {
      $(this).addClass("class" + count)

      count++
      // console.log(count)
    })

    var counter = 9

    if ($("#videosModals .col-12.d-none").length > 0) {
      $("#gallery-load-more").show()
    }

    // console.log(count - 3)
    let remainingPosts = $("#videosModals .col-12.d-none").length
    // console.log(remainingPosts)

    $("#gallery-load-more").on("click", function(event) {
      event.preventDefault()
      counter = counter + 6
      // console.log(counter);
      let i = ""
      for (i = 0; i < counter; i++) {
        $("#videosModals .col-12.d-none").each(function() {
          $(".class" + i).removeClass("d-none")
          // $( '.class3').removeClass( "d-none" );
          // $( '.class4').removeClass( "d-none" );
        })
        if (remainingPosts < 3) {
          $("#gallery-load-more").hide()
        }
      }
    })

    if ($(".calendly-inline-widget").length > 0) {
      const dataURL = document
        .querySelector(".calendly-inline-widget")
        .getAttribute("data-url")
      setTimeout(function() {
        ReactDOM.render(
          <CalendlyEmbed dataUrl={dataURL} />,
          document.querySelector(".calendly-inline-widget")
        )
      }, 500)
    }

    var i,
      e,
      d = document,
      s = "script"
    i = d.createElement("script")
    i.async = 1
    i.src =
      "https://cdn.curator.io/published/0484d69e-f57c-4792-846d-44794b960f1a.js"
    e = d.getElementsByTagName(s)[0]
    e.parentNode.insertBefore(i, e)
    // var config = { /* no comma on last item in list schools*/ schools: [ 'Switzerland', 'London' ] }; var schoolvalues = { /* no comma on last item in list schools*/ schoolvalue: [ 'a0M5800000107qGEAQ', 'a0M5800000107qaEAA' ] };
  }

  componentDidUpdate() {

    // console.log('did update')
    // console.log(window)
    if ($("#flywire-payex").length > 0) {
      setTimeout(function() {
        if ($("#flywire-payex").hasClass("eng")) {
          // const script4 = document.createElement('script');
          // script4.src = "https://payment.flywire.com/assets/js/flywire.js";
          // script4.async = true;
          // document.body.appendChild(script4);

          window.flywire.Payment.render(
            {
              env: "production",
              locale: "fr-FR",
              destination: "GIA",
              provider: "embed",
              amount: 17500,
              read_only: "amount",
              theme: {
                header: false,
                backgroundColor: "#ffffff",
                brandColor: "#3498db",
              },
            },
            "#flywire-payex"
          )
        } else {
          window.flywire.Payment.render(
            {
              env: "production",
              locale: "fr-FR",
              destination: "GCA",
              provider: "embed",
              amount: 27500,
              read_only: "amount",
              theme: {
                header: false,
                backgroundColor: "#ffffff",
                brandColor: "#3498db",
              },
            },
            "#flywire-payex"
          )
        }
      }, 500)
    }


    
    // console.log(this.state)

    // const script1 = var config = { /* no comma on last item in list schools*/ schools: [ 'Switzerland', 'London' ] }; var schoolvalues = { /* no comma on last item in list schools*/ schoolvalue: [ 'a0M5800000107qGEAQ', 'a0M5800000107qaEAA' ] };"

    if (
      this.state.formId == 741 ||
      this.state.formId == 14843 ||
      this.state.formId == 9223 ||
      this.state.formId == 739 ||
      this.state.formId == 14883 ||
      this.state.formId == 9223 ||
      this.state.formId == 14821 ||
      this.state.formId == 13958 ||
      this.state.formId == 740 ||
      this.state.formId == 738 ||
      this.state.formId == 15185
    ) {
      const script2 = document.createElement("script")
      const script3 = document.createElement("script")
      var schools = document.createElement("script") // is a node
      schools.innerHTML =
        "var config = { /* no comma on last item in list schools*/ schools: [ 'Switzerland', 'London' ] }; var schoolvalues = { /* no comma on last item in list schools*/ schoolvalue: [ 'a0M5800000107qGEAQ', 'a0M5800000107qaEAA' ] };"
      document.body.appendChild(schools)

      script2.src =
        "https://picklist.sommet-education.com/js.ext/sommet-form-init.js"
      script3.src =
        "https://picklist.sommet-education.com/js.ext/picklist-global.js"
      // config.async = true;
      script2.async = true
      script3.async = true
      // document.body.appendChild(config);
      document.body.appendChild(script2)
      document.body.appendChild(script3)
    }
    // Depending on your lint, you may need this
    // eslint-disable-next-line no-undef
    window.MktoForms2.loadForm(
      "//app-lon05.marketo.com",
      "411-MKX-040",
      this.state.formId
    )
    //   }
    // }
  }

  render() {
    // console.log(this.props.data)
    const currentPage = this.props.data.currentPage

    const allWpPage = this.props.data.allWpPage
    // console.log(currentPage)
    var parent = null
    var parentId = currentPage.parentId
    if (parentId !== null) {
      var parentObject = allWpPage.edges.filter(
        ({ node }) => node.id === parentId
      )
      var parent = parentObject[0].node
    }

    // var config = { /* no comma on last item in list schools*/ schools: [ 'Switzerland', 'London' ] }; var schoolvalues = { /* no comma on last item in list schools*/ schoolvalue: [ 'a0M5800000107qGEAQ', 'a0M5800000107qaEAA' ] };
    // console.log(parent)
    // const media = this.props.data.allWpMediaItem.edges
    const allfile = this.props.data.allFile.edges

    const Content = Parser(currentPage.content, {
      replace: domNode => {
        if (domNode.name === "a") {
          // var anchor = $(this).text();
          let pdf = allfile.filter(p => {
            //console.log($('a').text());

            // console.log(domNode.attribs.href)
            if (domNode.attribs.href !== undefined) {
              if (domNode.attribs.href.includes("/")) {
                var nameExtension = domNode.attribs.href.split("/").pop()
              }
            }

            // console.dir(nameExtension)
            return p.node.relativePath === nameExtension
          })

          if (pdf != "") {
            // console.log(pdf)
            pdf = pdf[0].node
            return (domNode.attribs.href = pdf.publicURL)
          }
        }
      },
    })

    return (
      <Layout
        polylang_translations={currentPage.translations}
        currentLang={currentPage.language.slug}
      >
        <Helmet>
          <script
            src="//payment.flywire.com/assets/js/flywire.js"
            defer={true}
          ></script>
        </Helmet>
        <SEO
          title={currentPage.seo.title}
          lang={currentPage.language.slug}
          description={currentPage.seo.metaDesc}
          image={
            currentPage.featuredImage !== null
              ? currentPage.featuredImage.node
              : null
          }
          uri={currentPage.uri}
        />

        {/* <BreadCrumb parent={parent} title={currentPage.title} /> */}
        {/* <Masthead title={currentPage.title}></Masthead> */}

        <div id="content">
          <div className="container-fluid">
            <BreadCrumb
              parent={parent}
              title={currentPage.title}
              currentLang={currentPage.language.slug}
            />
            {Content}
          </div>
        </div>
      </Layout>
    )
  }
}

export default PageTemplate

export const pageQuery = graphql`
  query($id: String!) {
    currentPage: wpPage(id: { eq: $id }) {
      title
      content
      uri
      language {
        slug
      }
      translations {
        title
        link
      }
      seo {
        title
        metaDesc
      }
      parentId
      featuredImage {
        node {
          mediaDetails {
            width
            height
          }
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
            id
          }
        }
      }
    }
    allWpPage {
      edges {
        node {
          id
          title
          link
        }
      }
    }
    site {
      id
      siteMetadata {
        title
      }
    }
    allFile {
      edges {
        node {
          id
          relativePath
          publicURL
          name
        }
      }
    }
  }
`
