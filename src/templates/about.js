import React, { Component } from 'react'
import Img from "gatsby-image"
import Helmet from "react-helmet"
import { graphql } from 'gatsby';
import SEO from '../components/seo';
import Layout from '../components/layout'
import BreadCrumb from '../components/Breadcrumb'
import CampusLocations from '../components/CampusLocations'
import HomePrograms from '../components/HomePrograms'
import '../components/styles/about-us.css';





class AboutPage extends Component {
  render() {
    const page = this.props.data.page
    const homePage = this.props.data.homePage
    const homePageFR = this.props.data.homePageFR
    //   const faculty = this.props.data.allWordpressAcfFaculty
    //   const media = this.props.data.allWpMediaItem.edges
    //   console.log(faculty)


    // console.log(page)
    return (
      <Layout polylang_translations={page.translations} currentLang={page.language.slug} frontPage={page.isFrontPage} >
        
        <SEO title={page.seo.title} lang={page.language.slug} description={page.seo.metaDesc} image={page.acfAboutUs.heroSectionAboutImage} uri={page.uri} author={page.author} />
        <div id="content">
          <div className="container-fluid">
            <div className="row hero line-after">
              <div className="background_image">

                {page.acfAboutUs.heroSectionAboutImage.localFile.childImageSharp !== null &&
                  <Img fluid={page.acfAboutUs.heroSectionAboutImage.localFile.childImageSharp.fluid} alt={page.acfAboutUs.heroSectionAboutImage.altText} />
                }
              </div>
              <div className="scrollElementStart foreground_text col-md-9 col-11 m-auto about-us">
                <h2 dangerouslySetInnerHTML={{ __html: page.acfAboutUs.heroSectionAboutTitle }} />


              </div>
            </div>
            <div className="wrapper row intro-glion grey-bg py-md-5">
              <div className="container">
                <div className="row py-5">
                  <div className=" col-12 pt-5">
                    <h1 dangerouslySetInnerHTML={{ __html: page.acfAboutUs.glionIntroTitle }} />
                    {/* <h1><?php the_field('glion_intro_title') ?></h1> */}
                  </div>
                </div>
                <div className="row offset">
                  <div className="col-12 col-md-8 d-flex justify-content-center align-items-center">
                    <div className="row mt-md-5">
                      <div className="scrollElementOffset col-12 col-md-6" dangerouslySetInnerHTML={{ __html: page.acfAboutUs.glionIntroText }} />

                    </div>
                  </div>
                  <div className="col-12 col-md-4">
                    <Img fluid={page.acfAboutUs.glionIntroImage.localFile.childImageSharp.fluid} alt={page.acfAboutUs.glionIntroImage.altText} />

                  </div>
                </div>
              </div>
            </div>
            <div className=" row stats blue-bg">
              <div className="col-12 my-5">
                <div className=" counter  no-background text-center white-text">
                  <div className="wrapper row">
                    <div className="col-md-4 ml-auto col-12">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAboutUs.introStat1 !== '' &&
                            <p className="col-12">{page.acfAboutUs.introStat1}</p>
                          }
                          <div className="scrollElement font font1"> <span className="value value1">{page.acfAboutUs.figureStat1}</span>{page.acfAboutUs.symbolStat1}</div>
                          <p className="col-12">{page.acfAboutUs.signoffStat1}
                          </p>

                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 mr-auto col-12 ">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAboutUs.introStat2 !== '' &&
                            <p className="col-12">{page.acfAboutUs.introStat2}</p>
                          }
                          <div className="scrollElementOffset font font2"> <span className="value value2">{page.acfAboutUs.figureStat2}</span>{page.acfAboutUs.symbolStat2}</div>
                          <p className="col-12">{page.acfAboutUs.signoffStat2}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="wrapper row">
                    <div className="col-md-4 mr-auto col-12">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAboutUs.introStat3 !== '' &&
                            <p className="col-12">{page.acfAboutUs.introStat3}</p>
                          }
                          <div className="scrollElement font font3"> <span className="value value3">{page.acfAboutUs.figureStat3}</span>{page.acfAboutUs.symbolStat3}</div>
                          <p className="col-12">{page.acfAboutUs.signoffStat3}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 ml-auto col-12">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAboutUs.introStat4 !== '' &&
                            <p className="col-12">{page.acfAboutUs.introStat4}</p>
                          }
                          <div className="scrollElementOffset font font4"> <span className="value value4">{page.acfAboutUs.figureStat4}</span>{page.acfAboutUs.symbolStat4}</div>
                          <p className="col-12">{page.acfAboutUs.signoffStat4}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-12 mr-auto">
                      <div className="row">
                        <div className="col-12  my-5">
                          {page.acfAboutUs.introStat5 !== '' &&
                            <p className="col-12">{page.acfAboutUs.introStat5}</p>
                          }
                          <div className="scrollElementOffsetThird font font5"> <span className="value value5">{page.acfAboutUs.figureStat5}</span>{page.acfAboutUs.symbolStat5}</div>
                          <p className="col-12">{page.acfAboutUs.signoffStat5}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  {page.acfAboutUs.aboutStatsCta &&
                    <div className="row">
                      <div className="col text-center m-auto">
                        <a className="btn btn-primary bronze  m-auto" href={page.acfAboutUs.aboutStatsCta.url}>{page.acfAboutUs.aboutStatsCta.title}</a>

                      </div>
                    </div>
                  }
                </div>
              </div>
            </div>
            <div className="wrapper row big_image ">
              <div className="background_image">
                <Img fluid={page.acfAboutUs.fullWidthImageAbout.localFile.childImageSharp.fluid} alt={page.acfAboutUs.fullWidthImageAbout.altText} />

              </div>
              <div className="scrollElement foreground_text">
                <h1>{page.acfAboutUs.fullWidthImageTitleAbout}</h1>
              </div>
            </div>
            <div className="wrapper row my-2">
              <div className="container">
                <div className="row">
                  <div className="col-md-6 col-12 py-5 ">
                    <div className="row ">
                      <div className="scrollElement col-12 col-md-8  small-font" dangerouslySetInnerHTML={{ __html: page.acfAboutUs.reputationSectionLeftContent }} />

                    </div>
                  </div>
                  <div className="col-md-6 col-12 rep">
                    <Img fluid={page.acfAboutUs.reputationSectionRightImage.localFile.childImageSharp.fluid} alt={page.acfAboutUs.reputationSectionRightImage.altText} />

                  </div>
                </div>
              </div>
            </div>
            <div className="wrapper row testimonial shield mt-5 mb-5 justify-content-center align-items-center">
              <div className="scrollElement col-9  p-5 m-auto text-center">
                <h2>“{page.acfAboutUs.aboutUsQuote}”</h2>
              </div>
            </div>
            <div className="wrapper row intro-glion remove py-md-5">
              <div className="col-12 col-md-10 m-auto">
                <div className="row py-5">
                  <div className="col-12 pt-5">
                    <h1>{page.acfAboutUs.careerSectionTitle}</h1>
                  </div>
                </div>
                <div className=" row offset fix">
                  <div className="col-12 col-md-8 d-flex justify-content-center align-items-center">
                    <div className="row mt-md-5 mb-5">
                      <div className="scrollElement col-12 col-md-8 fix" dangerouslySetInnerHTML={{ __html: page.acfAboutUs.careerSectionContent }} />

                    </div>
                  </div>
                  <div className="col-12 col-md-4">
                    <Img fluid={page.acfAboutUs.careerSectionImage.localFile.childImageSharp.fluid} alt={page.acfAboutUs.careerSectionImage.altText} />

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container mt-5">
            <div className="wrapper row  pt-5">
              <div className=" col-12 col-md-5 m-auto" dangerouslySetInnerHTML={{ __html: page.acfAboutUs.learnMoreIntro }} />


            </div>
            <div className="row my-5 alumni">
              <div id="alumni-carousel" className="wrapper carousel slide w-100" data-ride="carousel" data-interval="false">
                <ol className="carousel-indicators d-flex d-md-none">
                  <li data-target="#alumni-carousel" data-slide-to="0" className="active"></li>
                  <li data-target="#alumni-carousel" data-slide-to="1"></li>
                  <li data-target="#alumni-carousel" data-slide-to="2"></li>

                </ol>
                <div className="carousel-inner row w-100 mx-auto" role="listbox">
                  <div className="scrollElement carousel-item item col-12 col-md-4  p-0  active">
                    <div className="d-flex flex-column" >
                      <div className="row">
                        <div className="col-12">
                          <Img fluid={page.acfAboutUs.learnMoreImageCol1.localFile.childImageSharp.fluid} alt={page.acfAboutUs.learnMoreImageCol1.altText} />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-10 py-5 m-auto text-center">
                          <span dangerouslySetInnerHTML={{ __html: page.acfAboutUs.learnMoreContentCol1 }} />

                          <div className="row">
                            <div className="learn_more_container m-auto text-center">
                              <a className="learn_more" href={page.acfAboutUs.learnMoreLinkCol1.url}>{page.acfAboutUs.learnMoreLinkCol1.title}</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="scrollElementOffset carousel-item item col-12 col-md-4  p-0 ">
                    <div className="d-flex flex-column" >
                      <div className="row order-1 order-md-2">
                        <div className="col-12">
                          <Img fluid={page.acfAboutUs.learnMoreImageCol2.localFile.childImageSharp.fluid} alt={page.acfAboutUs.learnMoreImageCol2.altText} />
                        </div>
                      </div>
                      <div className="row order-2 order-md-1">
                        <div className="col-10 py-5 m-auto text-center">
                          <span dangerouslySetInnerHTML={{ __html: page.acfAboutUs.learnMoreContentCol2 }} />
                          <div className="row">
                            <div className="learn_more_container m-auto text-center">
                              <a className="learn_more" href={page.acfAboutUs.learnMoreLinkCol2.url}>{page.acfAboutUs.learnMoreLinkCol2.title}</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="scrollElementOffsetThird carousel-item item col-12 col-md-4  p-0 ">
                    <div className="d-flex flex-column" >
                      <div className="row ">
                        <div className="col-12">
                          <Img fluid={page.acfAboutUs.learnMoreImageCol3.localFile.childImageSharp.fluid} alt={page.acfAboutUs.learnMoreImageCol3.altText} />
                        </div>
                      </div>
                      <div className="row ">
                        <div className="col-10 py-5 m-auto text-center">
                          <span dangerouslySetInnerHTML={{ __html: page.acfAboutUs.learnMoreContentCol3 }} />
                          <div className="row">
                            <div className="learn_more_container m-auto text-center">
                              <a className="learn_more" href={page.acfAboutUs.learnMoreLinkCol3.url}>{page.acfAboutUs.learnMoreLinkCol3.title}</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a className="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true"></span>
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <h3 className="text-center">{page.language.slug === "en" ? "PROGRAMS" : "PROGRAMMES"}</h3>
              </div>
            </div>
            {page.language.slug === "en" &&
              <HomePrograms

                program_header=""
                program_1_tab_title={homePage.acfHome.program1TabTitle}
                program_1_title={homePage.acfHome.program1Title}
                program_1_details={homePage.acfHome.program1Details}
                program_1_image={homePage.acfHome.program1Image}
                program_1_link={homePage.acfHome.program1Link}
                program_2_tab_title={homePage.acfHome.program2TabTitle}
                program_2_title={homePage.acfHome.program2Title}
                program_2_details={homePage.acfHome.program2Details}
                // program_2_link={'Add title here'}
                program_2_link={homePage.acfHome.program2Link}
                program_2_block_1_title={homePage.acfHome.program2Block1Title}
                program_2_block_1_text={homePage.acfHome.program2Block1Text}
                program_2_block_1_link={homePage.acfHome.testingNewLink11}
                program_2_block_2_title={homePage.acfHome.program2Block2Title}
                program_2_block_2_link={homePage.acfHome.testingNewLink22}
                program_2_block_2_text={homePage.acfHome.program2Block2Text}
                program_2_block_3_title={homePage.acfHome.program2Block3Title}
                program_2_block_3_text={homePage.acfHome.program2Block3Text}
                program_2_block_3_link={homePage.acfHome.testingNewLink33}
                program_2_block_4_title={homePage.acfHome.program2Block4Title}
                program_2_block_4_text={homePage.acfHome.program2Block4Text}
                program_2_block_4_link={homePage.acfHome.testingNewLink44}
              />
            }
            {page.language.slug === "fr" &&
              <HomePrograms

                program_header=""
                program_1_tab_title={homePageFR.acfHome.program1TabTitle}
                program_1_title={homePageFR.acfHome.program1Title}
                program_1_details={homePageFR.acfHome.program1Details}
                program_1_image={homePageFR.acfHome.program1Image}
                program_1_link={homePageFR.acfHome.program1Link}
                program_2_tab_title={homePageFR.acfHome.program2TabTitle}
                program_2_title={homePageFR.acfHome.program2Title}
                program_2_details={homePageFR.acfHome.program2Details}
                // program_2_link={'Add title here'}
                program_2_link={homePageFR.acfHome.program2Link}
                program_2_block_1_title={homePageFR.acfHome.program2Block1Title}
                program_2_block_1_text={homePageFR.acfHome.program2Block1Text}
                program_2_block_1_link={homePageFR.acfHome.testingNewLink11}
                program_2_block_2_title={homePageFR.acfHome.program2Block2Title}
                program_2_block_2_link={homePageFR.acfHome.testingNewLink22}
                program_2_block_2_text={homePageFR.acfHome.program2Block2Text}
                program_2_block_3_title={homePageFR.acfHome.program2Block3Title}
                program_2_block_3_text={homePageFR.acfHome.program2Block3Text}
                program_2_block_3_link={homePageFR.acfHome.testingNewLink33}
                program_2_block_4_title={homePageFR.acfHome.program2Block4Title}
                program_2_block_4_text={homePageFR.acfHome.program2Block4Text}
                program_2_block_4_link={homePageFR.acfHome.testingNewLink44}
              />
            }
            {page.language.slug === "en" &&
              <CampusLocations
                main_campus_title={homePage.acfHome.mainCampusTitle}
                campus_1_image={homePage.acfHome.campus1Image}
                campus_1_title={homePage.acfHome.campus1Title}
                campus_1_location={homePage.acfHome.campus1Location}
                campus_1_link={homePage.acfHome.campus1LinkHomee}
                campus_2_image={homePage.acfHome.campus2Image}
                campus_2_title={homePage.acfHome.campus2Title}
                campus_2_location={homePage.acfHome.campus2Location}
                campus_2_link={homePage.acfHome.campus2LinkHomee}
                show_campuses={homePage.acfHome.showCampuses}
              />
            }
            {page.language.slug === "fr" &&
              <CampusLocations
                main_campus_title={homePageFR.acfHome.mainCampusTitle}
                campus_1_image={homePageFR.acfHome.campus1Image}
                campus_1_title={homePageFR.acfHome.campus1Title}
                campus_1_location={homePageFR.acfHome.campus1Location}
                campus_1_link={homePageFR.acfHome.campus1LinkHomee}
                campus_2_image={homePageFR.acfHome.campus2Image}
                campus_2_title={homePageFR.acfHome.campus2Title}
                campus_2_location={homePageFR.acfHome.campus2Location}
                campus_2_link={homePageFR.acfHome.campus2LinkHomee}
                show_campuses={homePageFR.acfHome.showCampuses}
              />
            }
          </div>
        </div>
      </Layout >
    )
  }
}
export default AboutPage;


export const pageQuery = graphql`
        query($id: String!) {
            page:  wpPage(id: { eq: $id }) {

        title
        content
        isFrontPage
        language {
          slug
        }
        translations {
          title
          link
       
        }
        uri
        author {
          node {
            name
          }
        }
        seo {
          title
          metaDesc
        
        }
     
        acfAboutUs {
          aboutStatsCta {
            title
            url
          }
        aboutUsQuote
        careerSectionContent
        careerSectionTitle
        fieldGroupName
        figureStat1
        figureStat2
        figureStat3
        figureStat4
        figureStat5
        fullWidthImageTitleAbout
        glionIntroText
        glionIntroTitle
        heroSectionAboutTitle
        introStat1
        introStat2
        introStat3
        introStat4
        introStat5
        learnMoreContentCol1
        learnMoreContentCol2
        learnMoreContentCol3
        learnMoreIntro
        reputationSectionLeftContent
        signoffStat1
        signoffStat2
        signoffStat3
        signoffStat4
        signoffStat5
        symbolStat1
        symbolStat2
        symbolStat3
        symbolStat4
        symbolStat5
        careerSectionImage {
      altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
        fullWidthImageAbout {
      altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 2500, webpQuality: 90) {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
        glionIntroImage {
      altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
        heroSectionAboutImage {
          mediaDetails{
            width
            height
          }
          sourceUrl
      altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 1500, webpQuality: 80) {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
        learnMoreImageCol1 {
      altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 500, webpQuality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
        learnMoreImageCol2 {
      altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 500, webpQuality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
        learnMoreImageCol3 {
      altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 500, webpQuality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
        learnMoreLinkCol1 {
          title
          url
        }
        learnMoreLinkCol2 {
          title
          url
        }
        learnMoreLinkCol3 {
          title
          url
        }
        reputationSectionRightImage {
      altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
                
              }
            } 
            publicURL  
          }
      
    }
      }    
    }
    homePageFR:  wpPage(id: { eq: "cG9zdDo0NTQ5" }) {

      acfHome {
          programHeader
        program1TabTitle
        program1Title
        program1Details
        program1Image { 
          id
          altText
          localFile {
            id
            childImageSharp {
                fluid(maxWidth: 1366, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
                base64
                tracedSVG
                srcWebp
                srcSetWebp
                originalImg
                originalName
              }
            }
          }
        }
        program1Link {
          target
          title
          url
        }
        program2TabTitle
        program2Title
        program2Details

        program2Block1Title
        program2Block1Text
        testingNewLink11 {
          target
          title
          url
        }
        program2Block2Title
        program2Block2Text
        testingNewLink22 {
          target
          title
          url
        }
        program2Block3Title
        program2Block3Text
        testingNewLink33 {
          target
          title
          url
        }
        program2Block4Title
        program2Block4Text
        testingNewLink44 {
          target
          title
          url
        }
        mainCampusTitle
      campus1Image{
        id
        altText
        localFile {
          id
          childImageSharp {
            fluid(maxWidth: 1366, quality: 100) {
              ...GatsbyImageSharpFluid_withWebp
              ...GatsbyImageSharpFluid
              base64
              tracedSVG
              srcWebp
              srcSetWebp
              originalImg
              originalName
            }
          }
        }
      }
      campus1Title
      campus1Location
      campus1LinkHomee{
        target
        title
        url
      }
      campus2Image{
        id
        altText
        localFile {
          id
          childImageSharp {
            fluid(maxWidth: 1366, quality: 100) {
              ...GatsbyImageSharpFluid_withWebp
              ...GatsbyImageSharpFluid
              base64
              tracedSVG
              srcWebp
              srcSetWebp
              originalImg
              originalName
            }
          }
        }
      }
      campus2Title
      campus2Location
      campus2LinkHomee{
        target
        title
        url
      }
       
      }
    }
      homePage:  wpPage(id: { eq: "cG9zdDoyMw==" }) {

        acfHome {
            programHeader
          program1TabTitle
          program1Title
          program1Details
          program1Image { 
            id
            altText
            localFile {
              id
              childImageSharp {
                  fluid(maxWidth: 1366, quality: 100) {
                  ...GatsbyImageSharpFluid_withWebp
                  ...GatsbyImageSharpFluid
                  base64
                  tracedSVG
                  srcWebp
                  srcSetWebp
                  originalImg
                  originalName
                }
              }
            }
          }
          program1Link {
            target
            title
            url
          }
          program2TabTitle
          program2Title
          program2Details
  
          program2Block1Title
          program2Block1Text
          testingNewLink11 {
            target
            title
            url
          }
          program2Block2Title
          program2Block2Text
          testingNewLink22 {
            target
            title
            url
          }
          program2Block3Title
          program2Block3Text
          testingNewLink33 {
            target
            title
            url
          }
          program2Block4Title
          program2Block4Text
          testingNewLink44 {
            target
            title
            url
          }
          mainCampusTitle
        campus1Image{
          id
          altText
          localFile {
            id
            childImageSharp {
              fluid(maxWidth: 1366, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
                base64
                tracedSVG
                srcWebp
                srcSetWebp
                originalImg
                originalName
              }
            }
          }
        }
        campus1Title
        campus1Location
        campus1LinkHomee{
          target
          title
          url
        }
        campus2Image{
          id
          altText
          localFile {
            id
            childImageSharp {
              fluid(maxWidth: 1366, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                ...GatsbyImageSharpFluid
                base64
                tracedSVG
                srcWebp
                srcSetWebp
                originalImg
                originalName
              }
            }
          }
        }
        campus2Title
        campus2Location
        campus2LinkHomee{
          target
          title
          url
        }
         
        }
      }
      
    }

    `
