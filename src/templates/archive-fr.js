/* eslint-disable react/no-danger */
import React from 'react';
import { Link, graphql } from 'gatsby';
import Helmet from "react-helmet"
import Img from "gatsby-image"
import Layout from '../components/layout';
// import Masthead from '../components/masthead'
import '../components/styles/blog.css';
import InsiderSVG from '../images/Insider-logo.png';
import BlogPagination from '../components/archive/BlogPagination';
import $ from "jquery"




const archiveTemplateFR = ({
  data: { allWpPost, blogPage },
  pageContext: {
    catName,
    catSlug,
    humanPageNumber,
    numberOfPages,
    description,
  },
}) => {
  function handleClick() {  
    var x = document.querySelector(".dropdown-menu");
    if (x.style.display === "none") {
      $('.dropdown').addClass('show');
      $('.dropdown-menu').show();
      $('.dropdown-menu').addClass('show');
    } else {
      $('.dropdown').removeClass('show');
      $('.dropdown-menu').hide();
      $('.dropdown-menu').removeClass('show');
    }
  }

  // console.log(allWpPost)
  return (

    <Layout polylang_translations={blogPage.translations} currentLang={blogPage.language.slug}  >
      

      <div id="content">
        <div className="container-fluid">
          <div className="container breacrumb-trail-container d-none d-md-block absolute archive">
            <div className="row">
              <div className="col">
                <ul id="breadcrumbs">
                  <li><a href="/fr/">PAGE D’ACCUEIL</a></li>
                  <li className="separator"> - </li>
                  <li><a href="/fr/magazine-fr/">Magazine</a></li>
                  <li className="separator"> - </li>
                  <li><u>CATÉGORIE: {catName}</u></li>
                </ul>
              </div>
            </div>
          </div>

          <div className="row magazine-header mb-4">
            <div className="col-12 col-md-6  mx-auto my-5">
              <Link to="/fr/magazine-fr/">
                <img className="blog-logo" src={InsiderSVG} style={{ maxWidth: "100%", height: "auto" }} />
              </Link>
              <div className="col-12 mr-auto d-flex align-items-center text-md-left text-center">
                <p className='text-center col'>le magazine de Glion Institut <br class="d-md-none d-block" /> de Hautes Études</p>
              </div>
            </div>

          </div>

        </div>

        <div class="magazine-header-section">
          <div className="container">
            <div className="row magazine-header d-md-flex d-none">
              <div className="col-12 p-0">
                <ul className="mag_filter mb-0">
                  <li><a className={catName === 'Les voix du leadership' ? 'active' : ''} href="/fr/magazine/category/les-voix-du-leadership/">Les voix du leadership</a></li>
                  <li><a className={catName === 'Au cœur de l’hospitality' ? 'active' : ''} href="/fr/magazine/category/au-coeur-de-lhospitality/">Au cœur de l’hospitality</a></li>
                  <li><a className={catName === 'Nouvelle vision du luxe' ? 'active' : ''} href="/fr/magazine/category/nouvelle-vision-du-luxe/">Nouvelle vision du luxe</a></li>
                  <li><a className={catName === 'Hédonisme' ? 'active' : ''} href="/fr/magazine/category/hedonisme/">Hédonisme</a></li>
                </ul>
              </div>
            </div>

            <div class="row magazine-header-mob d-md-none d-flex justify-content-center ">
              <div class="col-12">
                <p class="text-center">Activer les filtres:</p>
              </div>
              <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" onClick={handleClick}>
                  {catName}
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item " href="/fr/magazine-fr/">Toutes les publications</a>
                  <a class={`dropdown-item ${catName === 'Les voix du leadership' ? 'active' : ''}`} href="/fr/magazine/category/les-voix-du-leadership/">Les voix du leadership</a>
                  <a class={`dropdown-item ${catName === 'Au cœur de l’hospitality' ? 'active' : ''}`} href="/fr/magazine/category/au-coeur-de-lhospitality/">Au cœur de l’hospitality</a>
                  <a class={`dropdown-item ${catName === 'Nouvelle vision du luxe' ? 'active' : ''}`} href="/fr/magazine/category/nouvelle-vision-du-luxe/">Nouvelle vision du luxe</a>
                  <a class={`dropdown-item ${catName === 'Hédonisme' ? 'active' : ''}`} href="/fr/magazine/category/hedonisme/">Hédonisme</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container category-title mt-5 pt-4">
          <div className="row magazine-header mb-5">
            <div className="col-12 col-md-8 mb-5 m-auto">
              <h2 className="text-center mx-auto pb-0" style={{borderBottom: '3px solid #9E785B', width: 'max-content'}} dangerouslySetInnerHTML={{ __html: catName }} />
              {/* <div className="taxonomy-description text-center" dangerouslySetInnerHTML={{ __html: description }}></div> */}
            </div>
          </div>
        </div>
        <div className="container mt-0 archive-news overflow-hidden">
          <div className="row latest-news">
            <div className="col-12 col-md-12 m-auto">
              <div className="row ">
                {allWpPost.edges.map(post => (
                  <div className="blogPost col-12 col-md-6 item mb-5">
                    <div className="row ">
                      <div className="col-12 d-flex col-md-10 ml-auto mr-auto ">
                        <div className="row mt-auto mb-0 line-after archive-post">
                          <Link className="w-100" to={post.node.link} >
                            <div className="image-container">
                              {/* <Img fluid={post.node.featuredImage.node.localFile.childImageSharp.fluid} alt={post.node.featuredImage.node.altText} /> */}
                              <img src={post.node.featuredImage.node.localFile.publicURL} alt={post.node.featuredImage.node.altText} />
                              <div className="image-overlay"></div>
                            </div>
                          </Link>
                        </div>
                        <div className="row">
                          <div className="absolute-excerpt col-12 ml-md-auto text-center">
                            <p className="category mt-3" dangerouslySetInnerHTML={{ __html: catName }} />
                            <p class="post-date text-capitalize d-md-none d-block"><span>{post.node.date.replace(/,/g, "")}</span></p>
                            <Link to={post.node.link} ><h3 className="mt-3" dangerouslySetInnerHTML={{ __html: post.node.title }} /></Link>
                            <p class="post-date text-capitalize d-md-block d-none"><span>{post.node.date.replace(/,/g, "")}</span></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
                <BlogPagination
                  catSlug={catSlug}
                  page={humanPageNumber}
                  totalPages={numberOfPages}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
};

export default archiveTemplateFR;

export const pageQuery = graphql`
  query($catName: String!, $skip: Int!, $limit: Int!) {
    allWpPost(
      filter: {categories: {nodes: {elemMatch: {name:  { eq: $catName  } }}}} 
      skip: $skip
      limit: $limit
    ) {
      edges {
        node {
          id
          title
          excerpt
          slug
          link
          categories {
            nodes {
              id
              name
            }
          }
          date(formatString: "DD, MMM, YYYY", locale: "fr")
          featuredImage {
            node {
              altText
              localFile {
                publicURL
                id
                childImageSharp{
                  fluid{
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
                publicURL
              }
            }
          }
        }
      }
      
    }
    blogPage: wpPage(id: { eq: "cG9zdDo1MDk2Mg==" }) {
      language {
        slug
      }
      translations {
        title
        link
      
      }
      seo {
        title
        metaDesc
      }
    }
   
  }
`;

