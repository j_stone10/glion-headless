import React, { Component } from 'react'
import Img from "gatsby-image"
import { Link, graphql } from 'gatsby';
import SEO from '../components/seo';
import Layout from '../components/layout'
import Helmet from "react-helmet"
import BrochureDownload from '../components/BrochureDownload'
import '../components/styles/shield.css';




class VisitCampus extends Component {
   render() {
      const page = this.props.data.page
      //   const faculty = this.props.data.allWordpressAcfFaculty
      //   const media = this.props.data.allWpMediaItem.edges
      //   console.log(faculty)

      console.log(page)
      return (
         <Layout polylang_translations={page.translations} currentLang={page.language.slug}>
           
            <SEO title={page.seo.title} lang={page.language.slug} description={page.seo.metaDesc} image={page.acfVisitCampus.campusHero} uri={page.uri} author={page.author} />
            <div id="content">
               <div className="container-fluid">
                  <div className="row campus hero">
                     <div className="background_image">
                        <Img fluid={page.acfVisitCampus.campusHero.localFile.childImageSharp.fluid} alt={page.acfVisitCampus.campusHero.altText} />

                     </div>
                     <div className="scrollElementStart foreground_text col-md-9 col-11 m-auto">
                        <h1 className="mb-3" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.campusHeroTitle }} />
                     </div>
                  </div>
                  <div className="wrapper row white-strip mt-5 justify-content-center align-items-center">
                     <div className="scrollElement col-10 col-md-9  m-auto text-center">
                        <h2 className="mb-0">Which campus would you like to visit?</h2>
                     </div>
                  </div>
                  <div className=" row block-tabarea-02-2tabs pb-5">
                     <ul className=" nav nav-tabs m-auto home-programmes two-tabs d-flex" role="tablist">
                        <li className="nav-item col ml-auto">
                           <button className="mt-5  btn btn-primary active mr-5" href="#tabarea-02-2tabs-tab1" role="tab" data-toggle="tab">Switzerland</button>
                        </li>
                        <li className="nav-item col mr-auto">

                           <button className="mt-5 btn btn-primary" href="#tabarea-02-2tabs-tab2" role="tab" data-toggle="tab">London</button>
                        </li>
                     </ul>
                  </div>

                  <div className="row">
                     <div className=" tab-content module-tabs-content" style={{ overflow: "hidden" }}>

                        <div role="tabpanel" className="tab-pane visit-campus flex-tabs w-100 pb-5 active" id="tabarea-02-2tabs-tab1">
                           <div className="wrapper row">
                              <div className="col-md-6 col-12 image fill p-0 order-2 order-md-1">
                                 <Img fluid={page.acfVisitCampus.visitACampusTabs.campusTab1.leftImageBlock1.localFile.childImageSharp.fluid} alt={page.acfVisitCampus.visitACampusTabs.campusTab1.leftImageBlock1.altText} />

                              </div>
                              <div className=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
                                 <div className="scrollElement row program-text individual mr-md-0">
                                    <div className="col-xl-9 col-lg-11 col-11 m-auto p-5" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab1.upcomingOpenDays }} />

                                 </div>
                              </div>
                           </div>
                           <div className="wrapper row hero ">
                              <div className="background_image">
                                 <Img fluid={page.acfVisitCampus.visitACampusTabs.campusTab1.openDayImageUpdate.localFile.childImageSharp.fluid} alt={page.acfVisitCampus.visitACampusTabs.campusTab1.openDayImageUpdate.altText} />


                              </div>
                              <div className="scrollElement foreground_text col-md-9 col-11 m-auto about-us" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab1.alternativeDaysContent }} />


                           </div>
                           <div className="wrapper row">
                              <div className=" col-md-6 col-12 pr-md-0 d-flex   blue-bg">
                                 <div className="scrollElement row program-text individual mr-md-0">
                                    <div className="col-xl-9 col-lg-11 col-11 m-auto p-5" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab1.whatToExpectContent }} />


                                 </div>
                              </div>
                              <div className="col-md-6 col-12 image fill p-0 order-2 order-md-1">
                                 <Img fluid={page.acfVisitCampus.visitACampusTabs.campusTab1.rightImageUpdate.localFile.childImageSharp.fluid} alt={page.acfVisitCampus.visitACampusTabs.campusTab1.rightImageUpdate.altText} />

                              </div>
                           </div>
                           <div className="wrapper row">
                              <div className="col-md-6 col-12 image fill p-0" style={{ paddingBottom: "50%" }}>
                                 <div className="overlay"></div>
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2749.8332857024384!2d6.919475915804299!3d46.43218387635199!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478e9afcb5ec5c97%3A0xfe2cb1a9ab8f3f17!2sGlion%20Institute%20of%20Higher%20Education!5e0!3m2!1sen!2suk!4v1595924958822!5m2!1sen!2suk" width="1200" height="1200" frameborder="0" style={{ border: "0" }} allowfullscreen="" aria-hidden="false" tabIndex="0"></iframe>
                              </div>
                              <div className=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
                                 <div className="scrollElement row program-text individual mr-md-0">
                                    <div className="col-xl-9 col-lg-11 col-11 m-auto p-5" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab1.meetingPointContent }} />
                                 </div>
                              </div>
                           </div>
                           {/* <div className="row mt-1 blue-bg  py-5">
                              <div className="col-md-5 col-12 p-5">
                                 <h2 className="text-center text-md-left">{
                                    page.acfVisitCampus.visitACampusTabs.campusTab1.title}
                                 </h2>
                              </div>
                              <div className="col-md-7 col-12 p-5">
                                 <div className="col-md-7 col-12 p-5">


                                 </div>
                              </div>
                           </div> */}
                           <BrochureDownload
                              title={page.acfVisitCampus.visitACampusTabs.campusTab1.title}
                              formCode={page.acfVisitCampus.visitACampusTabs.campusTab1.marketoFormCode}
                              formId={page.acfVisitCampus.visitACampusTabs.campusTab1.formId}
                              lang={page.language.slug}
                           />
                           {page.acfVisitCampus.visitACampusTabs.campusTab1.accordionRow !== null && page.acfVisitCampus.visitACampusTabs.campusTab1.hideFaqSection !== true &&
                              <>
                                 <div className="wrapper row my-5 white-strip my justify-content-center align-items-center">
                                    <div className="scrollElement my-5 col-10 col-md-9 m-auto text-center">
                                       <h2>Frequently asked questions</h2>
                                    </div>
                                 </div>

                                 <div className="wrapper row">
                                    {page.acfVisitCampus.visitACampusTabs.campusTab1.accordionRow.map((item, i) => (
                                       <div className="col-12 p-0 accordion" key={i}>
                                          {(() => {
                                             return (
                                                <div className="col-12 p-0 accordion">
                                                   <h6 className="accordion_title_black_text">{item.title}</h6>
                                                   <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" alt="toggle" /></div>
                                                   <div className="accordion_text">
                                                      {item.number_acc_columns === 'Single column' &&
                                                         <div className="row ">
                                                            <div className="col-12" dangerouslySetInnerHTML={{ __html: item.content_left }} />


                                                         </div>
                                                      }
                                                      {item.number_acc_columns === 'Two columns' &&
                                                         <div className="row">
                                                            <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: item.content_left }} />
                                                            <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: item.content_right }} />

                                                         </div>
                                                      }
                                                   </div>
                                                </div>
                                             )
                                          }
                                             //}
                                          )()}
                                       </div>
                                    ))}


                                 </div>
                              </>
                           }
                        </div>



                        <div role="tabpanel" className="tab-pane visit-campus  flex-tabs w-100 pb-5" id="tabarea-02-2tabs-tab2">
                           <div className="wrapper row">
                              <div className="col-md-6 col-12 image fill p-0 order-2 order-md-1">
                                 <Img fluid={page.acfVisitCampus.visitACampusTabs.campusTab2.leftImage.localFile.childImageSharp.fluid} alt={page.acfVisitCampus.visitACampusTabs.campusTab2.leftImage.altText} />


                              </div>
                              <div className=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
                                 <div className="scrollElement row program-text individual mr-md-0">
                                    <div className="col-xl-9 col-lg-11 col-11 m-auto p-5" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab2.upcomingOpenDays }} />
                                 </div>
                              </div>
                           </div>
                           <div className="wrapper row hero ">
                              <div className="background_image">
                                 <Img fluid={page.acfVisitCampus.visitACampusTabs.campusTab2.openDayImage.localFile.childImageSharp.fluid} alt={page.acfVisitCampus.visitACampusTabs.campusTab2.openDayImage.altText} />


                              </div>
                              <div className="scrollElement foreground_text col-md-9 col-11 m-auto about-us" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab2.alternativeDaysContent }} />
                           </div>
                           <div className="wrapper row">
                              <div className=" col-md-6 col-12 pr-md-0 d-flex   blue-bg">
                                 <div className="scrollElement row program-text individual mr-md-0">
                                    <div className="col-xl-9 col-lg-11 col-11 m-auto p-5" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab2.whatToExpectContent }} />
                                 </div>
                              </div>
                              <div className="col-md-6 col-12 image fill p-0 order-2 order-md-1">
                                 <Img fluid={page.acfVisitCampus.visitACampusTabs.campusTab2.rightImage.localFile.childImageSharp.fluid} alt={page.acfVisitCampus.visitACampusTabs.campusTab2.rightImage.altText} />

                              </div>
                           </div>
                           <div className="wrapper row">
                              <div className="col-md-6 col-12 image fill p-0" style={{ paddingBottom: "50%" }}>
                                 <div className="overlay"></div>
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2486.2975132589345!2d-0.24635758406445235!3d51.45269382252235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760edc3a646d13%3A0x73f596efd4f47d6b!2sGlion%20Institute%20of%20Higher%20Education%2C%20London!5e0!3m2!1sen!2suk!4v1595924860194!5m2!1sen!2suk" width="1200" height="1200" frameborder="0" style={{ border: "0" }} allowfullscreen="" aria-hidden="false" tabIndex="0"></iframe>
                              </div>
                              <div className=" col-md-6 col-12 pr-md-0 d-flex   beige-bg">
                                 <div className="scrollElement row program-text individual mr-md-0">
                                    <div className="col-xl-9 col-lg-11 col-11 m-auto p-5" dangerouslySetInnerHTML={{ __html: page.acfVisitCampus.visitACampusTabs.campusTab2.meetingPointContent }} />


                                 </div>
                              </div>
                           </div>
                           {/* <div className="row mt-1 blue-bg  py-5">
                                 <div className="col-md-5 col-12 p-5">
                                    <h2 className="text-center text-md-left">
                                       {page.acfVisitCampus.visitACampusTabs.campusTab2.title}
                                    </h2>
                                 </div>
                                 <div className="col-md-7 col-12 p-5">
                                    <BrochureDownload

                                       formCode={page.acfVisitCampus.visitACampusTabs.campusTab2.marketoFormCode}
                                       formId={page.acfVisitCampus.visitACampusTabs.campusTab2.formId}
                                       lang={page.language.slug}
                                    />

                                 </div>
                              </div> */}

                           <BrochureDownload
                              title={page.acfVisitCampus.visitACampusTabs.campusTab2.title}
                              formCode={page.acfVisitCampus.visitACampusTabs.campusTab2.marketoFormCode}
                              formId={page.acfVisitCampus.visitACampusTabs.campusTab2.formId}
                              lang={page.language.slug}
                           />
                           {page.acfVisitCampus.visitACampusTabs.campusTab2.accordionRow &&
                              <div className="wrapper row my-5 white-strip my justify-content-center align-items-center">
                                 <div className="scrollElement my-5 col-10 col-md-9 m-auto text-center">
                                    <h2>Frequently asked questions</h2>
                                 </div>
                              </div>
                           }
                           <div className="wrapper row">
                              {page.acfVisitCampus.visitACampusTabs.campusTab2.accordionRow !== null && page.acfVisitCampus.visitACampusTabs.campusTab2.hideFaqSection !== true && page.acfVisitCampus.visitACampusTabs.campusTab2.accordionRow.map((item, i) => (
                                 <div className="col-12 p-0 accordion" key={i}>
                                    {(() => {
                                       return (
                                          <div className="col-12 p-0 accordion">
                                             <h6 className="accordion_title_black_text">{item.title}</h6>
                                             <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" alt="toggle" /></div>
                                             <div className="accordion_text">
                                                {item.number_acc_columns === 'Single column' &&
                                                   <div className="row ">
                                                      <div className="col-12" dangerouslySetInnerHTML={{ __html: item.content_left }} />


                                                   </div>
                                                }
                                                {item.number_acc_columns === 'Two columns' &&
                                                   <div className="row">
                                                      <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: item.content_left }} />
                                                      <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: item.content_right }} />

                                                   </div>
                                                }
                                             </div>
                                          </div>
                                       )
                                    }
                                       //}
                                    )()}
                                 </div>
                              ))}
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </Layout >
      )
   }
}



export default VisitCampus;


export const pageQuery = graphql`

    query($id: String!) {

               page:  wpPage(id: {eq: $id }) {
               title
          content
                      isFrontPage
          language {
               slug
            }
            translations {
               title
            link

          }
          uri
          author {
               node {
               name
            }
            }
          seo {
               title
            metaDesc

          }

          acfVisitCampus {
               campusHeroTitle
            campusHero {
               mediaDetails{
               width
                  height
}
sourceUrl
altText
               localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
       fieldGroupName
            visitACampusTabs {
               campusTab1 {
               title
                 marketoFormCode
       formId
hideFaqSection
                accordionRow {
               contentLeft
                  contentRight
numberAccColumns
title
}
alternativeDaysContent
                leftImage2Update {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
                leftImageBlock1 {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
                meetingPointContent
                openDayImageUpdate {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
                rightImageUpdate {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
                upcomingOpenDays
                whatToExpectContent
              }
              campusTab2 {
               hideFaqSection
               title
         formId
marketoFormCode
                accordionRow {
               contentLeft
                  contentRight
numberAccColumns
title
}
alternativeDaysContent
                leftImage {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
                leftImage2 {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
                meetingPointContent
                openDayImage {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          }
        }
                rightImage {
               altText
          localFile {
               childImageSharp {
               fluid {
               ...GatsbyImageSharpFluid_withWebp
            }
            }
            publicURL
          } 
        }
                upcomingOpenDays 
                whatToExpectContent
              }
            }
          }
          
        }    
    }
`