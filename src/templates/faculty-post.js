import React, { Component } from 'react'
import Helmet from "react-helmet"
import { graphql } from 'gatsby';
import SEO from '../components/seo';
import Img from "gatsby-image"
import Layout from '../components/layout'




class facultyPostTemplate extends Component {
  render() {
    const currentPage = this.props.data
    // console.log(currentPage)

    return (
      <Layout polylang_translations={currentPage.post.translations} currentLang={currentPage.post.language.slug}>
        
        <SEO title={currentPage.post.seo.title} lang={currentPage.post.language.slug} description={currentPage.post.seo.metaDesc} image={currentPage.post.acfFaculty.facultyImage} uri={currentPage.post.uri} author={currentPage.post.author} />
        <div id="content">
          <div className="container-fluid">
            <div id="alumni1" className="left_img_right_text alumni_profile">
              <div className="row mb-5">
                <div className="col-md-6 col-12  p-0  d-flex justify-content-center align-items-center grey-bg">
                  <div className=" col-9 m-auto text-center text-md-left">
                    <p dangerouslySetInnerHTML={{ __html: currentPage.post.acfFaculty.facultyContent }} />
                  </div>
                </div>
                <div className="col-md-6 col-12 p-0 d-md-flex  justify-content-center align-items-center">
                  {/* {currentPage.post.faculty_image &&
                    <img src={currentPage.post.acfFaculty.faculty_image.localFile.publicURL} alt={currentPage.post.acfFaculty.faculty_image.altText} />
                  } */}
                  {currentPage.post.acfFaculty.facultyImage &&
                    <Img fluid={currentPage.post.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={currentPage.post.acfFaculty.facultyImage.altText} />
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}



export default facultyPostTemplate;


export const pageQuery = graphql`
query($id: String!) {
  post: wpFaculty(id: {eq:  $id}) {

        date
        id

      language {
        slug
      }
      translations {
        title
        link
      
      }
      uri
    
      seo {
        title
        metaDesc
        
      }
            acfFaculty {
              facultyContent
              facultyImage {
                mediaDetails{
                  width
                  height
                }
                
                sourceUrl
                id
                altText
                localFile {
                  publicURL
                  childImageSharp{
                    fluid{
                      base64
                      ...GatsbyImageSharpFluid_withWebp
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              
            }
            
          
          }
        }
     
`