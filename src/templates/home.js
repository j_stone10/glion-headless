import React, { Component } from "react"
import Loadable from "react-loadable"
import Loading from "../components/LoadingIndicator"
import { Link, graphql } from "gatsby"
import Helmet from "react-helmet"
import Img from "gatsby-image"
import SEO from "../components/seo"
import Layout from "../components/layout"
import FixedCTAs from "../components/Fixed-ctas"
import Hero from "../components/Hero"
import HomePrograms from "../components/HomePrograms"
import ScrollMagic from "../components/ScrollMagic"
import TextRightImage from "../components/TextRightImage"
import CampusLocations from "../components/CampusLocations"
import Quote from "../components/Quote"
import OpenDays from "../components/OpenDays"
import facebook_logo from "../images/facebook.png"
import instagram_logo from "../images/instagram.png"
import youtube_logo from "../images/youtube.png"
import linkedin_logo from "../images/linkedin.png"
// import NewsSlider from '../components/NewsSlider'
// import SocialFeed from '../components/SocialFeed'

const SocialFeed = Loadable({
  loader: () => import("../components/SocialFeed"),
  loading: Loading,
})
// const NewsSlider = Loadable({
//   loader: () => import("../components/NewsSlider"),
//   loading: Loading,
// })
// const CampusLocations = Loadable({
//   loader: () => import('../components/CampusLocations'),
//   loading: Loading,
// });
// const Quote = Loadable({
//   loader: () => import('../components/Quote'),
//   loading: Loading,
// });
// const OpenDays = Loadable({
//   loader: () => import('../components/OpenDays'),
//   loading: Loading,
// });

class IndexPage extends Component {
  render() {
    const page = this.props.data.page
    const allWpPost = this.props.data.allWpPost
    const featuredWpPost1 = this.props.data.featuredWpPost1
    const featuredWpPost2 = this.props.data.featuredWpPost2
    const featuredWpPost3 = this.props.data.featuredWpPost3
    const featuredWpPost4 = this.props.data.featuredWpPost4
    const currentPage = this.props.data
    // if (page.
    // var newsPosts = ''
    // var pageCat = ''
    // if (page.acfHome.categorySelect !== null) {
    //   var pageCat = page.acfHome.categorySelect[0].name
    // }

    // console.log(allWpPost)

    // console.log(newsPosts)

    if (allWpPost.length > 5) {
      allWpPost.length = 5
    }

    var featuredWpPost1DisplayCat = featuredWpPost1.categories.nodes.filter(
      function(e) {
        return (
          e.name !== "Featured Priority 1" &&
          e.name !== "Featured Priority 2" &&
          e.name !== "Featured Priority 3" &&
          e.name !== "Featured Priority 4"
        )
      }
    )

    // console.log(featuredWpPost1DisplayCat);

    var featuredWpPost2DisplayCat = featuredWpPost2.categories.nodes.filter(
      function(e) {
        return (
          e.name !== "Featured Priority 1" &&
          e.name !== "Featured Priority 2" &&
          e.name !== "Featured Priority 3" &&
          e.name !== "Featured Priority 4"
        )
      }
    )

    // console.log(featuredWpPost2DisplayCat);

    var featuredWpPost3DisplayCat = featuredWpPost3.categories.nodes.filter(
      function(e) {
        return (
          e.name !== "Featured Priority 1" &&
          e.name !== "Featured Priority 2" &&
          e.name !== "Featured Priority 3" &&
          e.name !== "Featured Priority 4"
        )
      }
    )

    // console.log(featuredWpPost3DisplayCat);

    var featuredWpPost4DisplayCat = featuredWpPost4.categories.nodes.filter(
      function(e) {
        return (
          e.name !== "Featured Priority 1" &&
          e.name !== "Featured Priority 2" &&
          e.name !== "Featured Priority 3" &&
          e.name !== "Featured Priority 4"
        )
      }
    )

    // console.log(featuredWpPost4DisplayCat);

    return (
      <Layout
        polylang_translations={page.translations}
        currentLang={page.language.slug}
        frenchHome={page.link}
        showTopBannerCallout={page.acfHome.showTopBannerCallout}
        tbCalloutContent={page.acfHome.tbCalloutContent}
      >
        
        <SEO
          title={page.seo.title}
          lang={page.language.slug}
          description={page.seo.metaDesc}
          image={page.acfHome.heroImage}
          uri={page.uri}
          author={page.author}
        />
        <div id="content">
          <div
            className={`container-fluid ${page.acfHome.showTopBannerCallout ===
              true && " callout-adjust"} `}
          >
            <FixedCTAs currentLang={page.language.slug} />
            <Hero
              hero_image={page.acfHome.heroImage}
              hero_title={page.acfHome.heroTitle}
              hero_text={page.acfHome.heroText}
            ></Hero>

            <HomePrograms
              program_header={page.acfHome.programHeader}
              program_study_text={page.acfHome.programsHeaderRemoteStudyText}
              program_1_tab_title={page.acfHome.program1TabTitle}
              program_1_title={page.acfHome.program1Title}
              program_1_details={page.acfHome.program1Details}
              program_1_image={page.acfHome.program1Image}
              program_1_learning_overview_text={currentPage.internationalHospitalityBusinessPage.acfProgramFields.remoteLearningOverviewText}
              program_1_link={page.acfHome.program1Link}
              program_2_tab_title={page.acfHome.program2TabTitle}
              program_2_title={page.acfHome.program2Title}
              program_2_details={page.acfHome.program2Details}
              // program_2_link={'Add title here'}
              program_2_link={page.acfHome.program2Link}
              program_2_block_1_title={page.acfHome.program2Block1Title}
              program_2_block_1_text={page.acfHome.program2Block1Text}
              program_2_block_1_link={page.acfHome.testingNewLink11}
              program_2_block_2_title={page.acfHome.program2Block2Title}
              program_2_block_2_link={page.acfHome.testingNewLink22}
              program_2_block_2_text={page.acfHome.program2Block2Text}
              program_2_block_2_learning_overview_text={currentPage.realEstateFinanceAndHotelDevelopmentPage.acfProgramFields.remoteLearningOverviewText}
              program_2_block_3_title={page.acfHome.program2Block3Title}
              program_2_block_3_text={page.acfHome.program2Block3Text}
              program_2_block_3_learning_overview_text={currentPage.entrepreneurshipAndInnovationPage.acfProgramFields.remoteLearningOverviewText}
              program_2_block_3_link={page.acfHome.testingNewLink33}
              program_2_block_4_title={page.acfHome.program2Block4Title}
              program_2_block_4_text={page.acfHome.program2Block4Text}
              program_2_block_4_learning_overview_text={currentPage.luxuryManagementAndGuestExperiencePage.acfProgramFields.remoteLearningOverviewText}
              program_2_block_4_link={page.acfHome.testingNewLink44}
              batchelors_remote_learning_overview_text={currentPage.batchelorsPage.acfProgramFields.remoteLearningOverviewText}

            />

            <ScrollMagic
              image_mobile={page.acfHome.imageMobile}
              parallax_image_desktop={page.acfHome.parallaxImageDesktop}
              main_stats_title={page.acfHome.mainStatsTitle}
              stat_1_title={page.acfHome.stat1Title}
              stat_1_figure={page.acfHome.stat1Figure}
              stat_1_symbol_after={page.acfHome.stat1SymbolAfter}
              stat_1_sign_off={page.acfHome.stat1SignOff}
              stat_2_title={page.acfHome.stat2Title}
              stat_2_figure={page.acfHome.stat2Figure}
              stat_2_symbol_after={page.acfHome.stat2SymbolAfter}
              stat_2_sign_off={page.acfHome.stat2SignOff}
            />
            {page.acfHome.hideLeftRightSection !== true && (
              <TextRightImage
                title={page.acfHome.title}
                text={page.acfHome.text}
                image={page.acfHome.image}
                link={page.acfHome.leftRightLinkk}
              />
            )}

            <div className="row wrapper my-5" id="campusScrollInit">
              <div className=" scrollElement col-12">
                <h2 className="text-center">{page.acfHome.mainCampusTitle}</h2>
              </div>
            </div>

            <CampusLocations
              main_campus_title={page.acfHome.mainCampusTitle}
              campus_1_image={page.acfHome.campus1Image}
              campus_1_title={page.acfHome.campus1Title}
              campus_1_location={page.acfHome.campus1Location}
              campus_1_link={page.acfHome.campus1LinkHomee}
              campus_2_image={page.acfHome.campus2Image}
              campus_2_title={page.acfHome.campus2Title}
              campus_2_location={page.acfHome.campus2Location}
              campus_2_link={page.acfHome.campus2LinkHomee}
              show_campuses={page.acfHome.showCampuses}
            />
            <Quote
              quote_text={page.acfHome.quote1Text}
              classes="wrapper row d-none d-md-block testimonial mt-5 mb-5 justify-content-center align-items-center"
            />
            <OpenDays
              block_1_title={page.acfHome.block1Title}
              block_1_text={page.acfHome.block1Text}
              block_1_image={page.acfHome.block1Image}
              block_1_link={page.acfHome.block1LinkHomee}
              block_2_title={page.acfHome.block2Title}
              block_2_text={page.acfHome.block2Text}
              block_2_image={page.acfHome.block2Image}
              block_2_link={page.acfHome.block2LinkHomee}
            />

            {page.acfHome.categorySelect !== null &&
              page.language.slug === "en" && (
                <>
                  <div className="wrapper row">
                    <div className="scrollElement col-12 text-center">
                      <h2>Glion: Insider Magazine</h2>
                    </div>
                  </div>

                  <div className="row ">
                    <div className="blogPost col-12 p-0 d-flex justify-content-center">
                      <div className="line-after clear-mob w-md-100">
                        <div className="image_container">
                          <div className="overlay"></div>
                          <Img
                            fluid={
                              featuredWpPost1.blogHeroUpdated
                                .blogFeaturedHeroOverview.localFile
                                .childImageSharp.fluid
                            }
                            alt={
                              featuredWpPost1.blogHeroUpdated
                                .blogFeaturedHeroOverview.localFile.altText
                            }
                          />
                        </div>
                        <div className="col-10 py-5 m-auto text-center featuredCarousel d-block d-md-none">
                          <h6 className="text-center mb-2">
                            <Link to={featuredWpPost1.link}>
                              {featuredWpPost1.title}
                            </Link>
                          </h6>
                          <div className="row  mt-5">
                            <div className="learn_more_container m-auto text-center">
                              <a href="/magazine/history-revisited-how-georges-kern-is-transforming-breitling/">
                                {featuredWpPost1DisplayCat[0].name}
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row featured1Home d-none d-md-block">
                    <div className="col-12">
                      <h3 className="text-center mb-2">
                        <Link to={featuredWpPost1.link}>
                          {featuredWpPost1.title}
                        </Link>
                      </h3>
                      <span className="mag_span text-center mb-0">
                        <a
                          href={
                            "magazine/category/" +
                            featuredWpPost1DisplayCat[0].slug
                          }
                        >
                          {featuredWpPost1DisplayCat[0].name}
                        </a>
                      </span>
                    </div>
                  </div>

                  <div className="container-fluid p-0">
                    <div className="row my-md-5 alumni ">
                      <div
                        id="alumni-carousel"
                        className="wrapper carousel slide"
                        data-ride="carousel"
                        data-interval="false"
                      >
                        <ol className="carousel-indicators d-flex d-md-none">
                          <li
                            data-target="#alumni-carousel"
                            data-slide-to="0"
                            className="active"
                          ></li>
                          <li
                            data-target="#alumni-carousel"
                            data-slide-to="1"
                          ></li>
                          <li
                            data-target="#alumni-carousel"
                            data-slide-to="2"
                          ></li>
                        </ol>
                        <div
                          className="carousel-inner row w-100 mx-auto"
                          role="listbox"
                        >
                          <div className="carousel-item item col-12 col-md-4  p-0  active">
                            <div className="overlay"></div>
                            <div className="row ">
                              <div className="col-12">
                                <Img
                                  fluid={
                                    featuredWpPost2.featuredImage.node.localFile
                                      .childImageSharp.fluid
                                  }
                                  alt={
                                    featuredWpPost2.featuredImage.node.altText
                                  }
                                />
                              </div>
                            </div>
                            <div className="row d-flex  align-items-end">
                              <div className="col-10 py-5 m-auto text-center featuredCarousel">
                                <h6 className="text-center mb-2">
                                  <a href={featuredWpPost2.link}>
                                    {featuredWpPost2.title}
                                  </a>
                                </h6>
                                <div className="row  mt-5">
                                  <div className="learn_more_container m-auto text-center">
                                    <a
                                      href={
                                        "magazine/category/" +
                                        featuredWpPost2DisplayCat[0].slug
                                      }
                                    >
                                      {featuredWpPost2DisplayCat[0].name}
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="carousel-item item col-12 col-md-4  p-0  ">
                            <div className="overlay"></div>
                            <div className="row ">
                              <div className="col-12">
                                <Img
                                  fluid={
                                    featuredWpPost3.featuredImage.node.localFile
                                      .childImageSharp.fluid
                                  }
                                  alt={
                                    featuredWpPost3.featuredImage.node.altText
                                  }
                                />
                              </div>
                            </div>
                            <div className="row d-flex  align-items-end">
                              <div className="col-10 py-5 m-auto text-center featuredCarousel">
                                <h6 className="text-center mb-2">
                                  <a href={featuredWpPost3.link}>
                                    {featuredWpPost3.title}
                                  </a>
                                </h6>
                                <div className="row  mt-5">
                                  <div className="learn_more_container m-auto text-center">
                                    <a
                                      href={
                                        "magazine/category/" +
                                        featuredWpPost3DisplayCat[0].slug
                                      }
                                    >
                                      {featuredWpPost3DisplayCat[0].name}
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="carousel-item item col-12 col-md-4  p-0 ">
                            <div className="overlay"></div>
                            <div className="row ">
                              <div className="col-12">
                                <Img
                                  fluid={
                                    featuredWpPost4.featuredImage.node.localFile
                                      .childImageSharp.fluid
                                  }
                                  alt={
                                    featuredWpPost4.featuredImage.node.altText
                                  }
                                />
                              </div>
                            </div>
                            <div className="row d-flex  align-items-end">
                              <div className="col-10 py-5 m-auto text-center featuredCarousel">
                                <h6 className="text-center mb-2">
                                  <a href={featuredWpPost4.link}>
                                    {featuredWpPost4.title}
                                  </a>
                                </h6>
                                <div className="row  mt-5">
                                  <div className="learn_more_container m-auto text-center">
                                    <a
                                      href={
                                        "magazine/category/" +
                                        featuredWpPost4DisplayCat[0].slug
                                      }
                                    >
                                      {featuredWpPost4DisplayCat[0].name}
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <a
                          className="carousel-control-prev d-flex d-md-none"
                          href="#alumni-carousel"
                          role="button"
                          data-slide="prev"
                        >
                          <span
                            className="carousel-control-prev-icon"
                            aria-hidden="true"
                          ></span>
                          <span className="sr-only">Previous</span>
                        </a>
                        <a
                          className="carousel-control-next d-flex d-md-none"
                          href="#alumni-carousel"
                          role="button"
                          data-slide="next"
                        >
                          <span
                            className="carousel-control-next-icon"
                            aria-hidden="true"
                          ></span>
                          <span className="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                  </div>

                  {/* <NewsSlider newsPosts={allWpPost.edges} /> */}
                </>
              )}
            <Quote
              quote_text={page.acfHome.quote2Text}
              classes="wrapper row testimonial mt-5 mb-5 justify-content-center align-items-center testimonial shield"
            />

            <div className="wrapper row my-5">
              <div className="scrollElement col-12 text-center">
                <h2>#GlionSpirit</h2>
                <p className="follow-us">
                  {page.language.slug === "en" ? "FOLLOW US" : "SUIVEZ NOUS"}
                </p>
                <div className="row social_view">
                  <div className="col-12">
                    <a
                      target="_blank"
                      href="https://www.linkedin.com/school/glion-institute-of-higher-education/"
                    >
                      {" "}
                      <img src={linkedin_logo} alt="LinkedIn Logo" />
                    </a>
                    <a
                      target="_blank"
                      href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg"
                    >
                      {" "}
                      <img src={youtube_logo} alt="youtube Logo" />
                    </a>
                    <a
                      target="_blank"
                      href="https://www.facebook.com/glionswitzerland/"
                    >
                      {" "}
                      <img src={facebook_logo} alt="Facebook Logo" />
                    </a>
                    <a
                      target="_blank"
                      href="https://www.instagram.com/glionhospitalityschool/"
                    >
                      {" "}
                      <img src={instagram_logo} alt="Instagram Logo" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <SocialFeed
              currentLang={page.language.slug}
              code={page.acfHome.socialFeedCode}
            />
          </div>
        </div>
      </Layout>
    )
  }
}
export default IndexPage

export const pageQuery = graphql`
  query($id: String!) {
    page: wpPage(id: { eq: $id }) {
      title
      content
      link
      language {
        slug
      }
      translations {
        title
        link
      }
      uri
      author {
        node {
          name
        }
      }
      seo {
        title
        metaDesc
      }
      acfHome {
        categorySelect {
          name
        }
        showTopBannerCallout
        tbCalloutContent
        heroImage {
          mediaDetails {
            width
            height
          }
          sourceUrl
          altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 2000, webpQuality: 80) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        programsHeaderRemoteStudyText
        socialFeedCode
        heroText
        block1Text
        block1Title
        block2Text
        block2Title
        campus1Title
        campus2Location
        campus2Title
        fieldGroupName
        heroText
        heroTitle
        hideLeftRightSection
        mainCampusTitle
        mainStatsTitle
        program1Details
        program1TabTitle
        program1Title
        program2Block1Text
        program2Block1Title
        program2Block2Text
        program2Block2Title
        program2Block3Text
        program2Block3Title
        program2Block4Text
        program2Block4Title
        program2Details
        program2TabTitle
        program2Title
        programHeader
        quote1Text
        quote2Text
        socialFeedCode
        stat1Figure
        stat1SignOff
        stat1SymbolAfter
        stat1Title
        stat2Figure
        stat2SignOff
        stat2SymbolAfter
        stat2Title
        text
        title
        block1Image {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        block1LinkHomee {
          title
          url
        }
        block2Image {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        block2LinkHomee {
          title
          url
        }
        campus1Image {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        campus1ImageWide {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        campus1Location
        campus1LinkHomee {
          title
          url
        }
        campus2Image {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        campus2ImageWide {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        campus2LinkHomee {
          title
          url
        }
        image {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        imageMobile {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        leftRightLinkk {
          title
          url
        }
        parallaxImageDesktop {
          altText
          localFile {
            childImageSharp {
              fluid(maxWidth: 2000, webpQuality: 80) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        program1Image {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
        program1Link {
          title
          url
        }
        program2Link {
          title
          url
        }
        testingNewLink11 {
          title
          url
        }
        testingNewLink22 {
          title
          url
        }
        testingNewLink33 {
          title
          url
        }
        testingNewLink44 {
          title
          url
        }
      }
    }
    batchelorsPage: wpPage(id: {eq: "cG9zdDo0MTY5"}){
      acfProgramFields {
        remoteLearningOverviewText
      }
    }
    internationalHospitalityBusinessPage: wpPage(id: {eq: "cG9zdDoxNzQ4"}){
      acfProgramFields {
        remoteLearningOverviewText
      }
    }
    entrepreneurshipAndInnovationPage: wpPage(id: {eq: "cG9zdDoxNzY5"}){
      acfProgramFields {
        remoteLearningOverviewText
      }
    }
    realEstateFinanceAndHotelDevelopmentPage: wpPage(id: {eq: "cG9zdDoxNzU4"}){
      acfProgramFields {
        remoteLearningOverviewText
      }
    }
    luxuryManagementAndGuestExperiencePage: wpPage(id: {eq: "cG9zdDoxNzc3"}){
      acfProgramFields {
        remoteLearningOverviewText
      }
    }
    featuredWpPost1: wpPost(
      categories: {
        nodes: { elemMatch: { name: { eq: "Featured Priority 1" } } }
      }
    ) {
      id
      title
      excerpt
      slug
      link
      categories {
        nodes {
          id
          name
          slug
        }
      }
      blogHeroUpdated {
        blogFeaturedHeroOverview {
          date
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
      }
      date(formatString: "DD, MMM, YYYY")
      featuredImage {
        node {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
      }
    }
    featuredWpPost2: wpPost(
      categories: {
        nodes: { elemMatch: { name: { eq: "Featured Priority 2" } } }
      }
    ) {
      id
      title
      excerpt
      slug
      link
      categories {
        nodes {
          id
          name
          slug
        }
      }
      blogHeroUpdated {
        blogFeaturedHeroOverview {
          date
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
      }
      date(formatString: "DD, MMM, YYYY")
      featuredImage {
        node {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
            id
            publicURL
          }
        }
      }
    }
    featuredWpPost3: wpPost(
      categories: {
        nodes: { elemMatch: { name: { eq: "Featured Priority 3" } } }
      }
    ) {
      id
      title
      excerpt
      slug
      link
      categories {
        nodes {
          id
          name
          slug
        }
      }
      blogHeroUpdated {
        blogFeaturedHeroOverview {
          date
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
      }
      date(formatString: "DD, MMM, YYYY")
      featuredImage {
        node {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
            id
            publicURL
          }
        }
      }
    }
    featuredWpPost4: wpPost(
      categories: {
        nodes: { elemMatch: { name: { eq: "Featured Priority 4" } } }
      }
    ) {
      id
      title
      excerpt
      slug
      link
      categories {
        nodes {
          id
          name
          slug
        }
      }
      blogHeroUpdated {
        blogFeaturedHeroOverview {
          date
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
          }
        }
      }
      date(formatString: "DD, MMM, YYYY")
      featuredImage {
        node {
          altText
          localFile {
            childImageSharp {
              fluid {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
            publicURL
            id
            publicURL
          }
        }
      }
    }
    allWpPost(
      filter: {
        categories: {
          nodes: {
            elemMatch: {
              name: {
                nin: [
                  "Featured Priority 1"
                  "Featured Priority 2"
                  "Featured Priority 3"
                  "Featured Priority 4"
                ]
              }
            }
          }
        }
      }
    ) {
      edges {
        node {
          id
          title
          link
          categories {
            nodes {
              name
            }
          }
          featuredImage {
            node {
              altText
              localFile {
                publicURL
                id
                childImageSharp {
                  fluid {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
                publicURL
              }
            }
          }
        }
      }
    }
  }
`
