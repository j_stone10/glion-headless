import React from 'react';
import { Link } from "gatsby"
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import '../components/styles/left-image-right-text.css';

const TextLeftImage = ({
   quote_image_text,
   quote_image,
   main_quote,
   quote_link,
   cite
}) => (
 


      <div className="wrapper row left_img_right_text">
         <div className="col-md-6 col-12 p-0  d-flex justify-content-center align-items-center">
            <div className="overlay"></div>
            <div style={{ height: "100%", width: "100%" }}>
      
                  <Img fluid={quote_image.localFile.childImageSharp.fluid} alt={quote_image.altText} />
    
            </div>
            <div className="scrollElement absolute col-9 m-auto text-center white-text">
               <h3 dangerouslySetInnerHTML={{ __html: quote_image_text}}></h3>
            </div>
         </div>
         <div className=" col-md-6 col-12  py-5  d-flex justify-content-center align-items-center beige-bg">
            <div className="scrollElementOffsetThird col-9 col-sm-11  m-auto  text-center">
               <h3 className="mb-4">“{main_quote}”
        </h3>
               <p>{cite}</p>
               <Link to={quote_link.url} className="mt-5 btn btn-primary transparent" role="button">{quote_link.title}</Link>
            </div>
         </div>
      </div>



   )

TextLeftImage.propTypes = {
   title: PropTypes.string,
   main_quote: PropTypes.string,
   cite: PropTypes.string,
   quote_image_text: PropTypes.string,
   quote_image: PropTypes.object,
   quote_link: PropTypes.object,
};


export default TextLeftImage;
