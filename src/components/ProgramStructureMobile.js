import React from 'react';
// import { Link } from 'gatsby';
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import Parser from 'html-react-parser'
// import Video from "../components/video"
import './styles/program-structure.css';

const ProgramStructure = ({
   tab_1_content,
   tab_2_content,
   tab_3_content,
   tab_4_content,
   tab_5_content,
   tab_fees_content_1,
   tab_fees_content_2,
   allfile,
   lavender_background,
   lang

}) => {
   const content1 = Parser(tab_1_content.text, {
      replace: domNode => {

         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }

      },
   })
   const content2 = Parser(tab_2_content.text, {
      replace: domNode => {

         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   const content3 = Parser(tab_3_content.text, {
      replace: domNode => {

         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   const content4 = Parser(tab_4_content.text, {
      replace: domNode => {

         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   const content5 = Parser(tab_5_content.text, {
      replace: domNode => {

         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   return (
      <>
         {tab_1_content.title &&
            <div className="col-12 p-0 accordion"  id="slide7">
               <h6 className="accordion_title_black_text"  activate_slide="slide7">{tab_1_content.title}</h6>
               <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div className="accordion_text">

                  <div className="row">
                     {tab_1_content.statFigure != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_1_content.statFigure}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>{tab_1_content.statText}</h5>
                              </div>
                           </div>
                        </div>
                     }
                     {tab_1_content.statFigure2 != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_1_content.statFigure2}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>     {tab_1_content.statText2}</h5>
                              </div>
                           </div>
                        </div>
                     }
                  </div>
                  {content1}
               </div>
            </div>
         }
         {tab_2_content.title &&
            <div className="col-12 p-0 accordion"  id="slide8">
               <h6 className="accordion_title_black_text"  activate_slide="slide8">{tab_2_content.title}</h6>
               <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div className="accordion_text">

                  <div className="row">
                     {tab_2_content.statFigure != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_2_content.statFigure}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>{tab_2_content.statText}</h5>
                              </div>
                           </div>
                        </div>
                     }
                     {tab_2_content.statFigure2 != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_2_content.statFigure2}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>     {tab_2_content.statText2}</h5>
                              </div>
                           </div>
                        </div>
                     }
                  </div>
                  {content2}
               </div>
            </div>
         }
         {tab_3_content.title &&
            <div className="col-12 p-0 accordion" id="slide9">
               <h6 className="accordion_title_black_text"  activate_slide="slide9">{tab_3_content.title}</h6>
               <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div className="accordion_text">

                  <div className="row">
                     {tab_3_content.statFigure != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_3_content.statFigure}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>{tab_3_content.statText}</h5>
                              </div>
                           </div>
                        </div>
                     }
                     {tab_3_content.statFigure2 != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_3_content.statFigure2}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>     {tab_3_content.statText2}</h5>
                              </div>
                           </div>
                        </div>
                     }
                  </div>
                  {content3}
               </div>
            </div>
         }
         {tab_4_content.title &&
            <div className="col-12 p-0 accordion"  id="slide10">
               <h6 className="accordion_title_black_text"  activate_slide="slide10">{tab_4_content.title}</h6>
               <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div className="accordion_text">

                  <div className="row">
                     {tab_4_content.statFigure != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_4_content.statFigure}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>{tab_4_content.statText}</h5>
                              </div>
                           </div>
                        </div>
                     }
                     {tab_4_content.statFigure2 != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_4_content.statFigure2}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>     {tab_4_content.statText2}</h5>
                              </div>
                           </div>
                        </div>
                     }
                  </div>
                  {content4}
               </div>
            </div>
         }
         {tab_5_content.title &&
            <div className="col-12 p-0 accordion"  id="slide11">
               <h6 className="accordion_title_black_text"  activate_slide="slide11">{tab_5_content.title}</h6>
               <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
               <div className="accordion_text">

                  <div className="row">
                     {tab_5_content.statFigure != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_5_content.statFigure}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>{tab_5_content.statText}</h5>
                              </div>
                           </div>
                        </div>
                     }
                     {tab_5_content.statFigure2 != "" &&
                        <div className="col-md-6 col-12">
                           <div className="row counter">
                              <div className="col-12 font bronze-text text-center">
                                 {tab_5_content.statFigure2}
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-12 black-text text-center">
                                 <h5>     {tab_5_content.statText2}</h5>
                              </div>
                           </div>
                        </div>
                     }
                  </div>
                  {content5}
               </div>
            </div>

         }

         <div className="col-12 p-0 accordion">
            {lang === "en" ? <h6 className="accordion_title_black_text">FEES</h6> : <h6 className="accordion_title_black_text">Frais d'enseignement</h6>}

            <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
            <div className="accordion_text">

               {tab_fees_content_1 !== null &&
                  <div className="fees_table col-11 mx-auto my-3">
                     <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: tab_fees_content_1.tableRow1Intro }} />
                     <div className="row grey-bg py-2">
                        <div className="col-9">
                           {tab_fees_content_1.tableRow1Column1Title}
                        </div>
                        <div className="col-3">
                           {tab_fees_content_1.tableRow1Column2Value}
                        </div>
                     </div>
                     {tab_fees_content_1.tableRow1Items.map((item, i) => (
                        <div className="col-12 p-0 accordion" key={i}>
                           {(() => {
                              return (

                                 <div className="row py-3 opacity-bg">
                                    <div className="col-9">
                                       {item.product}
                                    </div>
                                    <div className="col-3">
                                       {item.fee}
                                    </div>
                                 </div>

                              )
                           }

                           )()}
                        </div>
                     ))}
                     {tab_fees_content_1.tableRow1SignOffTitle !== null &&

                        <div className="row grey-bg py-2 mb-4">
                           <div className="col-9">
                              {tab_fees_content_1.tableRow1SignOffTitle}

                           </div>
                           <div className="col-3">
                              {tab_fees_content_1.tableRow1SignOff}

                           </div>
                        </div>
                     }
                     {tab_fees_content_1.tableRow2Column1Title !== null &&
                        <div className="row grey-bg py-2">
                           <div className="col-9">
                              {tab_fees_content_1.tableRow2Column1Title}
                           </div>
                           <div className="col-3">
                              {tab_fees_content_1.tableRow2Column2Value}
                           </div>
                        </div>

                     }
                     {tab_fees_content_1.tableRow2Items !== null && tab_fees_content_1.tableRow2Items.map((item, i) => (
                        <div className="col-12 p-0 accordion" key={i}>
                           {(() => {
                              return (

                                 <div className="row py-3 opacity-bg">
                                    <div className="col-9">
                                       {item.product}
                                    </div>
                                    <div className="col-3">
                                       {item.fee}
                                    </div>
                                 </div>

                              )
                           }

                           )()}
                        </div>
                     ))}
                     {tab_fees_content_1.tableRow3SignOffTitle !== null &&
                        <div className="row grey-bg py-2 ">
                           <div className="col-9">
                              {tab_fees_content_1.tableRow3SignOffTitle}

                           </div>
                           <div className="col-3">
                              {tab_fees_content_1.tableRow3Column2Value}

                           </div>
                        </div>
                     }
                  </div>
               }

               {tab_fees_content_2.tableRow1Column1Title !== null &&
                  <div className="fees_table col-11 mx-auto my-3">
                     <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: tab_fees_content_2.tableRow1Intro }} />

                     <div className="row grey-bg py-2">
                        <div className="col-9">
                           {tab_fees_content_2.tableRow1Column1Title}
                        </div>
                        <div className="col-3">
                           {tab_fees_content_2.tableRow1Column2Value}
                        </div>
                     </div>

                     {tab_fees_content_2.tableRow111Items.map((item, i) => (
                        <div className="col-12 p-0 accordion" key={i}>
                           {(() => {
                              return (

                                 <div className="row py-3 opacity-bg">
                                    <div className="col-9">
                                       {item.product}
                                    </div>
                                    <div className="col-3">
                                       {item.fee}
                                    </div>
                                 </div>

                              )
                           }

                           )()}
                        </div>
                     ))}
                     {tab_fees_content_2.tableRow1SignOffTitle !== null &&
                        <div className="row grey-bg py-2">
                           <div className="col-9">
                              {tab_fees_content_2.tableRow1SignOffTitle}

                           </div>
                           <div className="col-3">
                              {tab_fees_content_2.tableRow1SignOffValue}

                           </div>
                        </div>
                     }
                     {tab_fees_content_2.tableRow2Column1Title !== null &&
                        <div className="row grey-bg mt-4 py-2">
                           <div className="col-9">
                              {tab_fees_content_2.tableRow2Column1Title}
                           </div>
                           <div className="col-3">
                              {tab_fees_content_2.tableRow2Column2Value}
                           </div>
                        </div>
                     }
                     {tab_fees_content_2.tableRow12Items !== null && tab_fees_content_2.tableRow12Items.map((item, i) => (
                        <div className="col-12 p-0 accordion" key={i}>
                           {(() => {
                              return (

                                 <div className="row py-3 opacity-bg">
                                    <div className="col-9">
                                       {item.product}
                                    </div>
                                    <div className="col-3">
                                       {item.fee}
                                    </div>
                                 </div>

                              )
                           }

                           )()}
                        </div>
                     ))}
                     {tab_fees_content_2.tableRow3SignOffTitle !== null &&
                        <div className="row grey-bg py-2 ">
                           <div className="col-9">
                              {tab_fees_content_2.tableRow3SignOffTitle}

                           </div>
                           <div className="col-3">
                              {tab_fees_content_2.tableRow3Column2Value}

                           </div>
                        </div>
                     }
                  </div>
               }
            </div>
         </div>

      </>
   );
}

ProgramStructure.propTypes = {
   tab_1_content: PropTypes.object,
   tab_2_content: PropTypes.object,
   tab_3_content: PropTypes.object,
   tab_4_content: PropTypes.object,
   tab_5_content: PropTypes.object,
   tab_fees_content_1: PropTypes.object,
   tab_fees_content_2: PropTypes.object,
   lavender_background: PropTypes.string,
   lang: PropTypes.string,
   allfile: PropTypes.array,

};

export default ProgramStructure;
