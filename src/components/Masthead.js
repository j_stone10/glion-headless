import React from 'react';
import { PropTypes } from 'prop-types';

// import styled from 'styled-components'

// import Img from "gatsby-image"

import { MastheadWrapper } from "./styles/MastheadStyles"

// const div = styled.div`
//   padding-top: 100px;
// `

const Masthead = ({ title }) =>  (

<MastheadWrapper>
    <div className="container-fluid">
      <div className="row"> 
  
        <div className="grey-section" style={{ padding: 0 }}>
        <h1 className="text-center" dangerouslySetInnerHTML={{ __html: title }} />
</div>
        </div>

    </div>
</MastheadWrapper>
)

Masthead.propTypes = {
  title: PropTypes.object
};


export default Masthead;
