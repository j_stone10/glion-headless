import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { Link, graphql } from 'gatsby';

import '../components/styles/faculty.css';




class SocialFeed extends Component {
    
    componentDidMount() {

        (function(){
            var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
            i.src = "https://cdn.curator.io/published/0484d69e-f57c-4792-846d-44794b960f1a.js";
            e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
            })();
    }
    render() {
        const code = this.props.code
        const currentLang = this.props.currentLang

        // const media = this.props.data.allWpMediaItem.edges
        // console.log(code)
        return (
            <>
               
                <div className="wrapper row my-5">
                    <div className=" col-12 p-0">
                        <div>
                            <div id="curator-feed-default-layout" className="scrollElement" dangerouslySetInnerHTML={{ __html: code }} />


                        </div>
                    </div>
                </div>
            </>
        )
    }
}



SocialFeed.propTypes = {
    code: PropTypes.string,
    currentLang: PropTypes.string,

};


export default SocialFeed;