import React from 'react'
import Spinner from 'react-spinkit'

export default ({ error }) => {
    return error? <div>An error occured loading component</div>
    : <Spinner name="ball-scale-ripple-multiple" />
};