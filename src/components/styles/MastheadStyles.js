import styled from 'styled-components';

export const MastheadWrapper = styled.div`
  
/***************************************************
 Grey section
 ***************************************************/
 .grey-section {
   background: #F8F8F8;
  //  max-width: 1440px;
   margin: 0 auto;
   margin-top: 50px;
   width: 100%;
 }
 
 .resource img {
   min-width: 100%;
   max-width: 100%;
   padding: 0 15px 0;
   margin-bottom: 0 !important;
 }
 
 @media (max-width: 767px) {
 .resource img {
   max-width: 100%;
   /* height: 100%; */
   height: auto;
   max-height: 100px;
 }}
 
 .resource select {
   font-size: 16px;
   font-size: 1.2rem;
   position: relative;
   display: block;
   width: 90%;
   margin: 30PX auto;
   color: #fff;
   display: block;
   height: 50px;
   border-radius: 0;
   background-image: -webkit-gradient(linear, left top, right top, from(#cf2f44), to(#8f191c));
   background-image: -o-linear-gradient(left, #cf2f44, #8f191c);
   background-image: linear-gradient(90deg, var(--blue), var(--blue));
 }
 
 .OD-002 .select-box__input-text {
   display: none;
   width: 100%;
   margin: 0;
   padding: 15px;
   background-color: #cf2f44;
 
 }
 
 
 .grey-section .header-accomp {
   width: 500px;
 }
 
 .grey-section h3 {
   font-size: 46px;
   font-weight: bold;
 }
 
 @media (max-width: 991px) {
   .grey-section h3 {
     font-size: 36px;
     font-weight: bold;
   }
 }
 @media (max-width: 767px) {
   .grey-section h3 {
     font-size: 26px;
     font-weight: bold;
   }
 }
 .grey-section h3 span {
   color: var(--green);
 }
 
 .grey-section p.lead {
   width: 480px;
   max-width: 100%;
   font-size: 18px;
 }
 .grey-section .col-lg-6:first-child {
   padding: 190px 0 90px 80px;
   -webkit-box-flex: 0;
   -ms-flex: 0 0 52%;
   flex: 0 0 51%;
   max-width: 51%;
 }
 
 @media (max-width: 991px) {
 .grey-section  {
   padding: 40px 0 0; 
   }
 .grey-section.single  {
   padding: 0; 
   }
 .grey-section .col-lg-6:first-child {
   padding: 0 20px;
   flex: unset;
   max-width: unset;
   }
 }
 
 .grey-section .col-lg-6:last-child {
   -webkit-box-flex: 0;
   -ms-flex: 0 0 48%;
   flex: 0 0 49%;
   max-width: 49%;
 }
 
 @media (max-width: 1350px) {
   .grey-section .col-lg-6:last-child img {
     -webkit-box-ordinal-group: 3;
     -ms-flex-order: 2;
     order: 2;
     min-width: 100%;
   }
 }
 
 .grey-section .col-lg-6:last-child .white {
   position: absolute;
   width: 291px;
   height: 578px;
   top: 103px;
   left: 15px;
   bottom: 100px;
   background: #fff;
   padding: 50px 30px;
 }
 
 @media (max-width: 991px) {
   .grey-section .col-lg-6:last-child .white {
     padding: 0;
     position: relative;
     top: unset;
     left: unset;
     bottom: unset;
     -webkit-box-flex: 0;
         -ms-flex: 0 0 100%;
             flex: 0 0 100%;
     max-width: 100%;
     width: unset;
     -webkit-box-ordinal-group: 2;
         -ms-flex-order: 1;
             order: 1;
     height: unset;
     padding: 18px
   }
 }
 
 .grey-section .col-lg-6:last-child .white h4, .grey-section .col-lg-6:last-child .white h5 {
   font-weight: bold;
 }
 
 .grey-section .col-lg-6:last-child .white h4 {
   margin-bottom: 70px;
 }
 
 .grey-section .col-lg-6:last-child .white h5 {
   margin-bottom: 45px;
 }
 
 @media (max-width: 991px) {
   .grey-section .col-lg-6:last-child {
     -webkit-box-flex: 0;
     -ms-flex: 0 0 100%;
     flex: 0 0 100%;
     max-width: 100%;
     display: flex;
     flex-direction: column;
   }
 }
 
`;
