import styled from 'styled-components';

export const HeaderWrapper = styled.header`
background-color: #fff;
box-shadow: none;
position: fixed;
top: 0;
left: 50%;
z-index: 100;
-ms-transform: translateX(-50%);
-moz-transform: translateX(-50%);
-webkit-tranform: translateX(-50%);
transform: translateX(-50%);
width: 100%;


`;
