import React from 'react';
import { Link } from "gatsby"
import { PropTypes } from 'prop-types';

import Img from "gatsby-image"
import '../components/styles/campus_locations.css';

const CampusLocations = ({
   main_campus_title,
   campus_1_image,
   campus_1_image_wide,
   campus_1_title,
   campus_1_location,
   campus_1_link,
   campus_2_image,
   campus_2_image_wide,
   campus_2_title,
   campus_2_location,
   campus_2_link,
   show_campuses
}) => {

   // let campusScroll = useRef(null)
   // useEffect(() => {
   //    ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);
   //    //CAMPUS SCROLL UP
   //    var campusTL = new TimelineMax({ onUpdate: updatePercentage });
   //    const campusController = new ScrollMagic.Controller();


   //    campusTL.from(".campus-rollover", .5, { y: 200, opacity: 0 });

   //    const campusScene = new ScrollMagic.Scene({
   //       triggerElement: "#campusScrollInit",
   //       triggerHook: "onCenter",
   //       // triggerHook: .80,
   //       duration: 200,
   //    })

   //       // .setPin(".campus")
   //       .setTween(campusTL)
   //       .addTo(campusController)
   //    // .addIndicators()


   //    function updatePercentage() {
   //       campusTL.progress();
   //       console.log(campusTL.progress())
   //    }

   // }, [])

   return (
      <>
        
         <div className="row my-5">
            <div id="campus-carousel" className="wrapper carousel slide mb-5 col-12 p-0" data-ride="carousel" data-interval="false">
               <ol className="carousel-indicators d-flex d-md-none">
                  <li data-target="#campus-carousel" data-slide-to="0" className="active"></li>
                  <li data-target="#campus-carousel" data-slide-to="1"></li>
               </ol>
               <div className="carousel-inner row w-100 mx-auto" role="listbox">
                  <div className="scrollElement carousel-item col-12 col-md-6 campus-rollover p-0 px-md-3 active ">
                     <div className="d-flex justify-content-center align-items-center ">
                        <div className="image col-12 p-0">
                           <div className="overlay"></div>
                           {campus_1_image.localFile &&
                              <Img fluid={campus_1_image.localFile.childImageSharp.fluid} alt={campus_1_image.altText} />
                           }
                        </div>
                        <div className="absolute col-9 m-auto text-center">
                           <h2>{campus_1_title}</h2>
                           <p>{campus_1_location}</p>
                        </div>
                        <button className="btn btn-primary blue" >{campus_1_link.title}</button>
                        <Link to={campus_1_link.url}> </Link>
                     </div>
                  </div>
                  <div className="scrollElementOffset carousel-item col-12 col-md-6 campus-rollover p-0 px-md-3 ">
                     <div className="d-flex justify-content-center align-items-center ">
                        <div className="image col-12 p-0">
                           <div className="overlay"></div>
                           {campus_2_image.localFile &&
                              <Img fluid={campus_2_image.localFile.childImageSharp.fluid} alt={campus_2_image.altText} />
                           }
                        </div>
                        <div className="absolute col-9 m-auto  text-center">
                           <h2>{campus_2_title}</h2>
                           <p>{campus_2_location}</p>
                        </div>
                        <button className="btn btn-primary blue" >{campus_2_link.title}</button>
                        <Link to={campus_2_link.url}> </Link>
                     </div>
                  </div>
               </div>
               <a className="carousel-control-prev d-flex d-md-none" href="#campus-carousel" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span className="sr-only">Previous</span>
               </a>
               <a className="carousel-control-next d-flex d-md-none" href="#campus-carousel" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true"></span>
                  <span className="sr-only">Next</span>
               </a>
            </div>
         </div>
      </>
   )
}




CampusLocations.propTypes = {
   main_campus_title: PropTypes.string,
   show_campuses: PropTypes.string,
   campus_1_image: PropTypes.object,
   campus_1_image_wide: PropTypes.object,
   campus_1_title: PropTypes.string,
   campus_1_location: PropTypes.string,
   campus_1_link: PropTypes.object,
   campus_2_image: PropTypes.object,
   campus_2_wide: PropTypes.object,
   campus_2_title: PropTypes.string,
   campus_2_location: PropTypes.string,
   campus_2_link: PropTypes.object,
};
export default CampusLocations;
