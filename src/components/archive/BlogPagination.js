import React from "react"
import { Link } from "gatsby"

import { PaginationWrapper } from "./styles/PaginationStyles"

const Pagination = ({ page, totalPages, catSlug }) => {
  // console.log(catSlug, page)
  return (
    <>
      <div className="row w-100 mt-3">
        <div className="col-12">
          <PaginationWrapper>
            {page > 1 ? (
              <Link
                to={`/magazine/${ catSlug ? "category/" + catSlug + "/" : ""}${
                  page === 2 ? "" : page - 1
                }`}
                className="navBack"
              >
                Previous
              </Link>
            ) : (
              <div />
            )}
            {page < totalPages ? (
              <Link
                to={`/magazine/${ catSlug ? "category/" + catSlug + "/" : ""}${page + 1}/#latestNews`}
                className="navForward"
              >
                Next
              </Link>
            ) : (
              <div />
            )}
          </PaginationWrapper>
        </div>
      </div>
      <div className="row w-100">
        <p className="text-center m-auto">
          {page} / {totalPages}
        </p>
      </div>
    </>
  )
}

export default Pagination
