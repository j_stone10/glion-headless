import styled from 'styled-components';

export const PaginationWrapper = styled.nav`
  display: flex;
  justify-content: space-between;

  .actualPage {
    display: block;
    background: var(--blue);
    height: 30px;
    width: 30px;
    text-align: center;
    border-radius: 25px;
    color: #fff;
    font-size: 1rem;
    font-weight: 800;
    font-family: 'Roboto', Arial, Helvetica, sans-serif;
    padding: 3px 0 0 0;
  }

  .navBack {
    font-family: 'Roboto', Arial, Helvetica, sans-serif;
    font-size: 1rem;
    font-weight: 800;
    color: #fff;
    position: relative;
    width: 120px;
    height: 30px;
    margin-right: 10px;
    display: block;
    background: var(--blue);
    padding: 3px 10px 0 25px;
    margin-bottom: 30px;
    
    :before {
      display: block;
      position: absolute;
      left: -15px;
      top: 50%;
      margin: 0;
      border: solid;
      content: '';
      pointer-events: none;
      border-right-color: var(--blue);
      border-width: 15px;
      margin-top: -15px;
    }

    :hover {
      background: var(--bronze);
    }

    :hover:before {
      border-right-color: var(--bronze);
    }
  }

  .navForward {
    font-family: 'Roboto', Arial, Helvetica, sans-serif;
    font-size: 1rem;
    font-weight: 800;
    color: #fff;
    position: relative;
    width: 120px;
    height: 30px;
    display: block;
    background: var(--blue);
    padding: 3px 10px 0 5px;
    margin-bottom: 30px;
    text-align: center;

    :after {
      display: block;
      position: absolute;
      left: 90%;
      top: 50%;
      margin: 0;
      border: solid;
      content: '';
      pointer-events: none;
      border-left-color: var(--blue);
      border-width: 15px;
      margin-top: -15px;
    }

    :hover {
      background: var(--bronze);
    }

    :hover:after {
      border-left-color: var(--bronze);
    }
  }
`;
