import React from 'react';
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import '../components/styles/scrollMagic.css';




const ScrollMagicComp = ({
  image_mobile,
  parallax_image_desktop,
  main_stats_title,
  stat_1_title,
  stat_1_figure,
  stat_1_symbol_after,
  stat_1_sign_off,
  stat_2_title,
  stat_2_figure,
  stat_2_symbol_after,
  stat_2_sign_off,
}) => {


  return (


    <div className="row big_image line-after">
      <div className="background_image adjust">
        <div className="d-none d-lg-block">
          {parallax_image_desktop.localFile &&
            <div><Img style={{maxWidth: "100%"}} fluid={parallax_image_desktop.localFile.childImageSharp.fluid} alt={parallax_image_desktop.altText} /></div>
          }
        </div>
        <div className="d-block d-lg-none">
          <div className="overlay"></div>
          {image_mobile.localFile &&
            <Img className="mobile" fluid={image_mobile.localFile.childImageSharp.fluid} alt={image_mobile.altText} />
          }
        </div>
      </div>
      <div className="counter text-center white-text">
        <div className="row wrapper">
          <div className=" col-sm-8 col-10 m-auto">
            <h2>{main_stats_title}</h2>
            <div className="row">
              <div className="col-md-6 col-12">
                <div className="scrollElementOffsetThird font font1">
                  {stat_1_title && stat_1_title}
                  <span className="value value1">{stat_1_figure}</span>{stat_1_symbol_after}

                </div>
                <p>{stat_1_sign_off}</p>
              </div>
              <div className=" col-md-6 col-12">
                <div className="scrollElementOffsetLast font font2">
                  {stat_2_title  &&  stat_2_title}
                  <span className="value value2">{stat_2_figure}</span>{stat_2_symbol_after}

                </div>
                <p>{stat_2_sign_off}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

ScrollMagicComp.propTypes = {


  image_mobile: PropTypes.object,
  parallax_image_desktop: PropTypes.object,
  main_stats_title: PropTypes.string,
  stat_1_title: PropTypes.string,
  stat_1_figure: PropTypes.number,
  stat_1_symbol_after: PropTypes.string,
  stat_1_sign_off: PropTypes.string,
  stat_2_title: PropTypes.string,
  stat_2_figure: PropTypes.number,
  stat_2_symbol_after: PropTypes.string,
  stat_2_sign_off: PropTypes.string,

};


export default ScrollMagicComp;
