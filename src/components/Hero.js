import React from 'react';
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import '../components/styles/hero.css';
 

const Hero = ({ hero_image, hero_title, hero_text, line_after }) =>  (


     
          <div className={`row hero ${line_after === "yes" && " line-after overflow-adjust"} `}  >
              <div className="background_image">
    
                <Img fluid={hero_image.localFile.childImageSharp.fluid}  alt={hero_image.altText}  />
                
              </div>
              <div className="scrollElementStart foreground_text col-md-9 col-11 m-auto">
                <h1 className="mb-3" dangerouslySetInnerHTML={{ __html: hero_title }} />
  
                <div dangerouslySetInnerHTML={{ __html: hero_text }} />
              </div>
          </div>
   

)

Hero.propTypes = {
  hero_image: PropTypes.object,
  hero_title: PropTypes.string,
  hero_text: PropTypes.string,
  line_after: PropTypes.string
};


export default Hero;
