/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import Helmet from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"

function SEO({ description, lang, meta, title, image: metaImage, uri, author, link }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            siteUrl
          }
        }
      }
    `
  )
  // console.log(metaImage)
  const metaDescription = description || site.siteMetadata.description



  const authorName = 
  author && author.node.name
  ? `${author.node.name}`
  : null


  const locale = 
  lang === 'en'
  ? 'en_GB'
  : 'fr-FR'


  // console.log(metaImage)
  return (
    <Helmet

      title={title}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          name: `og:locale`,
          content: locale,
        },
        {
          property: `og:type`,
          content: `article`,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:url`,
          content: site.siteMetadata.siteUrl + uri,
        },
        {
          property: `og:site_name`,
          content: "Glion Website",
        },
        {
          property: "og:image",
          itemprop: "image",
          content: metaImage !== null && site.siteMetadata.siteUrl + metaImage.localFile.publicURL,
        },
        {
          property: "og:image:width",
          content: metaImage !== null && metaImage.mediaDetails.width,
        },
        {
          property: "og:image:height",
          content: metaImage !== null && metaImage.mediaDetails.height
        },
        // {
        //   property: "og:image",
        //   // itemprop: "image",
        //   content: metaImage !== null && site.siteMetadata.siteUrl + metaImage.localFile.publicURL,
        // },
        // {
        //   property: "og:image:width",
        //   content: 300,
        // },
        // {
        // property: "og:image:height",
        //   content: 300
        // },
        {
          name: "twitter:card",
          content: "summary_large_image",
        },
      ]
        .concat(meta)}
      link={[
        {
          rel: `canonical`,
          href: site.siteMetadata.siteUrl + uri,
        },
      ].concat(link)}
    />
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
  link: [],
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  uri: PropTypes.string,
  author: PropTypes.object,
  meta: PropTypes.arrayOf(PropTypes.object),
  link: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
  image: PropTypes.shape({
    localFile: PropTypes.object.isRequired,
    mediaDetails: PropTypes.object.isRequired,
  }),
}

export default SEO
