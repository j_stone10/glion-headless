import React, { Component } from 'react'
// import { Link } from 'gatsby';
import { PropTypes } from 'prop-types';

import './styles/program-shapes.css';

class ProgramShapes extends Component {
   render() {

      const semester_shapes_options = this.props.semester_shapes_options
      const option_1_block_1 = this.props.option_1_block_1
      const option_1_block_2 = this.props.option_1_block_2
      const option_1_block_3 = this.props.option_1_block_3
      const option_1_block_4 = this.props.option_1_block_4
      const option_1_block_5 = this.props.option_1_block_5
      const option_1_block_6 = this.props.option_1_block_6
      const option_1_block_7 = this.props.option_1_block_7
      const option_1_block_8 = this.props.option_1_block_8
      const option_2_block_1 = this.props.option_2_block_1
      const option_2_block_2 = this.props.option_2_block_2
      const option_2_block_3 = this.props.option_2_block_3
      const option_2_block_4 = this.props.option_2_block_4
      const option_2_block_5 = this.props.option_2_block_5
      const asterisk_content_1 = this.props.asterisk_content_1
      const asterisk_content_2 = this.props.asterisk_content_2

      // console.log(option_2_block_3)

      return (

         //   *************************
         //   ********* DESKTOP SHAPES *****
         //   *****************************
         <div>
            {semester_shapes_options === 'Option 1' &&
               <div className="row courses-arrows mb-1  courses-two-row-arrows">
                  <div className="arrow">
                     <div className={`${option_1_block_1.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_1.title}</div>
                              <div className="semester_title">{option_1_block_1.text}</div>
                              <div className="semester_signoff">{option_1_block_1.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow">
                     <div className={`${option_1_block_2.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_2.title}</div>
                              <div className="semester_title">{option_1_block_2.text}</div>
                              <div className="semester_signoff">{option_1_block_2.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow ">
                     <div className={`${option_1_block_3.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_3.title}</div>
                              <div className="semester_title">{option_1_block_3.text}</div>
                              <div className="semester_signoff">{option_1_block_3.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow ">
                     <div className={`${option_1_block_4.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_4.title}</div>
                              <div className="semester_title">{option_1_block_4.text}</div>
                              <div className="semester_signoff">{option_1_block_4.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow">
                     <div className={`${option_1_block_5.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_5.title}</div>
                              <div className="semester_title" dangerouslySetInnerHTML={{ __html: option_1_block_5.text }}></div>
                              <div className="semester_signoff">{option_1_block_5.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow">
                     <div className={`${option_1_block_6.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_6.title}</div>
                              <div className="semester_title">{option_1_block_6.text}</div>
                              <div className="semester_signoff">{option_1_block_6.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow">
                     <div className={`${option_1_block_7.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_7.title}</div>
                              <div className="semester_title">{option_1_block_7.text}</div>
                              <div className="semester_signoff">{option_1_block_7.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow ">
                     <div className={`${option_1_block_8.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_1_block_8.title}</div>
                              <div className="semester_title">{option_1_block_8.text}</div>
                              <div className="semester_signoff">{option_1_block_8.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            }
            {asterisk_content_1 !== '' &&
               <div className="row mb-1 ">
                  <div className="col-md-12 col-10 m-auto">
                     <div className="semester_signoff_asterisk">{asterisk_content_1}</div>
                  </div>
               </div>
            }
            {/* *************************
         ********* Start desktop wide SHAPES *****
         *****************************--> */}
            {semester_shapes_options === 'Option 2' &&
               <div className="row mt-5 mb-1 courses-arrows wide-children">
                  <div className="arrow">
                     <div className={`${option_2_block_1.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_2_block_1.title}</div>
                              <div className="semester_title">{option_2_block_1.text}</div>
                              <div className="semester_signoff">{option_2_block_1.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="arrow wide">
                     <div className={`${option_2_block_2.colour} inner-arrow`}>
                        {option_2_block_2.isSplit === 'No' &&
                           <div className="content ">
                              <div className="text">
                                 <div className="semester_num">{option_2_block_2.title}</div>
                                 <div className="semester_title">{option_2_block_2.text}</div>
                                 <div className="semester_signoff">{option_2_block_2.signoff}</div>
                              </div>
                           </div>
                        }
                        {option_2_block_2.isSplit === 'Yes' &&
                           <>
                              <div className="content item">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_2.title1}</div>
                                    <div className="semester_title">{option_2_block_2.text1}</div>
                                    <div className="semester_signoff">{option_2_block_2.signoff1}</div>
                                 </div>
                              </div>
                              <div className="content item">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_2.title2}</div>
                                    <div className="semester_title">{option_2_block_2.text2}</div>
                                    <div className="semester_signoff">{option_2_block_2.signoff2}</div>
                                 </div>
                              </div>
                           </>
                        }
                     </div>
                  </div>
                  <div className="arrow wide">
                     <div className={`${option_2_block_3.colour} inner-arrow`}>
                        {option_2_block_3.isSplit === 'No' &&
                           <div className="content ">
                              <div className="text">
                                 <div className="semester_num">{option_2_block_3.title}</div>
                                 <div className="semester_title">{option_2_block_3.text}</div>
                                 <div className="semester_signoff">{option_2_block_3.signoff}</div>
                              </div>
                           </div>
                        }
                        {option_2_block_3.isSplit === 'Yes' &&
                           <>
                              <div className="content item">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_3.title1}</div>
                                    <div className="semester_title">{option_2_block_3.text1}</div>
                                    <div className="semester_signoff">{option_2_block_3.signoff1}</div>
                                 </div>
                              </div>
                              <div className="content item">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_3.title2}</div>
                                    <div className="semester_title">{option_2_block_3.text2}</div>
                                    <div className="semester_signoff">{option_2_block_3.signoff2}</div>
                                 </div>
                              </div>
                           </>
                        }
                     </div>
                  </div>
                  <div className="arrow  wide">
                     <div className={`${option_2_block_4.colour} inner-arrow`}>
                        {option_2_block_4.isSplit === 'No' &&
                           <div className="content ">
                              <div className="text">
                                 <div className="semester_num">{option_2_block_4.title}</div>
                                 <div className="semester_title">{option_2_block_4.text}</div>
                                 <div className="semester_signoff">{option_2_block_4.signoff}</div>
                              </div>
                           </div>
                        }
                        {option_2_block_4.isSplit === 'Yes' &&
                           <>
                              <div className="content item">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_4.title1}</div>
                                    <div className="semester_title">{option_2_block_4.text1}</div>
                                    <div className="semester_signoff">{option_2_block_4.signoff1}</div>
                                 </div>
                              </div>
                              <div className="content item">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_4.title2}</div>
                                    <div className="semester_title">{option_2_block_4.text2}</div>
                                    <div className="semester_signoff">{option_2_block_4.signoff2}</div>
                                 </div>
                              </div>
                           </>
                        }
                     </div>
                  </div>
                  <div className="arrow ">
                     <div className={`${option_2_block_5.colour} inner-arrow`}>
                        <div className="content">
                           <div className="text">
                              <div className="semester_num">{option_2_block_5.title}</div>
                              <div className="semester_title">{option_2_block_5.text}</div>
                              <div className="semester_signoff">{option_2_block_5.signoff}</div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            }
            {asterisk_content_2 !== '' &&
               <div className="row mb-1 ">
                  <div className="col-md-12 col-10 m-auto">
                     <div className="semester_signoff_asterisk">{asterisk_content_2}</div>
                  </div>
               </div>
            }
            {/* *************************
         ********* End desktop wide SHAPES *****
         *****************************--> */}
         </div>
      )
   }
}


ProgramShapes.propTypes = {
   semester_shapes_options: PropTypes.string,
   option_1_block_1: PropTypes.object,
   option_1_block_2: PropTypes.object,
   option_1_block_3: PropTypes.object,
   option_1_block_4: PropTypes.object,
   option_1_block_5: PropTypes.object,
   option_1_block_6: PropTypes.object,
   option_1_block_7: PropTypes.object,
   option_1_block_8: PropTypes.object,
   option_2_block_1: PropTypes.object,
   option_2_block_2: PropTypes.object,
   option_2_block_3: PropTypes.object,
   option_2_block_4: PropTypes.object,
   option_2_block_5: PropTypes.object,
   asterisk_content_1: PropTypes.string,
   asterisk_content_2: PropTypes.string,
};

export default ProgramShapes;
