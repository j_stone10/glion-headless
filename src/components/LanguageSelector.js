
import React, { Component } from 'react'
import { Link, useStaticQuery } from 'gatsby';
import { PropTypes } from 'prop-types';
import { Dropdown, DropdownToggle, NavLink, DropdownMenu, UncontrolledDropdown } from 'reactstrap';
// import './styles/navigation.css';

class LanguageSelector extends Component {
  constructor(props) {
    super(props);


    this.toggle1 = this.toggle1.bind(this);

    this.onMouseEnter1 = this.onMouseEnter1.bind(this);
    this.onMouseLeave1 = this.onMouseLeave1.bind(this);

    this.state = {
      isOpen: false,
      navCollapsed: true,
      showNavbar: false,
      dropdownOpen1: false,

    };
  }

  toggle1() {
    this.setState(prevState => ({
      dropdownOpen1: !prevState.dropdownOpen1,
    }));
  }


  onMouseEnter1() {
    this.setState({ dropdownOpen1: true });
  }

  onMouseLeave1() {
    this.setState({ dropdownOpen1: false });
  }



  render() {
    const polylang_translations = this.props.polylang_translations
    const currentLang = this.props.currentLang
    const frenchHome = this.props.frenchHome
    // console.log(polylang_translations)
    // const { navCollapsed } = this.state

    if(polylang_translations.length < 1 || polylang_translations == undefined){ 
      if ( currentLang == "fr" ) {
      // polylang_translations[0].link = '/fr/'
      polylang_translations.splice(0, 0, { "link": "/" });
      }
      else {
        polylang_translations.splice(0, 0, { "link": "/fr/" });
      }
    }
    
    // console.log(polylang_translations)
    // console.log(currentLang)
    // if (!polylang_translations) {
    //   polylang_translations = {}
    //   if (!polylang_translations[0]) {
    //     polylang_translations.splice(0, 0, { "link": "/" });
    //   }
    // let transID = ''
    // if ( currentLang === 'en') {
    //   transID = polylang_translations[0].translations.fr
    // } else {
    //   transID = polylang_translations[0].translations.en
    // }
    
    // console.log(transID)
    // const {
    //   translink 
    // } = useStaticQuery(
    //   graphql`
    //     query translink {
    //        wpPage(id: { eq: "4537" }) {
    //         link
    //        }
    //     }
    //   `
    // );
    // console.log(polylang_translations)

    // if (frenchHome === '/fr/') {
    //   polylang_translations = '/'
    // }

    return (
      <>
      {/* <h6>Lang</h6>  */}
        {/* {polylang_translations !== undefined && */}
    
      
              

                <UncontrolledDropdown onMouseOver={this.onMouseEnter1} onMouseLeave={this.onMouseLeave1} isOpen={this.state.dropdownOpen1} toggle={this.toggle1} >
                  <DropdownToggle nav caret className="nav-parent">
                    {currentLang === "en" ? "English" : "Français"}
                  </DropdownToggle>
                  <DropdownMenu right
                    className="animate slideIn  lang-dropdown"
                  >
                    <NavLink className="child" 
                    href={ frenchHome === '/fr/' ? '/' : polylang_translations[0].link}
                    >
                      <span className="pl-1"> {currentLang === "en" ? "Français" : " English"} </span>
                    </NavLink>
                  </DropdownMenu>
                </UncontrolledDropdown>
         
      
      
        {/* } */}
      </>
    )
  }
}

LanguageSelector.propTypes = {
  polylang_translations: PropTypes.array,
  currentLang: PropTypes.string,
  frenchHome: PropTypes.string
};

export default LanguageSelector;
