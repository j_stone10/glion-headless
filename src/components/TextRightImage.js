import React from 'react';
import { Link } from "gatsby"
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import '../components/styles/hero.css';

const TextRightImage = ({ 
  title, 
  text,
  image,
  link 
 }) =>  (


  <div className="row wrapper line-after mt-5 d-none d-md-block">
    <div className="col-12 bronze-bg left_text_right_img">
      <div className="row ">
          <div className="col-6 d-flex justify-content-center align-items-center  order-md-1 order-2">
            <div className="scrollElement col-xl-9 col-lg-11 m-auto">
                <h2>{title}</h2>
                <div dangerouslySetInnerHTML={{ __html: `<div className="mt-3"> ${text} </div>` }} />
                <Link to={link.url} className="btn btn-primary blue">{link.title}</Link>
            </div>
          </div>
          <div className="col-6 p-0  order-md-2 order-1">
            {image.localFile &&
          <Img fluid={image.localFile.childImageSharp.fluid} alt={image.altText} />
            }
          </div>
      </div>
    </div>
  </div>



)

TextRightImage.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  image: PropTypes.object,
  link: PropTypes.object,
};


export default TextRightImage;
