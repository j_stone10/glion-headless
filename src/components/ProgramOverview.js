import React from 'react';
import { Link } from "gatsby"
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import FrenchSVG from '../images/Share_Button_FR.svg'
import BreadCrumb from '../components/Breadcrumb';
// import share from "../images/share.svg"
// import call from "../images/email.svg"
// import whatson from "../images/whatson.svg"
// import messenger from "../images/messenger.svg"
// import email from "../images/mail.svg"


import './styles/program-overview.css';



const ProgramOverview = ({
    title,
    program_overview,
    next_intake,
    button_1,
    button_2,
    wattsapShare,
    program_header_image,
    parent,
    lavender_background,
    lang,
    currentLang,
    show_remote_learning,
    remote_learning_text,
    learn_more_button_text,
    remote_learning_overview_text
}) => (


        <>
            <div className="row">
                <div className={`col-md-6 col-12 pr-md-0 program-content ind order-2 order-md-1 ${lavender_background === true && 'lavender-bg'}`}>
                    <div className="breadcrumb-container d-none d-md-block">
                        <BreadCrumb parent={parent} title={title} currentLang={lang} />

                    </div>
                    <div className="row program-text individual mr-md-0">
                        <div className="scrollElementStart col-xl-8 col-lg-11 col-11 m-auto p-md-3">
                            <div className="">
                                <h1>
                                    {title}
                                </h1>
                                <div dangerouslySetInnerHTML={{ __html: program_overview }} />

                                <div className={`row ${show_remote_learning === null ? "d-none" : "d-block"}`}>
                                    <div className="d-inline pl-3">
                                        <p className="remote_learning_text remote_learning_text_program">{remote_learning_text}<a href="#remote_learning_option">{learn_more_button_text}</a></p>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="d-inline pl-3">
                                        <Link className="mt-5  btn btn-primary beige" to={button_1.url} >{button_1.title}</Link>
                                    </div>
                                    <div className="d-inline pl-3">
                                        <a className="mt-5  btn btn-primary bronze" href="#DownloadBrochure">{button_2.title}</a>
                                    </div>
                                </div>
                                {currentLang === "en" &&
                                <div className="row">
                                    <div className="d-inline pl-3"><a target="_blank" className="mt-0 btn btn-primary blue" href="https://api.whatsapp.com/send?phone=41219892677"><svg width="22" height="22" viewBox="0 0 22 22" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                                        <path fill="#fff" d="M11.0027 0H10.9973C4.93213 0 0 4.72563 0 10.5365C0 12.8414 0.7755 14.9777 2.09413 16.7123L0.72325 20.6266L4.95138 19.3319C6.69075 20.4356 8.76562 21.0731 11.0027 21.0731C17.0679 21.0731 22 16.3461 22 10.5365C22 4.72695 17.0679 0 11.0027 0ZM17.4034 14.8789C17.138 15.5967 16.0847 16.192 15.2446 16.3659C14.6699 16.4831 13.9191 16.5766 11.3919 15.573C8.15925 14.2902 6.0775 11.1437 5.91525 10.9396C5.75988 10.7354 4.609 9.27346 4.609 7.76147C4.609 6.24948 5.41062 5.51324 5.73375 5.19714C5.99913 4.93768 6.43775 4.81915 6.8585 4.81915C6.99463 4.81915 7.117 4.82573 7.227 4.831C7.55013 4.84417 7.71237 4.86261 7.9255 5.35124C8.19088 5.96368 8.83712 7.47567 8.91412 7.63108C8.9925 7.7865 9.07088 7.99723 8.96088 8.20137C8.85775 8.4121 8.767 8.50561 8.60475 8.68473C8.4425 8.86386 8.2885 9.00083 8.12625 9.19312C7.97775 9.36039 7.81 9.53951 7.997 9.84902C8.184 10.1519 8.83025 11.1621 9.78175 11.9734C11.0096 13.0205 12.0051 13.3551 12.3612 13.4973C12.6266 13.6027 12.9429 13.5776 13.1368 13.3801C13.3829 13.1259 13.6867 12.7044 13.9961 12.2895C14.2161 11.9919 14.4939 11.955 14.7854 12.0604C15.0824 12.1592 16.654 12.9033 16.9771 13.0574C17.3003 13.2128 17.5134 13.2866 17.5917 13.417C17.6687 13.5473 17.6687 14.1598 17.4034 14.8789Z" fill="#fff" />
                                    </svg>
                                        &nbsp; chat on WhatsApp</a> </div>
                                </div>
                                }
                                <div className="row">
                                    <p className="pl-3 intake mt-1">
                                        {next_intake}
                                    </p>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        {lang === "en" &&
                                            <a href="">
                                                <svg className="social_share_toggle" width="47" height="46" viewBox="0 0 47 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M2.2207 40.334C2.27539 40.7285 2.38281 41.0234 2.54297 41.2188C2.83594 41.5742 3.33789 41.752 4.04883 41.752C4.47461 41.752 4.82031 41.7051 5.08594 41.6113C5.58984 41.4316 5.8418 41.0977 5.8418 40.6094C5.8418 40.3242 5.7168 40.1035 5.4668 39.9473C5.2168 39.7949 4.82422 39.6602 4.28906 39.543L3.375 39.3379C2.47656 39.1348 1.85547 38.9141 1.51172 38.6758C0.929688 38.2773 0.638672 37.6543 0.638672 36.8066C0.638672 36.0332 0.919922 35.3906 1.48242 34.8789C2.04492 34.3672 2.87109 34.1113 3.96094 34.1113C4.87109 34.1113 5.64648 34.3535 6.28711 34.8379C6.93164 35.3184 7.26953 36.0176 7.30078 36.9355H5.56641C5.53516 36.416 5.30859 36.0469 4.88672 35.8281C4.60547 35.6836 4.25586 35.6113 3.83789 35.6113C3.37305 35.6113 3.00195 35.7051 2.72461 35.8926C2.44727 36.0801 2.30859 36.3418 2.30859 36.6777C2.30859 36.9863 2.44531 37.2168 2.71875 37.3691C2.89453 37.4707 3.26953 37.5898 3.84375 37.7266L5.33203 38.084C5.98438 38.2402 6.47656 38.4492 6.80859 38.7109C7.32422 39.1172 7.58203 39.7051 7.58203 40.4746C7.58203 41.2637 7.2793 41.9199 6.67383 42.4434C6.07227 42.9629 5.2207 43.2227 4.11914 43.2227C2.99414 43.2227 2.10938 42.9668 1.46484 42.4551C0.820312 41.9395 0.498047 41.2324 0.498047 40.334H2.2207ZM10.1062 43V34.3633H11.8934V37.6562H15.2684V34.3633H17.0613V43H15.2684V39.1445H11.8934V43H10.1062ZM22.3512 39.7363H24.5426L23.4645 36.3379L22.3512 39.7363ZM22.4625 34.3633H24.5016L27.5602 43H25.6031L25.0465 41.2246H21.8648L21.2672 43H19.3805L22.4625 34.3633ZM31.6664 35.8633V38.1836H33.7113C34.1176 38.1836 34.4223 38.1367 34.6254 38.043C34.9848 37.8789 35.1645 37.5547 35.1645 37.0703C35.1645 36.5469 34.9906 36.1953 34.643 36.0156C34.4477 35.9141 34.1547 35.8633 33.7641 35.8633H31.6664ZM34.1391 34.3633C34.7445 34.375 35.2094 34.4492 35.5336 34.5859C35.8617 34.7227 36.1391 34.9238 36.3656 35.1895C36.5531 35.4082 36.7016 35.6504 36.8109 35.916C36.9203 36.1816 36.975 36.4844 36.975 36.8242C36.975 37.2344 36.8715 37.6387 36.6645 38.0371C36.4574 38.4316 36.1156 38.7109 35.6391 38.875C36.0375 39.0352 36.3188 39.2637 36.4828 39.5605C36.6508 39.8535 36.7348 40.3027 36.7348 40.9082V41.4883C36.7348 41.8828 36.7504 42.1504 36.7816 42.291C36.8285 42.5137 36.9379 42.6777 37.1098 42.7832V43H35.1234C35.0688 42.8086 35.0297 42.6543 35.0063 42.5371C34.9594 42.2949 34.934 42.0469 34.9301 41.793L34.9184 40.9902C34.9105 40.4395 34.809 40.0723 34.6137 39.8887C34.4223 39.7051 34.0609 39.6133 33.5297 39.6133H31.6664V43H29.9027V34.3633H34.1391ZM46.1203 35.8926H41.55V37.7266H45.7453V39.2266H41.55V41.4473H46.3312V43H39.7863V34.3633H46.1203V35.8926Z" fill="#253645" />
                                                    <path d="M14.5 13.5V22.5C14.5 23.0967 14.7371 23.669 15.159 24.091C15.581 24.5129 16.1533 24.75 16.75 24.75H30.25C30.8467 24.75 31.419 24.5129 31.841 24.091C32.2629 23.669 32.5 23.0967 32.5 22.5V13.5" stroke="#253645" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M28 6.75L23.5 2.25L19 6.75" stroke="#253645" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                                                    <path d="M23.5 2.25V16.875" stroke="#253645" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                                                    <rect x="10" width="27" height="27" />
                                                </svg>
                                            </a>
                                        }
                                        {lang === "fr" &&

                                            <a href="">
                                                <img className="social_share_toggle" style={{ width: "67px" }} src={FrenchSVG} />
                                            </a>
                                        }
                                    </div>
                                    <div className="col-9  social_view pt-1">
                                        <a data-toggle='tooltip' data-placement='top' title='Share on WhatsApp' className='social-share wattsap' target='_blank' data-action='share/whatsapp/share'>
                                            <svg width="22" height="22" viewBox="0 0 22 22" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M11.0027 0H10.9973C4.93213 0 0 4.72563 0 10.5365C0 12.8414 0.7755 14.9777 2.09413 16.7123L0.72325 20.6266L4.95138 19.3319C6.69075 20.4356 8.76562 21.0731 11.0027 21.0731C17.0679 21.0731 22 16.3461 22 10.5365C22 4.72695 17.0679 0 11.0027 0ZM17.4034 14.8789C17.138 15.5967 16.0847 16.192 15.2446 16.3659C14.6699 16.4831 13.9191 16.5766 11.3919 15.573C8.15925 14.2902 6.0775 11.1437 5.91525 10.9396C5.75988 10.7354 4.609 9.27346 4.609 7.76147C4.609 6.24948 5.41062 5.51324 5.73375 5.19714C5.99913 4.93768 6.43775 4.81915 6.8585 4.81915C6.99463 4.81915 7.117 4.82573 7.227 4.831C7.55013 4.84417 7.71237 4.86261 7.9255 5.35124C8.19088 5.96368 8.83712 7.47567 8.91412 7.63108C8.9925 7.7865 9.07088 7.99723 8.96088 8.20137C8.85775 8.4121 8.767 8.50561 8.60475 8.68473C8.4425 8.86386 8.2885 9.00083 8.12625 9.19312C7.97775 9.36039 7.81 9.53951 7.997 9.84902C8.184 10.1519 8.83025 11.1621 9.78175 11.9734C11.0096 13.0205 12.0051 13.3551 12.3612 13.4973C12.6266 13.6027 12.9429 13.5776 13.1368 13.3801C13.3829 13.1259 13.6867 12.7044 13.9961 12.2895C14.2161 11.9919 14.4939 11.955 14.7854 12.0604C15.0824 12.1592 16.654 12.9033 16.9771 13.0574C17.3003 13.2128 17.5134 13.2866 17.5917 13.417C17.6687 13.5473 17.6687 14.1598 17.4034 14.8789Z" fill="#253645" />
                                            </svg>
                                        </a>
                                        <a data-toggle="tooltip" data-placement="top" title="Share on Facebook" className='social-share facebook' href="">
                                            <svg width="22" height="22" viewBox="0 0 22 22" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                                                <g clipPath="url(#clip0)">
                                                    <path d="M11 0C4.92525 0 0 4.36739 0 9.75551C0 12.8256 1.59913 15.5638 4.09888 17.3523V21.0731L7.84437 19.104C8.844 19.3688 9.90275 19.5123 11 19.5123C17.0748 19.5123 22 15.1449 22 9.75683C22 4.36871 17.0748 0 11 0ZM12.0931 13.1377L9.29225 10.2758L3.82662 13.1377L9.8395 7.02391L12.7091 9.8859L18.106 7.02391L12.0931 13.1377Z" fill="#253645" />
                                                </g>
                                                <defs>
                                                    <clipPath id="clip0">
                                                        <rect width="22" height="21.0731" fill="#253645" />
                                                    </clipPath>
                                                </defs>
                                            </svg>
                                        </a>
                                        <a data-toggle="tooltip" data-placement="top" title="Copy URL to clipboard" className='social-share copy' href="">
                                            <svg width="25" height="25" viewBox="0 0 25 25" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M20.3982 9.82129H11.7444C10.6823 9.82129 9.82129 10.6823 9.82129 11.7444V20.3982C9.82129 21.4603 10.6823 22.3213 11.7444 22.3213H20.3982C21.4603 22.3213 22.3213 21.4603 22.3213 20.3982V11.7444C22.3213 10.6823 21.4603 9.82129 20.3982 9.82129Z" stroke="#253645" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                <path d="M5.56333 15.1787H4.60179C4.09176 15.1787 3.60261 14.9761 3.24197 14.6155C2.88132 14.2548 2.67871 13.7657 2.67871 13.2556V4.60179C2.67871 4.09176 2.88132 3.60261 3.24197 3.24197C3.60261 2.88132 4.09176 2.67871 4.60179 2.67871H13.2556C13.7657 2.67871 14.2548 2.88132 14.6155 3.24197C14.9761 3.60261 15.1787 4.09176 15.1787 4.60179V5.56333" stroke="#253645" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </a>
                                        <a data-toggle="tooltip" data-placement="top" className='social-share email' title="Share via Email" href="">
                                            <svg width="25" height="25" viewBox="0 0 25 25" fill="#253645" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22.8027 2.97852H2.19727C0.987207 2.97852 0 3.96465 0 5.17578V19.8242C0 21.0312 0.982813 22.0215 2.19727 22.0215H22.8027C24.0098 22.0215 25 21.0387 25 19.8242V5.17578C25 3.96875 24.0172 2.97852 22.8027 2.97852ZM22.4993 4.44336L12.5466 14.3961L2.50776 4.44336H22.4993ZM1.46484 19.5209V5.47212L8.51948 12.4663L1.46484 19.5209ZM2.50063 20.5566L9.55972 13.4976L12.0332 15.9498C12.3195 16.2337 12.7816 16.2328 13.0667 15.9476L15.4785 13.5358L22.4994 20.5566H2.50063ZM23.5352 19.5208L16.5143 12.5L23.5352 5.4791V19.5208Z" fill="#253645" />
                                            </svg>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-12  p-0 order-1 order-md-2 image fill">
                    {program_header_image.localFile &&
                        <Img fluid={program_header_image.localFile.childImageSharp.fluid} alt={program_header_image.alt_text} />
                    }

                </div>
            </div>
            <div className="row">
                <div className="pr-4 pt-3 m-auto m-md-0 ml-md-auto">
                    {currentLang === "en" ? "Accredited by:" : "Accréditation: "}  <span className="d-block d-md-none">&nbsp; &nbsp;</span> <br className="d-block d-md-none" /> <br className="d-block d-md-none" /><img alt='' src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAgAGcDAREAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+Pj9nf/gqb/wUZ/ZQezT9n/8AbR/aG+H+k2DQtbeEo/iNrniTwAptzIYRL8OvGE/iDwJcqnmyDZc+HZkKuyspU4oA/rH+FX/BxB/wU91r/gh9+0f+11rnxO+HesftF/DP9sz4Tfs++FviTc/B/wAC2skHgnxx8P7vxRrF9eeEdK03TvAN54ki1G0VLC+k8KDT4bWWWKbSbmTypogD+WL9on/gsH/wU/8A2rFvLb45ftxftCeJ9G1Hd9v8I6B43uvhp4BvN0csR+0/Dz4XR+DPA037qaaJfM8PNtilljXCSMpAP9D7/gqt408XeAP+DV3QvH/gfxPr3hHxx4R/Ze/4JfeKPC3i/wAOare6P4k8PeJNH+NP7IepaVrujazYTQX+natp1/BDeWd/azx3MFzGk0ciuoNAH3P/AMEjv+CknxA/aTtvHX7Gn7a/h21+E/8AwUn/AGTtN0PS/jh4Kmk0+10341+DrjT9PPhn9pj4VQ2kVlZap4N8fWV7peqa/BoNlHpfh3V9b0+W1ttM0LxH4esYgD4n/Yv1/XZ/+Cl//B0tZz61q01poGg/sbvoNrLqN5Jb6I9x+zD8fridtIgeYxaa088MM8zWawmSaKORyzorAA+N/wDgjl/wTc/4JbftO/8ABNP9lX47ftReOPFmqfHv4g+E/Fl/8R7/AFD9vj9oTwFe3OqaX8TfG/h7T2uPCOh/Hnw/pWiSLoWkaVGbez0awjmCC7aJpbh5ZAD7w+PPiH4wftO/tYfD/wD4Iif8E+/jT46/Zd/Zk/ZS/Zy+GPjj9tb9p74f+K9S8T/G7Tvhp4j0PSNO+Bn7OPwf+KPiXUvEXiLRvH3j7wS9l4y1f4q6pqN/4huNBmGtW+s3v9h6z4a8eAHtkv8AwbTf8Eq7O2XWvBngj9ob4a/GyNZLqD9p3wL+11+0fY/H6DxHLBsbxgniTWPiLrnhR/EQvQmrxGfwTNo0Gpq8sOjx291e210AfKni258Zfsl/8Fn/APgkh8J/2hf2s9a+JFh4S/YZ/a70bxj8Z/i14i0zwHF8Sp7TXdbTwNrXxCt5dVtfDGoeNrbQJNE0e81i5eS81vWrC41dSLu/kUAH66ft2/tWfs5XH7EH7ZMHhv8AaS+Cc/iKf9lT9oaHQIdD+MPgWTWptak+EXi9NKi0eOw8RNfSapJfNAmnpZA3bXZhW2BmKCgD+aP9qPVLnxZ/wb3f8EE9P8b+NPFVrofxP/bC/YB8GfFfX4fHviXwnrGt+BPGXh74waZ43ttZ8caPrWk+ILK1v9HmuWv9Uj1q0ubUxpqEd5b3FrDcRAH7M+Av+CNX/BGu28WaVP8AD/W/GGpeLk+3f2RZaX/wUL/aW8S3827TbxL/AMjRZf2gtTS98vTGvZZd1jP9mhjkux5ZgE0YB/j60Afv78Gv+Va/9s//ALSf/s6f+qb1WgD8AqAP9Tv/AILC/wDKpdL/ANmgf8E0P/VufskUAfoP/wAFMv8Agm54y/ag8I/A39rz9jjxBo/we/4KXfsi6Xovin9nL4szLHY2HxC0K00+a41/9nT4sXKvDDrPw0+INrqOsaZBBrhuNN0O/wBc1SKY23hzxT4yg1EA/GD/AIIe/tGeMP2sv2g/+Dkb4+fEX4Ua58C/iR418B/slWPxL+EHiOO6i1f4efErwN8CP2ovh58QvDE8OoQ22pRW+n+MvCmtpp0WqW1tqsWmtaJqlvBqC3MSAH0N/wAEIf8Agkp/wTT/AGhv+CS37GXxk+N37FPwC+JvxT8ceCfGl74v8deLPBNpqPiHxDeaf8W/iDollc6pfM6tczW+k6ZYWEcjLu8i1iUklSSAekalqfgD/gjd/wAFn/iV8XPixpdr8Lv2Cv8Agpb8Bv2aPg/4M+Nj2gtfhX+z78f/ANkr4exfCf4efBnxvrCwvH4F8K+LPhZoEd54c1zWLqDS7nVLiOOWX+yfDXibVfDgB/SjrnxL+HPhnwTc/EvxJ4/8E+H/AIcWWmjWbz4ga54q0LSfBNppDAEarc+K7+/t9Cg00ggi+lv0tSCCJcUAfysftW6N+xx/wU4/4LYf8EprvW/D3w//AGov2XPif+xX+2Dr2hR+I9FudT8EeMJPBPirXNMsvEFhZ6rbWM15Z2niHQru40TVUhFpqEEUGqadNdafd21xMAfb/wC27/wRX/4JQ+BP2L/2u/HHg79gj9nDw34u8G/swfH3xX4V8RaT4DtbTVdB8SeHvhT4s1fQ9a0y6SXfbahpep2drfWdwnzQ3MEci8qKAPxg/aN8KeG/Hf8Awb3/APBvH4H8Y6Lp/iTwj4y/bf8A+CcfhTxV4d1a3W70rXvDfiHSPi7pGuaLqdq/yXOn6ppl5dWN5bv8s1tPJG3DGgD+n74W/wDBIH/gmL8EvHehfE/4SfsQ/AD4e/ELwz/af/CP+MPC3gu30vXdI/trR9Q8Par9hvoZhLB/aGiatqWl3W0/vbO9uIT8shoA/wASagD9/fg1/wAq1/7Z/wD2k/8A2dP/AFTeq0AfgFQB/tLfD74YfsyfHD/gkX+yH8FP2wPC2n+NfgF8UP2WP2PNE8YeGdYk8V2uh39zoHw9+G/j/wAO3mvar4Nu9P1jQND8P+IPB2m+JtW8RTalpmg6FYaNPqfifUbPw5bapOgB97eD/j/8CvE82k+HvCnxM8F3Wp3V5B4e0jw3HrVnDrU+pQ6fcXT6PY6RPKl/d3mmW2m6jDqkFrDOdJu9H1iw1Fre90nUYLYA+btEtP2D9A8e/tR/HK1s/BPgzxz+0KujfBT9pfxNLqt3pNz8Th+z9rvxY+Efh621/SdO1aS2t9e0q81f4p+E9P1yPTdH8beKvDfh2SaafVfC3gbQrzRgD8xLL/gix/wbuXK6JHd/sh/C3RdT1/wrpnjSy0LW/Hvx5sNZTw7q/h/RfEdlqM1ofiVIhtZrTxDpGnxXlvNcWN34ju08L2N1deIQ2mgA/T/wF8Pf2H4P2W/B37Lmh/Brwjqv7J9j8Hvhzf2nwg8deFovG/gbQ/h18UptUufht4e1/wAP+P7nxFqfiDWvEmr2t5ZaV4dt4fFHiO31qXTUngtDqWizXYB+ZWk/8ET/APg3i8PeNLLxpY/s2/CzUr1fGlloGleFdQ+Lnx58bfDm/wDHlxvltdN0v4U6t8Std+H+v6heWzW//EntfDOo6VNY6hpcw0/y/ENlNqYB+jK6J/wTmsPin8JP2gdM0v4O6T8Sv2e/Avin4P8Awi8R+E9b0/SNF+Fnwu1rVPDuh+KtC0jRfC+u2vw20PwzHbeLfCt9dwX2npqWiaL4i0SOG20+61Kz06UA+rb7W/gt+0LoPxV+C97qWj+O9F1Dw3rXw/8Aix4LE+pWc6aF4y0nUfD/AIh8Ma15J07UtOvLjTbi/wBL1e0t7i11nRJphDef2bfPACAfPXxR/wCCZn7C/wAaf2X/AIYfsYfE/wDZ88O+K/2YvgzfeG9S+GfwmufEXjux0fwrfeEdF17w74cuLbVtJ8Vaf4nvW0zRvE+vWca6rrl/HONRknuknuYreaEA+Lv+Ib3/AIIlf9GEfD//AMOD8cP/AJ6FAHxX/wAFRP8Ag1Z/Yd/bgl8SfFT9nGGz/Yz/AGi9Sje7e7+H2g2K/Abxtq5YyTXXjb4S6fb2UGh6pqB4ufEnw5vfDTy3c11rXiHQPFuqTySOAfht4U/4IB/8FNfAf/BJT9qT/gn1d/Bzw7rvxv8AHn/BRX4EfEXwRrnh74heF5/hb4m+FOhfC7XNI134oW3jLVbvSJtJ8M6PfW00F/pXibRtC8dpMbO1i8HTXur6NbaiAfq1/wAEuf8Ag0b/AGUf2X/+ET+LX7deraX+138dNNkt9WX4bx2s8H7MXhDU4sPFav4b1Wytdd+MclnKCzXvj2DSfCGoxyCC7+GRe2S+nAP6qfHt9Y/DzwhoSeH/AIV3HjHS9C83TNI8K+EtEs3j8N6Vpng3xCttHpOj2tlP9kt7nT7IeBNHstLsFtluvE2nadfy6T4bm1bVNPAPluTWvGPh27+ENzbfsleDxbzeF/CvibV9X0PwRBHcfCvxB4u8H/F3xj4o0jTLV7Gyube80jxT8MvhFoGr3CTaTImpeJ9Pu9T8q4GhwwAHkWg+NdC+OHge3v7z9iBp/BPxK+J3iDX7WXwjoiaXrVtc+CIn8c33ivxhqa+D9MD+IvE/xC8U+PbDTPEVjf2M994j13xnbwatBq0d14n8RAHd/EO31fwzqnijTfCn7E3w18Vrd2Pwd8P2dva/C+zksL7QfETS6r8QdO1bxOuh2+l6xpvhH+yY9J0LSRp+n2EGvXmk6p4nn0HSYPMmAO21X4gLH4HuNf0v9jLWNUg8G3GmeDvhv4Lu/B0umave6F4ZY3XhnUNP0A+AbyLwV4P0e7h03/hHILmJNZ0R49Wmt/Demi10r/hIgDQ8P6jYTeJdK04/sZW+lS2fjDwt4ibxQ3g/w1p1jpGq+Ib/AMLW154xsJhoEs0nijQJtSOq6hc2Fy1xHo/hm7vdQ1vSdbtv+EdtAD6Gf4G/BSSwj0p/g/8AC19LhmFxFpr/AA/8JtYRXC+ftnjszpBt0mH2q6xKsYcfaZ8N++k3AHVaB4I8F+Fbm/vPC/hDwv4bvNVkM2qXWgaBpOj3OpSsVJlv59OtLeW8kJRCXuGkYlFyflGADqKACgD/2Q==" />
                </div>
            </div>
        </>


    )
ProgramOverview.propTypes = {

    title: PropTypes.string,
    lavender_background: PropTypes.string,
    lang: PropTypes.string,
    program_overview: PropTypes.string,
    next_intake: PropTypes.string,
    wattsapShare: PropTypes.string,
    button_1: PropTypes.object,
    button_2: PropTypes.object,
    program_header_image: PropTypes.object,
    parent: PropTypes.object,
    translink: PropTypes.object,
    currentLang: PropTypes.string,

}


export default ProgramOverview;
