import React from 'react';
import { PropTypes } from 'prop-types';
import { Link, StaticQuery, graphql } from "gatsby"
import './styles/fixed-ctas.css';

class FixedCTAs extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {

    window.addEventListener('scroll', this.handleScroll, { passive: true })
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll(event) {
    // do something like call `this.setState`
    // access window.scrollY etc

    let body = document.body

    let height = body.scrollHeight;
    // console.log(height)

    let scroll = window.scrollY
    // console.log(scroll)
    // if ( scroll >= height / 10 ) {
    //     console.log('true')
    //     let element = document.getElementById("fixed-ctas");
    //     element.classList.add("slide");
    // }  
    // if ((scroll < 200) || (scroll > (height - 2000))) {
    if ((scroll < 200) || (scroll > (height - 1000))) {

      // console.log('false')
      let element = document.getElementById("fixed-ctas");
      element.classList.remove("slide");
    } else {
      let element = document.getElementById("fixed-ctas");
      element.classList.add("slide");
    }


  }
  render() {

    return (
      <div id="fixed-ctas" className="fixed-ctas  d-none d-sm-block">
        <div className="menu-header-actions-container" style={{ paddingLeft: "30px" }}>
          {this.props.currentLang === "en" &&
            <ul>
              {this.props.headerActionsMenu.nodes[0].menuItems.nodes.map((item, i) => (

                (() => {
                  if (i === 0) {
                    return (
                      <div key={item.id}>
                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )
                  } else if (i === 1) {
                    return (
                      <div key={item.id}>
                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )
                  } else if (i === 2) {
                    return (
                      <div key={item.id}>
                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )

                  } else if (i === 3) {
                    return (
                      <div key={item.id}>
                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62"}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )
                  }
                }
                )()

              ))}
            </ul>
          }
          {this.props.currentLang === "fr" &&
            <ul>
           {this.props.headerActionsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (

                (() => {
                  if (i === 0) {
                    return (
                      <div key={item.id}>
                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )
                  } else if (i === 1) {
                    return (
                      <div key={item.id}>
                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )
                  } else if (i === 2) {
                    return (
                      <div key={item.id}>
                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )

                  } else if (i === 3) {
                    return (
                      <div key={item.id}>
                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                          <Link to={item.url} >
                            {item.label}
                          </Link>
                        </li>
                      </div>
                    )
                  }
                }
                )()
              ))}
            </ul>
          }
        </div>
      </div>
    )
  }
}


export default (props) => (
  <StaticQuery
    query={graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
        
        headerActionsMenu: allWpMenu(filter: {name: {eq: "Header actions"}}) {
          nodes {
            count
            menuItems {
              nodes {
                label
                url
                id
              }
            }
          }
        }
        headerActionsMenuFR: allWpMenu(filter: {name: {eq: "Header actions FR"}}) {
          nodes {
            count
            menuItems {
              nodes {
                label
                url
                id
              }
            }
          }
        }
        

      }
    `}
    render={(data) => (
      <FixedCTAs site={data.site} headerActionsMenu={data.headerActionsMenu} headerActionsMenuFR={data.headerActionsMenuFR} {...props} />
    )}
  />
)

FixedCTAs.propTypes = {


  currentLang: PropTypes.string,



}
