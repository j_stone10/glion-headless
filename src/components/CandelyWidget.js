import React, { Component } from "react"
import "../components/styles/calendly.css"

class CalendlyEmbed extends Component {
  componentDidMount() {
    const head = document.querySelector("head")
    const script = document.createElement("script")
    script.setAttribute(
      "src",
      "https://assets.calendly.com/assets/external/widget.js"
    )
    head.appendChild(script)
    setTimeout(() => {
      // console.log(window.Calendly)
    }, 1500)
  }

  render() {
    return (
      <div>
        <div id="schedule_form">
          <div
            className="calendly-inline-widget"
            data-url={this.props.dataUrl}
            style={{
              minWidth: "320px",
              height: "630px",
              textAlign: "center",
              position: "relative",
            }}
          />
        </div>
      </div>
    )
  }
}

export default CalendlyEmbed
