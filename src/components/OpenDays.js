import React from 'react';
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"


const OpenDays = ({
   block_1_title,
   block_1_text,
   block_1_image,
   block_1_link,
   block_2_title,
   block_2_text,
   block_2_image,
   block_2_link
}) => (

      <>
         <div className="wrapper row open-days mt-md-5 mb-5 ">
            <div className="col-sm-6 col-12  blue-bg order-2 order-sm-1">
               <div className="row scrollElement">
                  <div className="col-md-6 col-9 py-5  text-center m-auto">
                  <h3 dangerouslySetInnerHTML={{ __html: block_1_title}}></h3>
                     <p>{block_1_text}</p>
                  </div>
               </div>
               <div className="row scrollElement">
                  <div className="col-md-8 col-11 m-auto">
                     {block_1_image.localFile &&
                        <Img fluid={block_1_image.localFile.childImageSharp.fluid} alt={block_1_image.altText} />
                     }
                  </div>
               </div>
               <div className="row scrollElement">
                  <div className="learn_more_container m-auto py-5 text-center">

                     <a className="learn_more white" href={block_1_link.url} >{block_1_link.title}</a>
                  </div>
               </div>
            </div>
            <div className="col-sm-6 col-12 beige-bg  order-1 order-sm-2">
               <div className="row scrollElementOffset">
                  <div className="col-md-6 col-9 py-5  text-center m-auto">
                     <h3 dangerouslySetInnerHTML={{ __html: block_2_title }} />
                     <p >{block_2_text}</p>
                  </div>
               </div>
               <div className="row scrollElementOffset">
                  <div className="col-md-8 col-11 m-auto">
                     {block_2_image.localFile &&
                        <Img fluid={block_2_image.localFile.childImageSharp.fluid} alt={block_2_image.altText} />
                     }
                  </div>
               </div>
               <div className="row scrollElementOffset">
                  <div className="learn_more_container m-auto py-5 text-center">
                     <a className="learn_more" href={block_2_link.url} >{block_2_link.title}</a>
                  </div>
               </div>
            </div>
         </div>
      </>


   )
OpenDays.propTypes = {
   block_1_title: PropTypes.string,
   block_1_text: PropTypes.string,
   block_1_image: PropTypes.object,
   block_1_link: PropTypes.object,
   block_2_title: PropTypes.string,
   block_2_text: PropTypes.string,
   block_2_image: PropTypes.object,
   block_2_link: PropTypes.object,

}


export default OpenDays;
