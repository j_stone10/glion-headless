import React, { useRef, useEffect } from "react"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import styled from "styled-components"
import Helmet from "react-helmet"
import ManageCookie from "./ManageCookie"
import Header from "./Header"
import CookieConsent, { Cookies } from "react-cookie-consent"
import Footer from "./Footer"
import { gsap, Power4, CSSRulePlugin } from "gsap/all"
import { CSSPlugin } from "gsap/CSSPlugin"
import * as ScrollMagic from "scrollmagic-with-ssr" // Or use scrollmagic-with-ssr to avoid server rendering problems
import { TweenMax, TimelineMax } from "gsap" // Also works with TweenLite and TimelineLite
import $ from "jquery"

// import 'animation.gsap'
// import 'debug.addIndicators';

import { ScrollMagicPluginGsap } from "scrollmagic-plugin-gsap"

import "../../node_modules/bootstrap/dist/css/bootstrap.css"

import "./layout.css"

const Primary = styled.main`
  padding-top: 0px;
`
gsap.registerPlugin(CSSRulePlugin)
gsap.config({ nullTargetWarn: false })
const Layout = ({
  children,
  currentLang,
  polylang_translations,
  frenchHome,
  showTopBannerCallout,
  tbCalloutContent,
}) => {
  let scrollMag = useRef(null)
  useEffect(() => {

    // if (!Cookies.get("gatsby-plugin-google-tagmanager")) { 
    //   $('body').addClass('fixed')
      
    // } else {
    //   // $('body').classList.remove('fixed')
    // }
    ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax)

    var rule = CSSRulePlugin.getRule(".container-fluid:after")

    var tl3 = gsap.timeline()

    tl3.to(rule, 0.8, { cssRule: { scaleY: 0, ease: Power4.easeOut } })
    if ($(".background_image img").length > 0) {
      tl3.from(
        ".background_image img",
        1.8,
        {
          scaleX: 1.2,
          scaleY: 1.2,
          transformOrigin: "center",
          ease: Power4.easeOut,
        },
        "-=.4"
      )
    }
    tl3.to("header", 0.4, { margin: 0, ease: Power4.easeOut }, "-=1")
    if ($(".scrollElementStart").length > 0) {
      tl3.from(
        ".scrollElementStart",
        0.6,
        { y: 150, opacity: 0, ease: Power4.easeOut },
        "-=.5"
      )
    }

    /***************************************************
Remove animations from blog posts
***************************************************/

    //  setTimeout(function(){
    if ($(".post-template-default").length > 0) {
      if ($(".post-template-default").find(".wrapper").length > 0) {
        // $(".scrollElement").addClass("scrollElementFixed");
        $(".scrollElement").removeClass("scrollElement")
        // alert("remove animation from form for pages where form is at the top");
      } else {
        // alert("doesnt have form in block");
      }
    }
    // }, 200);

    setTimeout(function() {
      if ($(".block-editorial-content-01").find(".mktoForm").length > 0) {
        $(".scrollElement").addClass("scrollElementFixed")
        $(".scrollElement").removeClass("scrollElement")
        // alert("remove animation from form for pages where form is at the top");
      } else {
        // alert("doesnt have form in block");
      }
    }, 1200)

    if ($(window).width() >= 768) {
      if (document.querySelector(".wrapper")) {
        var controller = new ScrollMagic.Controller()

        $(".wrapper").each(function() {
          var tl = new TimelineMax()
          var target = $(this).find(".scrollElement")

          tl.from(target, 0.5, { y: 150, opacity: 0 })

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.6,
            // duration: 200,
          })

            .setTween(tl)

            // .addIndicators()
            .addTo(controller)
        })

        var controller2 = new ScrollMagic.Controller()

        $(".wrapper").each(function() {
          var tl2 = new TimelineMax()
          var target2 = $(this).find(".scrollElementOffset")

          tl2.from(target2, 0.5, { y: 150, opacity: 0 })

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.5,
            // duration: 200,
          })

            .setTween(tl2)

            //  .addIndicators()
            .addTo(controller2)
        })

        $(".wrapper").each(function() {
          var tl2 = new TimelineMax()
          var target2 = $(this).find(".scrollElementOffsetThird")

          tl2.from(target2, 0.5, { y: 200, opacity: 0 })

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.4,
            // duration: 200,
          })

            .setTween(tl2)

            //  .addIndicators()
            .addTo(controller2)
        })
        $(".wrapper").each(function() {
          var tl2 = new TimelineMax()
          var target2 = $(this).find(".scrollElementOffsetLast")

          tl2.from(target2, 0.5, { y: 250, opacity: 0 })

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.3,
            // duration: 200,
          })

            .setTween(tl2)

            //  .addIndicators()
            .addTo(controller2)
        })
      }
    }

    /***************************************************
     Blog Scrollmagic transitions
      ***************************************************/

    var controller = new ScrollMagic.Controller()

    // $(".blogPost").each(function () {
    //   var tl4 = new TimelineMax();
    //   var target = $(this).find("img");

    //   tl4.from(target, 1.8, { scaleX: 1.5, scaleY: 1.5, opacity: 0, transformOrigin: "50% 50%", ease: Power4.easeOut });

    //   var scene = new ScrollMagic.Scene({
    //     triggerElement: this,
    //     triggerHook: 0.6,
    //     // duration: 200,
    //   })

    //     .setTween(tl4)

    //     // .addIndicators()
    //     .addTo(controller);
    // });

    //  console.log(showTopBannerCallout)
    //  console.log(tbCalloutContent)
  }, [])

  return (
    <>
      {process.env.NODE_ENV === `production` && (
        <Helmet>
          <script>
            {`  window.dataLayer = window.dataLayer || [];
            dataLayer.push({"event": "liveperson_pageview"});
`}
          </script>
          <script>
            {`window.dataLayer = window.dataLayer || [];
              window.dataLayer.push({
                    "platform": "gatsby"
              });
                    /* Separate DEV and LIVE output for Google Tag Manager using 
              Client-side switch. */
                    var GTMcode = "GTM-WP8B6B"; // DEFAULT - unmatched LIVE
                    var myHostname = window.location.hostname;
                    var isDevHostname = 
              /(^|.+\.)(lesroches-v2-preview\.elca-services\.com|(.+)-admin\.glion\.edu|eu-lon05\.marketodesigner\.com|localhost|workspace|212\.53\.86\.164)$/i;
                    if( isDevHostname.exec(myHostname) ) {
                        GTMcode = "GTM-5VDVPWT"; // DEV
                    };
              /*****************************************************************/
                    /* GTM Failsafe Switch
                    This renders GTM in Safe Mode - so no custom code can be injected.
                    Only enable as last resort. Try GTM rollback before enabling this 
              method.
                    */
                    /*
                    window.dataLayer = window.dataLayer || [];
                    dataLayer.push({"gtm.blacklist": ["customScripts"] });
                    */
              /*****************************************************************/
                    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                            new Date().getTime(),event:'gtm.js'});var 
              f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f) 
              ;
                    })(window,document,'script','dataLayer', GTMcode );`}
          </script>
        </Helmet>
      )}
      <Header
        polylang_translations={polylang_translations}
        currentLang={currentLang}
        frenchHome={frenchHome}
        showTopBannerCallout={showTopBannerCallout}
        tbCalloutContent={tbCalloutContent}
      />

      <Primary
        id="primary"
        className="content-area"
        ref={el => {
          scrollMag = el
        }}
      >
        {children}
      </Primary>
      <ManageCookie currentLang={currentLang} />
      <Footer currentLang={currentLang} />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  polylang_translations: PropTypes.array,
  currentLang: PropTypes.string,
  frenchHome: PropTypes.string,
  showTopBannerCallout: PropTypes.bool,
  tbCalloutContent: PropTypes.string,
}

export default Layout
