import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';

import '../components/styles/brochure-download.css';

const marketoScriptId = 'mktoForms';

function BrochureDownload({ formId, lang, title }) {
   const [isLoaded, setIsLoaded] = useState(false);

   useEffect(() => {
      if (!document.getElementById(marketoScriptId)) {
         loadScript();
      } else {
         setIsLoaded(true);
      }
   }, []);

   useEffect(() => {
      isLoaded &&
         window.MktoForms2.loadForm(
            '//app-lon05.marketo.com',
            '411-MKX-040',
            formId
         );
   }, [isLoaded, formId]);

   const loadScript = () => {
      var s = document.createElement('script');
      s.id = marketoScriptId;
      s.type = 'text/javascript';
      s.async = true;
      s.src = '//app-lon05.marketo.com/js/forms2/js/forms2.min.js';
      s.onreadystatechange = function () {
         if (this.readyState === 'complete' || this.readyState === 'loaded') {
            setIsLoaded(true);
         }
      };
      s.onload = () => setIsLoaded(true);
      document.getElementsByTagName('head')[0].appendChild(s);
   };

   return (
      <div id='DownloadBrochure' className="row blue-bg line-after py-5">
         <div className="col-md-5 col-12 p-5">
            {title !== null && <h2 className="text-center text-md-left">{title}</h2>}
            {title == null && lang === "en" && <h2 className="text-center text-md-left">Download <br />a brochure </h2> }
            {title == null && lang === "fr" && <h2 className="text-center text-md-left">  Télécharger <br /> une brochure <br /> </h2>}
         </div>
         <div className="col-md-7 col-12 p-5">
            <form id={`mktoForm_${formId}`}></form>
         </div>
      </div>
   );
}



BrochureDownload.propTypes = {

   title: PropTypes.string,
   formId: PropTypes.string,
   lang: PropTypes.string,

};


export default BrochureDownload;