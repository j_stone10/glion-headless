import React from "react"
import { Link } from 'gatsby';
import { Highlight } from 'react-instantsearch-dom';

import {
  StyledDate,
  StyledReadMore,
} from '../templates/styles/archiveStyles';




const PostPreview = ({ hit }) => (
  <article className="entry-content">
    <Link to={`/blog/${hit.slug}/`}>
      <h2><Highlight hit={hit} attribute="title" /></h2>
    </Link>
    <StyledDate dangerouslySetInnerHTML={{ __html: hit.date }} />
    <Highlight hit={hit} attribute="excerpt" dangerouslySetInnerHTML={{ __html: hit.excerpt }} />

    {/* <Highlight hit={hit} attribute="excerpt" /> */}

    <StyledReadMore to={`/blog/${hit.slug}/`}>
      Read More
              </StyledReadMore>
    <div className="dot_divider">
      <hr /> 
    </div>
  </article>
);

export default PostPreview;
