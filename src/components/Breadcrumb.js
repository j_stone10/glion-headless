import React from 'react';
import { Link } from 'gatsby';
import { PropTypes } from 'prop-types';

import './styles/breadcrumb-styles.css';

const BreadCrumb = ({ parent, title, currentLang}) => (

  <div className="breadcrumb-container d-none d-md-block absolute">
    <ul id="breadcrumbs">
    <li><Link to={currentLang === "en" ? "/" : '/fr/'}>
      {currentLang === "en" ? "Home" : 'PAGE D’ACCUEIL'}
    </Link></li>
    <li className="separator"> - </li>
    {parent ? (
      <>
         <li>
        <Link to={parent.link}>
          <span dangerouslySetInnerHTML={{ __html: parent.title }} />
        </Link>
        </li>
        <li className="separator"> - </li>
      </>
    ) : null}
    <u><li className="noHover" dangerouslySetInnerHTML={{ __html: title }} /></u>
    </ul>
 </div>

);

BreadCrumb.propTypes = {
  parent: PropTypes.object,
  title: PropTypes.string,
  currentLang: PropTypes.string,

};

export default BreadCrumb;
