import React from 'react';
// import { Link } from 'gatsby';
import { PropTypes } from 'prop-types';

import './styles/program-shapes.css';

const ProgramShapesMobile = ({
   semester_shapes_options,
   option_1_block_1,
   option_1_block_2,
   option_1_block_3,
   option_1_block_4,
   option_1_block_5,
   option_1_block_6,
   option_1_block_7,
   option_1_block_8,
   option_2_block_1,
   option_2_block_2,
   option_2_block_3,
   option_2_block_4,
   option_2_block_5,
   asterisk_content_1,
   asterisk_content_2,
   lang
}) => (

      //   *************************
      //   ********* DESKTOP SHAPES *****
      //   *****************************
      <>
         <div className="col-12 p-0 accordion">
            <h6 className="accordion_title_black_text">  {lang === "en" ? "Course overview" : "Aperçu du cours" }</h6>
            <div className="expand-icon"> <img alt='' src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" /></div>
            <div className="accordion_text">
               {semester_shapes_options === 'Option 1' &&
                  <div className="row courses-arrows mb-1 ">
                     <div className="arrow">
                        <div className={`${option_1_block_1.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_1.title}</div>
                                 <div className="semester_title">{option_1_block_1.text}</div>
                                 <div className="semester_signoff">{option_1_block_1.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow">
                        <div className={`${option_1_block_2.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_2.title}</div>
                                 <div className="semester_title">{option_1_block_2.text}</div>
                                 <div className="semester_signoff">{option_1_block_2.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow ">
                        <div className={`${option_1_block_3.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_3.title}</div>
                                 <div className="semester_title">{option_1_block_3.text}</div>
                                 <div className="semester_signoff">{option_1_block_3.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow ">
                        <div className={`${option_1_block_4.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_4.title}</div>
                                 <div className="semester_title">{option_1_block_4.text}</div>
                                 <div className="semester_signoff">{option_1_block_4.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow">
                        <div className={`${option_1_block_5.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_5.title}</div>
                                 <div className="semester_title" dangerouslySetInnerHTML={{ __html: option_1_block_5.text}}></div>
                                 <div className="semester_signoff">{option_1_block_5.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow">
                        <div className={`${option_1_block_6.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_6.title}</div>
                                 <div className="semester_title">{option_1_block_6.text}</div>
                                 <div className="semester_signoff">{option_1_block_6.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow">
                        <div className={`${option_1_block_7.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_7.title}</div>
                                 <div className="semester_title">{option_1_block_7.text}</div>
                                 <div className="semester_signoff">{option_1_block_7.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow ">
                        <div className={`${option_1_block_8.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_1_block_8.title}</div>
                                 <div className="semester_title">{option_1_block_8.text}</div>
                                 <div className="semester_signoff">{option_1_block_8.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               }
               {asterisk_content_1 !=='' &&
                  <div className="row mb-1 ">
                     <div className="col-md-12 col-10 m-auto">
                        <div className="semester_signoff_asterisk">{asterisk_content_1}</div>
                     </div>
                  </div>
               }
               {semester_shapes_options === 'Option 2' &&
                  <div className="row mt-5 mb-1 courses-arrows wide-children">
                     <div className="arrow">
                        <div className={`${option_2_block_1.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_2_block_1.title}</div>
                                 <div className="semester_title">{option_2_block_1.text}</div>
                                 <div className="semester_signoff">{option_2_block_1.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="arrow wide">
                        <div className={`${option_2_block_2.colour} inner-arrow`}>
                           {option_2_block_2.isSplit === 'No' &&
                              <div className="content ">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_2.title}</div>
                                    <div className="semester_title">{option_2_block_2.text}</div>
                                    <div className="semester_signoff">{option_2_block_2.signoff}</div>
                                 </div>
                              </div>
                           }
                           {option_2_block_2.isSplit === 'Yes' &&
                              <>
                                 <div className="content item">
                                    <div className="text">
                                       <div className="semester_num">{option_2_block_2.title1}</div>
                                       <div className="semester_title">{option_2_block_2.text1}</div>
                                       <div className="semester_signoff">{option_2_block_2.signoff1}</div>
                                    </div>
                                 </div>
                                 <div className="content item">
                                    <div className="text">
                                       <div className="semester_num">{option_2_block_2.title2}</div>
                                       <div className="semester_title">{option_2_block_2.text2}</div>
                                       <div className="semester_signoff">{option_2_block_2.signoff2}</div>
                                    </div>
                                 </div>
                              </>
                           }
                        </div>
                     </div>
                     <div className="arrow wide">
                        <div className={`${option_2_block_3.colour} inner-arrow`}>
                           {option_2_block_3.isSplit === 'No' &&
                              <div className="content ">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_3.title}</div>
                                    <div className="semester_title">{option_2_block_3.text}</div>
                                    <div className="semester_signoff">{option_2_block_3.signoff}</div>
                                 </div>
                              </div>
                           }
                           {option_2_block_3.isSplit === 'Yes' &&
                              <>
                                 <div className="content item">
                                    <div className="text">
                                       <div className="semester_num">{option_2_block_3.title1}</div>
                                       <div className="semester_title">{option_2_block_3.text1}</div>
                                       <div className="semester_signoff">{option_2_block_3.signoff1}</div>
                                    </div>
                                 </div>
                                 <div className="content item">
                                    <div className="text">
                                       <div className="semester_num">{option_2_block_3.title2}</div>
                                       <div className="semester_title">{option_2_block_3.text2}</div>
                                       <div className="semester_signoff">{option_2_block_3.signoff2}</div>
                                    </div>
                                 </div>
                              </>
                           }
                        </div>
                     </div>
                     <div className="arrow  wide">
                        <div className={`${option_2_block_4.colour} inner-arrow`}>
                           {option_2_block_4.isSplit === 'No' &&
                              <div className="content ">
                                 <div className="text">
                                    <div className="semester_num">{option_2_block_4.title}</div>
                                    <div className="semester_title">{option_2_block_4.text}</div>
                                    <div className="semester_signoff">{option_2_block_4.signoff}</div>
                                 </div>
                              </div>
                           }
                           {option_2_block_4.isSplit === 'Yes' &&
                              <>
                                 <div className="content item">
                                    <div className="text">
                                       <div className="semester_num">{option_2_block_4.title1}</div>
                                       <div className="semester_title">{option_2_block_4.text1}</div>
                                       <div className="semester_signoff">{option_2_block_4.signoff1}</div>
                                    </div>
                                 </div>
                                 <div className="content item">
                                    <div className="text">
                                       <div className="semester_num">{option_2_block_4.title2}</div>
                                       <div className="semester_title">{option_2_block_4.text2}</div>
                                       <div className="semester_signoff">{option_2_block_4.signoff2}</div>
                                    </div>
                                 </div>
                              </>
                           }
                        </div>
                     </div>
                     <div className="arrow ">
                        <div className={`${option_2_block_5.colour} inner-arrow`}>
                           <div className="content">
                              <div className="text">
                                 <div className="semester_num">{option_2_block_5.title}</div>
                                 <div className="semester_title">{option_2_block_5.text}</div>
                                 <div className="semester_signoff">{option_2_block_5.signoff}</div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               }
               {asterisk_content_2 !=='' &&
                  <div className="row mb-1 ">
                     <div className="col-md-12 col-10 m-auto">
                        <div className="semester_signoff_asterisk">{asterisk_content_2}</div>
                     </div>
                  </div>
               }
            </div>
         </div>
      </>
   )

ProgramShapesMobile.propTypes = {
   semester_shapes_options: PropTypes.string,
   option_1_block_1: PropTypes.object,
   option_1_block_2: PropTypes.object,
   option_1_block_3: PropTypes.object,
   option_1_block_4: PropTypes.object,
   option_1_block_5: PropTypes.object,
   option_1_block_6: PropTypes.object,
   option_1_block_7: PropTypes.object,
   option_1_block_8: PropTypes.object,
   option_2_block_1: PropTypes.object,
   option_2_block_2: PropTypes.object,
   option_2_block_3: PropTypes.object,
   option_2_block_4: PropTypes.object,
   option_2_block_5: PropTypes.object,
   asterisk_content_1: PropTypes.string,
   asterisk_content_2: PropTypes.string,
   lang: PropTypes.string,
};

export default ProgramShapesMobile;
