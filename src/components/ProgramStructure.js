import React from 'react';

// import { Link } from 'gatsby';
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import Parser from 'html-react-parser'

// import Video from "../components/video"
import './styles/program-structure.css';

const ProgramStructure = ({
   tab_1_content,
   tab_2_content,
   tab_3_content,
   tab_4_content,
   tab_5_content,
   tab_fees_content_1,
   tab_fees_content_2,
   allfile,
   lavender_background,
   lang
}) => {

   // console.log(media)
   // console.log(tab_fees_content_2)
   const content1 = Parser(tab_1_content.text, {
      replace: domNode => {


         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }

      },
   })
   const content2 = Parser(tab_2_content.text, {
      replace: domNode => {


         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   const content3 = Parser(tab_3_content.text, {
      replace: domNode => {


         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   const content4 = Parser(tab_4_content.text, {
      replace: domNode => {



         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   const content5 = Parser(tab_5_content.text, {
      replace: domNode => {



         // if (domNode.name === 'video') {
         //    // TODO: also check src attribute for 'wp-content/uploads'
         //    let video = allfile.filter(v => {


         //       // console.dir(domNode.attribs.href)
         //       if (domNode.attribs.src.includes("/")) {
         //          var nameExtension = domNode.attribs.src.split('/').pop()
         //       }


         //       // console.log(nameExtension)
         //       return v.node.relativePath === nameExtension

         //    })
         //    let poster = media.filter(v => {
         //       // console.log("1: " + domNode.attribs.poster)
         //       // console.log("2: " + v.node.sourceUrl)
         //       return v.node.sourceUrl === domNode.attribs.poster
         //    })

         //    if (video != '') {
         //       // console.log(video)
         //       // console.log(poster)
         //       video = video[0].node
         //       poster = poster[0].node.localFile

         //       return <video className="video" controlsList="nodownload" controls src={video.publicURL}
         //          poster={poster.publicURL}
         //       // <video poster="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/20190923_GLION_0103-2.jpg" controls src="https://gy635czgvgogs1a3tdly-admin.glion.edu/wp-content/uploads/2020/07/mountain.mp4"></video>


         //       />
         //       // return <Video videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ" videoTitle="Official Music Video on YouTube"

         //    }
         // }
      },
   })
   return (
      <div className="row program-structure">
         <div className={`col-md-4 col-12 px-0 grey-bg blue-text menu ${lavender_background == true && 'lavender-bg'}`}>
            {lang === "en" ? <h3 className="d-none d-md-block">Program <br /> Structure</h3> : <h3 className="d-none d-md-block">Structure <br /> du programme</h3>}
            <ul className="structure_nav">
               {tab_1_content.title && <li activate_slide="slide1" data_target="0" className="active">{tab_1_content.title}</li>}
               {tab_2_content.title && <li activate_slide="slide2" data_target="100">{tab_2_content.title}</li>}
               {tab_3_content.title && <li activate_slide="slide3" data_target="200">{tab_3_content.title}</li>}
               {tab_4_content.title && <li activate_slide="slide4" data_target="300">{tab_4_content.title}</li>}
               {tab_5_content.title && <li activate_slide="slide5" data_target="400">{tab_5_content.title}</li>}
               {lang === "en" ? <li activate_slide="slide6" data_target="500">FEES</li> : <li activate_slide="slide6" data_target="500">Frais d'enseignement</li>}
            </ul>
         </div>
         <div className="col-md-8 col-12 content">
            <div className="row">
               <div className="col-lg-11 col-md-12 scrollbar m-auto">
                  <div className="row">
                     <div className="wrap">
                        <div id="slide1" className="content-inner slide active_slide">
                           <div className="row">
                              {tab_1_content.statFigure != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_1_content.statFigure}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>{tab_1_content.statText}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                              {tab_1_content.statFigure2 != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_1_content.statFigure2}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>     {tab_1_content.statText2}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                           </div>
                           {content1}
                        </div>
                        <div id="slide2" className="content-inner slide">
                           <div className="row">
                              {tab_2_content.statFigure != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_2_content.statFigure}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>{tab_2_content.statText}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                              {tab_2_content.statFigure2 != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_2_content.statFigure2}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>     {tab_2_content.statText2}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                           </div>
                           {content2}
                        </div>
                        <div id="slide3" className="content-inner slide">
                           <div className="row">
                              {tab_3_content.statFigure != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_3_content.statFigure}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>{tab_3_content.statText}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                              {tab_3_content.statFigure2 != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_3_content.statFigure2}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>     {tab_3_content.statText2}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                           </div>
                           {content3}
                        </div>
                        <div id="slide4" className="content-inner slide">
                           <div className="row">
                              {tab_4_content.statFigure != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_4_content.statFigure}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>{tab_4_content.statText}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                              {tab_4_content.statFigure2 != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_4_content.statFigure2}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>     {tab_4_content.statText2}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                           </div>
                           {content4}
                        </div>
                        <div id="slide5" className="content-inner slide">
                           <div className="row">
                              {tab_5_content.statFigure != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_5_content.statFigure}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>{tab_5_content.statText}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                              {tab_5_content.statFigure2 != "" &&
                                 <div className="col-md-6 col-12">
                                    <div className="row counter">
                                       <div className="col-12 font bronze-text text-center">
                                          {tab_5_content.statFigure2}
                                       </div>
                                    </div>
                                    <div className="row">
                                       <div className="col-12 black-text text-center">
                                          <h5>     {tab_5_content.statText2}</h5>
                                       </div>
                                    </div>
                                 </div>
                              }
                           </div>
                           {content5}
                        </div>
                        <div id="slide6" className="content-inner slide">
                           {tab_fees_content_1 !== null &&
                              <div className="fees_table">
                                 <div className="col-12" dangerouslySetInnerHTML={{ __html: tab_fees_content_1.tableRow1Intro }} />

                                 <div className="row grey-bg py-2">
                                    <div className="col-10">
                                       {tab_fees_content_1.tableRow1Column1Title}
                                    </div>
                                    <div className="col-2">
                                       {tab_fees_content_1.tableRow1Column2Value}
                                    </div>
                                 </div>

                                 {tab_fees_content_1.tableRow1Items != null && tab_fees_content_1.tableRow1Items.map((item, i) => (
                                    <div className="col-12 p-0 accordion" key={i}>
                                       {(() => {
                                          return (

                                             <div className="row py-3 opacity-bg">
                                                <div className="col-10">
                                                   {item.product}
                                                </div>
                                                <div className="col-2">
                                                   {item.fee}
                                                </div>
                                             </div>

                                          )
                                       }

                                       )()}
                                    </div>
                                 ))}


                                 <div className="row grey-bg py-2">
                                    <div className="col-10">
                                       {tab_fees_content_1.tableRow1SignOffTitle}

                                    </div>
                                    <div className="col-2">
                                       {tab_fees_content_1.tableRow1SignOff}

                                    </div>
                                 </div>
                                 {tab_fees_content_1.tableRow2Items !== null &&
                                    <div className="row grey-bg mt-4 py-2">
                                       <div className="col-10">
                                          {tab_fees_content_1.tableRow2Column1Title}
                                       </div>
                                       <div className="col-2">
                                          {tab_fees_content_1.tableRow2Column2Value}
                                       </div>
                                    </div>
                                 }
                                 {tab_fees_content_1.tableRow2Items !== null && tab_fees_content_1.tableRow2Items.map((item, i) => (
                                    <div className="col-12 p-0 accordion" key={i}>
                                       {(() => {
                                          return (

                                             <div className="row py-3 opacity-bg">
                                                <div className="col-10">
                                                   {item.product}
                                                </div>
                                                <div className="col-2">
                                                   {item.fee}
                                                </div>
                                             </div>

                                          )
                                       }

                                       )()}
                                    </div>
                                 ))}
                                 {tab_fees_content_1.tableRow2Items !== null &&
                                    <div className="row grey-bg py-2 ">
                                       <div className="col-10">
                                          {tab_fees_content_1.tableRow3SignOffTitle}

                                       </div>
                                       <div className="col-2">
                                          {tab_fees_content_1.tableRow3Column2Value}

                                       </div>
                                    </div>
                                 }
                              </div>
                           }
                           {/* { tab_fees_content_2 !== null || tab_fees_content_2.tableRow1Column1Title !== ''  && */}

                           {tab_fees_content_2.tableRow1Column1Title !== null &&
                              <div className="fees_table">
                                 <div className="col-12" dangerouslySetInnerHTML={{ __html: tab_fees_content_2.tableRow1Intro }} />

                                 <div className="row grey-bg py-2">
                                    <div className="col-10">
                                       {tab_fees_content_2.tableRow1Column1Title}
                                    </div>
                                    <div className="col-2">
                                       {tab_fees_content_2.tableRow1Column2Value}
                                    </div>
                                 </div>

                                 {tab_fees_content_2.tableRow111Items.map((item, i) => (
                                    <div className="col-12 p-0 accordion" key={i}>
                                       {(() => {
                                          return (

                                             <div className="row py-3 opacity-bg">
                                                <div className="col-10">
                                                   {item.product}
                                                </div>
                                                <div className="col-2">
                                                   {item.fee}
                                                </div>
                                             </div>

                                          )
                                       }

                                       )()}
                                    </div>
                                 ))}
                                 <div className="row grey-bg py-2">
                                    <div className="col-10">
                                       {tab_fees_content_2.tableRow1SignOffTitle}

                                    </div>
                                    <div className="col-2">
                                       {tab_fees_content_2.tableRow1SignOffValue}

                                    </div>
                                 </div>
                                 {tab_fees_content_2.tableRow2Column1Title !== null &&
                                    <div className="row grey-bg mt-4 py-2">
                                       <div className="col-10">
                                          {tab_fees_content_2.tableRow2Column1Title}
                                       </div>
                                       <div className="col-2">
                                          {tab_fees_content_2.tableRow2Column2Value}
                                       </div>
                                    </div>
                                 }
                                 {tab_fees_content_2.tableRow12Items !== null && tab_fees_content_2.tableRow12Items.map((item, i) => (
                                    <div className="col-12 p-0 accordion" key={i}>
                                       {(() => {
                                          return (

                                             <div className="row py-3 opacity-bg">
                                                <div className="col-10">
                                                   {item.product}
                                                </div>
                                                <div className="col-2">
                                                   {item.fee}
                                                </div>
                                             </div>

                                          )
                                       }

                                       )()}
                                    </div>
                                 ))}
                                 {tab_fees_content_2.tableRow3SignOffTitle !== null &&
                                    <div className="row grey-bg py-2 ">
                                       <div className="col-10">
                                          {tab_fees_content_2.tableRow3SignOffTitle}

                                       </div>
                                       <div className="col-2">
                                          {tab_fees_content_2.tableRow3Column2Value}

                                       </div>
                                    </div>
                                 }
                              </div>
                           }
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

   );
}
ProgramStructure.propTypes = {
   tab_1_content: PropTypes.object,
   tab_2_content: PropTypes.object,
   tab_3_content: PropTypes.object,
   tab_4_content: PropTypes.object,
   tab_5_content: PropTypes.object,
   tab_fees_content_1: PropTypes.object,
   tab_fees_content_2: PropTypes.object,
   allfile: PropTypes.array,
   lavender_background: PropTypes.string,
   lang: PropTypes.string,

};

export default ProgramStructure;
