import React from "react" 
import { Link } from 'gatsby'

import { StyledIMG, WbnSlide } from './styles/HeroSliderStyles'

const Slide = ({ slide, active }) => (
    <WbnSlide className={active ? 'active' : ''}>
        <StyledIMG fluid={slide.featured_media.localFile.childImageSharp.fluid} />
        <div className="wbn-overlay-test">
            <h1 className="wbn-header">{slide.acf.slider_header}</h1>
            {/* <Link to={"test.html"} >
                <button>Click Here</button>
            </Link> */}
        </div>
    </WbnSlide> 
)

export default Slide