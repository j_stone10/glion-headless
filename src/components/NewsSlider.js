import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import '../components/styles/draggable-news.css';

class NewsSlider extends Component {
    render() {
        const newsPosts = this.props.newsPosts

        return (
            <>
                {/* <div className="wrapper row m-0">
                    <div className="scrollElement col-12 text-center">
                        <h2>News from campus</h2>
                    </div>
                </div> */}
                <div className="wrapper mb-5 d-none d-md-block">


                    <Splide


                        options={{
                            perPage: 3.5,
                            perMove: 1,
                            width: '100%',
                            type: 'loop',
                            //  autoplay: true,
                            gap: '1rem',
                            pagination: false,
                            isNavigation: false,
                            arrows: false,
                            heightRatio: 0.3,
                        }}
                        onMoved={(splide, newIndex) => { console.log('moved', newIndex) }}

                    >
                        {newsPosts.map((item, i) => (
                            <SplideSlide key={i}>
                               
                                <div className="overlay">    </div>
                                <div className="image">
                                    <Img fluid={item.node.featuredImage.node.localFile.childImageSharp.fluid} alt={item.node.featuredImage.node.altText} />

                                </div>
                                <div className="title">
                                    <p className="text-center">
                                    {item.node.categories.nodes[0].name}

                                </p>
                                    <h6 className="text-center">
                                        {item.node.title}

                                    </h6>
                                    <div className="learn_more_container mx-auto pt-5 text-center">
                                        <a className="learn_more white" href={item.node.link}>Learn More </a>
                                    </div>
                                </div>
                            
                            </SplideSlide>
                        ))}

                    </Splide>
                </div>
                <div className="wrapper mb-5 d-none d-sm-block d-md-none">


                    <Splide


                        options={{
                            perPage: 2.5,
                            perMove: 1,
                            width: '100%',
                            type: 'loop',
                            //  autoplay: true,
                            gap: '1rem',
                            pagination: false,
                            isNavigation: false,
                            arrows: false,
                            heightRatio: 0.4,
                        }}
                        onMoved={(splide, newIndex) => { console.log('moved', newIndex) }}

                    >
                        {newsPosts.map((item, i) => (
                            <SplideSlide  key={i}>
                                <div className="overlay">    </div>
                                <div className="image">
                                    <Img fluid={item.node.featuredImage.node.localFile.childImageSharp.fluid} alt={item.node.featuredImage.node.altText} />

                                </div>
                                <div className="title">
                                    <p className="text-center">
                                        {item.node.categories.nodes[0].name}

                                    </p>
                                    <h6 className="text-center">
                                        {item.node.title}

                                    </h6>
                                    <div className="learn_more_container mx-auto pt-5 text-center">
                                        <a className="learn_more white" href={item.node.link}>Learn More </a>
                                    </div>
                                </div>
                            </SplideSlide>
                        ))}

                    </Splide>
                </div>
                <div className="wrapper mb-5 d-block d-sm-none">


                    <Splide


                        options={{
                            perPage: 1.2,
                            perMove: 1,
                            width: '100%',
                            type: 'loop',
                            //  autoplay: true,
                            gap: '1rem',
                            pagination: false,
                            isNavigation: false,
                            arrows: false,
                            heightRatio: 0.6,
                        }}
                        onMoved={(splide, newIndex) => { console.log('moved', newIndex) }}

                    >
                        {newsPosts.map((item, i) => (
                            <SplideSlide  key={i}>
                                <div className="overlay">    </div>
                                <div className="image">
                                    <Img fluid={item.node.featuredImage.node.localFile.childImageSharp.fluid} alt={item.node.featuredImage.node.altText} />

                                </div>
                                <div className="title">
                                    <p className="text-center">
                                        {item.node.categories.nodes[0].name}

                                    </p>
                                    <h6 className="text-center">
                                        {item.node.title}

                                    </h6>
                                    <div className="learn_more_container mx-auto pt-5 text-center">
                                        <a className="learn_more white" href={item.node.link}>Learn More </a>
                                    </div>
                                </div>
                            </SplideSlide>
                        ))}

                    </Splide>

                </div>
            </>
        );
    }
}
NewsSlider.propTypes = {
    newsPosts: PropTypes.array,
};

export default NewsSlider
