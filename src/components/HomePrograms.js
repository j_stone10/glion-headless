
import React, {  useState } from 'react';
import { Link } from "gatsby"
import Img from "gatsby-image"
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './styles/home-programs.css';



const HomeProgram = ({
   program_header,
   program_1_tab_title,
   program_1_title,
   program_1_details,
   program_1_image,
   program_1_link,
   program_2_tab_title,
   program_2_title,
   program_2_details,
   program_2_link,
   program_2_block_1_title,
   program_2_block_1_text,
   program_2_block_1_link,
   program_2_block_2_title,
   program_2_block_2_text,
   program_2_block_2_link,
   program_2_block_3_title,
   program_2_block_3_text,
   program_2_block_3_link,
   program_2_block_4_title,
   program_2_block_4_text,
   program_2_block_4_link,
   batchelors_remote_learning_overview_text,
   program_1_learning_overview_text,
   program_2_block_2_learning_overview_text,
   program_2_block_3_learning_overview_text,
   program_2_block_4_learning_overview_text,
   program_study_text
}) => {
   const [activeTab, setActiveTab] = useState('1');

   const toggle = tab => {
      if (activeTab !== tab) setActiveTab(tab);
   }


   return (
      <>


         <div className="row wrapper">
            <div className="scrollElement col-md-6 col-12 pr-md-0 program-content">
               {program_header !== '' && 
               <div className="row mr-md-0">
                  <div className="col-12 program-title">
                     <h3>{program_header}</h3>
                     <div className="program_study_text">
                        <svg width="28" height="18" viewBox="0 0 28 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M25.042 1.15227C25.038 0.510246 24.544 -0.00422567 23.9376 2.61653e-05L14.0461 0.0361668L4.15451 2.61653e-05C3.54808 -0.00422567 3.05411 0.512372 3.05009 1.15227L3.04407 2.03028V14.9303L14.0461 14.9027L25.0481 14.9303L25.042 1.15227ZM23.7087 13.4932L14.0461 13.5187L4.38342 13.4932L4.36535 1.65824C4.36535 1.54344 4.4537 1.45203 4.56013 1.45203H23.532" fill="#F0D4C6"/>
                        <path d="M27.8092 15.9504L17.6385 15.9122L17.3494 16.4139H10.6245L10.3634 15.8867L0.194779 15.8484C0.0863453 15.8484 0 15.9398 0 16.0546V16.6966C0 16.7795 0.0461847 16.8539 0.116466 16.8858L1.55622 17.5513C1.93574 17.7256 2.34337 17.817 2.75702 17.817H25.247C25.6606 17.8191 26.0542 17.8149 26.4337 17.6427L27.8775 16.9879C27.9498 16.956 27.9959 16.8816 27.9959 16.7987L28 16.1566C28.002 16.044 27.9156 15.9504 27.8092 15.9504Z" fill="#F0D4C6"/>
                        <path d="M15.3172 8.56258C16.3232 8.04386 17.018 6.95114 17.018 5.68622C17.018 3.92171 15.6666 2.49097 13.9999 2.49097C12.3333 2.49097 10.9819 3.92171 10.9819 5.68622C10.9819 6.95114 11.6766 8.04386 12.6826 8.56258C10.6365 9.1706 9.13647 11.1605 9.13647 13.5202H18.8634C18.8634 11.1583 17.3634 9.1706 15.3172 8.56258Z" fill="#F0D4C6"/>
                        </svg>
                        <div dangerouslySetInnerHTML={{ __html: program_study_text }} />
                     </div>
                  </div>
               </div>  
}
               <div className="row mr-md-0">
                  <div className="col-12 p-0">
                     <Nav tabs id="myTab">
                        <NavItem className="nav-item darken m-0 active">
                           <NavLink
                              className={classnames({ active: activeTab === '1' })} className=" nav-link text-center nav-item"
                              onClick={() => { toggle('1'); }}
                           >
                              {program_1_tab_title}
                           </NavLink>
                        </NavItem>
                        <NavItem className="nav-item darken m-0">
                           <NavLink
                              className={classnames({ active: activeTab === '2' })} className=" nav-link text-center nav-item"
                              onClick={() => { toggle('2'); }}
                           >
                              {program_2_tab_title}
                           </NavLink>
                        </NavItem>
                     </Nav>
                  </div>
               </div>
               <div className="row program-text mr-md-0">
                  <div className="col-xl-9 col-lg-11 col-11 m-auto pl-md-4 pl-lg-0">
                     <TabContent activeTab={activeTab} id="myTabContent" className="tab-content">

                        <TabPane tabId="1">

                           <h3 dangerouslySetInnerHTML={{ __html: program_1_title }} />
                           <div dangerouslySetInnerHTML={{ __html: program_1_details }} />
                           <p className="remote_learning_text batchelors"><strong>{batchelors_remote_learning_overview_text}</strong></p>

                           <Link to={program_1_link.url} className="mt-5 btn btn-primary bronze">{program_1_link.title}</Link>
                        </TabPane>

                        <TabPane tabId="2">
                           <div className="tab-pane">
                              <h3 dangerouslySetInnerHTML={{ __html: program_2_title }} />
                              <div dangerouslySetInnerHTML={{ __html: program_2_details }} />
                              {/* {program_2_link != '' && <Link to={program_2_link.url} className="mt-5 btn btn-primary bronze">{program_2_link.title}</Link>} */}
                           </div>
                        </TabPane>
                     </TabContent>

                  </div>
               </div>
            </div>
            <div className="col-md-6 col-12  p-0">
               <TabContent activeTab={activeTab} id="myTabContent" className="tab-content">

                  <TabPane className="bachelors_that" tabId="1">
                     {program_1_image.localFile &&
                     <Img fluid={program_1_image.localFile.childImageSharp.fluid} alt={program_1_image.altText} />
                     }
                  </TabPane>
                  <TabPane className="masters_that" tabId="2">
                  {program_1_image.localFile &&
                     <Img fluid={program_1_image.localFile.childImageSharp.fluid} style={{ opacity: "0" }} alt="alt text" />
                  }
                     <div className="row p-0 m-0">
                        <div className="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion  coloured">
                           <div className="col-xl-9 col-lg-11 col-12 m-auto px-0 px-md-3">
                              <h6 className="masters-int-title accordion_title accordion_title_black_text coloured">{program_2_block_1_title}</h6>
                              <div className="expand-icon d-block d-lg-none">  <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" alt="expand_icon" /></div>
                              <div className="white-bg masters-int-text accordion_text  pb-5 p-md-0">
                                 
                                 <span dangerouslySetInnerHTML={{ __html: program_2_block_1_text }} />
                                 <p className="remote_learning_text masters">{program_1_learning_overview_text}</p>
                                 <Link to={program_2_block_1_link.url} className="learn_more" >
                                    {program_2_block_1_link.title}
                                 </Link>
                              </div>
                           </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion coloured ">
                           <div className="col-xl-9 col-lg-11  col-12 m-auto px-0 px-md-3">
                              <h6 className=" business-masters-title accordion_title accordion_title_white_text coloured">{program_2_block_2_title}</h6>
                              <div className="expand-icon d-block d-lg-none">  <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" alt="expand_icon"  /></div>
                              <div className="white-bg business-masters-text accordion_text  pb-5 p-md-0">
                                 <span dangerouslySetInnerHTML={{ __html: program_2_block_2_text }} />
                                 <p className="remote_learning_text masters white">{program_2_block_2_learning_overview_text}</p>
                                 <Link className="learn_more white" to={program_2_block_2_link.url} >{program_2_block_2_link.title}</Link>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="row p-0 m-0">
                        <div className="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion  coloured">
                           <div className="col-xl-9 col-lg-11 col-12 m-auto px-0 px-md-3">
                              <h6 className=" masters-hosp-title accordion_title accordion_title_black_text coloured">{program_2_block_3_title}</h6>
                              <div className="expand-icon d-block d-lg-none">  <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" alt="expand_icon"  /></div>
                              <div className="white-bg masters-hosp-text accordion_text  pb-5 p-md-0">
                                 <span dangerouslySetInnerHTML={{ __html: program_2_block_3_text }} />
                                 <p className="remote_learning_text masters">{program_2_block_3_learning_overview_text}</p>
                                 <Link  className="learn_more" to={program_2_block_3_link.url} >{program_2_block_3_link.title}</Link>
                              </div>
                           </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-12 px-0 px-md-3 accordion  coloured">
                           <div className="col-xl-9 col-lg-11 col-12 m-auto px-0 px-md-3">
                              <h6 className=" masters-lux-title accordion_title accordion_title_white_text coloured">{program_2_block_4_title}</h6>
                              <div className="expand-icon white d-block d-lg-none">  <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iI2ZmZiIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iI2ZmZiIvPgo8L3N2Zz4K" alt="expand_icon"  /></div>
                              <div className="white-bg masters-lux-text accordion_text  pb-5 p-md-0">
                                 <span dangerouslySetInnerHTML={{ __html: program_2_block_4_text }} />
                                 <p className="remote_learning_text masters white">{program_2_block_4_learning_overview_text}</p>
                                 <Link  className="learn_more white" to={program_2_block_4_link.url} >{program_2_block_4_link.title}</Link>
                              </div>
                           </div>
                        </div>
                     </div>
                  </TabPane>
               </TabContent>

            </div>
         </div>
      </>

   )
}

HomeProgram.propTypes = {
   program_header: PropTypes.string,
   program_1_tab_title: PropTypes.string,
   program_1_title: PropTypes.string,
   program_1_details: PropTypes.string,
   program_1_image: PropTypes.object,
   program_1_link: PropTypes.object,
   program_2_tab_title: PropTypes.string,
   program_2_title: PropTypes.string,
   program_2_details: PropTypes.string,
   program_2_link: PropTypes.string,
   // program_2_link: PropTypes.object,
   program_2_block_1_title: PropTypes.string,
   program_2_block_1_text: PropTypes.string,
   program_2_block_1_link: PropTypes.object,
   program_2_block_2_title: PropTypes.string,
   program_2_block_2_text: PropTypes.string,
   program_2_block_2_link: PropTypes.object,
   program_2_block_3_title: PropTypes.string,
   program_2_block_3_text: PropTypes.string,
   program_2_block_3_link: PropTypes.object,
   program_2_block_4_title: PropTypes.string,
   program_2_block_4_texts: PropTypes.string,
   program_2_block_4_link: PropTypes.object,
};


export default HomeProgram;

