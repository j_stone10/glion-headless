import React from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'gatsby'
import '../components/styles/program-stats.css';



class ProgramStats extends React.Component {

    render() {
        const {
            stats_title,
            stat_1_intro,
            stat_1_figure,
            stat_1_figure_after,
            stat_1_close_text,
            stat_2_intro,
            stat_2_figure,
            stat_2_figure_after,
            stat_2_close_text,
            stat_3_intro,
            stat_3_figure,
            stat_3_figure_after,
            stat_3_close_text,
            stat_4_intro,
            stat_4_figure,
            stat_4_figure_after,
            stat_4_close_text,
            stats_link
        } = this.props;

        return (
            <div className="wrapper row stats">
                <div className="scrollElement col-12 my-5">
                    <div className="counter inner no-background text-center">
                        <div className="row">
                            <div className="col-sm-8 col-10 m-auto">
                                <h3 className="my-5" dangerouslySetInnerHTML={{ __html: stats_title }} />
                                <div className="row">
                                    <div className="col-md-6 col-12">
                                        <div className="row">
                                            <div className="col-12 bronze-text">
                                                {stat_1_intro === '' &&
                                                    <h4 className="black-text col-12" style={{ marginTop: 65 }} />
                                                }
                                                {stat_1_intro !== '' &&
                                                    <h4 className="black-text col-12" dangerouslySetInnerHTML={{ __html: stat_1_intro }} />
                                                }
                                                <div className="font">
                                                <span className="value ">{stat_1_figure}</span>{stat_1_figure_after}
                                                </div>
                                                <p className="col-12 black-text" dangerouslySetInnerHTML={{ __html: stat_1_close_text }} />

                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12 bronze-text">
                                                {stat_2_intro === '' &&
                                                    <h4 className="black-text col-12" style={{ marginTop: 65 }} />
                                                }
                                                {stat_2_intro !== '' &&
                                                    <h4 className="black-text col-12" dangerouslySetInnerHTML={{ __html: stat_2_intro }} />
                                                }
                                                         <div className="font">
                                                <span className="value ">{stat_2_figure}</span>{stat_2_figure_after}
                                                </div>
                                                <p className="col-12 black-text" dangerouslySetInnerHTML={{ __html: stat_2_close_text }} />

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-12">
                                        <div className="row">
                                            <div className="col-12 bronze-text">
                                                {stat_3_intro === '' &&
                                                    <h4 className="black-text col-12" style={{ marginTop: 65 }} />
                                                }
                                                {stat_3_intro !== '' &&
                                                    <h4 className="black-text col-12" dangerouslySetInnerHTML={{ __html: stat_3_intro }} />
                                                }
                                                   <div className="font">
                                                <span className="value ">{stat_3_figure}</span>{stat_3_figure_after}
                                                </div>
                                                <p className="col-12 black-text" dangerouslySetInnerHTML={{ __html: stat_3_close_text }} />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12 bronze-text">
                                                {stat_4_intro === '' &&
                                                    <h4 className="black-text col-12" style={{ marginTop: 65 }} />
                                                }
                                                {stat_4_intro !== '' &&
                                                    <h4 className="black-text col-12" dangerouslySetInnerHTML={{ __html: stat_4_intro }} />
                                                }
                                                     <div className="font">
                                                <span className="value ">{stat_4_figure}</span>{stat_4_figure_after}
                                                </div>
                                                <p className="col-12 black-text" dangerouslySetInnerHTML={{ __html: stat_4_close_text }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row"></div>
                                <div className="col text-center m-auto">
                                   <Link to={stats_link.url} className="btn btn-primary blue  m-auto">{stats_link.title}</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


ProgramStats.propTypes = {

    main_stats_title: PropTypes.string,
    stat_1_title: PropTypes.string,
    stat_1_figure: PropTypes.number,
    stat_1_symbol_after: PropTypes.string,
    stat_1_sign_off: PropTypes.string,
    stat_2_title: PropTypes.string,
    stat_2_figure: PropTypes.number,
    stat_2_symbol_after: PropTypes.string,
    stat_2_sign_off: PropTypes.string,
    stats_link: PropTypes.object,

};


export default ProgramStats;
