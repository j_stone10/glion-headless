import React from 'react';
import { PropTypes } from 'prop-types';

// import Img from "gatsby-image"
import '../components/styles/home-programs.css';



class Accordion extends React.Component {
    componentDidMount() {



    }
    render() {
        // console.log(this.props.data)
        const accordion_row = this.props.accordion_row
        // console.log(accordion_row)

        return (

            <div className="row my-5">
                {accordion_row.map((item, i) => (
                    <div className="col-12 p-0 accordion" key={i}>
                        {(() => {
                            return (
                                <div className="col-12 p-0 accordion">
                                    <h2 className="accordion_title_black_text program">{item.title}</h2>
                                    <div className="expand-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K" alt="toggle" /></div>
                                    <div className="accordion_text">
                                        {item.numberAccColumns === 'Single column' &&
                                            <div className="row ">
                                                <div className="col-12" dangerouslySetInnerHTML={{ __html: item.contentLeft }} />


                                            </div>
                                        }
                                        {item.numberAccColumns === 'Two columns' &&
                                            <div className="row">
                                                <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: item.contentLeft }} />
                                                <div className="col-12 col-md-6" dangerouslySetInnerHTML={{ __html: item.contentRight }} />

                                            </div>
                                        }
                                    </div>
                                </div>
                            )
                        }
                            //}
                        )()}
                    </div>
                ))}
            </div>
        )
    }
}


Accordion.propTypes = {
    accordion_row: PropTypes.array,

   
};
export default Accordion;
