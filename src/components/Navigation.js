
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { StaticQuery, graphql, Link } from 'gatsby';
import Neche_Logo from "../images/neche_logo.svg"
import facebook_logo from "../images/facebook.png"
import instagram_logo from "../images/instagram.png"
import youtube_logo from "../images/youtube.png"
import linkedin_logo from "../images/linkedin.png"
import Img from 'gatsby-image'
import accelerate from "../images/Navigation_block_accelerate_your_career.jpg"
import apply from "../images/Navigation_block_apply.jpg"
import campus from "../images/Navigation_block_campus_visit.jpg"
import london from "../images/Navigation_block_london.jpg"
import switzerland from "../images/Navigation_block_switzerland.jpg"
import hospitality from "../images/Navigation_block_what_is_hospitality.jpg"



import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap';
import './styles/navigation.css';


class Navigation extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggle1 = this.toggle1.bind(this);
    this.toggle2 = this.toggle2.bind(this);
    this.toggle3 = this.toggle3.bind(this);
    this.toggle4 = this.toggle4.bind(this);
    this.toggle5 = this.toggle5.bind(this);
    this.onMouseEnter1 = this.onMouseEnter1.bind(this);
    this.onMouseLeave1 = this.onMouseLeave1.bind(this);
    this.onMouseEnter2 = this.onMouseEnter2.bind(this);
    this.onMouseLeave2 = this.onMouseLeave2.bind(this);
    this.onMouseEnter3 = this.onMouseEnter3.bind(this);
    this.onMouseLeave3 = this.onMouseLeave3.bind(this);
    this.onMouseEnter4 = this.onMouseEnter4.bind(this);
    this.onMouseLeave4 = this.onMouseLeave4.bind(this);
    this.onMouseEnter5 = this.onMouseEnter5.bind(this);
    this.onMouseLeave5 = this.onMouseLeave5.bind(this);
    this.state = {
      isOpen: false,
      navCollapsed: true,
      showNavbar: false,
      dropdownOpen1: false,
      dropdownOpen2: false,
      dropdownOpen3: false,
      dropdownOpen4: false,
      dropdownOpen5: false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  toggle1() {
    this.setState(prevState => ({
      dropdownOpen1: !prevState.dropdownOpen1,
    }));
  }
  toggle2() {
    this.setState(prevState => ({
      dropdownOpen2: !prevState.dropdownOpen2
    }));
  }
  toggle3() {
    this.setState(prevState => ({
      dropdownOpen3: !prevState.dropdownOpen3
    }));
  }
  toggle4() {
    this.setState(prevState => ({
      dropdownOpen4: !prevState.dropdownOpen4
    }));
  }
  toggle5() {
    this.setState(prevState => ({
      dropdownOpen5: !prevState.dropdownOpen5
    }));
  }

  onMouseEnter1() {
    this.setState({ dropdownOpen1: true });
  }

  onMouseLeave1() {
    this.setState({ dropdownOpen1: false });
  }
  onMouseEnter2() {
    this.setState({ dropdownOpen2: true });
  }

  onMouseLeave2() {
    this.setState({ dropdownOpen2: false });
  }

  onMouseLeave3() {
    this.setState({ dropdownOpen3: false });
  }
  onMouseEnter3() {
    this.setState({ dropdownOpen3: true });
  }

  onMouseLeave4() {
    this.setState({ dropdownOpen4: false });
  }
  onMouseEnter4() {
    this.setState({ dropdownOpen4: true });
  }

  onMouseLeave5() {
    this.setState({ dropdownOpen5: false });
  }
  onMouseEnter5() {
    this.setState({ dropdownOpen5: true });
  }



  render() {
    // console.log(this.props.primaryMenu.nodes[0].acfNavImagesUpdate.aboutMenuImage)
    // const { navCollapsed } = this.state

    const polylang_translations = this.props.polylang_translations
    const currentLang = this.props.currentLang
    const frenchHome = this.props.frenchHome

    if (polylang_translations.length < 1 || polylang_translations == undefined) {
      if (currentLang == "fr") {
        // polylang_translations[0].link = '/fr/'
        polylang_translations.splice(0, 0, { "link": "/" });
      }
      else {
        polylang_translations.splice(0, 0, { "link": "/fr/" });
      }
    }

    return (
      <>
        {this.props.currentLang === "en" &&
          <Navbar
            // color="light" light 
            expand="md">

            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter1} onMouseLeave={this.onMouseLeave1} isOpen={this.state.dropdownOpen1} toggle={this.toggle1} >
                  <>

                    <DropdownToggle nav caret className="nav-parent aboutMenu" >

                      About

                    </DropdownToggle>

                  </>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">About Us</span>
                          {this.props.aboutMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">Work with us</span>
                          {this.props.partnershipsMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Why hospitality?</h4>
                            <NavLink className="toggler-link" href="/about-us/your-future-in-hospitality-and-beyond/" > <img src={hospitality} alt="accelerate" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenu.nodes[0].acfNavImagesUpdate.aboutMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.aboutMenuLink.url} > <Img fluid={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.aboutMenuImage.localFile.childImageSharp.fluid} alt="accelerate" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenu.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter2} onMouseLeave={this.onMouseLeave2} isOpen={this.state.dropdownOpen2} toggle={this.toggle2} >
                  <DropdownToggle nav caret className="nav-parent programMenu">
                    Programs
                </DropdownToggle>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-4 col-12">
                          <span className="mega-block-title">Bachelors</span>
                          {this.props.bachelorsMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                          <span className="mega-block-title">Masters</span>
                          {this.props.mastersMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} dangerouslySetInnerHTML={{ __html: item.label }}>
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-2 col-12">
                          <span className="mega-block-title">Short Programs</span>
                          {this.props.shortMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}

                          <span className="mega-block-title">Executive Education</span>
                          {this.props.execMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Accelerate Your Career</h4>
                            <NavLink className="toggler-link" href="/careers/" > <img src={accelerate} alt="accelerate" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenu.nodes[0].acfNavImagesUpdate.programMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.programMenuLink.url} > <Img fluid={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.programMenuImage.localFile.childImageSharp.fluid} alt="accelerate" /></NavLink>

                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenu.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter3} onMouseLeave={this.onMouseLeave3} isOpen={this.state.dropdownOpen3} toggle={this.toggle3} >
                  <DropdownToggle nav caret className="nav-parent locationMenu">
                    Locations
                </DropdownToggle>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Switzerland</h4>
                            <NavLink className="toggler-link" href="/locations/switzerland" > <img src={switzerland} alt="switzerland" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenu.nodes[0].acfNavImagesUpdate.switzerlandMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.switzerlandMenuLink.url} > <Img fluid={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.switzerlandMenuImage.localFile.childImageSharp.fluid} alt="switzerland" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">London</h4>
                            <NavLink className="toggler-link" href="/locations/london" > <img src={london} alt="london" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenu.nodes[0].acfNavImagesUpdate.londonMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.londonMenuLink.url} > <Img fluid={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.londonMenuImage.localFile.childImageSharp.fluid} alt="london" /></NavLink>


                          </div>
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Campus Taster Days</h4>
                            <NavLink className="toggler-link" href="/campus-taster-days/" > <img src={campus} alt="visit a campus" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenu.nodes[0].acfNavImagesUpdate.visitCampusLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.visitCampusLink.url} > <Img fluid={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.visitCampusImage.localFile.childImageSharp.fluid} alt="visit campus" /></NavLink>

                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenu.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <NavItem>
                  <NavLink className="nav-parent toggler-link" href="/alumni/">Alumni</NavLink>
                </NavItem>

                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter5} onMouseLeave={this.onMouseLeave5} isOpen={this.state.dropdownOpen5} toggle={this.toggle5} >
                  <DropdownToggle nav caret className="nav-parent admissionMenu">
                    Admission
                </DropdownToggle>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">Apply</span>
                          {this.props.applyMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">ACCEPTED STUDENT INFORMATION</span>
                          {this.props.preArrivalMenu.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Apply Now</h4>
                            <NavLink className="toggler-link" href="/apply-to-glion/" > <img src={apply} alt="accelerate" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenu.nodes[0].acfNavImagesUpdate.admissionMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.admissionMenuLink.url} > <Img fluid={this.props.primaryMenu.nodes[0].acfNavImagesUpdate.admissionMenuImage.localFile.childImageSharp.fluid} alt="apply now" /></NavLink>

                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenu.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <NavItem >
                  <NavLink className="nav-parent toggler-link" href="/magazine/">Magazine</NavLink>
                </NavItem>
                <NavItem className=" d-block d-md-none">
                  <NavLink target="_blank" className="nav-parent toggler-link" href="https://alumni.glion.edu/">Alumni Log In</NavLink>
                </NavItem>
                <NavItem className=" d-block d-md-none">
                  <NavLink className="nav-parent toggler-link"
                    href={frenchHome === '/fr/' ? '/' : polylang_translations[0].link}
                  >
                    <span> {currentLang === "en" ? "Français" : "English"} </span>
                  </NavLink>

                </NavItem>
                <div className="col-12 d-block d-md-none">
                  <div className="menu-header-actions-container">
                    <ul>
                      {this.props.headerActionsMenu.nodes[0].menuItems.nodes.map((item, i) => (

                        (() => {
                          if (i === 0) {
                            return (
                              <div key={item.id}>
                                <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )
                          } else if (i === 1) {
                            return (
                              <div key={item.id}>
                                <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )
                          } else if (i === 2) {
                            return (
                              <div key={item.id}>
                                <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )

                          } else if (i === 3) {
                            return (
                              <div key={item.id}>
                                <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )
                          }
                        }
                        )()
                      ))}
                    </ul>
                  </div>
                </div>
              </Nav>
              <div className="d-block d-md-none nav_social">
                <div className="row m-0">
                  <div className="col-6 logo">

                    <Link to="">
                      <img src={Neche_Logo} alt="Neche Logo" />
                    </Link>
                  </div>
                  <div className="col-6 icons">

                    <Link to="">  <img src={linkedin_logo} alt="LinkedIn Logo" /></Link>
                    <Link to="">  <img src={youtube_logo} alt="youtube Logo" /></Link>
                    <Link to="">  <img src={facebook_logo} alt="Facebook Logo" /></Link>
                    <Link to="">  <img src={instagram_logo} alt="Instagram Logo" /></Link>
                  </div>
                </div>
              </div>
            </Collapse>
          </Navbar>
        }
        {this.props.currentLang === "fr" &&
          <Navbar
            // color="light" light 
            expand="md">

            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter1} onMouseLeave={this.onMouseLeave1} isOpen={this.state.dropdownOpen1} toggle={this.toggle1} >
                  <DropdownToggle nav caret className="nav-parent proposMenu">
                    À propos
                  </DropdownToggle>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">À propos</span>
                          {this.props.aboutMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">TRAVAILLER AVEC NOUS</span>
                          {this.props.partnershipsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Qu’est-ce que l’hôtellerie ?</h4>
                            <NavLink className="toggler-link" href="/fr/a-propos-de-glion/quest-ce-que-lhotellerie/" > <img src={hospitality} alt="accelerate" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.aboutMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.aboutMenuLink.url} > <Img fluid={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.aboutMenuImage.localFile.childImageSharp.fluid} alt="accelerate" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter2} onMouseLeave={this.onMouseLeave2} isOpen={this.state.dropdownOpen2} toggle={this.toggle2} >
                  <DropdownToggle nav caret className="nav-parent programmesMenu">
                    Programmes
              </DropdownToggle>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-4 col-12">
                          <span className="mega-block-title">BACHELOR</span>
                          {this.props.bachelorsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                          <span className="mega-block-title">MASTERS</span>
                          {this.props.mastersMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-2 col-12">
                          <span className="mega-block-title">PROGRAMMES COURTS</span>
                          {this.props.shortMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}

                          <span className="mega-block-title">FORMATIONS MANAGÉRIALES</span>
                          {this.props.execMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Accélérez votre carrière</h4>
                            <NavLink className="toggler-link" href="/fr/programmes/accelerez-votre-carriere/" > <img src={accelerate} alt="accelerate" /></NavLink> */}
                            <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.programMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.programMenuLink.url} > <Img fluid={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.programMenuImage.localFile.childImageSharp.fluid} alt="program" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter3} onMouseLeave={this.onMouseLeave3} isOpen={this.state.dropdownOpen3} toggle={this.toggle3} >
                  <DropdownToggle nav caret className="nav-parent nosMenu">
                    Nos-Campus
              </DropdownToggle>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Suisse</h4>
                            <NavLink className="toggler-link" href="/fr/nos-campus/suisse/" > <img src={switzerland} alt="switzerland" /></NavLink> */}
                                                        <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.switzerlandMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.switzerlandMenuLink.url} > <Img fluid={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.switzerlandMenuImage.localFile.childImageSharp.fluid} alt="program" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Londres</h4>
                            <NavLink className="toggler-link" href="/fr/nos-campus/londres/"> <img src={london} alt="london" /></NavLink> */}
                                                        <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.londonMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.londonMenuLink.url} > <Img fluid={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.londonMenuImage.localFile.childImageSharp.fluid} alt="program" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">JOURNÉE DÉCOUVERTE</h4>
                            <NavLink className="toggler-link" href="/fr/nos-campus/journee-decouverte/" > <img src={campus} alt="visit a campus" /></NavLink> */}
                                                        <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.visitCampusLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.visitCampusLink.url} > <Img fluid={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.visitCampusImage.localFile.childImageSharp.fluid} alt="program" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <NavItem>
                  <NavLink className="nav-parent toggler-link" href="/fr/anciens-etudiants/">ANCIENS ÉTUDIANTS</NavLink>
                </NavItem>

                <UncontrolledDropdown nav inNavbar onMouseOver={this.onMouseEnter5} onMouseLeave={this.onMouseLeave5} isOpen={this.state.dropdownOpen5} toggle={this.toggle5} >
                  <DropdownToggle nav caret className="nav-parent inscrireMenu">
                    S’INSCRIRE
              </DropdownToggle>
                  <DropdownMenu right className="animate slideIn">
                    <div className="row m-0">
                      <div className="container p-0 py-md-3">

                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">S’INSCRIRE</span>
                          {this.props.applyMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <span className="mega-block-title">INFORMATIONS RELATIVES AUX ÉTUDES</span>
                          {this.props.preArrivalMenuFR.nodes[0].menuItems.nodes.map((item, i) => (
                            <span className={"nav_item "} key={i}>
                              <NavLink className="child toggler-link" href={item.url} >
                                {item.label}
                              </NavLink>
                            </span>
                          ))}
                        </div>
                        <div className="col-md-3 col-12">
                          <div className="widget_media_image">
                            {/* <h4 className="mega-block-title">Postuler</h4>
                            <NavLink className="toggler-link" href="/fr/postuler-a-glion/" > <img src={apply} alt="accelerate" /></NavLink> */}
                                                        <div className="widget_media_image_title_container"><span className="mega-block-title">{this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.admissionMenuLink.title}</span></div>
                            <NavLink className="toggler-link" href={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.admissionMenuLink.url} > <Img fluid={this.props.primaryMenuFR.nodes[0].acfNavImagesUpdate.admissionMenuImage.localFile.childImageSharp.fluid} alt="program" /></NavLink>
                          </div>
                        </div>
                        <div className="col-md-3 d-none d-md-block">
                          <div className="menu-header-actions-container">
                            <ul>
                              {this.props.headerActionsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (

                                (() => {
                                  if (i === 0) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 1) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  } else if (i === 2) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )

                                  } else if (i === 3) {
                                    return (
                                      <div key={item.id}>
                                        <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                          <NavLink href={item.url} >
                                            {item.label}
                                          </NavLink>
                                        </li>
                                      </div>
                                    )
                                  }
                                }
                                )()
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
                <NavItem >
                  <NavLink className="nav-parent toggler-link" href="/fr/magazine-fr/">Magazine</NavLink>
                </NavItem>
                <NavItem className=" d-block d-md-none">
                  <NavLink target="_blank" className="nav-parent toggler-link" href="https://alumni.glion.edu/">Alumni Log In</NavLink>
                </NavItem>
                <NavItem className=" d-block d-md-none">
                  <NavLink className="nav-parent toggler-link"
                    href={frenchHome === '/fr/' ? '/' : polylang_translations[0].link}
                  >
                    <span> {currentLang === "en" ? "Français" : "English"} </span>
                  </NavLink>


                </NavItem>
                <div className="col-12 d-block d-md-none">
                  <div className="menu-header-actions-container">
                    <ul>
                      {this.props.headerActionsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (

                        (() => {
                          if (i === 0) {
                            return (
                              <div key={item.id}>
                                <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )
                          } else if (i === 1) {
                            return (
                              <div key={item.id}>
                                <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )
                          } else if (i === 2) {
                            return (
                              <div key={item.id}>
                                <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )

                          } else if (i === 3) {
                            return (
                              <div key={item.id}>
                                <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62 "}>
                                  <NavLink href={item.url} >
                                    {item.label}
                                  </NavLink>
                                </li>
                              </div>
                            )
                          }
                        }
                        )()
                      ))}
                    </ul>
                  </div>
                </div>
              </Nav>
              <div className="d-block d-md-none nav_social">
                <div className="row m-0">
                  <div className="col-6 logo">

                    <Link to="">
                      <img src={Neche_Logo} alt="Neche Logo" />
                    </Link>
                  </div>
                  <div className="col-6 icons">

                    <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/">  <img src={linkedin_logo} alt="LinkedIn Logo" /></a>
                    <a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg">  <img src={youtube_logo} alt="youtube Logo" /></a>
                    <a target="_blank" href="https://www.facebook.com/glionswitzerland/">  <img src={facebook_logo} alt="Facebook Logo" /></a>
                    <a target="_blank" href="https://www.instagram.com/glionhospitalityschool/">  <img src={instagram_logo} alt="Instagram Logo" /></a>
                  </div>
                </div>
              </div>
            </Collapse>
          </Navbar>
        }
      </>
    )
  }
}



export default (props) => (
  <StaticQuery
    query={graphql`
        query {
          site {
            siteMetadata {
              title
            }
          }
          primaryMenu: allWpMenu(filter: {name: {eq: "Primary Navigation"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
              acfNavImagesUpdate {
                aboutMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                aboutMenuLink {
                  target
                  title
                  url
                }
                programMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                programMenuLink {
                  target
                  title
                  url
                }
                switzerlandMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                switzerlandMenuLink {
                  target
                  title
                  url
                }
                londonMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                londonMenuLink {
                  target
                  title
                  url
                }
                visitCampusImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                visitCampusLink {
                  target
                  title
                  url
                }
                admissionMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                admissionMenuLink {
                  target
                  title
                  url
                }
                
              }
            } 
          }
          aboutMenu: allWpMenu(filter: {name: {eq: "Sub Menu - About Glion"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          partnershipsMenu: allWpMenu(filter: {name: {eq: "Sub Menu - partnerships and recruitment"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                }
              }
            }
          }
          headerActionsMenu: allWpMenu(filter: {name: {eq: "Header actions"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          bachelorsMenu: allWpMenu(filter: {name: {eq: "Sub Menu - bachelors"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          mastersMenu: allWpMenu(filter: {name: {eq: "Sub Menu - masters"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          shortMenu: allWpMenu(filter: {name: {eq: "Sub Menu - short programs"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          execMenu: allWpMenu(filter: {name: {eq: "Sub Menu - executive education"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          applyMenu: allWpMenu(filter: {name: {eq: "Sub Menu - Applying to Glion"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          preArrivalMenu: allWpMenu(filter: {name: {eq: "Sub Menu - pre-arrival information"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          primaryMenuFR: allWpMenu(filter: {name: {eq: "Primary Navigation French"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
              acfNavImagesUpdate {
                aboutMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100 ) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                aboutMenuLink {
                  target
                  title
                  url
                }
                programMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                programMenuLink {
                  target
                  title
                  url
                }
                switzerlandMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                switzerlandMenuLink {
                  target
                  title
                  url
                }
                londonMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                londonMenuLink {
                  target
                  title
                  url
                }
                visitCampusImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                visitCampusLink {
                  target
                  title
                  url
                }
                admissionMenuImage {
                  altText
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 500, webpQuality: 100) {
                        ...GatsbyImageSharpFluid_withWebp
                        
                      }
                      fixed(width: 235, height: 172)  {
                        ...GatsbyImageSharpFixed
                        
                      }
                    } 
                    publicURL  
                  } 
                }
                admissionMenuLink {
                  target
                  title
                  url
                }
                
              }
            }
          }
          aboutMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - About Glion FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          partnershipsMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - partnerships and recruitment FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                }
              }
            }
          }
          headerActionsMenuFR: allWpMenu(filter: {name: {eq: "Header actions FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          bachelorsMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - bachelors FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          mastersMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - masters FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          shortMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - short programs FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          execMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - executive education FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          applyMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - Applying to Glion FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }
          preArrivalMenuFR: allWpMenu(filter: {name: {eq: "Sub Menu - pre-arrival information FR"}}) {
            nodes {
              count
              menuItems {
                nodes {
                  label
                  url
                  id
                }
              }
            }
          }

        }
      `}
    render={(data) => (
      <Navigation site={data.site} primaryMenu={data.primaryMenu} aboutMenu={data.aboutMenu} partnershipsMenu={data.partnershipsMenu} headerActionsMenu={data.headerActionsMenu} bachelorsMenu={data.bachelorsMenu} mastersMenu={data.mastersMenu} shortMenu={data.shortMenu} execMenu={data.execMenu} preArrivalMenu={data.preArrivalMenu} applyMenu={data.applyMenu} primaryMenuFR={data.primaryMenuFR} aboutMenuFR={data.aboutMenuFR} partnershipsMenuFR={data.partnershipsMenuFR} headerActionsMenuFR={data.headerActionsMenuFR} bachelorsMenuFR={data.bachelorsMenuFR} mastersMenuFR={data.mastersMenuFR} shortMenuFR={data.shortMenuFR} execMenuFR={data.execMenuFR} preArrivalMenuFR={data.preArrivalMenuFR} applyMenuFR={data.applyMenuFR} {...props} />
    )}
  />
)


Navigation.propTypes = {

  polylang_translations: PropTypes.array,
  currentLang: PropTypes.string,
  frenchHome: PropTypes.string


}

