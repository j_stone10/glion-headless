import React from 'react';
import { PropTypes } from 'prop-types';
import Img from "gatsby-image"
import '../components/styles/programs-alumni.css';
import '../components/styles/campus_locations.css';
let $;
if (typeof window !== `undefined`) {
   require("../../node_modules/bootstrap/dist/js/bootstrap.js")
   $ = require("jquery");
}

const ProgramFaculty = ({ currentLang, faculty, facultyTitle, facultyText }) => (

   <>

      {faculty !== null &&
         <>
            <div className="wrapper row my-5">
               <div className="scrollElement col-12  my-3">
                  <h2 className="text-center mt-5">{facultyTitle}</h2>
                  <p className="text-center" dangerouslySetInnerHTML={{ __html: facultyText }}></p>
               </div>
            </div>


            <div className="container-fluid p-0">
               <div className="row my-5 alumni">
                  <div id="alumni-carousel" className="wrapper carousel slide w-100" data-ride="carousel" data-interval="false">
                     <ol className="carousel-indicators d-flex d-md-none">
                        {faculty.map((item, i) => {


                           return (

                              <li key={i} data-target="#alumni-carousel" data-slide-to={i} className={i === 0 ? "active" : ''} ></li>

                           )
                        }


                        )}

                     </ol>
                     <div className="carousel-inner row w-100 mx-auto" role="listbox">
                        {faculty.map((items, c) => {


                           return (

                              <div key={c} className={`carousel-item item col-12 col-md-3  p-0 ${c === 0 ? " active" : ''} `}>
                                 {/* ${c === 0 ? "scrollElement active" : ''} ${c == 1 && 'scrollElementOffset '} ${c === 2 && 'scrollElementOffsetThird '} ${c === 3 && 'scrollElementOffsetLast'}  */}
                                 <div className="d-flex flex-column h-100" >
                                    <div className={`row ${c === 1 && " order-1 order-md-2 "} ${c === 3 && " order-1 order-md-2 "}`}>
                                       <div className="col-12">
                                          <Img fluid={items.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={items.acfFaculty.facultyImage.altText} />
                                       </div>
                                    </div>
                                    <div className={`row ${c === 0 && " order-2 order-md-1 "} ${c === 2 && " order-2 order-md-1 "}`}>
                                       <div className="col-10 py-5 m-auto text-center">
                                          <div dangerouslySetInnerHTML={{ __html: items.acfFaculty.facultyExcerpt }}></div>

                                          <div className="row">
                                             <div className="learn_more_container m-auto text-center">

                                                <a data-toggle="modal" data-target="#alumniModal" data-slide-to={c} className="facultyModal learn_more" href="">{currentLang == "en" ? "Learn more" : "En Savoir Plus" }</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                           )
                        }


                        )}

                     </div>
                     <a className="carousel-control-prev d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                     </a>
                     <a className="carousel-control-next d-flex d-md-none" href="#alumni-carousel" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                     </a>
                  </div>
               </div>
            </div>
         </>
      }
      {faculty !== null &&
         <>
            <div className="modal fade alumniModal" id="alumniModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div className="modal-dialog" role="document">
                  <div className="modal-content">
                     <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">X</button>
                     </div>
                     <div className="modal-body">
                        <div id="carouselExampleControls" className="carousel slide alumni" data-ride="carousel" data-interval="false">
                           <div className="carousel-inner">
                              {faculty.map((item, c) => {


                                 return (


                                    <div key={c} className={`carousel-item${c === 0 ? " active" : ''}`}>
                                       <div id="alumni1" className="left_img_right_text alumni_profile">
                                          <div className="row">
                                             <div className={`col-md-6 col-12  p-0  d-flex justify-content-center align-items-center ${c === 1 && 'order-1 order-md-2 beige-bg'} ${c === 2 && ' grey-bg'}  ${c === 3 && 'order-1 order-md-2 beige-bg'} ${c === 4 && ' grey-bg'} `} >
                                                <div className=" col-9 m-auto text-center text-md-left">
                                                   <div dangerouslySetInnerHTML={{ __html: item.acfFaculty.facultyContent }}></div>
                                                </div>
                                             </div>
                                             <div className="col-md-6 col-12 p-0 d-md-flex  justify-content-center align-items-center facImage">
                                                <Img fluid={item.acfFaculty.facultyImage.localFile.childImageSharp.fluid} alt={item.acfFaculty.facultyImage.altText} />
                                             </div>
                                          </div>
                                       </div>
                                    </div>



                                 )
                              }


                              )}


                           </div>
                           <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                              <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span className="sr-only">Previous</span>
                           </a>
                           <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                              <span className="carousel-control-next-icon" aria-hidden="true"></span>
                              <span className="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>









         </>

      }
   </>
)

ProgramFaculty.propTypes = {

   faculty: PropTypes.array,
   facultyTitle: PropTypes.string,
   facultyText: PropTypes.string,
   currentLang: PropTypes.string,

};


export default ProgramFaculty;
