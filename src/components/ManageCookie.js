import React from "react"
import CookieConsent, { Cookies } from "react-cookie-consent"
import { Link } from "gatsby"
import { PropTypes } from "prop-types"

class ManageCookie extends React.Component {
  constructor(props) {
    super(props)

    // const lang = this.toggle1.bind(this)

    // console.log(currentLang)
    this.state = {
      switch1: false,
      switch2: true,
      switch3: false,
      firstLoad: true,
    }
  }

  handleSwitchChange = nr => () => {
    let switchNumber = `switch${nr}`
    console.log(switchNumber)
    // const currentState = this.state.switch3
    this.setState({
      [switchNumber]: !this.state[switchNumber],

      switch3: true,
      firstLoad: false,
    })
    // if (switchNumber === "switch3") {
    //   this.setState({
    //     switch3: false,
    //   })
    // }
  }

  cookieSwitchChange = () => {
    // alert('clickeed')

    const currentState = this.state.switch3
    this.setState({ switch3: !currentState })
  }

  acceptAll = () => {
    // alert('clickeed')

  
    this.setState({ switch1: true,    switch3: true, })
  }

  modal = () => {
    const currentLang = this.props.currentLang
    return (
      <div
        className="modal fade"
        id="exampleModal2233"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModal2233Label"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModal2233Label">
                {currentLang === "en"
                  ? "Privacy Overview"
                  : "Politique de confidentialité"}
              </h5>

              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {currentLang === "en" && (
                <p>
                  This website uses cookies to improve your experience while you
                  navigate through the website. Out of these cookies, the
                  cookies that are categorized as necessary are stored on your
                  browser as they are essential for the working of basic
                  functionalities of the website. We also use third-party
                  cookies that help us analyze and understand how you use this
                  website. These cookies will be stored in your browser only
                  with your consent. You also have the option to opt-out of
                  these cookies. But opting out of some of these cookies may
                  have an effect on your browsing experience.
                </p>
              )}
              {currentLang !== "en" && (
                <p>
                  Ce site utilise des cookies pour améliorer votre expérience de
                  navigation. Certains de ces cookies sont nécessaires et sont
                  stockés dans votre navigateur, car ils sont indispensables au
                  bon fonctionnement des fonctionnalités de base de ce site.
                  Nous utilisons également des cookies tiers ; ceux-ci nous
                  permettent d’analyser et de comprendre comment vous utilisez
                  ce site. Ces cookies ne seront stockés dans votre navigateur
                  qu’avec votre consentement. Vous pouvez choisir de refuser ces
                  cookies. Toutefois, la désactivation de certains de ces
                  cookies peut affecter votre expérience de navigation.
                </p>
              )}
              <div className="custom-control custom-switch">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id="customSwitches"
                  checked={this.state.switch2}
                  disabled
                  onChange={this.handleSwitchChange(2)}
                  readOnly
                />
                <label
                  className="custom-control-label"
                  htmlFor="customSwitches"
                ></label>
              </div>
              <div className="row">
                <div className="col-md-12 accordion col-12">
                  <div>
                    <h6 className="accordion_title_black_text coloured">
                      {currentLang === "en"
                        ? "Necessary (Always Enabled)"
                        : "Nécessaires"}
                    </h6>
                    <div className="expand-icon">
                      <img
                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
                        alt="expand-icon"
                      />
                    </div>
                    <div className="accordion_text">
                      {currentLang === "en" && (
                        <p>
                          Necessary cookies are absolutely essential for the
                          website to function properly. This category only
                          includes cookies that ensures basic functionalities
                          and security features of the website. These cookies do
                          not store any personal information.
                        </p>
                      )}
                      {currentLang !== "en" && (
                        <p>
                          Les cookies nécessaires sont absolument essentiels au
                          bon fonctionnement du site. Cette catégorie comprend
                          uniquement les cookies qui garantissent les
                          fonctionnalités de base et les fonctions de sécurité
                          du site. Ces cookies ne stockent aucune information
                          personnelle.
                        </p>
                      )}
                    </div>
                  </div>
                </div>
              </div>

              <div className="custom-control custom-switch">
                <input
                  type="checkbox"
                  className="custom-control-input accordion_title_black_text coloured"
                  id="customSwitches2"
                  checked={this.state.switch1}
                  onChange={this.handleSwitchChange(1)}
                  readOnly
                />
                <label
                  className="custom-control-label"
                  htmlFor="customSwitches2"
                ></label>
              </div>
              <div className="row">
                <div className="col-md-12 accordion col-12">
                  <div>
                    <h6 className="accordion_title_black_text coloured">
                      {currentLang === "en"
                        ? "Non-Necessary"
                        : "Non nécessaires"}
                    </h6>
                    <div className="expand-icon">
                      <img
                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHZpZXdCb3g9IjAgMCAyMCAyMCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxsaW5lIHgxPSIxMC41IiB5MT0iMi4xODU1N2UtMDgiIHgyPSIxMC41IiB5Mj0iMjAiIHN0cm9rZT0iIzI1MzY0NSIvPgo8bGluZSB5MT0iOS41IiB4Mj0iMjAiIHkyPSI5LjUiIHN0cm9rZT0iIzI1MzY0NSIvPgo8L3N2Zz4K"
                        alt="expand-icon"
                      />
                    </div>
                    <div className="accordion_text">
                      {currentLang === "en" && (
                        <p>
                          Any cookies that may not be particularly necessary for
                          the website to function and are used specifically to
                          collect user personal data via analytics, ads, other
                          embedded contents are termed as non-necessary cookies.
                          It is mandatory to procure user consent prior to
                          running these cookies on your website.
                        </p>
                      )}
                      {currentLang !== "en" && (
                        <p>
                          Sont appelés cookies non nécessaires, tous les cookies
                          qui ne sont pas indispensables au bon fonctionnement
                          du site et qui sont utilisés spécifiquement pour
                          collecter des données personnelles de l'utilisateur
                          via des analyses, des publicités ou d'autres contenus
                          intégrés. Il est obligatoire d’obtenir le consentement
                          de l’utilisateur avant d’exécuter ces cookies sur le
                          site.
                        </p>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer"></div>
          </div>
        </div>
      </div>
    )
  }

  componentDidMount() {




    let cookieComp = document.getElementById('cookieComponent')



    setTimeout(function(){ cookieComp.classList.add('d-block'); }, 1000);

    // Cookies.set('gatsby-plugin-google-tagmanager', 'true', { expires: 180, path: '/' })
    // console.log(this.state)
    if (Cookies.get("cookielaw-checkbox-necessary")) {
      this.setState({
        // switch1: true,
        switch3: true,
      })
    }

    if (!Cookies.get("gatsby-plugin-google-tagmanager") && !this.state.switch3) {
      document.body.classList.add("fixed")
      
    } else {
      document.body.classList.remove("fixed")
    }

    if (Cookies.get("gatsby-plugin-google-tagmanager")) {
      this.setState({
        switch1: true,
        switch3: true,
        firstLoad: false,
      })
      // console.log(document)
      // document.body.classList.remove('fixed')
      //  document.body.classList.remove('fixed')

      // setCookie(false)
      // console.log(cookie)
    }
    Cookies.set("cookielaw-checkbox-necessary", "true", {
      expires: 180,
      path: "/",
    })
  }

  componentDidUpdate() {
    // console.log("updated")
    // console.log(this.state)

    // if (this.state.firstLoad == false) {
    //   document.body.classList.add("fixed")
    // } else {
    //   document.body.classList.remove("fixed")
    // }
    if (this.state.switch1 == true) {
      Cookies.set("gatsby-plugin-google-tagmanager", "true", {
        expires: 180,
        path: "/",
      })

      // console.log(this.state)
    } else if (this.state.switch1 == false) {
      Cookies.remove("gatsby-plugin-google-tagmanager", { path: "/" })
      // console.log(this.state)
    }
    if (!this.state.switch3) {
      document.body.classList.add("fixed")
    } else {
      document.body.classList.remove("fixed")
    }

    if (this.state.switch1 == true) {
      Cookies.set("gatsby-plugin-google-tagmanager-legacy", "true", {
        expires: 180,
        path: "/",
      })
      // console.log(this.state)
    } else if (this.state.switch1 == false) {
      Cookies.remove("gatsby-plugin-google-tagmanager-legacy", { path: "/" })
      // console.log(this.state)
    }

    if (this.state.switch3 == false) {
      // alert('hello')
      document.querySelector("body").classList.add("fixed")
      // document.body.classList.add("fixed");
    }
  }

  render() {
    const currentLang = this.props.currentLang
    {
      //   if (Cookies.get("gatsby-plugin-google-tagmanager") == undefined) {
      // console.log("GTM false")
      // console.log(Cookies.get("gatsby-plugin-google-tagmanager"))
      return (
        <div id="cookieComponent"  className="d-none">
          <div className="d-flex justify-content-center align-items-center cookie_container">
            <div
              className={`CookieConsent  ${
                this.state.switch3 ? "d-none" : "d-block"
              } `}
            >
              <div className="" style={{ flex: "1 0 300px", margin: "15px" }}>
                <div className="container">
                  <div>
                    <h4 className="text-center">
                      {currentLang === "en"
                        ? "WELCOME TO GLION. "
                        : "Bienvenue à Glion. "}
                    </h4>
                  </div>
                  <div>
                    <p className="text-center">
                      {currentLang === "en"
                        ? "This site uses cookies. Some are used for statistical purposes and others are set up by third party services. By clicking ‘Accept all’, you accept the use of cookies"
                        : "Ce site utilise des cookies"}
                    </p>
                  </div>
                  <div className="d-flex justify-content-center align-items-center">
                    <a
                      href=""
                      data-toggle="modal"
                      data-target="#exampleModal2233"
                      style={{
                        textDecoration: "underline",
                        color: "rgb(255, 255, 255)",
                      }}
                    >
                      {currentLang === "en"
                        ? "Manage Cookies"
                        : "Gérer les cookies"}
                    </a>
                    &nbsp;&nbsp;
                    {currentLang === "en" && (
                      <a
                      onClick={this.cookieSwitchChange}
                        aria-current="page"
                        className=""
                        href="/privacy-policy/"
                        style={{
                          textDecoration: "underline",
                          color: "rgb(255, 255, 255)",
                        }}
                      >
                        Read more
                      </a>
                    )}
                    {currentLang !== "en" && (
                      <a
                      onClick={this.cookieSwitchChange}
                        aria-current="page"
                        className=""
                        href="/fr/privacy-policy-fr/"
                        style={{
                          textDecoration: "underline",
                          color: "rgb(255, 255, 255)",
                        }}
                      >
                        En savoir plus
                      </a>
                    )}
                    <button
                      className=""
                      id="rcc-confirm-button"
                      onClick={this.acceptAll}
                      style={{
                        background: "rgb(255, 212, 45)",
                        border: "0px",
                        borderRadius: "0px",
                        boxShadow: "none",
                        color: "rgb(78, 80, 59)",
                        cursor: "pointer",
                        flex: "0 0 auto",
                        padding: "5px 10px",
                        margin: "15px",
                        fontSize: "13px",
                      }}
                    >
                      {currentLang === "en" ? "Accept all" : "Tout accepter"}
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div
              className={`manage_cookies  ${
                this.state.switch3 ? "d-block" : "d-none"
              }`}
            >
              <a
                // type="button"
                // className="btn btn-primary"
                href=""
                // className="switch3"
                onClick={this.cookieSwitchChange}
                style={{ textDecoration: "underline", color: "#fff" }}
              >
                {currentLang === "en" ? "Manage Cookies" : "Gérer les cookies"}
              </a>
              &nbsp;&nbsp;
            </div>
          </div>
          {this.modal()}
        </div>
      )
    }
  }
}

ManageCookie.propTypes = {
  currentLang: PropTypes.string,
}
export default ManageCookie
