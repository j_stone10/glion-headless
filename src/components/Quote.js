import React from 'react';
import { PropTypes } from 'prop-types';
import '../components/styles/shield.css';

const Quote = ({quote_text, classes}) =>  (

   <div className={classes}>
      <div className="scrollElement col-md-7 col-9 p-5 m-auto text-center">
         <h2 dangerouslySetInnerHTML={{ __html: `<div>“${quote_text}”</div>` }} />
      </div>
   </div>


)


Quote.propTypes = {
   quote_text: PropTypes.string,
  classes: PropTypes.string,

};


export default Quote;
