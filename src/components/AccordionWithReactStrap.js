import React, { useState } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import { PropTypes } from 'prop-types';

const Accordion = ( { title, content, bodyClass }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
<>
      <div className="accordion"  onClick={toggle} style={{ marginBottom: '1rem' }}>
          <h6>{title}</h6>
    </div>
      <Collapse isOpen={isOpen} className={bodyClass}>
        <Card>
          <CardBody>
          {content}
          </CardBody>
        </Card>
      </Collapse>
</>
  );
}

Accordion.propTypes = {

    title: PropTypes.object,
    content: PropTypes.object,
    bodyClass: PropTypes.object,

  };

export default Accordion;