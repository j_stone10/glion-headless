import React from 'react';
import { Link, useStaticQuery } from "gatsby"
import PropTypes from 'prop-types';
import '../components/styles/footer.css';
import Neche_Logo from "../images/neche_logo.svg"
import facebook_logo from "../images/facebook.png"
import instagram_logo from "../images/instagram.png"
import youtube_logo from "../images/youtube.png"
import linkedin_logo from "../images/linkedin.png"
import plus from "../images/plus.svg"


// import { useStaticQuery, graphql } from "gatsby"
// import tangoFB from '../images/tango-facebook-icon.svg';


// import Navigation from "./Navigation"



const Footer = ({ currentLang }) => {
    const { aboutMenuFooter, partnershipsMenuFooter, quickLinksMenuFooter, headerActionsMenu, aboutMenuFooterFR, headerActionsMenuFR, partnershipsMenuFooterFR, quickLinksMenuFooterFR } = useStaticQuery(
        graphql`
        query {
            aboutMenuFooter: allWpMenu(filter: {name: {eq: "Sub Menu - About Glion Footer"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
              headerActionsMenu: allWpMenu(filter: {name: {eq: "Header actions"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
            partnershipsMenuFooter: allWpMenu(filter: {name: {eq: "Sub Menu - partnerships and recruitment"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
            quickLinksMenuFooter: allWpMenu(filter: {name: {eq: "Sub Menu - Quick Links Footer"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
            aboutMenuFooterFR: allWpMenu(filter: {name: {eq: "Sub Menu - About Glion Footer FR"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
              headerActionsMenuFR: allWpMenu(filter: {name: {eq: "Header actions FR"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
            partnershipsMenuFooterFR: allWpMenu(filter: {name: {eq: "Sub Menu - partnerships and recruitment FR"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
            quickLinksMenuFooterFR: allWpMenu(filter: {name: {eq: "Sub Menu - Quick Links Footer FR"}}) {
                nodes {
                  count
                  menuItems {
                    nodes {
                      label
                      url
                      id
                    }
                  }
                }
              }
           
  
          }
        `
    );
    //   console.log(aboutMenuFooterFR)
    return (

        <>
            {currentLang === "en" &&
                <footer className="site-footer" role="contentinfo">

                    <div className="container">

                        <div className="row">
                            <div className="col-md-3 px-0 px-md-3 col-12">

                                <div className="footer-about-us accordion item">
                                    <span>About</span>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <div className="accordion_text">
                                        {aboutMenuFooter.nodes[0].menuItems.nodes.map((item, i) => (
                                            <li className={"nav_item " + item.id} key={i}>
                                                <Link to={item.url} >
                                                    {item.label}
                                                </Link>
                                            </li>
                                        ))}
                                    </div>

                                </div>
                                <div className="item logo">
                                    <span>Accedited By</span>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <Link to="/about-us/accreditation-and-recognition/">
                                        <img src={Neche_Logo} alt="Neche Logo" />
                                    </Link>
                                </div>
                            </div>
                            <div className="col-md-3 px-0 px-md-3 accordion col-12">
                                <div className="footer-work-with-glion item">
                                    <span>Work with us</span>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <div className="accordion_text">
                                        {partnershipsMenuFooter.nodes[0].menuItems.nodes.map((item, i) => (
                                            <li className={"nav_item " + item.id} key={i}>
                                                <Link to={item.url} >
                                                    {item.label}
                                                </Link>
                                            </li>
                                        ))}
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-3 px-0 px-md-3 accordion col-12">
                                <div className="footer-quick-links item">
                                    <span>Quick Links</span>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <div className="accordion_text">
                                        {quickLinksMenuFooter.nodes[0].menuItems.nodes.map((item, i) => (
                                            <li className={"nav_item " + item.id} key={i}>
                                                <Link to={item.url} >
                                                    {item.label}
                                                </Link>
                                            </li>
                                        ))}
                                    </div>
                                </div>
                                <div className="item d-none d-lg-block">
                                    <span>Follow Us</span>
                                    <div className="row social">
                                        <div className="col-12">

                                            <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/">  <img src={linkedin_logo} alt="LinkedIn Logo" /></a>
                                            <a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg">  <img src={youtube_logo} alt="youtube Logo" /></a>
                                            <a target="_blank" href="https://www.facebook.com/glionswitzerland/">  <img src={facebook_logo} alt="Facebook Logo" /></a>
                                            <a target="_blank" href="https://www.instagram.com/glionhospitalityschool/">  <img src={instagram_logo} alt="Instagram Logo" /></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 px-0 px-md-3 accordion col-12">
                                <div className="item">
                                    <span>Get Started</span>
                                    <div className="menu-header-actions-container">
                                        <ul>
                                            {headerActionsMenu.nodes[0].menuItems.nodes.map((item, i) => (

                                                (() => {
                                                    if (item.label === 'Download a brochure') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                                                    <Link to={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )
                                                    } else if (item.label === 'Apply to a program') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                                                    <Link to={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )
                                                    } else if (item.label === 'Contact') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                                                    <Link to={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )

                                                    } else if (item.label === 'Virtual tour') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62"}>
                                                                    <Link to={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )
                                                    }
                                                }
                                                )()
                                            ))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row social d-block d-md-none">
                            <div className="col-12">
                                <div className="item">
                                    <div className="row social">
                                        <div className="col-3">
                                            <Link to="/about-us/accreditation-and-recognition/">
                                                <img src={Neche_Logo} alt="Neche Logo" />
                                            </Link>
                                        </div>
                                        <div className="col-9">
                                            <div className="col text-right ml-auto">
                                                <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/">  <img src={linkedin_logo} alt="LinkedIn Logo" /></a>
                                                <a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg">  <img src={youtube_logo} alt="youtube Logo" /></a>
                                                <a target="_blank" href="https://www.facebook.com/glionswitzerland/">  <img src={facebook_logo} alt="Facebook Logo" /></a>
                                                <a target="_blank" href="https://www.instagram.com/glionhospitalityschool/">  <img src={instagram_logo} alt="Instagram Logo" /></a>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            }
            {currentLang === "fr" &&
                <footer className="site-footer" role="contentinfo">

                    <div className="container">

                        <div className="row">
                            <div className="col-md-3 px-0 px-md-3 col-12">

                                <div className="footer-about-us accordion item">
                                    <h6>A PROPOS</h6>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <div className="accordion_text">
                                        {aboutMenuFooterFR.nodes[0].menuItems.nodes.map((item, i) => (
                                            <li className={"nav_item " + item.id} key={i}>
                                                <Link to={item.url} >
                                                    {item.label}
                                                </Link>
                                            </li>
                                        ))}
                                    </div>

                                </div>
                                <div className="item logo">
                                    <h6>ACCRÉDITATIONS</h6>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <Link to="/about-us/accreditation-and-recognition/">
                                        <img src={Neche_Logo} alt="Neche Logo" />
                                    </Link>
                                </div>
                            </div>
                            <div className="col-md-3 px-0 px-md-3 accordion col-12">
                                <div className="footer-work-with-glion item">
                                    <h6>Travailler avec nous</h6>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <div className="accordion_text">
                                        {partnershipsMenuFooterFR.nodes[0].menuItems.nodes.map((item, i) => (
                                            <li className={"nav_item " + item.id} key={i}>
                                                <Link to={item.url} >
                                                    {item.label}
                                                </Link>
                                            </li>
                                        ))}
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-3 px-0 px-md-3 accordion col-12">
                                <div className="footer-quick-links item">
                                    <h6>LIENS RAPIDES</h6>
                                    <div className="expand-icon d-block d-md-none"> <img src={plus} alt="expand-icon" /></div>
                                    <div className="accordion_text">
                                        {quickLinksMenuFooterFR.nodes[0].menuItems.nodes.map((item, i) => (
                                            <li className={"nav_item " + item.id} key={i}>
                                                <Link to={item.url} >
                                                    {item.label}
                                                </Link>
                                            </li>
                                        ))}
                                    </div>
                                </div>
                                <div className="item d-none d-lg-block">
                                    <h6>SUIVEZ NOUS</h6>
                                    <div className="row social">
                                        <div className="col-12">

                                            <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/">  <img src={linkedin_logo} alt="LinkedIn Logo" /></a>
                                            <a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg">  <img src={youtube_logo} alt="youtube Logo" /></a>
                                            <a target="_blank" href="https://www.facebook.com/glionswitzerland/">  <img src={facebook_logo} alt="Facebook Logo" /></a>
                                            <a target="_blank" href="https://www.instagram.com/glionhospitalityschool/">  <img src={instagram_logo} alt="Instagram Logo" /></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 px-0 px-md-3 accordion col-12">
                                <div className="item">
                                    <h6>COMMENCER</h6>
                                    <div className="menu-header-actions-container">
                                        <ul>
                                            {headerActionsMenuFR.nodes[0].menuItems.nodes.map((item, i) => (

                                                (() => {
                                                    if (item.label === 'Télécharger nos brochures') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"download-brochure darken menu-item menu-item-type-post_type menu-item-object-page menu-item-60"}>
                                                                    <Link href={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )
                                                    } else if (item.label === 'S’inscrire') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"apply-program darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                                                    <Link href={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )
                                                    } else if (item.label === 'Nous contacter') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"contact darken menu-item menu-item-type-post_type menu-item-object-page menu-item-59"}>
                                                                    <Link href={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )

                                                    } else if (item.label === 'Visiter un campus') {
                                                        return (
                                                            <div key={item.id}>
                                                                <li className={"visit-campus darken menu-item menu-item-type-post_type menu-item-object-page menu-item-62"}>
                                                                    <Link href={item.url} >
                                                                        {item.label}
                                                                    </Link>
                                                                </li>
                                                            </div>
                                                        )
                                                    }
                                                }
                                                )()
                                            ))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row social d-block d-md-none">
                            <div className="col-12">
                                <div className="item">
                                    <div className="row social">
                                        <div className="col-3">
                                            <Link to="/about-us/accreditation-and-recognition/">
                                                <img src={Neche_Logo} alt="Neche Logo" />
                                            </Link>
                                        </div>
                                        <div className="col-9">
                                            <div className="col text-right ml-auto">
                                                <a target="_blank" href="https://www.linkedin.com/school/glion-institute-of-higher-education/">  <img src={linkedin_logo} alt="LinkedIn Logo" /></a>
                                                <a target="_blank" href="https://www.youtube.com/channel/UCyK6S3k-FTa3ATLF9GX4SJg">  <img src={youtube_logo} alt="youtube Logo" /></a>
                                                <a target="_blank" href="https://www.facebook.com/glionswitzerland/">  <img src={facebook_logo} alt="Facebook Logo" /></a>
                                                <a target="_blank" href="https://www.instagram.com/glionhospitalityschool/">  <img src={instagram_logo} alt="Instagram Logo" /></a>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            }
        </>
    )
}

Footer.propTypes = {
    currentLang: PropTypes.string,
}


export default Footer;