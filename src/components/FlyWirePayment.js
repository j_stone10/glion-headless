import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { Link, graphql } from 'gatsby';





class FlyWirePayment extends Component {
    
    componentDidMount() {

        window.flywire.Payment.render({
            env: 'production',
            locale: 'fr-FR',
            destination: 'GCA',
            provider: 'embed',
            amount: 27500,
            read_only: 'amount',
            theme: {
            header: false,
            backgroundColor: '#ffffff',
            brandColor: '#3498db',
            },
            }, '#flywire-payex');

    }
    render() {
        const code = this.props.code

        // const media = this.props.data.allWpMediaItem.edges
        // console.log(code)
        return (

                <div id="flywire-payex"></div>

        )
    }
}



FlyWirePayment.propTypes = {
    code: PropTypes.string,

};


export default FlyWirePayment;